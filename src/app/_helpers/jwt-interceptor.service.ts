import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { MatDialog } from '@angular/material';
import { DialogCerrarSesionComponent } from '../dialogs/dialog-cerrar-sesion/dialog-cerrar-sesion.component';

@Injectable({
  providedIn: 'root'
})
export class JwtInterceptorService {
  private expiredToken: boolean = false;
  constructor(
    private _authService: AuthService,
    public dialog: MatDialog,
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (this._authService.isLoggedIn()) {
      this.expiredToken = this._authService.isExpireded();
    }

    if (this.expiredToken) {
      this.openDialog();
      this._authService.logout();
      this.expiredToken = false;
    }
    return next.handle(request);
  }

  openDialog(): void {
    let data;
    data = {
      urlimg: true
    };
    const dialogRef = this.dialog.open(DialogCerrarSesionComponent, {
      width: "40rem",
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
