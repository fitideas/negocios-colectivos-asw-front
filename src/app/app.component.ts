import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Spinkit } from 'ng-http-loader';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Acción Fiduciaria';
  spinnerStyle = Spinkit;
  constructor(
    public translate: TranslateService
  ) {
    translate.addLangs(['es']);
    translate.setDefaultLang('es');
  }
}
