import { Component, OnInit, Inject } from "@angular/core";
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material";
export interface DialogData {
  button: string;
  urlimg: boolean;
  titulo:string,
  contenido:string,
  buttonCancelar: string;
  buttonAceptar: string;
  dataDeAlla: [];
}


@Component({
  selector: 'app-modal-sincronizacion-constantes',
  templateUrl: './modal-sincronizacion-constantes.component.html',
  styleUrls: ['./modal-sincronizacion-constantes.component.css']
})


/**
* Autor:Lizeth Patiño
* Proposito: Mensaje de sincronización de bus, caso de constantes incompletas
* Fecha: 15/04/2020
*/

export class ModalSincronizacionConstantesComponent implements OnInit {
  arrayConstantes: any;

  constructor(
    public dialogRef: MatDialogRef<ModalSincronizacionConstantesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  ngOnInit() {
    this.arrayConstantes=this.data['constante'];
  }

}
