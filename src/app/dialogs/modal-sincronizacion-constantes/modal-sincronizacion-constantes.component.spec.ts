import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSincronizacionConstantesComponent } from './modal-sincronizacion-constantes.component';

describe('ModalSincronizacionConstantesComponent', () => {
  let component: ModalSincronizacionConstantesComponent;
  let fixture: ComponentFixture<ModalSincronizacionConstantesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSincronizacionConstantesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSincronizacionConstantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
