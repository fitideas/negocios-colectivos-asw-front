import { Component, OnInit, Inject, ViewChild } from "@angular/core";
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
  MatPaginator,
  MatSort,
  MatDialog,
} from "@angular/material";

export interface DialogData {
  button: string;
  urlimg: boolean;
  buttonCancelar: string;
  buttonAceptar: string;
  dataDeAlla: [];
  titulo:string;
  contenido:string;
}

@Component({
  selector: 'app-modal-sincronizacion-titularnuevo',
  templateUrl: './modal-sincronizacion-titularnuevo.component.html',
  styleUrls: ['./modal-sincronizacion-titularnuevo.component.css']
})

/**
* Autor:Lizeth Patiño
* Proposito: Mensaje de sincronización de bus, caso titulares nuevos
* Fecha: 15/04/2020
*/
export class ModalSincronizacionTitularnuevoComponent implements OnInit {

  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns: string[] = [
    "nombreApellido",
    "tipoDocumento",
    "numDocumento",
  ];
  resultsLength: any;
  titularNuevo: any;

 
  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ModalSincronizacionTitularnuevoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  ngOnInit() {
    this.titularNuevo=this.data['titularNuevo'];
    this.dataSource = new MatTableDataSource(this.titularNuevo);
    setTimeout(() => this.dataSource.paginator = this.paginator);
    this.resultsLength = this.titularNuevo.length;
    this.dataSource.sort = this.sort;
  }

}
