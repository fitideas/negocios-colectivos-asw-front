import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSincronizacionTitularnuevoComponent } from './modal-sincronizacion-titularnuevo.component';

describe('ModalSincronizacionTitularnuevoComponent', () => {
  let component: ModalSincronizacionTitularnuevoComponent;
  let fixture: ComponentFixture<ModalSincronizacionTitularnuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSincronizacionTitularnuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSincronizacionTitularnuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
