import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAddIngresoComponent } from './dialog-add-ingreso.component';

describe('DialogAddIngresoComponent', () => {
  let component: DialogAddIngresoComponent;
  let fixture: ComponentFixture<DialogAddIngresoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogAddIngresoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAddIngresoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
