import { Component, OnInit,Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

export interface DialogData {
  titulo: string,
  contenido: string,
  button: string,
  urlimg: boolean
}

@Component({
  selector: 'app-dialog-add-ingreso',
  templateUrl: './dialog-add-ingreso.component.html',
  styleUrls: ['./dialog-add-ingreso.component.css']
})
export class DialogAddIngresoComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogAddIngresoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}
