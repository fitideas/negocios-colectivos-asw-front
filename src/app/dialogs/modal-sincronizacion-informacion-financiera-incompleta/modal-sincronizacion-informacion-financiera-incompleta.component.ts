import { Component, OnInit, Inject, ViewChild } from "@angular/core";
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
  MatPaginator,
  MatSort,
} from "@angular/material";
export interface DialogData {
  button: string;
  urlimg: boolean;
  contenido:string;
  titulo:string;
  buttonCancelar: string;
  buttonAceptar: string;
  dataDeAlla: [];
}

@Component({
  selector: 'app-modal-sincronizacion-informacion-financiera-incompleta',
  templateUrl: './modal-sincronizacion-informacion-financiera-incompleta.component.html',
  styleUrls: ['./modal-sincronizacion-informacion-financiera-incompleta.component.css']
})

/**
* Autor:Lizeth Patiño
* Proposito: Mensaje de sincronización de bus, caso de información financiera incompleta
* Fecha: 15/04/2020
*/

export class ModalSincronizacionInformacionFinancieraIncompletaComponent implements OnInit {

  arrayInfoIncompleta: any;
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns: string[] = [
    "nombreApellido",
    "tipoDocumento",
    "numDocumento",
    "datosFaltantes"
  ];
  resultsLength: any;

  constructor(
    public dialogRef: MatDialogRef<ModalSincronizacionInformacionFinancieraIncompletaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  ngOnInit() {
    this.arrayInfoIncompleta=this.data['infoIncorrecta'];
    this.dataSource = new MatTableDataSource(this.arrayInfoIncompleta);
    setTimeout(() => this.dataSource.paginator = this.paginator);
    this.resultsLength = this.arrayInfoIncompleta.length;
    this.dataSource.sort = this.sort;
  }

}
