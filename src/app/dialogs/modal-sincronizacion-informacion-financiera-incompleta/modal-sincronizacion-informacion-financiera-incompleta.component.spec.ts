import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSincronizacionInformacionFinancieraIncompletaComponent } from './modal-sincronizacion-informacion-financiera-incompleta.component';

describe('ModalSincronizacionInformacionFinancieraIncompletaComponent', () => {
  let component: ModalSincronizacionInformacionFinancieraIncompletaComponent;
  let fixture: ComponentFixture<ModalSincronizacionInformacionFinancieraIncompletaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSincronizacionInformacionFinancieraIncompletaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSincronizacionInformacionFinancieraIncompletaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
