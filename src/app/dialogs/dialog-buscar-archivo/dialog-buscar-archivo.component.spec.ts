import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogBuscarArchivoComponent } from './dialog-buscar-archivo.component';

describe('DialogBuscarArchivoComponent', () => {
  let component: DialogBuscarArchivoComponent;
  let fixture: ComponentFixture<DialogBuscarArchivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogBuscarArchivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogBuscarArchivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
