import { Component, Inject, EventEmitter, Output, Input } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from "@angular/material";
import { DataService } from 'src/app/services/data.service';
import { Subscription, of } from 'rxjs';
import { trigger, state, transition, animate, style } from '@angular/animations';
import { DialogCargarArchivoComponent } from '../dialog-cargar-archivo/dialog-cargar-archivo.component';
import { FileUploadModel } from 'src/app/modelo/list-cotitulares';
import { StorageDataService } from 'src/app/services/storage-data.service';

export interface DialogData {
  titulo: string,
  contenido: string,
  button: string,
  urlimg: boolean,
  buttonCancelar: string,
  buttonContinuar:string,
  buttonBuscar:string,
  buttonCargar:string
}

@Component({
  selector: 'app-dialog-buscar-archivo',
  templateUrl: './dialog-buscar-archivo.component.html',
  styleUrls: ['./dialog-buscar-archivo.component.css'],
  animations: [
    trigger('fadeInOut', [
      state('in', style({ opacity: 100 })),
      transition('* => void', [
        animate(300, style({ opacity: 0 }))
      ])
    ])
  ]
})
export class DialogBuscarArchivoComponent {

  @Input() accept = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
  /** Allow you to add handler after its completion. Bubble up response text from remote. */
  @Output() complete = new EventEmitter<string>();

  public files: Array<FileUploadModel> = [];
  public filesNull = true;

  uploadResponse = { status: '', message: '', filePath: '' };
  public swListError: boolean = false;
  public listError: any = null;
  infoGeneralDesdeNegocio: any;
  codigoSFC: string;

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<DialogBuscarArchivoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private _dataService: DataService,
    private _storageData: StorageDataService

  ) {
    this.infoGeneralDesdeNegocio = this._storageData.getInfoNegocioDetalle();
    this.codigoSFC = this.infoGeneralDesdeNegocio.codigoSFC;
  }

  subirArchivo() {
        const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
    fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++) {
        const file = fileUpload.files[index];
        this.files[0] = {
          data: file, state: 'in',
          inProgress: false, progress: 0, canLoad: true, canRetry: false, canCancel: true
        };
        this.filesNull = false;
      }
    };
    fileUpload.click();
  }

  cancelFile(file: FileUploadModel) {
    file.sub.unsubscribe();
    this.removeFileFromArray(file);
  }

  retryFile(file: FileUploadModel) {
    this.uploadFile(file);
    file.canRetry = false;
  }

  private uploadFile(file: FileUploadModel) {
    let context: any;
    file.inProgress = true;
    this._dataService.loadNegociosCargarArchivoClientes(file, this.codigoSFC)
      .subscribe(
        (data: any) => { 
          this.uploadResponse = data;
          if (typeof (event) === 'object') {
            this.removeFileFromArray(file);
            this.complete.emit(this.uploadResponse.status);
          }
          this.getReport(data);
          this.dialogRef.close(true);
          this.openDialog(true, null);
        },
        (err) => {
          file.inProgress = false;
          file.canRetry = true;
          file.canLoad = false;
          of(`${file.data.name} upload failed.`);
          this.getReport(err.error);
          this.openDialog(false, err.error);
          this.dialogRef.close(false);
        }
      );
  }

  getReport(data): void {
    let blob = new Blob([data], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    });    
    let fileUrl = window.URL.createObjectURL(blob);
    
    if (window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob, fileUrl.split(':')[1]);
    } else {
     
      window.open(fileUrl);
    }
  }

  public uploadFiles() {
    const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
    fileUpload.value = '';

    this.files.forEach(file => {
      this.uploadFile(file);
    });
  }

  private removeFileFromArray(file: FileUploadModel) {
    const index = this.files.indexOf(file);
    if (index > -1) {
      this.files.splice(index, 1);
    }
  }

  openDialog(ok, context): void {
    let data;
    if (ok) {
      data = {
        titulo: "Datos guardados",
        contenido: "La carga de información sobre el negocio desde el archivo han sido guardadas de manera exitosa.",
        button: "ACEPTAR",
        urlimg: false,
        listError: null
      };
    }
    if (!ok) {
      this.listError = null;
      let contenido = "El archivo no se puede cargar. Verificar el archivo de errores descargardo automáticamente.";
      if (context.mensaje != null) {
        contenido = context.mensaje;

        if (context.lista != null) {
          this.swListError = true;
          this.listError = context.lista;
        }
      }
      data = {
        titulo: "Datos no guardados",
        contenido: contenido,
        button: "ACEPTAR",
        urlimg: true,
        listError: this.listError
      };
    }
    const dialogRef = this.dialog.open(DialogCargarArchivoComponent, {
      width: "40rem",
      data
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
