import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSincronizacionDetalleDatocambiadoComponent } from './modal-sincronizacion-detalle-datocambiado.component';

describe('ModalSincronizacionDetalleDatocambiadoComponent', () => {
  let component: ModalSincronizacionDetalleDatocambiadoComponent;
  let fixture: ComponentFixture<ModalSincronizacionDetalleDatocambiadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSincronizacionDetalleDatocambiadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSincronizacionDetalleDatocambiadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
