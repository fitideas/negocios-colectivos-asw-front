import { Component, OnInit, Inject, ViewChild } from "@angular/core";
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
  MatPaginator,
  MatSort,
} from "@angular/material";
export interface DialogData {
  button: string;
  urlimg: boolean;
  buttonCancelar: string;
  buttonAceptar: string;
  dataDeAlla: [];
  titulo:string;
  contenido:string;
}

@Component({
  selector: 'app-modal-sincronizacion-detalle-datocambiado',
  templateUrl: './modal-sincronizacion-detalle-datocambiado.component.html',
  styleUrls: ['./modal-sincronizacion-detalle-datocambiado.component.css']
})

/**
* Autor:Lizeth Patiño
* Proposito: Mensaje de sincronización de bus, caso detalles en el cambio de datos de un titular
* Fecha: 15/04/2020
*/
export class ModalSincronizacionDetalleDatocambiadoComponent implements OnInit {

  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns: string[] = [
    "dato",
    "datoNuevo",
    "datoAnterior"
  ];
  
  resultsLength: any;
  detalleCambioTitular: any;

  constructor(
    public dialogRef: MatDialogRef<ModalSincronizacionDetalleDatocambiadoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  ngOnInit() {
    this.detalleCambioTitular=this.data['detalleCambio'];
    this.dataSource = new MatTableDataSource(this.detalleCambioTitular);
    setTimeout(() => this.dataSource.paginator = this.paginator);
    this.resultsLength = this.detalleCambioTitular.length;
    this.dataSource.sort = this.sort;
  }

}
