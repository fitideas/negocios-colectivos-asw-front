import { Component, Inject, OnInit, ViewChild } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, MatSort } from "@angular/material";
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Criterio } from 'src/app/componentes/beneficiario/beneficiario.component';
import { ResponseBeneficiarios } from 'src/app/modelo/response-beneficiarios';
import { DataService } from 'src/app/services/data.service';
import { ListBeneficiarios } from 'src/app/modelo/list-beneficiarios';

export interface DialogData {
  titulo: string,
  tipo: string
}

@Component({
  selector: 'app-dialog-buscar-directivo',
  templateUrl: './dialog-buscar-directivo.component.html',
  styleUrls: ['./dialog-buscar-directivo.component.css']
})
export class DialogBuscarDirectivoComponent implements OnInit {

  public buscarDirectivoForm: FormGroup;
  public submitted: boolean = false;
  public dataNotFound: boolean = false;
  public dataFound: boolean = false;
  public detalleListado: boolean = false;
  public directivos: ResponseBeneficiarios;
  public listBeneficiario: ListBeneficiarios[] = new Array();
  public resultsLength = 0;
  public beneficiarios: ResponseBeneficiarios;
  public guardarBotton = false;
  public listDirectivos: any[] = new Array();
  public listRoles: any;
  displayedColumns: string[] = [
    "name",
    "documentType",
    "documentNumber",
    "rolTercero",
    "isDirectivo",
    "id"
  ];
  dataSource: MatTableDataSource<ListBeneficiarios>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  selectCriterio: Criterio[] = [
    { value: "documento", viewValue: "Número de identificación" },
    { value: "nombre", viewValue: "Nombres y/o apellidos" }
  ];

  public page: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPages: number = 1; //Número total de páginas 
  public numElementos: number = 10; //Total de elemento por pagina
  public request: any;
  public tipoRolMesaDirectiva: boolean = false;
  public tipoRolExplositor: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<DialogBuscarDirectivoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public fb: FormBuilder,
    private _dataService: DataService
  ) {

  }

  ngOnInit() {
    this.listBeneficiario = new Array();
    this.dataNotFound = false;
    this.dataFound = false;
    this.detalleListado = false;
    this.buscarDirectivoForm = this.fb.group({
      selectCriterio: new FormControl("", [Validators.required]),
      argumento: new FormControl("", Validators.required)
    });
    if (this.data.tipo == 'mesadirectiva') {
      this.tipoRolMesaDirectiva = true;
      this.tipoRolExplositor = false;
    }
    if (this.data.tipo == 'expositor') {
      this.tipoRolMesaDirectiva = false;
      this.tipoRolExplositor = true;
    }
  }

  public resetTables() {
    this.dataNotFound = false;
    this.dataFound = false;
    this.detalleListado = false;
    this.listBeneficiario = null;
    this.buscarDirectivoForm.controls['argumento'].setValue('');
  }

  cerrarDialog(): void {
    this.dialogRef.close();
  }

  guardarDirectivo(): void {
    this.dialogRef.close(this.listDirectivos);
  }


  goToPage(page: number) {
    this.page = page;
    let requestPaginado: any = this.request;
    requestPaginado.pagina = page;
    requestPaginado.elementos = this.numElementos;
    this.buscarBeneficiarioList(requestPaginado);
  }

  enviarDatos(): void {
    this.dataNotFound = false;
    this.dataFound = false;
    this.submitted = true;
    this.detalleListado = false;
    if (this.buscarDirectivoForm.invalid) {
      return;
    }
    /*let requestBuscar: RequestBuscarBeneficiarios;*/
    if (this.buscarDirectivoForm.value.selectCriterio) {
      this.request = {
        argumento: this.buscarDirectivoForm.value.argumento,
        criterioBuscar: this.buscarDirectivoForm.value.selectCriterio
      }
    }
    let requestPaginado: any = this.request;
    requestPaginado.pagina = 1;
    requestPaginado.elementos = this.numElementos;
    this.buscarBeneficiarioList(requestPaginado);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  buscarBeneficiarioList(requestBuscar) {
    this.directivos = null;

    this._dataService.getParametrosValoresDominio('ROLTER').subscribe(
      (data: any) => {
        if (!data) {
          return;
        }
        this.listRoles = data;
      },
      (err: any) => {
      }
    );
    this._dataService
      .getBeneficiarioListRequest(requestBuscar)
      .subscribe((data: any) => {
        if (!data) {
          this.dataNotFound = true;
          this.dataFound = false;
          this.detalleListado = false;
          return;
        }
        this.dataFound = true;
        this.dataNotFound = false;
        this.detalleListado = false;
        this.directivos = data;
        setTimeout(() => {
          this.loadDirectivo(this.directivos['beneficiarios']);
          this.dataSource = new MatTableDataSource(this.listBeneficiario);
          // this.resultsLength = this.listBeneficiario.length;
          // this.dataSource.paginator = this.paginator;
          this.resultsLength = data.totalbeneficiarios;
          this.dataSource.sort = this.sort;
         // this.totalPages = Math.round(this.resultsLength / this.numElementos);
         // this.totalPages = (this.totalPages < 1) ? 1 : this.totalPages;
          this.totalPages = this.resultsLength / this.numElementos;          
          let totalPagina = this.resultsLength % this.numElementos;
          if (totalPagina > 1) {
            this.totalPages = Number(parseInt(String(this.totalPages))) + 1;
          }
          if (this.totalPages < 1) {
            this.totalPages = 1;
          } 
        });
      },
        (err) => {
          this.dataNotFound = true;
        }
      );
  }

  loadDirectivo(beneficiarios): void {
    this.listBeneficiario = [];
    beneficiarios.forEach(beneficiario => {
      beneficiario.isDirectivo = false;
      this.listBeneficiario.push(beneficiario);
    });
  }

  agregarRol(rolSeleccionado, item) {
    item.rol = rolSeleccionado.value;
    this.guardarBotton = (item.isDirectivo && item.rol) ? true : false;
    if (this.guardarBotton) {
      this.listDirectivos.push(item);
    }
  }

  agregarMesaDirectiva(item) {
    this.guardarBotton = (item.isDirectivo && item.rol) ? true : false;
    if (this.guardarBotton) {
      this.listDirectivos.push(item);
    }
  }

}
