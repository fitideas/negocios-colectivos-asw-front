import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogBuscarDirectivoComponent } from './dialog-buscar-directivo.component';

describe('DialogBuscarDirectivoComponent', () => {
  let component: DialogBuscarDirectivoComponent;
  let fixture: ComponentFixture<DialogBuscarDirectivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogBuscarDirectivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogBuscarDirectivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
