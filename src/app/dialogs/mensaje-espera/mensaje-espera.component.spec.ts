import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajeEsperaComponent } from './mensaje-espera.component';

describe('MensajeEsperaComponent', () => {
  let component: MensajeEsperaComponent;
  let fixture: ComponentFixture<MensajeEsperaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensajeEsperaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajeEsperaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
