import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, ThemePalette, ProgressSpinnerMode } from '@angular/material';
export interface DialogData {  
  urlimg: boolean
}

@Component({
  selector: 'app-mensaje-espera',
  templateUrl: './mensaje-espera.component.html',
  styleUrls: ['./mensaje-espera.component.css']
})
export class MensajeEsperaComponent implements OnInit {

  color: ThemePalette = 'warn';
  mode: ProgressSpinnerMode = 'indeterminate';
  value = 20;

  constructor(
    public dialogRef: MatDialogRef<MensajeEsperaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  ngOnInit() {
  }

}
