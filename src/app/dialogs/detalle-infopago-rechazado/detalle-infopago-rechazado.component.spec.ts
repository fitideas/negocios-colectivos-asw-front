import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleInfopagoRechazadoComponent } from './detalle-infopago-rechazado.component';

describe('DetalleInfopagoRechazadoComponent', () => {
  let component: DetalleInfopagoRechazadoComponent;
  let fixture: ComponentFixture<DetalleInfopagoRechazadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleInfopagoRechazadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleInfopagoRechazadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
