import { Component, OnInit, Inject } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialog,
} from "@angular/material";
import { DataService } from 'src/app/services/data.service';
import { DialogParametrosSaveComponent } from 'src/app/componentes/dialog-parametros-save/dialog-parametros-save.component';
import { StorageDataService } from 'src/app/services/storage-data.service';
import moment from 'moment';
import { DatePipe } from '@angular/common';
export interface DialogData {
  button: string;
  urlimg: boolean;
  buttonCancelar: string;
  buttonAceptar: string;
  dataDeAlla: {};
  detalleCambio:{};
}

@Component({
  selector: 'app-detalle-infopago-rechazado',
  templateUrl: './detalle-infopago-rechazado.component.html',
  styleUrls: ['./detalle-infopago-rechazado.component.css']
})

/**
* Autor:Lizeth Patiño
* Proposito: Formulario de cambios de detalles de medio de pago para los pagos rechazados
* Fecha: 06/05/2020
*/
export class DetalleInfopagoRechazadoComponent implements OnInit {

  detallePagoForm: FormGroup;
  guardandoDataProvisional: any;

  public listamediosPago =
    [
      { id:"2" ,codigo: "CHE", descripcion: "CHEQUE" },
      { id:"2", codigo: "CHEGER", descripcion: "CHEQUE DE GERENCIA" },
      { id:"1", codigo: "TRANS", descripcion: "TRANSFERENCIA" },
      { id:"11", codigo: "TRASL", descripcion: "TRASLADO" }
    ]

  public tipoCuenta =
    [
      { id:"302", codigo: "COR", descripcion: "CORRIENTE" },
      { id:"301" ,codigo: "AHO", descripcion: "AHORROS" }

    ]
  listaDeBancos: any;
  medioSeleccionado: any;
  public todayDate = moment();
  transferencia: boolean;
  traslado: boolean;
  cheque: boolean;


 

  constructor(
    private datePipe: DatePipe,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DetalleInfopagoRechazadoComponent>,
    private _dataService: DataService,
    public dialog: MatDialog,
    private _storageData: StorageDataService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.detallePagoForm = this.fb.group({
      codigoEntidadBancaria: [ ],
      entidadBancariaFidecomiso: [ ],
      estadoPago: [ ],
      nombreBeneficiario: [ ],
      nombreTitular: [ ],
      numeroDocumentoBeneficiario: [ ],
      numeroDocumentoTitular: [ ],
      tipoCuenta: [ ],
      tipoDocumentoBeneficiario: [ ],
      tipoDocumentoTitular: [ ],
      tipoPago: [ ],
      numeroCuentaEncargo:[],
      valorAGirar: [ ],
      fechaPago:[],
      prefijoTipoPago:[],

    });
  }


  ngOnInit() {
    this.obtenerInformacion();
    if(this.data){
      this.llenarInformacionFormulario(this.data['detalleCambio']);
      this.mostrarCampoSegunSeleccion(this.detallePagoForm.value.tipoPago);
    }
  }


  public obtenerInformacion(){
    this._dataService.getObtenerBancos().subscribe((listaDeBancos: any) => {
      this.listaDeBancos = listaDeBancos;
    });
  }

  public llenarInformacionFormulario(dataParaElForm){
     this.detallePagoForm.get('prefijoTipoPago').setValue(dataParaElForm.prefijoTipoPago);
    this.detallePagoForm.get('codigoEntidadBancaria').setValue(dataParaElForm.codigoEntidadBancaria);
    this.detallePagoForm.get('entidadBancariaFidecomiso').setValue(dataParaElForm.entidadBancariaFidecomiso);
    this.detallePagoForm.get('estadoPago').setValue(dataParaElForm.estadoPago);
    this.detallePagoForm.get('nombreBeneficiario').setValue(dataParaElForm.nombreBeneficiario);
    this.detallePagoForm.get('nombreTitular').setValue(dataParaElForm.nombreTitular);
    this.detallePagoForm.get('numeroDocumentoBeneficiario').setValue(dataParaElForm.numeroDocumentoBeneficiario);
    this.detallePagoForm.get('numeroDocumentoTitular').setValue(dataParaElForm.numeroDocumentoTitular);
    this.detallePagoForm.get('tipoCuenta').setValue(dataParaElForm.tipoCuenta);
    this.detallePagoForm.get('tipoDocumentoBeneficiario').setValue(dataParaElForm.tipoDocumentoBeneficiario);
    this.detallePagoForm.get('tipoDocumentoTitular').setValue(dataParaElForm.tipoDocumentoTitular);
    this.detallePagoForm.get('tipoPago').setValue(dataParaElForm.tipoPago);
    this.detallePagoForm.get('fechaPago').setValue(this.datePipe.transform(this.todayDate, 'dd/MM/yyyy'));
    this.detallePagoForm.get('valorAGirar').setValue(dataParaElForm.valorAGirar);
    this.detallePagoForm.get('numeroCuentaEncargo').setValue(dataParaElForm.numeroCuentaEncargo);
  } 
  
  public tomarMedioSeleccionado(tomarValorSeleccionado) {
    this.medioSeleccionado = tomarValorSeleccionado.value;   
   // this.mostrarCampoSegunSeleccion(this.medioSeleccionado);
  }

  public mostrarCampoSegunSeleccion(medioPagoSeleccionado){
    switch (medioPagoSeleccionado) {
      case "1":
        this.transferencia=true;
        this.traslado=false;
        this.cheque=false;
        break;
      case "11":
        this.traslado=true;
        this.transferencia=false;
        this.cheque=false;
       break;
       case  "2":
         this.cheque=true;
         this.transferencia=false;
         this.traslado=false;
       break;   
    }
  }

  public cambiarDatos(){ 
  this._storageData.setInfoNuevaReproceso(this.detallePagoForm.value);
  }

}
