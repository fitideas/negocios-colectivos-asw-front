import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSincronizacionCambiodatotitularComponent } from './modal-sincronizacion-cambiodatotitular.component';

describe('ModalSincronizacionCambiodatotitularComponent', () => {
  let component: ModalSincronizacionCambiodatotitularComponent;
  let fixture: ComponentFixture<ModalSincronizacionCambiodatotitularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSincronizacionCambiodatotitularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSincronizacionCambiodatotitularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
