import { Component, OnInit, Inject, ViewChild } from "@angular/core";
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
  MatPaginator,
  MatSort,
  MatDialog,
} from "@angular/material";
import { ModalSincronizacionDetalleDatocambiadoComponent } from '../modal-sincronizacion-detalle-datocambiado/modal-sincronizacion-detalle-datocambiado.component';
export interface DialogData {
  button: string;
  urlimg: boolean;
  buttonCancelar: string;
  buttonAceptar: string;
  dataDeAlla: [];
  titulo:string;
  contenido:string;
}

/**
* Autor:Lizeth Patiño
* Proposito: Mensaje de sincronización de bus, caso cambio de datos del titular
* Fecha: 15/04/2020
*/

@Component({
  selector: 'app-modal-sincronizacion-cambiodatotitular',
  templateUrl: './modal-sincronizacion-cambiodatotitular.component.html',
  styleUrls: ['./modal-sincronizacion-cambiodatotitular.component.css']
})
export class ModalSincronizacionCambiodatotitularComponent implements OnInit {

  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns: string[] = [
    "nombreApellido",
    "tipoDocumento",
    "numDocumento",
    "resumenDatosCambiados"
  ];
  resultsLength: any;
  arrayInfoTitular: any;

 
  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ModalSincronizacionCambiodatotitularComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  ngOnInit() {
    this.arrayInfoTitular=this.data['infoTitular'];
    this.dataSource = new MatTableDataSource(this.arrayInfoTitular);
    setTimeout(() => this.dataSource.paginator = this.paginator);
    this.resultsLength = this.arrayInfoTitular.length;
    this.dataSource.sort = this.sort;
  }

  public llamarModalDetalleCambios(personaQueTieneDetalles){
    let dataMensaje = {
      titulo: "Detalle de cambios en los datos de titulares del negocio",
      buttonAceptar: "ACEPTAR",
      urlimg: false,
      detalleCambio: personaQueTieneDetalles,
    }
    this.dialog.open(ModalSincronizacionDetalleDatocambiadoComponent, {
      width: "70rem",
      data: dataMensaje
    });
  }
}
