import { Component, OnInit, Inject } from "@angular/core";
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material";
export interface DialogData {
  button: string;
  urlimg: boolean;
  buttonCancelar: string;
  buttonAceptar: string;
  dataDeAlla: {};
}

@Component({
  selector: 'app-dialog-detallenovedad-juridica',
  templateUrl: './dialog-detallenovedad-juridica.component.html',
  styleUrls: ['./dialog-detallenovedad-juridica.component.css']
})

/**
* Autor:Lizeth Patiño
* Proposito: Consulta de detalle de novedades juridicas
* Fecha: 08/04/2020
*/

export class DialogDetallenovedadJuridicaComponent implements OnInit {

  eventoForm: FormGroup;
  guardandoDataProvisional: any;
 
  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogDetallenovedadJuridicaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.eventoForm = this.fb.group({
      codigoEvento: [ ],
      tipoEvento: [ ],
      fechaRegistro: [ ],
      fechaInicial: [ ],
      fechaFinal: [ ],
      usuarioRegistro: [ ],
      oficinaRegistro: [ ]
    });
  }


  ngOnInit() {
    if(this.data){
      this.llenarInformacionFormulario(this.data['dataDeAlla']);
    }
  }

  public llenarInformacionFormulario(dataParaElForm){
    this.eventoForm.get('codigoEvento').setValue(dataParaElForm.codigoEvento);
    this.eventoForm.get('tipoEvento').setValue(dataParaElForm.tipoEvento);
    this.eventoForm.get('fechaRegistro').setValue(dataParaElForm.fechaRegistro);
    this.eventoForm.get('fechaInicial').setValue(dataParaElForm.fechaInicial);
    this.eventoForm.get('fechaFinal').setValue(dataParaElForm.fechaFinal);
    this.eventoForm.get('usuarioRegistro').setValue(dataParaElForm.usuarioRegistro);
    this.eventoForm.get('oficinaRegistro').setValue(dataParaElForm.oficinaRegistro);
  }  
}
