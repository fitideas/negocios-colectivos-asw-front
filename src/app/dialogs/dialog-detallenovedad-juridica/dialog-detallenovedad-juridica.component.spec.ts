import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogDetallenovedadJuridicaComponent } from './dialog-detallenovedad-juridica.component';

describe('DialogDetallenovedadJuridicaComponent', () => {
  let component: DialogDetallenovedadJuridicaComponent;
  let fixture: ComponentFixture<DialogDetallenovedadJuridicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogDetallenovedadJuridicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogDetallenovedadJuridicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
