import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogNegocioListTitularesComponent } from './dialog-negocio-list-titulares.component';

describe('DialogNegocioListTitularesComponent', () => {
  let component: DialogNegocioListTitularesComponent;
  let fixture: ComponentFixture<DialogNegocioListTitularesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogNegocioListTitularesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogNegocioListTitularesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
