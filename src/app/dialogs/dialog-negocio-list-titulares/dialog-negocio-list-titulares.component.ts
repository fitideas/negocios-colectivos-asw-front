import { Component, Inject, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { IconService } from 'src/app/services/icon.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';
import { TitularList } from 'src/app/modelo/list-cotitulares';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Router } from '@angular/router';

export interface DialogData {
  titulo: string,
  button: string,
  urlimg: boolean,
  codigoSFC: any
}

export interface ListTitulares {
  tipoTitularidad?: string;
	nombreTitular?: string;
	tipoDocumentoTitular?: string;
	documentoTitular?: string;
  numeroEncargoInd?: string;
  porcentajeParticipacion?: string;
	numeroDerechosFiduciarios?: string;
	beneficiario?: string;
	tipoDocumentoBeneficiario?: string;
	numeroDocumentoBeneficiario?: string;
	tipoCuenta?: string;
	numeroCuenta?: string;
	fidecomiso?: string;
	ficDestino?: string;
  codigoMovimiento?: string;
}

@Component({
  selector: 'app-dialog-negocio-list-titulares',
  templateUrl: './dialog-negocio-list-titulares.component.html',
  styleUrls: ['./dialog-negocio-list-titulares.component.css']
})
export class DialogNegocioListTitularesComponent implements OnInit {

  displayedColumns: string[] = [
    'tipoTitularidad',
		'nombreTitular',
		'tipoDocumentoTitular',
		'documentoTitular',
    'numeroEncargoInd',
    'porcentajeParticipacion',
		'numeroDerechosFiduciarios',
		'beneficiario',
		'tipoDocumentoBeneficiario',
		'numeroDocumentoBeneficiario',
		'tipoCuenta',
		'numeroCuenta'
  ];

  dataSource: MatTableDataSource<ListTitulares>;

  constructor(
    private _dataService: DataService,
    private _iconService: IconService,
    private _storageData: StorageDataService,
    private _router: Router,
    public dialogRef: MatDialogRef<DialogNegocioListTitularesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    
  }

  ngOnInit() {
    let infoTitular: TitularList[] = new Array();
    let titularBeneficiario: TitularList;
    this._iconService.registerIcons();
    this._dataService.getObtenerTitulares(this.data.codigoSFC)
      .subscribe(
        (data: any) => { 
          data.forEach(itemTitular => {           
            itemTitular.beneficiarios.forEach(itemBeneficiarios => {
              titularBeneficiario = {
                tipoTitularidad: itemTitular.tipoTitularidad,
		            nombreTitular: itemTitular.nombreTitular,
		            tipoDocumentoTitular: itemTitular.tipoDocumentoTitular,
		            documentoTitular: itemTitular.documentoTitular,
		            numeroEncargoInd: itemTitular.numeroEncargoInd,
		            porcentajeParticipacion: itemTitular.porcentajeParticipacion,
                numeroDerechosFiduciarios: itemTitular.numeroDerechosFiduciarios,
                beneficiario: itemBeneficiarios.beneficiario,
                tipoDocumentoBeneficiario: itemBeneficiarios.tipoDocumentoBeneficiario,
                numeroDocumentoBeneficiario: itemBeneficiarios.numeroDocumentoBeneficiario,
                tipoCuenta: itemBeneficiarios.tipoCuenta,
                numeroCuenta: itemBeneficiarios.numeroCuenta
              }
              infoTitular.push(titularBeneficiario);
            });
            
          });
          this.dataSource = new MatTableDataSource(infoTitular);
        },
        (err: any) => {
        });  
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public vinculoDetalleBeneficiarios(tipoDocumentoTitular, documentNumber) {
    this._dataService
      .getBeneficiarioDetailsRequest(documentNumber)
      .subscribe((data: any) => {
        if (!data) {          
          return;
        }        
        this._storageData.setInfoBeneficiario(data);
        this._router.navigate(['accionfiduciaria/vinculados/detalles']);
      },
        (err) => {
        }
      );
      this.dialogRef.close();
  }

}
