import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAddCotitularComponent } from './dialog-add-cotitular.component';

describe('DialogAddCotitularComponent', () => {
  let component: DialogAddCotitularComponent;
  let fixture: ComponentFixture<DialogAddCotitularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogAddCotitularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAddCotitularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
