import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, MatSort, MatDialog } from "@angular/material";
import { Criterio } from 'src/app/componentes/beneficiario/beneficiario.component';
import { Validators, FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { ListCotitulares, ParamAdministrarCotitular, ArrayCotitulares } from 'src/app/modelo/list-cotitulares';
import { ParamBusquedaCotitular } from 'src/app/modelo/beneficiario-detalle';
import { DialogParametrosSaveComponent } from 'src/app/componentes/dialog-parametros-save/dialog-parametros-save.component';

export interface DialogData {
  titulo: string,
  contenido: string
  urlimg: boolean,
  buttonCancelar: string,
  buttonAceptar: string,
  param: any
}

@Component({
  selector: 'app-dialog-add-cotitular',
  templateUrl: './dialog-add-cotitular.component.html',
  styleUrls: ['./dialog-add-cotitular.component.scss']
})
export class DialogAddCotitularComponent implements OnInit {

  displayedColumns: string[] = [
    "name",
    "documentType",
    "documentNumber",
    "id"
  ];
  dataSource: MatTableDataSource<ListCotitulares>;
  selection = new SelectionModel<ListCotitulares>(true, []);

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  public buscarCotitularForm: FormGroup;
  public dataNotFound: boolean = false;
  public dataFound: boolean = false;
  public listCotitulares: ListCotitulares[] = new Array();
  private listCotitularesNuevos: ListCotitulares[] = new Array();
  public resultsLength = 0;
  public guardarBotton = false;
  theCheckbox = false;

  selectCriterio: Criterio[] = [
    { value: "documento", viewValue: "Número de identificación" },
    { value: "nombre", viewValue: "Nombres y/o apellidos" }
  ];

  public page: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPages: number = 1; //Número total de páginas 
  public numElementos: number = 10; //Total de elemento por pagina

  constructor(
    public fb: FormBuilder,
    private _dataService: DataService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<DialogAddCotitularComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.buscarCotitularForm = this.fb.group({
      selectCriterio: new FormControl("", [Validators.required]),
      argumento: new FormControl("", Validators.required)
    });
  }

  ngOnInit() {
    this.dataNotFound = false;
    this.dataFound = false;
    this.serviceLoadCotitularList(this.data.param);
  }

  public resetTables() {
    this.dataNotFound = false;
    this.dataFound = false;
    this.buscarCotitularForm.controls['argumento'].setValue('');
  }

  get f() {
    return this.buscarCotitularForm.controls;
  }

  private serviceLoadCotitularList(param: ParamBusquedaCotitular): void {
    this._dataService.getBeneficiarioObtenerCotitularRequest(param)
      .subscribe((result: any) => {
        if (!result) {
          this.dataNotFound = true;
          this.dataFound = false;
          return;
        }
        this.dataFound = true;
        this.dataNotFound = false;
        this.loadCotitulares(result);
        let pagina = 1;
        this.mostraPaginaDataLocal(pagina);
        /*
        this.dataSource = new MatTableDataSource(this.listCotitulares);
        this.resultsLength = this.listCotitulares.length;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        */
      },
        (err) => {
          this.dataNotFound = true;
        }
      );
  }

  private buscarObjetoCotitular(array, valor) {
    return array.filter(function (e) {
      if (e.documentoCotitular == valor.documentoCotitular) {
        e.asociado = valor.asociado;
      }
      return e.documentoCotitular == valor.documentoCotitular;
    });
  }
  public buscarCotitular(array, valor) {
    let objeto = this.buscarObjetoCotitular(array, valor);
    return (objeto.length > 0) ? true : false;
  }

  public agragarCambios(row?: ListCotitulares): void {
    if (this.listCotitularesNuevos.length > 0) {
      if (!this.buscarCotitular(this.listCotitularesNuevos, row)) {
        this.listCotitularesNuevos.push(row);
      }
    } else {
      this.listCotitularesNuevos.push(row);
    }
    this.guardarBotton = true;
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public loadCotitulares(cotitulares: any): void {
    this.listCotitulares = [];
    let cotitular: ListCotitulares;

    cotitulares.forEach((item) => {
      cotitular = {
        nombreApellidoCotitular: item.nombreApellidoCotitular,
        tipoDocumentoCotitular: item.tipoDocumentoCotitular,
        documentoCotitular: item.documentoCotitular,
        asociado: item.asociado
      };
      this.listCotitulares.push(cotitular);
    });
  }

  enviarDatos(): void {
    this.dataNotFound = false;
    this.dataFound = false; 
    if (this.buscarCotitularForm.invalid) {
      return;
    }
    let requestBuscar: ParamBusquedaCotitular;
    if (this.buscarCotitularForm.value.selectCriterio) {
      requestBuscar = {
        documentoTitular: this.data.param.documentoTitular,
        tipoDocumentoTitular: this.data.param.tipoDocumentoTitular,
        codigoNegocio: this.data.param.codigoNegocio,
        argumento: this.buscarCotitularForm.value.argumento,
        criterioBuscar: this.buscarCotitularForm.value.selectCriterio
      }
    }    
    this.serviceLoadCotitularList(requestBuscar);
  }

  guardarCotitulares(): void {
    let paramAdministrarCotitular: ParamAdministrarCotitular;
    let listCotitulares: ArrayCotitulares[] = new Array();
    let cotitular: ArrayCotitulares;    
    this.listCotitularesNuevos.forEach(element => {
      cotitular = {
        asociado: element.asociado,
        documentoCotitular: element.documentoCotitular,
        tipoDocumentoCotitular: element.tipoDocumentoCotitular
      }
      listCotitulares.push(cotitular);
    });
    paramAdministrarCotitular = {
      documentoTitular: this.data.param.documentoTitular,
      tipoDocumentoTitular: this.data.param.tipoDocumentoTitular,
      codigoNegocio: this.data.param.codigoNegocio,
      cotitulares: listCotitulares
    }
    this._dataService.administrarCotitular(paramAdministrarCotitular).subscribe(
      (result: any) => {
        this.openDialog(true, null);
      },
      (err) => {
        this.openDialog(false, err.error);
      });
    this.dialogRef.close();
  }

  openDialog(ok, context): void {
    let data;
    if (ok) {
      data = {
        titulo: "Datos guardados",
        contenido: "Las acciones sobre Cotitulares han sido guardadas de manera exitosa.",
        button: "ACEPTAR",
        urlimg: false
      };
    }
    if (!ok) {
      let contenido = "Las acciones sobre Cotitulares no se pueden guardar. Intente más tarde.";
      if (context != null) {
        contenido = context;
      }
      data = {
        titulo: "Datos no guardados",
        contenido: contenido,
        button: "ACEPTAR",
        urlimg: true
      };
    }
    const dialogRef = this.dialog.open(DialogParametrosSaveComponent, {
      width: "40rem",
      data
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  mostraPaginaDataLocal(pagina) {
    let registrosPagina: any[] = new Array();
    this.page = pagina;
    this.numElementos = 10;
    this.resultsLength = this.listCotitulares.length;
    this.totalPages = this.resultsLength / this.numElementos;
    let totalPagina = this.resultsLength % this.numElementos;
    if (totalPagina > 1) {
      this.totalPages = Number(parseInt(String(this.totalPages))) + 1;
    }
    if (this.totalPages < 1) {
      this.totalPages = 1;
    }   

    let inicioRegistros = ((pagina - 1) * this.numElementos);
    let ultimoRegistros = pagina * this.numElementos;
    for (let i = inicioRegistros; ((i < this.listCotitulares.length) && (i < ultimoRegistros)); i++) {
      registrosPagina.push(this.listCotitulares[i]);
    }

    this.dataSource = new MatTableDataSource(registrosPagina);         
    this.dataSource.sort = this.sort;
  }

  goToPage(page: number) {
    this.mostraPaginaDataLocal(page);
  }
  
}
