import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSincronizacionErrorComponent } from './modal-sincronizacion-error.component';

describe('ModalSincronizacionErrorComponent', () => {
  let component: ModalSincronizacionErrorComponent;
  let fixture: ComponentFixture<ModalSincronizacionErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSincronizacionErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSincronizacionErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
