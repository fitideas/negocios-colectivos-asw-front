import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, ThemePalette, ProgressSpinnerMode } from '@angular/material';
export interface DialogData {  
  urlimg: boolean
  buttonAceptar:string,
  titulo:string,
  contenido:string
}


@Component({
  selector: 'app-modal-sincronizacion-error',
  templateUrl: './modal-sincronizacion-error.component.html',
  styleUrls: ['./modal-sincronizacion-error.component.css']
})

/**
* Autor:Lizeth Patiño
* Proposito: Mensajes de sincronización con errores
* Fecha: 16/04/2020
*/
export class ModalSincronizacionErrorComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ModalSincronizacionErrorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }


  ngOnInit() {
  }

}
