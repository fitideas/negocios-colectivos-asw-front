import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogInfopagoRechazadoComponent } from './dialog-infopago-rechazado.component';

describe('DialogInfopagoRechazadoComponent', () => {
  let component: DialogInfopagoRechazadoComponent;
  let fixture: ComponentFixture<DialogInfopagoRechazadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogInfopagoRechazadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogInfopagoRechazadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
