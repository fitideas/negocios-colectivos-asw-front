import { Component, OnInit, Inject, ViewChild } from "@angular/core";
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTableDataSource,
  MatPaginator,
  MatSort,
  MatDialog,
} from "@angular/material";
import { ModalSincronizacionDetalleDatocambiadoComponent } from '../modal-sincronizacion-detalle-datocambiado/modal-sincronizacion-detalle-datocambiado.component';
import { DetalleInfopagoRechazadoComponent } from '../detalle-infopago-rechazado/detalle-infopago-rechazado.component';
export interface DialogData {
  button: string;
  urlimg: boolean;
  buttonCancelar: string;
  buttonAceptar: string;
  dataDeAlla: [];
  titulo:string;
  contenido:string;
  informacionPago:{};
}


@Component({
  selector: 'app-dialog-infopago-rechazado',
  templateUrl: './dialog-infopago-rechazado.component.html',
  styleUrls: ['./dialog-infopago-rechazado.component.css']
})

/**
* Autor:Lizeth Patiño
* Proposito: Modal que presenta la información de un pago rechazado en reprocesar pagos
* Fecha: 04/05/2020
*/
export class DialogInfopagoRechazadoComponent implements OnInit {

  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns: string[] = [
    "titular",
    "tipoDoc",
    "NumDoc",
    "porcentajeP",
    "benef",
    "tipoDocB",
    "noDocB",
    "PocentajeG",
    "actualizar"
  ];
  resultsLength: any;
  infoPago: any[];
  actualizado: boolean=false;


  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<DialogInfopagoRechazadoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  ngOnInit() {
    this.infoPago=[];
    this.infoPago.push(this.data['informacionPago']);
    this.dataSource = new MatTableDataSource(this.infoPago);
    setTimeout(() =>{ this.dataSource.paginator = this.paginator;
    this.resultsLength = this.infoPago.length;
    this.dataSource.sort = this.sort});
  }

  public llamarModalDetalleCambios(detallesPagoPersona){
    let dataMensaje = {
      titulo: "Pagos rechazados",
      buttonAceptar: "CAMBIAR DATOS",
      buttonCancelar: "CANCELAR",
      urlimg: false,
      detalleCambio: detallesPagoPersona
    }
    const dialogRef=this.dialog.open(DetalleInfopagoRechazadoComponent, {
      width: "101rem",
      data: dataMensaje
    });

    dialogRef.afterClosed().subscribe(result => {
     if(result){
      this.actualizado=true;
     }  
    });
  }

}
