import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCerrarSesionComponent } from './dialog-cerrar-sesion.component';

describe('DialogCerrarSesionComponent', () => {
  let component: DialogCerrarSesionComponent;
  let fixture: ComponentFixture<DialogCerrarSesionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogCerrarSesionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCerrarSesionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
