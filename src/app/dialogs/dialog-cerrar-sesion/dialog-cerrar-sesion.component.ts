import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
export interface DialogData {  
  urlimg: boolean
}

@Component({
  selector: 'app-dialog-cerrar-sesion',
  templateUrl: './dialog-cerrar-sesion.component.html',
  styleUrls: ['./dialog-cerrar-sesion.component.scss']
})
/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Marzo 2020
 * @Proposito: Dialog para cerrar sesion  .
 */
export class DialogCerrarSesionComponent  {

  public cerrarSesion: boolean = true;

  constructor(
    public dialogRef: MatDialogRef<DialogCerrarSesionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }
  
}
