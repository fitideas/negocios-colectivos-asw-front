import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajeEsperaGenericoComponent } from './mensaje-espera-generico.component';

describe('MensajeEsperaGenericoComponent', () => {
  let component: MensajeEsperaGenericoComponent;
  let fixture: ComponentFixture<MensajeEsperaGenericoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensajeEsperaGenericoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajeEsperaGenericoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
