import { Component, OnInit, Inject } from '@angular/core';
import { ThemePalette, ProgressSpinnerMode, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-mensaje-espera-generico',
  templateUrl: './mensaje-espera-generico.component.html',
  styleUrls: ['./mensaje-espera-generico.component.css']
})
/**
* Autor:Fabian Herrera
* Proposito: Modal que recibe un titulo y
* un contenido para mostrar un mensaje de espera generico
* Fecha: 08/05/2020
*/
export class MensajeEsperaGenericoComponent implements OnInit {

  
  color: ThemePalette = 'warn';
  mode: ProgressSpinnerMode = 'indeterminate';
  value = 20;

  constructor(
    public dialogRef: MatDialogRef<MensajeEsperaGenericoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }


  ngOnInit() {
  }

}
