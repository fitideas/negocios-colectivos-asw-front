import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

export interface DialogData {
  titulo: string,
  contenido: string,
  button: string,
  urlimg: boolean,
  listError: any
}
@Component({
  selector: 'app-dialog-cargar-archivo',
  templateUrl: './dialog-cargar-archivo.component.html',
  styleUrls: ['./dialog-cargar-archivo.component.css']
})
export class DialogCargarArchivoComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogCargarArchivoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}