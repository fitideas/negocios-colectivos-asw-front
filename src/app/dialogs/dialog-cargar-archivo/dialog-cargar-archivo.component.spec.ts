import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCargarArchivoComponent } from './dialog-cargar-archivo.component';

describe('DialogCargarArchivoComponent', () => {
  let component: DialogCargarArchivoComponent;
  let fixture: ComponentFixture<DialogCargarArchivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogCargarArchivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogCargarArchivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
