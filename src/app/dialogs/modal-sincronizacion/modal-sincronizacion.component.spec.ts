import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSincronizacionComponent } from './modal-sincronizacion.component';

describe('ModalSincronizacionComponent', () => {
  let component: ModalSincronizacionComponent;
  let fixture: ComponentFixture<ModalSincronizacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSincronizacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSincronizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
