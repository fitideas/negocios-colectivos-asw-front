import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, ThemePalette, ProgressSpinnerMode } from '@angular/material';
export interface DialogData {  
  urlimg: boolean
  buttonAceptar:string
}

@Component({
  selector: 'app-modal-sincronizacion',
  templateUrl: './modal-sincronizacion.component.html',
  styleUrls: ['./modal-sincronizacion.component.css']
})

/**
* Autor:Lizeth Patiño
* Proposito: Mensajes de sincronización correcta o incorrecta con el bus de datos
* Fecha: 15/04/2020
*/

export class ModalSincronizacionComponent implements OnInit {

  color: ThemePalette = 'warn';
  mode: ProgressSpinnerMode = 'indeterminate';
  value = 20;

  constructor(
    public dialogRef: MatDialogRef<ModalSincronizacionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

  ngOnInit() {
  }

}
