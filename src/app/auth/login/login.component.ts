import { Component, OnInit } from "@angular/core";
import {
  FormControl,
  FormGroup,
  Validators,
  FormBuilder
} from "@angular/forms";
import { Router } from "@angular/router";
import { DataService } from "src/app/services/data.service";
import { IconService } from "src/app/services/icon.service";
import { RequestLogin } from "src/app/modelo/request-login"
import { ResponseLogin } from 'src/app/modelo/response-login';
import { AuthService } from 'src/app/services/auth.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  public hide: boolean = true;
  public submitted: boolean = false;
  public creadetialError: boolean = false;
  public loginForm: FormGroup;
  public placeholderUsuario: string;
  public placeholderPassword: string;

  constructor(
    private _dataService: DataService,
    private _authService: AuthService,
    private _router: Router,
    private _iconService: IconService,
    public fb: FormBuilder,
    private httpClient: HttpClient
  ) {
    this.loginForm = this.fb.group({
      usuario: new FormControl("", [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(30)
      ]),
      clave: new FormControl("", Validators.required)
    });
  }

  ngOnInit() {
    this._iconService.registerIcons();
    this._authService.logout();
    this.getDataJson();
  }

  getDataJson() {
    this.httpClient.get("assets/i18n/es.json").subscribe(data => {
      this.placeholderUsuario = data['login'].usuario;
      this.placeholderPassword = data['login'].password;
    })
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  showPassword(): boolean {    
    this.hide = !this.hide;
    return this.hide;
  }

  enviarDatos(): void {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }

    let credenciales: ResponseLogin;
    let requestLogin: RequestLogin;
    requestLogin = this.loginForm.value;

    this._dataService
      .getAuthenticationRequest(requestLogin)
      .subscribe(
        (data) => {
          credenciales = data;
          if (credenciales.token && credenciales.tiempoVidaToken) {
            this._authService.setInfoSesion(credenciales);
            this._router.navigate(["accionfiduciaria"]);
          }
        },
        (err) => {
          this.creadetialError = true;
        });
  }
}
