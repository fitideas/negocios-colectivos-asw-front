import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
  HttpParams,
  HttpEventType
} from "@angular/common/http";
import { throwError, Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { environment } from "./../../environments/environment";
import { RequestLogin } from "../modelo/request-login";
import { AuthService } from "./auth.service";
import { Documento, ParamDatosNegocio, ParamBusquedaCotitular } from '../modelo/beneficiario-detalle';
import { RequestBuscarNegocios } from '../modelo/request-buscar-negocios';
import { ParamAdministrarCotitular, FileUploadModel } from '../modelo/list-cotitulares';
const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable({
  providedIn: "root"
})
export class DataService {
  private hostUrl: string = environment.restApiServer;
  private serviceLogin: string = this.hostUrl + "/autenticacion/login";
  private serviceLogout: string = this.hostUrl + "/autenticacion/cerrarsesion";
  private serviceBuscarBeneficiarios: string = this.hostUrl + "/beneficiario/buscar";
  private serviceDetDatGenBeneficiarios: string = this.hostUrl + "/beneficiario/buscarpordocumento";
  private serviceBeneficiarioObtenerSolicitudes: string = this.hostUrl + "/beneficiario/obtenersolicitudes";
  private serviceBeneficiarioEnviarSolicitud: string = this.hostUrl + "/beneficiario/enviarsolicitud";
  private serviceBeneficiarioBuscarNegociosAsociados: string = this.hostUrl + "/beneficiario/obtenernegocios";
  private serviceBeneficiarioObtenerDatosDistribucion: string = this.hostUrl + "/beneficiario/obtenerdatosdistribucion";
  private serviceBeneficiarioGuardarDatosDistribucion: string = this.hostUrl + "/beneficiario/guardardatosdistribucion";
  private serviceBeneficiarioObtenerCotitular: string = this.hostUrl + '/beneficiario/obtenercotitular';
  private serviceBeneficiarioAdministrarCotitular: string = this.hostUrl + '/beneficiario/administrarcotitular';
  private serviceBeneficiarioHistorialPagos: string = this.hostUrl + '/beneficiario/historialpagos';
  private serviceBeneficiarioDescargarArchivoPagos: string = this.hostUrl + '/beneficiario/descargararchivohistorialpagos';
  private serviceRolesObtenerRol: string = this.hostUrl + "/roles/obtenerroles";
  private serviceRolesObtenerPermiso: string = this.hostUrl + "/roles/obtenerpermisos";
  private serviceRolesActualizar: string = this.hostUrl + "/roles/actualizarroles";
  private serviceParametrosObtenerSimples: string = this.hostUrl + "/parametros/obtenersimples";
  private serviceParametrosObtenerCompuestos: string = this.hostUrl + "/parametros/obtenercompuestos";
  private serviceParametrosActualizarSimples: string = this.hostUrl + "/parametros/actualizarsimples";
  private serviceParametrosActualizarCompuestos: string = this.hostUrl + "/parametros/actualizarcompuestos";
  private serviceParametrosParametrosEliminar: string = this.hostUrl + "/parametros/eliminar";
  private serviceParametrosValoresDominio: string = this.hostUrl + '/parametros/valoresdominio';
  private serviceParametrosObtenerRetencionesAsociadas: string = this.hostUrl +'/parametros/obtenerretencionesasociadas';
  private serviceTipoDocumentos: string = this.hostUrl + '/util/obtenertiposdocumento';
  private serviceObtenerBancos: string = this.hostUrl + '/util/obtenerbancos';
  private serviceObtenerInformacionFinanciera: string = this.hostUrl + '/beneficiario/obtenerinformacionfinanciera';
  private serviceGuardarInformacionFinanciera: string = this.hostUrl + '/beneficiario/guardarinformacionfinanciera';
  private serviceValidarEncargo: string = this.hostUrl + '/beneficiario/validarencargo';
  private serviceNegociosListarNegocios: string = this.hostUrl + '/negocio/buscar';
  private serviceNegociosInformacionNegocio: string = this.hostUrl + '/negocio/obtenerinformacionnegocio';
  private serviceNegociosInformacionEquipo: string = this.hostUrl + '/negocio/obtenerequipo';
  private serviceNegociosinformacionTipoNegocio: string = this.hostUrl + '/negocio/obtenerinformaciontiposnegocio';
  private serviceNegociosCargararchivoclientes: string = this.hostUrl + '/negocio/cargararchivoclientes';
  private serviceObtenerNegocioTitulares: string = this.hostUrl + '/negocio/obtenertitulares';
  private serviceNegociosRegistrarAsamblea: string = this.hostUrl + '/negocio/registrarasamblea';
  private serviceNegociosGuardarNegocio: string = this.hostUrl + '/negocio/guardarinformacionnegocio';
  private serviceNegociosObtenerInformacionGeneralPagos: string = this.hostUrl + '/negocio/obtenerinformaciongeneralpagos';
  private serviceNegociosObtenerInformacionGeneralPagosIngresoEgreso: string = this.hostUrl + '/negocio/obtenerinformaciondistribucionpagos';
  private serviceNegociosObtenerInformacionPagosBeneficiarios: string = this.hostUrl + '/negocio/detallebeneficiariospago';
  private serviceNegociosDescargarArchivoPagos: string = this.hostUrl + '/negocio/archivodetallebeneficiariospago';
  private serviceBeneficiarioListarNovedadesJuridicas: string = this.hostUrl + '/beneficiario/obtenernovedadesjuridicas';
  private serviceBeneficiarioEventoNovedadJuridica: string = this.hostUrl + '/beneficiario/detallenovedadjuridica';
  private serviceObtenerListaIngresosOperacion: string = this.hostUrl + '/pagos/obtenerlistaingresosoperacion';
  private serviceGuardarListaIngresosOperacionales: string = this.hostUrl + '/pagos/guardarlistaingresosoperacionales';
  private serviceCalcularObligacion: string = this.hostUrl + '/pagos/calcularobligacion';
  private serviceCalcularDistribucion: string = this.hostUrl + '/pagos/calculardistribucion';
  private serviceGenerarPago: string = this.hostUrl + '/pagos/generararchivopago';
  private serviceObtenerListaComite: string = this.hostUrl + '/negocio/obtenerlistacomite';
  private serviceParametrosObtenerTipoTercero: string = this.hostUrl + '/parametros/obtenertipotercero';
  private serviceParametrosValoresTipoCompuesto: string = this.hostUrl + '/parametros/valorestipocompuesto';
  private serviceGuardarmiembroComite: string = this.hostUrl + '/negocio/guardarmiembrocomite';
  private serviceSincronizarDatos: string = this.hostUrl + '/negocio/sincronizardatos';
  private serviceRegistrarComite: string = this.hostUrl + '/negocio/registrardatoscomite';
  private serviceReprocesarPagos: string = this.hostUrl + '/pagos/reprocesarpagos';
  private serviceBeneficiarioHistorialCambios: string = this.hostUrl + '/beneficiario/obtenerhistoricomodificaciones';
  private serviceBeneficiarioHistorialSolicitudCambios: string = this.hostUrl + '/beneficiario/obtenerhistoricsolicitudesomodificaciones';
  private serviceBeneficiarioHistorialCesiones: string = this.hostUrl + '/beneficiario/obtenerhistoricocesionesmodificaciones';
  private serviceListarUsuarios: string = this.hostUrl + '/reportes/obtenerusuarios';
  private serviceNegocioHistorialCambios: string = this.hostUrl + '/negocio/obtenerhistoricomodificaciones'
  private servicereportesolicitudescambios: string = this.hostUrl + '/reportes/reportesolicitudesdecambios';
  private servicereportesdistribucion: string = this.hostUrl + '/reportes/reportesdistribucion';
  private servicereportebeneficiaros: string = this.hostUrl + '/reportes/reportebeneficiarios';
  private servicereportecesiones: string = this.hostUrl + '/reportes/reportecesiones';
  private serviceListarNegocios: string = this.hostUrl + '/negocio/obtenernegocios';
  private serviceReporteModificacionesNegocio: string = this.hostUrl + '/reportes/reportemodificacionesvalores';
  private serviceReporteTiposNegocio: string = this.hostUrl + '/reportes/reportetiposnegocio';
  private serviceObtenerCifrasPago: string = this.hostUrl + '/reportes/modificacionpagos';
  private serviceAsambleasHistorial: string = this.hostUrl + '/negocio/obtenerreuniones';
  private serviceAsambleasDetalle: string = this.hostUrl + '/negocio/obtenerasamblea';
  private serviceComiteDetalle: string = this.hostUrl + '/negocio/obtenercomite';
  private serviceCertificadosListarTitulares : string = this.hostUrl + '/certificados/obtenertitulares';
  private serviceCertificadosListarNegocios : string = this.hostUrl + '/certificados/obtenernegocios';
  private serviceGenerarCertificado : string = this.hostUrl + '/certificados/decargarcertificados';
  private serviceCertificadoObtenerDatos : string = this.hostUrl + '/certificados/obtenerdatoscertificados';
  private serviceResponsablesNegocio: string = this.hostUrl + '/negocio/obtenerresponsablesnegocio';
  private serviceObtenerPaises: string = this.hostUrl + '/negocio/obtenerpaises';
  private serviceObtenerDepartamentos: string = this.hostUrl + '/negocio/obtenerdepartamentos';
  private serviceObtenerMunicipios: string = this.hostUrl + '/negocio/obtenermunicipios';
  private serviceObtenerTipoFideicomiso: string = this.hostUrl + '/negocio/obtenertipofideicomiso';
  private serviceObtenerSubTipoFideicomiso: string = this.hostUrl + '/negocio/obtenersubtipofideicomiso';
  private serviceObtenerTiposEspecificosNegocio: string = this.hostUrl + '/negocio/obtenertiposnegocio';

  httpOptionsPlain = {
    headers: new HttpHeaders({
      'Accept': 'text/plain',
      'Content-Type': 'text/plain'
    }),
    'responseType': 'json'
  };

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
  ) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage: any;
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = {
        error: `Error: ${error.error.message}`,
        code: 0,
        message: error.error.message
      };
    } else {
      // Server-side errors
      errorMessage = {
        error: error.error,
        code: error.status,
        message: error.message
      };
    }
    return throwError(errorMessage);
  }

  public getAuthenticationRequest(requestLogin: RequestLogin): any {
    return this.httpClient
      .post(this.serviceLogin, requestLogin, httpOptions)
      .pipe(catchError(this.handleError));
  }

  public getLogout(): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .get(this.serviceLogout, {
        headers: headers
      })
      .pipe(catchError(this.handleError));
  }

  public getBeneficiarioListRequest(
    requestBuscarBeneficiarios: any
  ): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("argumento", requestBuscarBeneficiarios.argumento)
      .set("criterioBuscar", requestBuscarBeneficiarios.criterioBuscar)
      .set("pagina", requestBuscarBeneficiarios.pagina)
      .set("elementos", requestBuscarBeneficiarios.elementos);
    return this.httpClient
      .get(this.serviceBuscarBeneficiarios, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public getBeneficiarioDetailsRequest(
    requestBuscarBeneficiarios: string
  ): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams().set("documento", requestBuscarBeneficiarios);
    return this.httpClient
      .get(
        this.serviceDetDatGenBeneficiarios, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public getBeneficiarioObtenerSolicitudes(
    tipoDocumento: any
  ): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("documento", tipoDocumento.documento)
      .set("tipodocumento", tipoDocumento.tipodocumento);
    return this.httpClient
      .get(
        this.serviceBeneficiarioObtenerSolicitudes, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public putBeneficiarioEnviarSolicitud(param: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(param);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .put(
        this.serviceBeneficiarioEnviarSolicitud,
        body,
        {
          headers: headers,
          responseType: 'json'
        })
      .pipe(catchError(this.handleError));
  }

  public getBeneficiarioObtenerNegociosListRequest(
    requestObtenerNegocios: Documento
  ): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("documento", requestObtenerNegocios.documento.toString())
      .set("tipoDocumento", requestObtenerNegocios.tipodocumento)
      .set("pagina", "1")
      .set("elementos", "10");
    return this.httpClient
      .get(this.serviceBeneficiarioBuscarNegociosAsociados, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public getBeneficiarioObtenerDatosDistribucionRequest(
    requestObtenerDatosDistribucion: ParamDatosNegocio
  ): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("documento", requestObtenerDatosDistribucion.documento)
      .set("tipoDocumento", requestObtenerDatosDistribucion.tipodocumento)
      .set("codigo", requestObtenerDatosDistribucion.codigo);
    return this.httpClient
      .get(this.serviceBeneficiarioObtenerDatosDistribucion, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public getBeneficiarioObtenerCotitularRequest(
    requestObtenerCotitular: ParamBusquedaCotitular
  ): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("documentoTitular", requestObtenerCotitular.documentoTitular)
      .set("tipoDocumentoTitular", requestObtenerCotitular.tipoDocumentoTitular)
      .set("codigoNegocio", requestObtenerCotitular.codigoNegocio)
      .set("criterioBuscar", requestObtenerCotitular.criterioBuscar)
      .set("argumento", requestObtenerCotitular.argumento)
      ;
    return this.httpClient
      .get(this.serviceBeneficiarioObtenerCotitular, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public administrarCotitular(param: ParamAdministrarCotitular): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(param);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post(
        this.serviceBeneficiarioAdministrarCotitular,
        body,
        {
          headers: headers,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }

  public putBeneficiarioGuardarDatosDistribucion(param: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(param);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .put(
        this.serviceBeneficiarioGuardarDatosDistribucion,
        body,
        {
          headers: headers,
          responseType: 'json'
        })
      .pipe(catchError(this.handleError));
  }

  public getRolesRequest(): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .get(
        this.serviceRolesObtenerRol, {
        headers: headers,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public getPermisosRequest(): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .get(
        this.serviceRolesObtenerPermiso, {
        headers: headers,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public patchActualizarRolesActualizar(param: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(param);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .patch(
        this.serviceRolesActualizar,
        body,
        {
          headers: headers,
          responseType: 'json'
        })
      .pipe(catchError(this.handleError));
  }

  public getParametrosSimplesRequest(): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .get(
        this.serviceParametrosObtenerSimples, {
        headers: headers,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public getParametrosCompuestosRequest(): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .get(
        this.serviceParametrosObtenerCompuestos, {
        headers: headers,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public patchActualizarParametrosSimples(param: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(param);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .patch(
        this.serviceParametrosActualizarSimples,
        body,
        {
          headers: headers,
          responseType: 'json'
        })
      .pipe(catchError(this.handleError));
  }

  public patchActualizarParametrosCompuestos(param: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(param);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .patch(
        this.serviceParametrosActualizarCompuestos,
        body,
        {
          headers: headers,
          responseType: 'json'
        })
      .pipe(catchError(this.handleError));
  }

  public deleteParametros(param: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(param);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post(
        this.serviceParametrosParametrosEliminar,
        body,
        {
          headers: headers,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }

  public getParametrosValoresDominio(codigo: string): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams().set("codigo", codigo);
    return this.httpClient
      .get(
        this.serviceParametrosValoresDominio, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public getParametrosObtenerRetencionesAsociadas(idValorDominio: string): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams().set("idValorDominio", idValorDominio);
    return this.httpClient
      .get(
        this.serviceParametrosObtenerRetencionesAsociadas, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public getTipoDocumentos(): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .get(
        this.serviceTipoDocumentos, {
        headers: headers,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public getObtenerBancos(): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .get(
        this.serviceObtenerBancos, {
        headers: headers,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public getObtenerInformacionFinanciera(codigonegocio: string, tipodocumento: string, numerodocumento: string): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("codigonegocio", codigonegocio)
      .set("tipodocumento", tipodocumento)
      .set("numerodocumento", numerodocumento);
    return this.httpClient
      .get(
        this.serviceObtenerInformacionFinanciera, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public putGuardarInformacionFinanciera(param: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(param);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .put(
        this.serviceGuardarInformacionFinanciera,
        body,
        {
          headers: headers,
          responseType: 'json'
        })
      .pipe(catchError(this.handleError));
  }


  public putGuardarInformacionNegocio(param: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(param);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .put(
        this.serviceNegociosGuardarNegocio,
        body,
        {
          headers: headers,
          responseType: 'json'
        })
      .pipe(catchError(this.handleError));
  }

  public getValidarEncargo(request: any): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("numeroencargo", request.numeroencargo)
      .set("documento", request.documento);
    return this.httpClient
      .get(
        this.serviceValidarEncargo, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }
  /**/
  public loadNegociosCargarArchivoClientes(file: FileUploadModel, param: any): Observable<any> {
    const formData = new FormData();
    formData.append('file', file.data);
    formData.append('codigoNegocio', param);
    let headers = new HttpHeaders();
    //.set("Content-Type", "multipart/form-data");
    headers = headers.append("Authorization", this.authService.getAccessToken());

    return this.httpClient
      .post<any>(
        this.serviceNegociosCargararchivoclientes,
        formData,
        {
          headers: headers,
          responseType: 'blob' as 'json',
          reportProgress: true,
          observe: 'events'
        },
      )
      .pipe(map((event) => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            const progress = Math.round(100 * event.loaded / event.total);
            return { status: 'progress', message: progress };
          case HttpEventType.Response:
            return event.body;
          default:
            return `Unhandled event: ${event.type}`;
        }
      })
      );
  }
  public getObtenerTitulares(codigoSFC: string): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("codigoSFC", codigoSFC);
    return this.httpClient
      .get(this.serviceObtenerNegocioTitulares, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public getNegociosListRequest(
    requestBuscarNegocios: any
  ): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("argumento", requestBuscarNegocios.argumento)
      .set("criterioBuscar", requestBuscarNegocios.criterioBuscar)
      .set("pagina", requestBuscarNegocios.pagina)
      .set("elementos", requestBuscarNegocios.elementos);
    return this.httpClient
      .get(this.serviceNegociosListarNegocios, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public getObtenerInformacionNegocio(codigonegocio: string): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("codigonegocio", codigonegocio)
    return this.httpClient
      .get(
        this.serviceNegociosInformacionNegocio, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public getObtenerEquiposDeNegocio(
    requestBuscarNegocios: RequestBuscarNegocios
  ): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("argumento", requestBuscarNegocios.argumento)
      .set("criterioBuscar", requestBuscarNegocios.criterioBuscar)
      .set("pagina", "1")
      .set("elementos", "10");
    return this.httpClient
      .get(this.serviceNegociosInformacionEquipo, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }
  /**/
  public negociosRegistrarAsamblea(param: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(param);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post(
        this.serviceNegociosRegistrarAsamblea,
        body,
        {
          headers: headers,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }

  public getObtenerInformacionTipoNegocio(codigonegocio: string): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("codigonegocio", codigonegocio)
    return this.httpClient
      .get(
        this.serviceNegociosinformacionTipoNegocio, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }


  public getNovedadesJuridicas(
    request: any
  ): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("tipoDocumento", request.tipodocumento)
      .set("documento", request.documento)
      .set("codigoNegocio", request.codigoNegocio)
      .set("pagina", "1")
      .set("elementos", "10");
    return this.httpClient
      .get(this.serviceBeneficiarioListarNovedadesJuridicas, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public getEventoNovedadJuridica(
    request: any
  ): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("tipoDocumento", request.tipodocumento)
      .set("documento", request.documento)
      .set("codigoNegocio", request.codigoNegocio)
      .set("codigoEvento", request.codigoEvento)
    return this.httpClient
      .get(this.serviceBeneficiarioEventoNovedadJuridica, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public listadoHistorialPagos(param: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(param);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post(
        this.serviceBeneficiarioHistorialPagos,
        body,
        {
          headers: headers,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }

  public descargarListadoPagos(param: any): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(param);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post<Blob>(
        this.serviceBeneficiarioDescargarArchivoPagos,
        body,
        {
          headers: headers,
          responseType: 'blob' as 'json',
          reportProgress: true,
          observe: 'events'
        }
      )
      .pipe(map((event) => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            const progress = Math.round(100 * event.loaded / event.total);
            return { status: 'progress', message: progress };

          case HttpEventType.Response:
            return event.body;
          default:
            return `Unhandled event: ${event.type}`;
        }
      })
      );
  }

  public getObtenerIngresosOperacionPorPeriodo(codigonegocio: string, periodo: string): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("codigoNegocio", codigonegocio)
      .set("periodo", periodo)
    return this.httpClient
      .get(
        this.serviceObtenerListaIngresosOperacion, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public putGuardarListaIngresosOperacionales(nuevaEntradaIngresoDatos: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(nuevaEntradaIngresoDatos);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .put(
        this.serviceGuardarListaIngresosOperacionales,
        body,
        {
          headers: headers,
          responseType: 'json'
        })
      .pipe(catchError(this.handleError));
  }

  public negocioCalcularObligacion(periodo: string, codigonegocio: string, codigotiponegocio: string): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let params = new HttpParams()
      .set("periodo", periodo)
      .set("codigoNegocio", codigonegocio)
      .set("codigoTipoNegocio", codigotiponegocio)
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .get(
        this.serviceCalcularObligacion,
        {
          headers: headers,
          params: params,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }
  public negocioCalcularDistribucion(periodo: string, codigonegocio: string, codigotiponegocio: string): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let params = new HttpParams()
      .set("periodo", periodo)
      .set("codigoNegocio", codigonegocio)
      .set("codigoTipoNegocio", codigotiponegocio)
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .get(
        this.
          serviceCalcularDistribucion,
        {
          headers: headers,
          params: params,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }

  public listadoNegocioHistorialGeneralPagos(param: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(param);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post(
        this.serviceNegociosObtenerInformacionGeneralPagos,
        body,
        {
          headers: headers,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }

  /*
  public negocioListHistorialPagosIngresoEgreso (param: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(param);
    headers = headers.append("Authorization", this.authService.getAccessToken());

    return this.httpClient
      .post(
        this.serviceNegociosObtenerInformacionGeneralPagosIngresoEgreso,
        body,
        {
          headers: headers,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }
  */

  public negocioListHistorialPagosIngresoEgreso(param: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams(
      { fromObject: param }
    );
    return this.httpClient
      .get(this.serviceNegociosObtenerInformacionGeneralPagosIngresoEgreso, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public negocioListHistorialPagosBeneficiarios(param: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams(
      { fromObject: param }
    );
    return this.httpClient
      .get(this.serviceNegociosObtenerInformacionPagosBeneficiarios, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public descargarHistorialPagosBeneficiarios(param: any): any {
    let params = new HttpParams()
      .set('periodo', param.periodo)
      .set('codigoNegocio', param.codigoNegocio)
      .set('codigoTipoNegocio', param.codigoTipoNegocio);
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());

    return this.httpClient
      .get<any>(
        this.serviceNegociosDescargarArchivoPagos,
        {
          headers: headers,
          params: params,
          responseType: 'blob' as 'json',
          reportProgress: true,
          observe: 'events'
        },
      )
      .pipe(map((event) => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            const progress = Math.round(100 * event.loaded / event.total);
            return { status: 'progress', message: progress };

          case HttpEventType.Response:
            return event.body;
          default:
            return `Unhandled event: ${event.type}`;
        }
      })
      );
  }

  public negocioGenerarPago(periodo: string, codigonegocio: string, codigotiponegocio: string): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let params = new HttpParams()
      .set("periodo", periodo)
      .set("codigoNegocio", codigonegocio)
      .set("codigoTipoNegocio", codigotiponegocio)
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .get(
        this.
          serviceGenerarPago,
        {
          headers: headers,
          params: params,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }


  public getObtenerInformacionComite(codigonegocio: string): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("codigoNegocio", codigonegocio)
    return this.httpClient
      .get(
        this.serviceObtenerListaComite, {
      headers: headers,
      params: params,
      responseType: 'json'
    })
    .pipe(catchError(this.handleError));
}

public putGuardarInformacpionMiembroComite(param: any): Observable<any> {
  let headers = new HttpHeaders().set("Content-Type", "application/json");
  let body = JSON.stringify(param);
  headers = headers.append("Authorization", this.authService.getAccessToken());
  return this.httpClient
    .put(
      this.serviceGuardarmiembroComite,
      body,
      {
        headers: headers,
        params: param,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  // TODO: Renombrar Lizeth
  public putGuardarInformacpionMiembroComiteEntrada(param: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(param);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .put(
        this.serviceGuardarmiembroComite,
        body,
        {
          headers: headers,
          responseType: 'json'
        })
      .pipe(catchError(this.handleError));
  }


  public getServiceParametrosObtenerTipoTercero(codigo: any) {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams().set("codigo", codigo);
    return this.httpClient
      .get(this.serviceParametrosObtenerTipoTercero, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public getServiceParametrosValoresTipoCompuesto(codigo: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams().set("codigo", codigo);
    return this.httpClient
      .get(this.serviceParametrosValoresTipoCompuesto, {
        headers: headers,
        params: params,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  public negocioSincronizarDatos(codigonegocio: string): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let params = new HttpParams()
      .set("codigoNegocio", codigonegocio)
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .get(
        this.
          serviceSincronizarDatos,
        {
          headers: headers,
          params: params,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }


  public negociosRegistrarComite(param: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(param);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post(
        this.serviceRegistrarComite,
        body,
        {
          headers: headers,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }

  listadoBeneficiarioHistorialCambios(request: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(request);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post(
        this.serviceBeneficiarioHistorialCambios, body, {
        headers: headers,
        responseType: 'json'
      }
      )
      .pipe(catchError(this.handleError))
  }

  listadoBeneficiarioHistorialSolicitudCambios(request: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(request);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post(
        this.serviceBeneficiarioHistorialSolicitudCambios, body, {
        headers: headers,
        responseType: 'json'
      }
      )
      .pipe(catchError(this.handleError))
  }

  listadoBeneficiarioHistorialCesiones(request: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(request);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post(
        this.serviceBeneficiarioHistorialCesiones, body, {

        headers: headers,
        responseType: 'json'
      }
      )
      .pipe(catchError(this.handleError))
  }

  listadoNegocioHistorialCambios(request: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(request);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post(
        this.serviceNegocioHistorialCambios, body, {
        headers: headers,
        responseType: 'json'
      }
      )
      .pipe(catchError(this.handleError))
  }

  listadoAsambleasHistorial(request: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(request);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post(
        this.serviceAsambleasHistorial, body, {
        headers: headers,
        responseType: 'json'
      }
      )
      .pipe(catchError(this.handleError))
  }

  public getUsuariosAplicacion(): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .get(
        this.serviceListarUsuarios,
        {
          headers: headers,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }

  public postServiceReporteSolicitudesCambios(body): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");

    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post(
        this.servicereportesolicitudescambios,
        body,
        {
          headers: headers,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }

  public postServiceReportesDistribucion(body): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");

    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post(
        this.servicereportesdistribucion,
        body,
        {
          headers: headers,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }
  public postServiceReporteBeneficiaros(body): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");

    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post(
        this.servicereportebeneficiaros,
        body,
        {
          headers: headers,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }
  public postServiceReporteCesiones(body): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");

    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post(
        this.servicereportecesiones,
        body,
        {
          headers: headers,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }

  public getObtenerNegociosAplicacion(): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .get(
        this.
          serviceListarNegocios,
        {
          headers: headers,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }

public postServiceReporteTiposNegocio(body): Observable<any> {
  let headers = new HttpHeaders().set("Content-Type", "application/json");

  headers = headers.append("Authorization", this.authService.getAccessToken());
  return this.httpClient
    .post(
      this.serviceReporteTiposNegocio,
      body,
      {
        headers: headers,
        responseType: 'json'
      }
    )
    .pipe(catchError(this.handleError));
}



  public postServiceReporteModificacionesNegocio(body): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");

    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post(
        this.serviceReporteModificacionesNegocio,
        body,
        {
          headers: headers,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }



  public postConsultarCifrasPago(param: any): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let body = JSON.stringify(param);
    headers = headers.append("Authorization", this.authService.getAccessToken());
    return this.httpClient
      .post(
        this.serviceObtenerCifrasPago,
        body,
        {
          headers: headers,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }

  public postCertificadosTitularesListRequest(
    requestTitulares: any
  ): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let body = JSON.stringify(requestTitulares);
    return this.httpClient
      .post(
        this.serviceCertificadosListarTitulares, 
        body,
        {
        headers: headers,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

  
  public postCertificadosNegociosListRequest(
    request: any
  ): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let body = JSON.stringify(request);
    return this.httpClient
      .post(
        this.serviceCertificadosListarNegocios, 
        body,
        {
        headers: headers,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }

public negociosReprocesarPagos(param: any): Observable<any> {
  let headers = new HttpHeaders().set("Content-Type", "application/json");
  let body = JSON.stringify(param);
  headers = headers.append("Authorization", this.authService.getAccessToken());
  return this.httpClient
    .post(
      this.serviceReprocesarPagos,
      body,
      {
        headers: headers,
        responseType: 'json'
      }
    )
    .pipe(catchError(this.handleError));
}

  public generarCertificado(tipo:string, request:any ): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");

    headers = headers.append("Authorization", this.authService.getAccessToken());
    let body = JSON.stringify(request);
    let params = new HttpParams()
      .set("paramTipo", tipo);
    return this.httpClient
      .post(
        this.serviceGenerarCertificado,
        body, 
        {
          headers: headers,
          params:params,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
  }
  public postCertificadosObtenerDatos(
    request: any, tipo:string): any {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    let params = new HttpParams()
      .set("paramTipo", tipo)
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let body = JSON.stringify(request);
    return this.httpClient
      .post(
        this.serviceCertificadoObtenerDatos, 
        body,
        {
        params:params,
        headers: headers,
        responseType: 'json'
      })
      .pipe(catchError(this.handleError));
  }



getObtenerInformacionReunion(codigoSFC:string, fecha:string, numeroRadicado:string){
  let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("codigoNegocio", codigoSFC)
      .set("fechaHora",fecha)
      .set("numeroRadicado",numeroRadicado);
    return this.httpClient
      .get(
        this.serviceAsambleasDetalle,
        {
          headers: headers,
          params:params,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
}

getObtenerInfoComite(codigoSFC:string, fecha:string, numeroRadicado:string){
  let headers = new HttpHeaders().set("Content-Type", "application/json");
    headers = headers.append("Authorization", this.authService.getAccessToken());
    let params = new HttpParams()
      .set("codigoNegocio", codigoSFC)
      .set("fechaHora",fecha)
      .set("numeroRadicado",numeroRadicado);
    return this.httpClient
      .get(
        this.serviceComiteDetalle,
        {
          headers: headers,
          params:params,
          responseType: 'json'
        }
      )
      .pipe(catchError(this.handleError));
}

public getResponsablesNegocio(): Observable<any> {
  let headers = new HttpHeaders().set("Content-Type", "application/json");
  headers = headers.append("Authorization", this.authService.getAccessToken());
  return this.httpClient
    .get(
      this.serviceResponsablesNegocio,
      {
        headers: headers,
        responseType: 'json'
      }
    )
    .pipe(catchError(this.handleError));
}

public getPaises(): Observable<any> {
  let headers = new HttpHeaders().set("Content-Type", "application/json");
  headers = headers.append("Authorization", this.authService.getAccessToken());
  return this.httpClient
    .get(
      this.serviceObtenerPaises,
      {
        headers: headers,
        responseType: 'json'
      }
    )
    .pipe(catchError(this.handleError));
}

public getDepartamentos(
  codigoPais: string
): any {
  let headers = new HttpHeaders().set("Content-Type", "application/json");
  headers = headers.append("Authorization", this.authService.getAccessToken());
  let params = new HttpParams().set("codigoPais", codigoPais);
  return this.httpClient
    .get(
      this.serviceObtenerDepartamentos, {
      headers: headers,
      params: params,
      responseType: 'json'
    })
    .pipe(catchError(this.handleError));
}

public getMunicipios(
  codigoDepartamento: string
): any {
  let headers = new HttpHeaders().set("Content-Type", "application/json");
  headers = headers.append("Authorization", this.authService.getAccessToken());
  let params = new HttpParams().set("codigoDepartamento", codigoDepartamento);
  return this.httpClient
    .get(
      this.serviceObtenerMunicipios, {
      headers: headers,
      params: params,
      responseType: 'json'
    })
    .pipe(catchError(this.handleError));
}

public getTipoFideicomiso(): Observable<any> {
  let headers = new HttpHeaders().set("Content-Type", "application/json");
  headers = headers.append("Authorization", this.authService.getAccessToken());
  return this.httpClient
    .get(
      this.serviceObtenerTipoFideicomiso,
      {
        headers: headers,
        responseType: 'json'
      }
    )
    .pipe(catchError(this.handleError));
}

public getSubTipoFideicomiso(
  tipoFideicomiso: string
): any {
  let headers = new HttpHeaders().set("Content-Type", "application/json");
  headers = headers.append("Authorization", this.authService.getAccessToken());
  let params = new HttpParams().set("tipoFideicomiso", tipoFideicomiso);
  return this.httpClient
    .get(
      this.serviceObtenerSubTipoFideicomiso, {
      headers: headers,
      params: params,
      responseType: 'json'
    })
    .pipe(catchError(this.handleError));
}

public getTipoEspecificoNegocio(
  codigoNegocio: string
): any {
  let headers = new HttpHeaders().set("Content-Type", "application/json");
  headers = headers.append("Authorization", this.authService.getAccessToken());
  let params = new HttpParams().set("codigoNegocio", codigoNegocio);
  return this.httpClient
    .get(
      this.serviceObtenerTiposEspecificosNegocio, {
      headers: headers,
      params: params,
      responseType: 'json'
    })
    .pipe(catchError(this.handleError));
}

}

