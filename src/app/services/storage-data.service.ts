import { Injectable } from "@angular/core";
import {
  RolFuncionalidad,
  Perfil,
  ListaFuncionalidades,
  Funcionalidades
} from "../modelo/perfil";
import { BeneficiarioDetalle } from '../modelo/beneficiario-detalle';

@Injectable({
  providedIn: "root"
})
export class StorageDataService {  
  private infoBeneficiario: BeneficiarioDetalle;
  private rolesFuncionalidad: RolFuncionalidad[];
  private datos_generales: string;
  private showActiveDatosDistribucion: boolean = false;
  private showActiveDatosFinanciera: boolean = false;
  private showActiveBeneficiarioHistorial: boolean = false;
  private showActiveNegocioHistorial: boolean = false;
  private beneficiarioHistorialPagos: boolean = false;
  private beneficiarioHistorialModificaciones: boolean = false;
  private negocioHistorialModificaciones: boolean = false;
  private infoDatosDistribucion: any;
  public beneficiariosDistribucionRetencionesNuevas: any[] = [];
  public beneficiariosDistribucionRetencionesEliminar: any[] = [];
  public infoNegocio: any;
  private codigoSFC: string;
  estadoProy: any;
  private menuViculado: boolean = false;
  private menuNegocio: boolean = false;
  infoNegocioTB: any;
  listarConcepto: any;
  administracionComite: any;

  private showActiveAsambleaHistorial: boolean = false;
  private asambleaHistorial: boolean = false;

  private negocioPagosDistribucion: boolean = false;
  private negocioHistorialPagosDistribucion: boolean = false;
  private pagosDistribucion: boolean = false;
  private pagosIngresoEgreso: boolean = false;
  private pagosHistorialBeneficiarios: boolean = false; s
  private negocioHistorialPagosTipoNegocio: string;
  private negocioHistorialPagosPeriodo: any[];
  private negocioCargararchivo: boolean = false;
  private negocioVerTitulares: boolean = false;
  private swInicio: boolean = false;
  private reprocesoPago: any;
  private retencionesTipoNegocioEliminadas = new Array();
  private tipoReunion: string;
  public infoReunion: any;
  public showActiveReunionDetalles:boolean = false;
  public showActiveComiteDetalles:boolean = false;
  public showAsambleaNuevaEntrada: boolean = false;
  public showComiteNuevaEntrada: boolean = false;
  private codigoNegocioBenficiarioPago: string = null;
  private inicializarCertificadosVinculados: number = 0;
  private inicializarCertificadosNegocios: number = 0;
  private pagoDistribucionPeriodo: any;
  private regresarPagoDistribucionVistaUno: boolean=false;
  private regresarPagoDistribucionVistaDos: boolean =false;
  private infoNegocioCompleta: boolean = false;

  constructor() {
    this.rolesFuncionalidad = new Array<RolFuncionalidad>();
  }

  public setSwInicio(sw){
    this.swInicio = sw;
  }

  public getSwInicio(){
    return this.swInicio;
  }

  public setInfoNegocioListaTitularBen(negocio: any): void {
    this.infoNegocioTB = negocio;
  }

  public setListarConceptos(concepto: any): void {
    this.listarConcepto = concepto;
  }

  public setInfoBeneficiario(beneficiario: any): void {
    this.infoBeneficiario = beneficiario;
  }

  public setInfoNegocioDetalle(negocioDetallel: any): void {
    this.infoNegocio = negocioDetallel;
  }

  public setInfoNuevaReproceso(reproceso: any): void {
    this.reprocesoPago = reproceso;
  }

  public getInfoNuevaReproceso(): any {
    return this.reprocesoPago;
  }

  public setEstadoProyecto(estadoProyecto: any): void {
    this.estadoProy = estadoProyecto;
  }

  public getListarConcepto(): any {
    return this.listarConcepto;
  }

  public getInfoNegocioTB(): any {
    return this.infoNegocioTB;
  }

  public getEstadoProyecto(): any {
    return this.estadoProy;
  }

  public getInfoNegocioDetalle(): any {
    return this.infoNegocio;
  }

  public setcodigoSFCNegocio(codigoSFC) {
    this.codigoSFC = codigoSFC;
  }

  public getcodigoSFCNegocio() {
    return this.codigoSFC;
  }

  public getInfoBeneficiario(): any {
    return this.infoBeneficiario;
  }

  public setPerfilesRolPermisos(rol: RolFuncionalidad): any {
    this.rolesFuncionalidad.push(rol);
    this.rolesFuncionalidad = this.removeDuplicates(rol);
  }

  public removeItemFromArr(arr, item) {
    return arr.filter(function (e) {
      return e.idRol !== item;
    });
  }

  public eliminarRol(idRol) {
    this.rolesFuncionalidad = this.removeItemFromArr(
      this.rolesFuncionalidad,
      idRol
    );
  }

  public removeDuplicates(rol) {
    let newArray = [];
    this.rolesFuncionalidad.forEach(value => {
      if (
        value.idRol == rol.idRol &&
        value.idFuncionalidad == rol.idFuncionalidad
      ) {
        value.permiteEscribir = rol.permiteEscribir;
      }
    });

    this.rolesFuncionalidad.forEach((value, key) => {
      let exists = false;
      newArray.forEach(val2 => {
        if (
          value.idRol == val2.idRol &&
          value.idFuncionalidad == val2.idFuncionalidad &&
          value.permiteEscribir == val2.permiteEscribir
        ) {
          exists = true;
        }
      });
      if (exists == false) {
        newArray.push(value);
      }
    });

    return newArray;
  }

  public removeRolDuplicates(objPerfil) {
    let newArray = [];
    objPerfil.forEach(item => {
      let exists = false;
      newArray.forEach(element => {
        if (item.idRol == element.idRol) {
          exists = true;
        }
      });
      if (exists == false) {
        newArray.push(item);
      }
    });
    return newArray;
  }

  public getPerfilesRolPermisos() {
    let objPerfil: Perfil[];
    let perfil: Perfil;
    let listaFuncionalidades: ListaFuncionalidades;
    let funcionalidad: Funcionalidades;
    let funcionalidades: Funcionalidades[];

    funcionalidades = new Array<RolFuncionalidad>();
    objPerfil = new Array<Perfil>();

    this.rolesFuncionalidad.forEach(element => {
      funcionalidades = [];

      this.rolesFuncionalidad.forEach(item => {
        if (element.idRol == item.idRol) {
          if (item.idFuncionalidad == null) {
            listaFuncionalidades = {
              funcionalidades: []
            };
          } else {
            funcionalidad = {
              funcionalidad: null,
              idFuncionalidad: item.idFuncionalidad,
              permiteEscribir: item.permiteEscribir
            };
            funcionalidades.push(funcionalidad);

            listaFuncionalidades = {
              funcionalidades: funcionalidades
            };
          }
        }
      });

      perfil = {
        idRol: element.idRol,
        rol: null,
        listaFuncionalidades: listaFuncionalidades
      };
      objPerfil.push(perfil);
    });
    objPerfil = this.removeRolDuplicates(objPerfil);
    return objPerfil;

  }

  public inicializarRolesFuncionalidad() {
    this.rolesFuncionalidad = [];
  }

  public setPDatosGenerales(datos_generales) {
    this.datos_generales = datos_generales;
  }
  public getPDatosGenerales() {
    return this.datos_generales;
  }

  public setActiveDistribucion(swDatoDistribucion) {
    this.showActiveDatosDistribucion = swDatoDistribucion;
  }

  public getActiveDistribucion() {
    return this.showActiveDatosDistribucion;
  }

  public setInfoDatosDistribucion(datosDistribucion): void {
    this.infoDatosDistribucion = datosDistribucion;
  }

  public getInfoDatosDistribucion(): any {
    return this.infoDatosDistribucion;
  }
  public setActiveFinanciera(swDatoFinanciera) {
    this.showActiveDatosFinanciera = swDatoFinanciera;
  }

  public getActiveFinanciera() {
    return this.showActiveDatosFinanciera;
  }

  public setActiveBeneficiarioHistorial(swDatoHistorial) {
    this.showActiveBeneficiarioHistorial = swDatoHistorial;
  }

  public setActiveNegocioHistorial(swDatoHistorial){
    this.showActiveNegocioHistorial = swDatoHistorial;
  }

  public getActiveNegocioHistorial(){
    return this.showActiveNegocioHistorial;
  }

  public setActiveAsambleaHistorial(swDatoHistorial){
    this.showActiveAsambleaHistorial = swDatoHistorial;
  }

  public getActiveAsambleaHistorial(){
    return this.showActiveAsambleaHistorial;
  }

  public setAsambleaNuevaEntrada(sw){
    this.showAsambleaNuevaEntrada = sw;
  }

  public getAsambleaNuevaEntrada(){
    return this.showAsambleaNuevaEntrada;
  }

  public setComiteNuevaEntrada(sw){
    this.showComiteNuevaEntrada = sw;
  }

  public getComiteNuevaEntrada(){
    return this.showComiteNuevaEntrada;
  }

  public getActiveBeneficiarioHistorial() {
    return this.showActiveBeneficiarioHistorial;
  }

  public setBeneficiarioHistorialPagos(swDatoHistorial) {
    this.beneficiarioHistorialPagos = swDatoHistorial;
  }

  public getBeneficiarioHistorialPagos() {
    return this.beneficiarioHistorialPagos;
  }

  public setBeneficiarioHistoriallModificaciones(swDatoHistorial) {
    this.beneficiarioHistorialModificaciones = swDatoHistorial;
  }

  public getBeneficiarioHistorialModificaciones() {
    return this.beneficiarioHistorialModificaciones;
  }

  public setNegocioHistoriallModificaciones(swDatoHistorial){
    this.negocioHistorialModificaciones = swDatoHistorial;
  }

  public getNegocioHistoriallModificaciones(){
    return this.negocioHistorialModificaciones;
  }

  public setAsambleaHistorial(swDatoHistorial){
    this.asambleaHistorial = swDatoHistorial;
  }

  public getAsambleaHistorial(){
    return this.asambleaHistorial;
  }

  public setBeneficiarioDistribucionRentencionesNuevas(rentencioId) {
    this.beneficiariosDistribucionRetencionesNuevas.push(rentencioId);
  }

  public resetBeneficiarioDistribucionRentencionesNuevas() {
    this.beneficiariosDistribucionRetencionesNuevas = [];
  }

  public getBeneficiarioDistribucionRentencionesNuevas() {
    return this.beneficiariosDistribucionRetencionesNuevas;
  }

  public setBeneficiariosDistribucionRetencionesEliminar(rentencioId) {
    this.beneficiariosDistribucionRetencionesEliminar.push(rentencioId);
  }

  public resetBeneficiariosDistribucionRetencionesEliminar() {
    this.beneficiariosDistribucionRetencionesEliminar = [];
  }

  public getBeneficiariosDistribucionRetencionesEliminar() {
    return this.beneficiariosDistribucionRetencionesEliminar;
  }

  public setMenuViculado(sw) {
    this.menuViculado = sw;
  }

  public getMenuViculado() {
    return this.menuViculado;
  }

  public setMenuNegocio(sw) {
    this.menuNegocio = sw;
  }

  public getMenuNegocio() {
    return this.menuNegocio;
  }

  public setActiveNegocioPagosDistribucion(swDatoNegociosPagosDistribucion) {
    this.negocioPagosDistribucion = swDatoNegociosPagosDistribucion;
  }

  public getActiveNegocioPagosDistribucion() {
    return this.negocioPagosDistribucion;
  }

  public setActiveNegocioHistorialPagosDistribucion(swDatoHistorialPagosDistribucion) {
    this.negocioHistorialPagosDistribucion = swDatoHistorialPagosDistribucion;
  }

  public getActiveNegocioHistorialPagosDistribucion() {
    return this.negocioHistorialPagosDistribucion;
  }

  public setActivePagosDistribucion(swDatoPagosDistribucion) {
    this.pagosDistribucion = swDatoPagosDistribucion;
  }

  public getActivePagosDistribucion() {
    return this.pagosDistribucion;
  }

  public setActiveNegocioHistorialPagosIngresoEgreso(swDatoPagosIngresoEgreso) {
    this.pagosIngresoEgreso = swDatoPagosIngresoEgreso;
  }

  public getActiveNegocioHistorialPagosIngresoEgreso() {
    return this.pagosIngresoEgreso;
  }
  public setActiveNegocioHistorialPagosBeneficiarios(swDatoPagosBeneficiarios) {
    this.pagosHistorialBeneficiarios = swDatoPagosBeneficiarios;
  }

  public getActiveNegocioHistorialPagosBeneficiarios() {
    return this.pagosHistorialBeneficiarios;
  }

  public setNegocioHistorialPagosIngresoEgresoTipoNegocio(tipoNegocio) {
    this.negocioHistorialPagosTipoNegocio = tipoNegocio;
  }

  public getNegocioHistorialPagosIngresoEgresoTipoNegocio() {
    return this.negocioHistorialPagosTipoNegocio;
  }

  public setNegocioHistorialPagosIngresoEgresoPeriodo(rangoFecha) {
    this.negocioHistorialPagosPeriodo = rangoFecha;
  }

  public getNegocioHistorialPagosIngresoEgresoPeriodo() {
    return this.negocioHistorialPagosPeriodo;
  }

  public setNegocioCargarArchivoCliente(swArchivo) {
    this.negocioCargararchivo = swArchivo;
  }

  public getNegocioCargarArchivoCliente() {
    return this.negocioCargararchivo;
  }

  public setNegocioVerTitulares(swTitulares) {
    this.negocioVerTitulares = swTitulares;
  }

  public getNegocioVerTitulares() {
    return this.negocioVerTitulares;
  }

  public setPagoDistribucionEnviarPeriodo(periodo) {
    this.pagoDistribucionPeriodo = periodo;
  }

  public getPagoDistribucionEnviarPeriodo() {
    return this.pagoDistribucionPeriodo;
  }

  public setRegresarPagoDistribucionVistaUno(bandera) {
    this.regresarPagoDistribucionVistaUno = bandera;
  }

  public getRegresarPagoDistribucionVistaUno() {
    return this.regresarPagoDistribucionVistaUno;
  }

  public setRegresarPagoDistribucionVistaDos(bandera) {
    this.regresarPagoDistribucionVistaDos = bandera;
  }

  public getRegresarPagoDistribucionVistaDos() {
    return this.regresarPagoDistribucionVistaDos;
  }



  
  public setBeneficiariosDistribucionRetencionesTipoNegocioEliminar(retenciones) {
    this.retencionesTipoNegocioEliminadas.push(retenciones);
  }

  public resetBeneficiariosDistribucionRetencionesTipoNegocioEliminar() {
    this.retencionesTipoNegocioEliminadas = [];
  }

  public getBeneficiariosDistribucionRetencionesTipoNegocioEliminar() {
    return this.retencionesTipoNegocioEliminadas;
  }

  public setTipoReunion(tipo){
    this.tipoReunion = tipo;
  }

  public getTipoReunion(){
    return this.tipoReunion;
  }

  public setInfoReunion(datosReunion: any){
    this.infoReunion = datosReunion;
  }

  public getInfoReunion(): any {
    return this.infoReunion;
  }

  public setActiveReunionDetalles(activar:boolean){
    this.showActiveReunionDetalles = activar;
  }

  public getActiveReunionDetalles():boolean{
    return this.showActiveReunionDetalles;
  }

  public setActiveComiteDetalles(activar:boolean){
    this.showActiveComiteDetalles = activar;
  }

  public getActiveComiteDetalles():boolean{
    return this.showActiveComiteDetalles;
  }

  public setCodigoNegocioBenficiarioPago(codigoNegocio: any) {
    this.codigoNegocioBenficiarioPago = codigoNegocio;
  }

  public getCodigoNegocioBenficiarioPago() {
    return  this.codigoNegocioBenficiarioPago;
  }

  public setInicializarCertificadosVinculados(sw) {
    this.inicializarCertificadosVinculados = sw;
  }

  public getInicializarCertificadosVinculados() {
    return this.inicializarCertificadosVinculados;
  }

  public setInicializarCertificadosNegocios(sw) {
    this.inicializarCertificadosNegocios = sw;
  }

  public getInicializarCertificadosNegocios() {
    return this.inicializarCertificadosNegocios;
  }

  public setInfoNegocioDetalleCompletoPagos(infoNegocioCompleta) {
    this.infoNegocioCompleta = infoNegocioCompleta;
  }

  public getInfoNegocioDetalleCompletoPagos() {
    return this.infoNegocioCompleta;
  }

}
