import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: "root"
})
export class AuthService {
  private fechaUltimoAcceso: string;

  helper = new JwtHelperService();
  decodedToken: any;
  expirationDate: Date;
  isExpired: boolean = false;

  constructor(private router: Router) { }

  // After login save token and other values(if any) in localStorage
  setInfoSesion(resp: any) {
    this.isExpired = false;
    this.fechaUltimoAcceso = resp.fechaUltimoAcceso;
    sessionStorage.setItem("nombreUsuario", resp.nombreUsuario);
    sessionStorage.setItem("nombreCompleto", resp.nombreCompleto);
    sessionStorage.setItem("accessToken", resp.token);
    sessionStorage.setItem("tiempoVidatoken", resp.tiempoVidaToken);
    this.setOpcionesMenu(resp.roles);
  }

  getNombreUsuario() {
    return sessionStorage.getItem("nombreUsuario");
  }

  setOpcionesMenu(roles: any) {
    let opcionesMenu = [];
    let funcionalidades = [];
    roles.forEach(rolesItem => {
      rolesItem.menu.forEach(menuItem => {
        menuItem.funcionalidades.forEach(itemFuncionalidad => {
          funcionalidades.push(itemFuncionalidad);
        });
        opcionesMenu.push(menuItem.opcion);
      });
    });
    sessionStorage.setItem("funcionalidades", JSON.stringify(funcionalidades));
    sessionStorage.setItem("menu", JSON.stringify(opcionesMenu));
  }

  getOpcionesMenu() {
    return JSON.parse(sessionStorage.getItem("menu"));
  };

  getFuncionalidades() {
    return JSON.parse(sessionStorage.getItem("funcionalidades"));
  };

  getNombreCompletoUsuario() {
    return sessionStorage.getItem("nombreCompleto");
  }

  getAccessToken() {
    return sessionStorage.getItem("accessToken");
  }

  getFechaUltimoAcceso() {
    return this.fechaUltimoAcceso;
  }

  // Checking if token is set
  isLoggedIn() {
    return sessionStorage.getItem("accessToken") != null;
  }

  // After clearing localStorage redirect to login screen
  logout() {
    sessionStorage.clear();
    this.router.navigate(["/login"]);
    this.isExpired = false;
  }

  isExpireded() {
    this.isExpired = false;
    let tkn = this.getAccessToken();
    this.decodeToken(tkn);
    return this.isExpired;
  }

  decodeToken(tkn: string): void {
    if (tkn) {
      this.decodedToken = this.helper.decodeToken(tkn);
      this.expirationDate = this.helper.getTokenExpirationDate(tkn);
      this.isExpired = this.helper.isTokenExpired(tkn);
    }
  }

}
