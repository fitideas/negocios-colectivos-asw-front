import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateRangePeriodoComponent } from './date-range-periodo.component';

describe('DateRangePeriodoComponent', () => {
  let component: DateRangePeriodoComponent;
  let fixture: ComponentFixture<DateRangePeriodoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateRangePeriodoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateRangePeriodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
