import { Component, EventEmitter, OnInit, ViewChild, Output } from '@angular/core';
/*
import { ControlContainer, NgForm } from "@angular/forms";
import { MatInput, MatDatepicker } from '@angular/material';
*/
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepicker} from '@angular/material/datepicker';
import { SatDatepicker } from 'saturn-datepicker';
import { HttpClient } from '@angular/common/http';
import { formatDate } from '@angular/common';
import moment, { Moment } from 'moment';
import { FormControl } from '@angular/forms';




export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-date-range-periodo',
  templateUrl: './date-range-periodo.component.html',
  styleUrls: ['./date-range-periodo.component.css'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class DateRangePeriodoComponent implements OnInit {

  @ViewChild('picker', { static: false }) dateInput: SatDatepicker<any>;
  @Output() emitRange: EventEmitter<any> = new EventEmitter<any>();
  @Output() emitRangeFin: EventEmitter<any> = new EventEmitter<any>();
  delay;
  public ref: any;
  public todayDate = moment();
  public placeholderFecha: string;
  pastDate: string;
  picker: any;
  date = new FormControl(moment());
  dateFin = new FormControl(moment());
  dateMin: any;
  constructor(
    private _httpClient: HttpClient
  ) { }

  ngOnInit() {
    var square=document.getElementById("square");
    this.getDataJson();
  }

  
  public formatDate(fechaAtransformar) {
    const format = 'dd/MM/yyyy';
    
    const myDate = fechaAtransformar;
    const locale = 'en-US';
    const formattedDate = formatDate(myDate, format, locale);
    return formattedDate;
  }

  getDataJson() {
    this._httpClient.get("assets/i18n/es.json").subscribe(data =>{
      this.placeholderFecha = data['beneficiario_historial_pagos'].seleccionar_rango_fecha;
    })
  }



  public monthSelected(params) {

    this.picker.close();
  }



  chosenYearHandler(normalizedYear: Moment) {
    this.cambiar();
    const ctrlValue = this.date.value;
    ctrlValue.year(normalizedYear.year());
    this.date.setValue(ctrlValue);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value;
    ctrlValue.month(normalizedMonth.month());
    this.date.setValue(ctrlValue);
    
    datepicker.close();
  }

  cambiar(){
    var square=document.getElementById("square");
    square.style.width = "67px";
  }

  cambiarDos(){
    var square=document.getElementById("squareDos");
    square.style.width = "67px";
  }


  chosenYearHandler2(normalizedYear: Moment) {
    this.cambiarDos();
    const ctrlValue = this.dateFin.value;
    ctrlValue.year(normalizedYear.year());
    this.dateFin.setValue(ctrlValue);
    this.emitRange.emit(this.date.value);
  }

  chosenMonthHandler2(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.dateFin.value;
    ctrlValue.month(normalizedMonth.month());
    this.dateFin.setValue(ctrlValue);
    this.emitRangeFin.emit(this.dateFin.value);
    datepicker.close();
  }
 
}

