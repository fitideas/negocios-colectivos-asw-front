
import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { BeneficiarioDetalle, Documento } from 'src/app/modelo/beneficiario-detalle';
import { FormControl } from '@angular/forms';
import { Subject, ReplaySubject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { MatSelect } from '@angular/material';

export interface Negocio {
  nombre: any;
  codigoSFC: any;
  
}
@Component({
  selector: 'app-select-multiple-negocio-search',
  templateUrl: './select-multiple-negocio-search.component.html',
  styleUrls: ['./select-multiple-negocio-search.component.css']
})
export class SelectMultipleNegocioSearchComponent implements OnInit {

  @Output() emitNegocioSeleccionado: EventEmitter<any> = new EventEmitter<any>();
  negociosList: any[] = Array();
  
  negocio: Negocio;

  public documento: Documento;
  public infoBeneficiario: BeneficiarioDetalle;
  negocioForm = new FormControl();
  selected: string[];

  public negociosMulti: Negocio[] = Array();
  public negocioMultiCtrl: FormControl = new FormControl();
  public negocioMultiFilterCtrl: FormControl = new FormControl();
  public filteredNegociosMulti: ReplaySubject<Negocio[]> = new ReplaySubject<Negocio[]>(1);

  protected _onDestroy = new Subject<void>();

  constructor(
    private _dataService: DataService,
    private _storageData: StorageDataService
  ) {
    this.negociosMulti = [];
  }

  ngOnInit() {
    this.loadInfoNegocio();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private loadInfoNegocio() {
    this._dataService
      .getObtenerNegociosAplicacion()
      .subscribe((data: any) => {
        if (!data) {
          return;
        }
        setTimeout(() => {
          data.forEach(element => {
            this.negocio = {
              nombre: element.nombre,
              codigoSFC:element.codigoSFC
              }
            this.negociosMulti.push(this.negocio);
            this.filteredNegociosMulti.next(this.negociosMulti.slice());
            this.negocioMultiFilterCtrl.valueChanges
              .pipe(takeUntil(this._onDestroy))
              .subscribe(() => {
                this.filterNegociosMulti();
              });
          });
        });
      },
        (err) => {
        }
      );
  }


  itemSeleccionado(event) {
    // this.emitNegocioSeleccionado.emit(event.value);
    let negocio = [];
    if (event.value[0] != undefined) {
      event.value.forEach(element => {
        if (element.codigoSFC) {
          negocio.push(element.codigoSFC);
        }
      });
      this.emitNegocioSeleccionado.emit(negocio);
    }
  }

  selectAll( select: MatSelect ) {
    let negocio = [];
    select.value = this.negociosMulti;
    this.negociosMulti.forEach(element => {
      negocio.push(element.codigoSFC)
    });
    this.emitNegocioSeleccionado.emit(negocio);
  }
  
  deselectAll( select: MatSelect ) { 
    select.value = [];
    this.emitNegocioSeleccionado.emit([]);    
  }

  private filterNegociosMulti() {
    if (!this.negociosMulti) {
      return;
    }
    // get the search keyword
    let search = this.negocioMultiFilterCtrl.value;
    if (!search) {
      this.filteredNegociosMulti.next(this.negociosMulti.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filtro de rentenciones
    this.filteredNegociosMulti.next(
      this.negociosMulti.filter(
        negocio => negocio.nombre.toLowerCase().indexOf(search) > -1
      )
    );
  }
}
