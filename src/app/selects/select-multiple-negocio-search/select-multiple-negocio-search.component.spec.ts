import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectMultipleNegocioSearchComponent } from './select-multiple-negocio-search.component';

describe('SelectMultipleNegocioSearchComponent', () => {
  let component: SelectMultipleNegocioSearchComponent;
  let fixture: ComponentFixture<SelectMultipleNegocioSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectMultipleNegocioSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectMultipleNegocioSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
