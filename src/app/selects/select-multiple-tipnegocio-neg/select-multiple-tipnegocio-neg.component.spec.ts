import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectMultipleTipnegocioNegComponent } from './select-multiple-tipnegocio-neg.component';

describe('SelectMultipleTipnegocioNegComponent', () => {
  let component: SelectMultipleTipnegocioNegComponent;
  let fixture: ComponentFixture<SelectMultipleTipnegocioNegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectMultipleTipnegocioNegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectMultipleTipnegocioNegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
