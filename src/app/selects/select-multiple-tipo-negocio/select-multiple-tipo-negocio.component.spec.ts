import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectMultipleTipoNegocioComponent } from './select-multiple-tipo-negocio.component';

describe('SelectMultipleTipoNegocioComponent', () => {
  let component: SelectMultipleTipoNegocioComponent;
  let fixture: ComponentFixture<SelectMultipleTipoNegocioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectMultipleTipoNegocioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectMultipleTipoNegocioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
