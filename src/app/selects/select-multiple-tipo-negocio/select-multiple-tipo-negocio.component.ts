import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material';

@Component({
  selector: 'app-select-multiple-tipo-negocio',
  templateUrl: './select-multiple-tipo-negocio.component.html',
  styleUrls: ['./select-multiple-tipo-negocio.component.css']
})

/**
 * @Autor: Lizeth Patiño
 * @Fecha: Junio 2020
 * @Proposito: clase para obtener filtro de tipos de negocio asociados a la parametrización .
 */
export class SelectMultipleTipoNegocioComponent implements OnInit {

  tipoNegocioForm = new FormControl();

  tipoNegocioList: any[] = Array();
  selected: string[];
  @Output() emitTipoNegocio: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private _dataService: DataService,
  ) {
  }

  ngOnInit() {

    this._dataService
      .getParametrosValoresDominio("TIPNEG")
      .subscribe((data: any) => {
        if (!data) {
          return;
        }
        setTimeout(() => {
          data.forEach(element => {
            this.tipoNegocioList.push(element);
          });
        });

      },
        (err) => {
        }
      );
  }

  itemSeleccionado(event) {
    let tipoNegocio = [];
    if (event.value[0] != undefined) {
      event.value.forEach(element => {
        if (element.id) {
          tipoNegocio.push(element.id);
        }      
      });
      this.emitTipoNegocio.emit(tipoNegocio);
    }
  }

  selectAll( select: MatSelect ) {
    let tipoNegocio = [];
    select.value = this.tipoNegocioList;
    this.tipoNegocioList.forEach(element => {
      tipoNegocio.push(element.id)
    });
    this.emitTipoNegocio.emit(tipoNegocio);
  }
  
  deselectAll( select: MatSelect ) { 
    select.value = [];
    this.emitTipoNegocio.emit([]);    
  }

}
