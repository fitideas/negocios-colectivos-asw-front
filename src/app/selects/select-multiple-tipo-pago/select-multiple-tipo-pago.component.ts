import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MediosPagos } from 'src/app/modelo/medios-pagos';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-select-multiple-tipo-pago',
  templateUrl: './select-multiple-tipo-pago.component.html',
  styleUrls: ['./select-multiple-tipo-pago.component.css']
})

/**
 * @Autor: Fabian Herrera
 * @Fecha: Mayo 2020
 * @Proposito: clase para obtener filtro de tipos de pago .
 */

export class SelectMultipleTipoPagoComponent implements OnInit {

  medioPagoForm = new FormControl();
  mediosPagos = new MediosPagos();
  mediosPagosList: any[] = Array();
  selected: string[];
  @Output() emitTipoPago: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private _dataService: DataService
  ) {
  }

  ngOnInit() {

    this._dataService
      .getParametrosValoresDominio("COSMEDPAG")
      .subscribe((data: any) => {
        if (!data) {
          return;
        }
        setTimeout(() => {
          data.forEach(element => {
            this.mediosPagosList.push(element);
          });
        });

      },
        (err) => {
        }
      );
  }

  itemSeleccionado(event) {
    this.emitTipoPago.emit(event.value);
  }

}
