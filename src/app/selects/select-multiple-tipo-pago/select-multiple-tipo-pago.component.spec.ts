import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectMultipleTipoPagoComponent } from './select-multiple-tipo-pago.component';

describe('SelectMultipleTipoPagoComponent', () => {
  let component: SelectMultipleTipoPagoComponent;
  let fixture: ComponentFixture<SelectMultipleTipoPagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectMultipleTipoPagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectMultipleTipoPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
