import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectMultipleResponsablesComponent } from './select-multiple-responsables.component';

describe('SelectMultipleResponsablesComponent', () => {
  let component: SelectMultipleResponsablesComponent;
  let fixture: ComponentFixture<SelectMultipleResponsablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectMultipleResponsablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectMultipleResponsablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
