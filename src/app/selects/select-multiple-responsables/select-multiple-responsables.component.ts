import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { BeneficiarioDetalle, Documento } from 'src/app/modelo/beneficiario-detalle';
import { FormControl } from '@angular/forms';
import { Subject, ReplaySubject } from 'rxjs';
import { takeUntil} from 'rxjs/operators';



export interface Usuario {
  tipoDocumento: string;
  numeroDocumento: string;
  nombre: string;
}
@Component({
  selector: 'app-select-multiple-responsables',
  templateUrl: './select-multiple-responsables.component.html',
  styleUrls: ['./select-multiple-responsables.component.css']
})

/**
 * @Autor: Lizeth Patiño
 * @Fecha: Mayo 2020
 * @Proposito: clase para obtener filtro de responsables de un negocio .
 */
export class SelectMultipleResponsablesComponent implements OnInit {

  @Output() emitResponsableSeleccionado: EventEmitter<any> = new EventEmitter<any>();
  usuariosList: any[] = Array();
  
  usuario: Usuario;

  public documento: Documento;
  public infoBeneficiario: BeneficiarioDetalle;
  usuarioForm = new FormControl();
  selected: string[];

  public usuariosMulti: Usuario[] = Array();
  public usuarioMultiCtrl: FormControl = new FormControl();
  public usuarioMultiFilterCtrl: FormControl = new FormControl();
  public filteredUsuariosMulti: ReplaySubject<Usuario[]> = new ReplaySubject<Usuario[]>(1);

  protected _onDestroy = new Subject<void>();

  constructor(
    private _dataService: DataService
  ) {
    this.usuariosMulti = [];
  }

  ngOnInit() {
    this.loadInfoNegocio();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private loadInfoNegocio() {
    this._dataService
      .getResponsablesNegocio()
      .subscribe((data: any) => {
        if (!data) {
          return;
        }
        setTimeout(() => {
          data.forEach(element => {
            this.usuario = {
              tipoDocumento: element.tipoDocumento,
              numeroDocumento: element.numeroDocumento,
              nombre: element.nombre
            }
            this.usuariosMulti.push(this.usuario);
            this.filteredUsuariosMulti.next(this.usuariosMulti.slice());
            this.usuarioMultiFilterCtrl.valueChanges
              .pipe(takeUntil(this._onDestroy))
              .subscribe(() => {
                this.filterUsuariosMulti();
              });
          });
        });
      },
        (err) => {
        }
      );
    
  }


  itemSeleccionado(event) {
    this.emitResponsableSeleccionado.emit(event.value);
  }

  private filterUsuariosMulti() {
    if (!this.usuariosMulti) {
      return;
    }
    // get the search keyword
    let search = this.usuarioMultiFilterCtrl.value;
    if (!search) {
      this.filteredUsuariosMulti.next(this.usuariosMulti.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filtro de rentenciones
    this.filteredUsuariosMulti.next(
      this.usuariosMulti.filter(
        usuario => usuario.numeroDocumento.toLowerCase().indexOf(search) > -1
      )
    );
  }
}
