import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { BeneficiarioDetalle, Documento } from 'src/app/modelo/beneficiario-detalle';
import { FormControl } from '@angular/forms';
import { Subject, ReplaySubject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { MatSelect } from '@angular/material';

export interface Usuario {
  tipoDocumento: string;
  numeroDocumento: string;
  nombreCompleto: string;
}

@Component({
  selector: 'app-select-multiple-usuarios',
  templateUrl: './select-multiple-usuarios.component.html',
  styleUrls: ['./select-multiple-usuarios.component.css']
})

/**
 * @Autor: Fabian Herrera
 * @Fecha: Mayo 2020
 * @Proposito: clase para obtener filtro de usuarios segun auditoria.
 */
export class SelectMultipleUsuariosComponent implements OnInit {

  @Output() emitUsuarioSeleccionado: EventEmitter<any> = new EventEmitter<any>();
  usuariosList: any[] = Array();
  
  usuario: Usuario;

  public documento: Documento;
  public infoBeneficiario: BeneficiarioDetalle;
  usuarioForm = new FormControl();
  selected: string[];

  public usuariosMulti: Usuario[] = Array();
  public usuarioMultiCtrl: FormControl = new FormControl();
  public usuarioMultiFilterCtrl: FormControl = new FormControl();
  public filteredUsuariosMulti: ReplaySubject<Usuario[]> = new ReplaySubject<Usuario[]>(1);

  protected _onDestroy = new Subject<void>();

  constructor(
    private _dataService: DataService
  ) {
    this.usuariosMulti = [];
  }

  ngOnInit() {
    this.loadInfoNegocio();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private loadInfoNegocio() {
    this._dataService
      .getUsuariosAplicacion()
      .subscribe((data: any) => {
        if (!data) {
          return;
        }
        setTimeout(() => {
          data.forEach(element => {
            this.usuario = {
              tipoDocumento: element.tipoDocumento,
              numeroDocumento: element.numeroDocumento,
              nombreCompleto: element.nombreCompleto
            }
            this.usuariosMulti.push(this.usuario);
            this.filteredUsuariosMulti.next(this.usuariosMulti.slice());
            this.usuarioMultiFilterCtrl.valueChanges
              .pipe(takeUntil(this._onDestroy))
              .subscribe(() => {
                this.filterUsuariosMulti();
              });
          });
        });
      },
        (err) => {
        }
      );
    
  }


  itemSeleccionado(event) {
    this.emitUsuarioSeleccionado.emit(event.value);
  }

  private filterUsuariosMulti() {
    if (!this.usuariosMulti) {
      return;
    }
    // get the search keyword
    let search = this.usuarioMultiFilterCtrl.value;
    if (!search) {
      this.filteredUsuariosMulti.next(this.usuariosMulti.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filtro de rentenciones
    this.filteredUsuariosMulti.next(
      this.usuariosMulti.filter(
        usuario => usuario.numeroDocumento.toLowerCase().indexOf(search) > -1
      )
    );
  }

  selectAll( select: MatSelect ) {
    select.value = this.usuariosMulti;
    this.emitUsuarioSeleccionado.emit(this.usuariosMulti);
  }

  deselectAll( select: MatSelect ) { 
    select.value = [];
    this.emitUsuarioSeleccionado.emit([]);    
  }
}
