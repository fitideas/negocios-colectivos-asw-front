import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectMultipleUsuariosComponent } from './select-multiple-usuarios.component';

describe('SelectMultipleUsuariosComponent', () => {
  let component: SelectMultipleUsuariosComponent;
  let fixture: ComponentFixture<SelectMultipleUsuariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectMultipleUsuariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectMultipleUsuariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
