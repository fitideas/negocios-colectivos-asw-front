import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material';

@Component({
  selector: 'app-select-multiple-negocio',
  templateUrl: './select-multiple-negocio.component.html',
  styleUrls: ['./select-multiple-negocio.component.css']
})
export class SelectMultipleNegocioComponent implements OnInit {

  negocioForm = new FormControl();
  negocioList: any[] = Array();
  selected: string[];
  @Output() emitirNegocio: EventEmitter<any> = new EventEmitter<any>();
  negocio: { codigoNegocio: any; nombreNegocio: any;};

  
  constructor(
    private _dataService: DataService,
  ) {
  }

  ngOnInit() {
    this._dataService
      .getObtenerNegociosAplicacion()
      .subscribe((data: any) => {
        if (!data) {
          return;
        }
        setTimeout(() => {
          data.forEach(element => {
            this.negocio = {
              codigoNegocio: element.codigoNegocio,
              nombreNegocio: element.nombreNegocio
            }
            this.negocioList.push(this.negocio);
          });
        });

      },
        (err) => {
        }
      );
  }

  itemSeleccionado(event) {
    let negocios = [];
    if (event.value[0] != undefined) {
      event.value.forEach(element => {
        if (element.codigoNegocio) {
          negocios.push(element.codigoNegocio)
        }
      });
      this.emitirNegocio.emit(negocios);
    }
  }

  selectAll( select: MatSelect ) {
    let negocios = [];
    select.value = this.negocioList;
    this.negocioList.forEach(element => {
      negocios.push(element.codigoNegocio)
    });
    this.emitirNegocio.emit(negocios);
  }

  deselectAll( select: MatSelect ) { 
    select.value = [];
    this.emitirNegocio.emit([]);    
  }

}
