import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectMultipleNegocioComponent } from './select-multiple-negocio.component';

describe('SelectMultipleNegocioComponent', () => {
  let component: SelectMultipleNegocioComponent;
  let fixture: ComponentFixture<SelectMultipleNegocioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectMultipleNegocioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectMultipleNegocioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
