import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
/*
import { throttleTime } from 'rxjs/operators';
*/
@Component({
  selector: 'app-select-multiple-estado-pagos',
  templateUrl: './select-multiple-estado-pagos.component.html',
  styleUrls: ['./select-multiple-estado-pagos.component.css']
})
export class SelectMultipleEstadoPagosComponent implements OnInit {

  @Output() emitEstadoPago: EventEmitter<any> = new EventEmitter<any>();
  estadoPagoForm = new FormControl();
  estadoPagosPagosList: any[] = Array();

  constructor() { }

  ngOnInit() {
    this.estadoPagosPagosList = 
    [
      { codigo: "ACEPTADO", descripcion: "ACEPTADO" },
      { codigo: "RECHAZADO", descripcion: "RECHAZADO" },
      { codigo: "REPROCESADO", descripcion: "REPROCESADO" }
    ];
  }

  itemSeleccionado(event){
    this.emitEstadoPago.emit(event.value);
  }

}
