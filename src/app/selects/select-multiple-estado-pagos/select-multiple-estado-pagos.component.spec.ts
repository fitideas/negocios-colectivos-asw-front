import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectMultipleEstadoPagosComponent } from './select-multiple-estado-pagos.component';

describe('SelectMultipleEstadoPagosComponent', () => {
  let component: SelectMultipleEstadoPagosComponent;
  let fixture: ComponentFixture<SelectMultipleEstadoPagosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectMultipleEstadoPagosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectMultipleEstadoPagosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
