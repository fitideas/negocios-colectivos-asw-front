import { Component, EventEmitter, OnInit, ViewChild, Output } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, SatDatepickerModule } from 'saturn-datepicker'
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter'
import { SatDatepicker } from 'saturn-datepicker';
import { HttpClient } from '@angular/common/http';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-date-range-picker',
  templateUrl: './date-range-picker.component.html',
  styleUrls: ['./date-range-picker.component.css'],
  providers: [
      {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
      {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ],
})
export class DateRangePickerComponent implements OnInit {

  @ViewChild('picker', { static: false }) dateInput: SatDatepicker<any>;
  @Output() emitRange: EventEmitter<any> = new EventEmitter<any>();
  delay;
  public ref: any;

  public placeholderFecha: string;

  constructor(
    private _httpClient: HttpClient
  ) { }

  ngOnInit() {
    this.getDataJson();
  }

  
  public formatDate(fechaAtransformar) {
    const format = 'dd/MM/yyyy';
    const myDate = fechaAtransformar;
    const locale = 'en-US';
    const formattedDate = formatDate(myDate, format, locale);
    return formattedDate;
  }

  getDataJson() {
    this._httpClient.get("assets/i18n/es.json").subscribe(data =>{
      this.placeholderFecha = data['beneficiario_historial_pagos'].seleccionar_rango_fecha;
    })
  }

  emitDates(ref) {
    let daterange: any[] = [];
    const start = this.formatDate(this.dateInput.beginDate);
    const end = this.formatDate(this.dateInput.endDate);    
    daterange.push(start);
    daterange.push(end);
    this.emitRange.emit(daterange);
  }

}
