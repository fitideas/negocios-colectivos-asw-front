import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { BeneficiarioDetalle, Documento } from 'src/app/modelo/beneficiario-detalle';
import { FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material';

@Component({
  selector: 'app-select-multiple-negocio-asociado',
  templateUrl: './select-multiple-negocio-asociado.component.html',
  styleUrls: ['./select-multiple-negocio-asociado.component.css']
})
export class SelectMultipleNegocioAsociadoComponent implements OnInit {

  @Output() emitNegocioAsociado: EventEmitter<any> = new EventEmitter<any>();
  negocioAsociadoList: any[] = Array();
  negocioAsociado: any;
  public documento: Documento;
  public infoBeneficiario: BeneficiarioDetalle;
  negocioAsociadoForm = new FormControl();

  constructor(
    private _dataService: DataService,
    private _storageData: StorageDataService
  ) {
    this.negocioAsociadoList = [];
  }

  ngOnInit() {
    this.loadInfoNegocio();
  }

  private loadInfoNegocio() {
    this.infoBeneficiario = this._storageData.getInfoBeneficiario();
    this.documento = {
      documento: this.infoBeneficiario.numeroDocumento,
      tipodocumento: this.infoBeneficiario.tipoDocumento.id
    }

    this._dataService
      .getBeneficiarioObtenerNegociosListRequest(this.documento)
      .subscribe((data: any) => {
        if (!data) {
          return;
        }
        setTimeout(() => {
          data.negocios.forEach(element => {
            this.negocioAsociado = {
              codigo: element.codigo,
              descripcion: element.nombre
            }
            this.negocioAsociadoList.push(this.negocioAsociado);
          });
        });
      },
        (err) => {
        }
      );
  }

  itemSeleccionado(event) {
    let negocios = [];
    if (event.value[0] != undefined) {
      event.value.forEach(element => {
        if (element.codigo) {
          negocios.push(element.codigo)
        }      
      });
      this.emitNegocioAsociado.emit(negocios);
    }
  }

  selectAll( select: MatSelect ) {
    let negocios = [];
    select.value = this.negocioAsociadoList;
    this.negocioAsociadoList.forEach(element => {
      negocios.push(element.codigo)
    });
    this.emitNegocioAsociado.emit(negocios);
  }

  deselectAll( select: MatSelect ) { 
    select.value = [];
    this.emitNegocioAsociado.emit([]);    
  }

}
