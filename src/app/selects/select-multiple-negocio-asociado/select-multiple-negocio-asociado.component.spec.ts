import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectMultipleNegocioAsociadoComponent } from './select-multiple-negocio-asociado.component';

describe('SelectMultipleNegocioAsociadoComponent', () => {
  let component: SelectMultipleNegocioAsociadoComponent;
  let fixture: ComponentFixture<SelectMultipleNegocioAsociadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectMultipleNegocioAsociadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectMultipleNegocioAsociadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
