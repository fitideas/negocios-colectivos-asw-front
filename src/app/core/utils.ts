export class Utils {

  private buscarObjetoRetencion(array, valor) {
    return array.filter(function (e) {
      return e.descripcion == valor;
    });
  }
  public buscarRetencion(array, valor) {
    let objeto = this.buscarObjetoRetencion(array, valor);
    return (objeto.length > 0) ? objeto[0] : null;
  }

  private buscarObjetoFuncionalidad(array, valor) {
    return array.filter(function (e) {
      return e.funcionalidad == valor;
    });
  }
  public buscarFuncionalidad(array, valor) {
    let objeto = this.buscarObjetoFuncionalidad(array, valor);
    return (objeto.length > 0) ? true : false;
  }

  public buscarRepetidos(arrayElementos) {
    let repetidos = [];
    let temporal = [];

    arrayElementos.forEach((value, index) => {
      temporal = Object.assign([], arrayElementos);
      temporal.splice(index, 1);
      if (temporal.indexOf(value) != -1 && repetidos.indexOf(value) == -1) {
        repetidos.push(value);
      }
    });
    return (repetidos.length > 0) ? true : false;
  }

  public permiteEscribir(array, valor) {
    let objeto = this.buscarObjetoFuncionalidad(array, valor);
    return (objeto.length > 0) ? objeto[0].permiteEscribir : false;
  }

  public toCamelCase(str) {
    str = str.replace(/_/g, " ");
    const result = str.toUpperCase().replace(/_([a-z])/, function (m) { return m.toUpperCase(); }).replace(/_/, ' ');
    return result;
  };

  public numberCaracterEspecial(event: KeyboardEvent, caracterPermido): boolean {
    /*
    caracterPermido null permite decimales
    caracterPermido 46 punto (.)
    caracterPermido 45 guion corto (-)
    */
    const charCode = (event.which) ? event.which : event.keyCode;

    if (caracterPermido == null && charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != caracterPermido) {
      return false;
    }
    return true;
  };
  
  private buscarObjetoXId(array, id) {
    return array.filter(function (e) {
      return e.id == id;
    });
  }
  public buscarObjetoId(array, id) {
    let objeto = this.buscarObjetoXId(array, id);
    return (objeto.length > 0) ? objeto[0] : null;
  }

  public cambiarFloatante(number, decimal = 2) {
    if (typeof(number) =='string') {   
      number = number.replace("$", "");
      number = number.replace(/,/gi, "");
    }
    number = parseFloat(number).toFixed(decimal);
    if (number == 'NaN') {
      number = '0.00';
    }
    return number;
  }  

}