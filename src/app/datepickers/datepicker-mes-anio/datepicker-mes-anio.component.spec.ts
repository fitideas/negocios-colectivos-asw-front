import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatepickerMesAnioComponent } from './datepicker-mes-anio.component';

describe('DatepickerMesAnioComponent', () => {
  let component: DatepickerMesAnioComponent;
  let fixture: ComponentFixture<DatepickerMesAnioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatepickerMesAnioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatepickerMesAnioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
