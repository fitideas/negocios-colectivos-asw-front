import { Component, OnInit, NgZone, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { trigger, state, transition, animate, style } from '@angular/animations';
import { FileUploadModel } from 'src/app/modelo/list-cotitulares';
import { DialogBuscarDirectivoComponent } from 'src/app/dialogs/dialog-buscar-directivo/dialog-buscar-directivo.component';
import { MatDialog } from '@angular/material';
import { DataService } from 'src/app/services/data.service';
import { DialogParametrosSaveComponent } from '../dialog-parametros-save/dialog-parametros-save.component';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { AuthService } from 'src/app/services/auth.service';
import { Utils } from 'src/app/core/utils';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-negocios-asambleas-nueva-entrada',
  templateUrl: './negocios-asambleas-nueva-entrada.component.html',
  styleUrls: ['./negocios-asambleas-nueva-entrada.component.scss'],
  animations: [
    trigger('fadeInOut', [
      state('in', style({ opacity: 100 })),
      transition('* => void', [
        animate(300, style({ opacity: 0 }))
      ])
    ])
  ]
})
/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Marzo 2020
 * @Proposito: clase para crear nueva asamblea de negocios.
 */
export class NegociosAsambleasNuevaEntradaComponent implements OnInit {

  public placeholderFechaReunion: string;
  public placeholderHoraReunion: string;
  public placeholderLugarReunion: string;
  public placeholderQuorumAprobatorio: string;
  public placeholderResponsable: string;
  public placeholderDecisionesTomada: string;
  public placeholderTareaPendiente: string;
  public placeholderNombre: string;
  public placeholderRol: string;
  public placeholderRolFuncionario: string;
  public placeholderNumeroAsistentes: string;
  public placeholderNumeroRadicado: string;
  public placeholderTipoArchivo: string;


  public dynamicForm: FormGroup;
  public seedDataDecisionTomada: any[];
  public seedDataTareaPendiente: any[];
  public seedDataMesaDirectiva: any[];
  public seedDataExpositor: any[];
  public mesaDirectiva: any[];
  public listExpositor: any[];
  public date: moment.Moment;
  public dateControl = new FormControl(moment());
  public files: Array<FileUploadModel> = [];
  public filesNull = true;
  uploadResponse = { status: '', message: '', filePath: '' };
  public fileUpload: any;
  public nombreArchivo: string;
  public guardarBotton = false;
  private codigoSFC: string = '';
  public asamblea_agregar: boolean = false;
  public fechaLimite = new Date();
  //base64s
  archivoBase64: string;

  @Input() accept = 'application/pdf, application/vnd.ms-powerpoint';
  /** Allow you to add handler after its completion. Bubble up response text from remote. */
  @Output() complete = new EventEmitter<string>();

  @ViewChild('picker', { static: false }) picker: any;

  constructor(
    public fb: FormBuilder,
    private httpClient: HttpClient,
    private zone: NgZone,
    public dialog: MatDialog,
    public _dataService: DataService,
    private _storageData: StorageDataService,
    public _util: Utils,
    private _authService: AuthService,
    private datePipe: DatePipe
  ) {
  }

  ngOnInit() {
    this.getDataJson();
    this.date = null;
    this.dynamicForm = this.fb.group({
      decisionTomada: this.fb.array([]),
      tareaPendiente: this.fb.array([]),
      mesaDirectiva: this.fb.array([]),
      expositor: this.fb.array([]),
      codigoSFC: new FormControl(""),
      fechaReunion: new FormControl("", Validators.required),
      direccionReunion: new FormControl("", Validators.required),
      ordenDia: new FormControl("", Validators.required),
      nombreArchivo: new FormControl("", Validators.required),
      archivo: new FormControl(""),
      numeroAsistentes: new FormControl("", Validators.required),
      numeroRadicado: new FormControl("", Validators.required)
    });
    this.mesaDirectiva = new Array();
    this.listExpositor = new Array();

    // this.addToDecisionTomadaFormArray();
    // this.addToTareaPendienteFormArray();
    this.addToMesaDirectivaFormArray();
    this.addToExpositorFormArray();
    this.codigoSFC = this._storageData.getcodigoSFCNegocio();
    this.loadPermisos();

  }

  private loadPermisos() {
    this.asamblea_agregar = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'asamblea_agregar');
  }

  closePicker() {
    this.picker.cancel();
  }

  seedDecisionAprobadaFormArray(item: any) {
    this.seedDataDecisionTomada = item;
    this.seedDataDecisionTomada.forEach(seedDatum => {
      const formGroup = this.createGroup();
      formGroup.patchValue(seedDatum);
      this.decisionTomadaFormArray.push(formGroup);
    });
  }

  get decisionTomadaFormArray() {
    return <FormArray>this.dynamicForm.get("decisionTomada");
  }

  get tareaPendienteFormArray() {
    return <FormArray>this.dynamicForm.get("tareaPendiente");
  }

  seedTareaPendienteFormArray(item: any) {
    this.seedDataTareaPendiente = item;
    this.seedDataTareaPendiente.forEach(seedDatum => {
      const formGroup = this.createGroup();
      formGroup.patchValue(seedDatum);
      this.tareaPendienteFormArray.push(formGroup);
    });
  }

  get mesaDirectivaFormArray() {
    return <FormArray>this.dynamicForm.get("mesaDirectiva");
  }

  seedMesaDirectivaFormArray(item: any) {
    let seedDatum = {
      nombreCompleto:item.nombreCompleto,
      tipoDocumento: item.tipoDocumento,
      numeroDocumento: item.numeroDocumento,
      rol: item.rol
    }
    const formGroup = this.createGroupMesaDirectiva();
    formGroup.patchValue(seedDatum);
    this.mesaDirectivaFormArray.push(formGroup);
  }

  get expositorFormArray() {
    return <FormArray>this.dynamicForm.get("expositor");
  }

  seedExpositorFormArray(item: any) {
    let seedDatum = {
      nombreCompleto:item.nombreCompleto,
      tipoDocumento: item.tipoDocumento,
      numeroDocumento: item.numeroDocumento,
      rol: item.rol
    }
    const formGroup = this.createGroupExpositor();
    formGroup.patchValue(seedDatum);
    this.expositorFormArray.push(formGroup);
  }

  createGroup() {
    return this.fb.group({
      descripcion: ['', Validators.compose([Validators.required])],
      responsable: ['', Validators.compose([Validators.required])],
      quorumAprobatorio: ['', Validators.compose([Validators.required])]
    });
  }

  createGroupMesaDirectiva() {
    return this.fb.group({
      nombreCompleto:[''],
      tipoDocumento: [''],
      numeroDocumento: [''],
      rol: ['']
    });
  }

  createGroupExpositor() {
    return this.fb.group({
      nombreCompleto:[''],
      tipoDocumento: [''],
      numeroDocumento: [''],
      rol: ['']
    });
  }

  addToDecisionTomadaFormArray() {
    this.decisionTomadaFormArray.push(this.createGroup());
  }

  removeFromDecisionTomadaFormArray(index) {
    this.decisionTomadaFormArray.removeAt(index);
  }

  addToTareaPendienteFormArray() {
    this.tareaPendienteFormArray.push(this.createGroup());
  }

  removeFromTareaPendienteFormArray(index) {
    this.tareaPendienteFormArray.removeAt(index);
  }

  addToMesaDirectivaFormArray() {
    this.mesaDirectivaFormArray.push(this.createGroupMesaDirectiva());
  }

  removeFromMesaDirectivaFormArray(index) {
    this.mesaDirectivaFormArray.removeAt(index);
  }

  addToExpositorFormArray() {
    this.expositorFormArray.push(this.createGroupExpositor());
  }

  removeFromExpositorFormArray(index) {
    this.expositorFormArray.removeAt(index);
  }

  removeFromMesaDirectiva(item) {
    for (var i = 0; i < this.mesaDirectiva.length; i++) {
      if (this.mesaDirectiva[i].numeroDocumento === item.numeroDocumento && this.mesaDirectiva[i].tipoDocumento === item.tipoDocumento) {
        this.mesaDirectiva.splice(i, 1);
      }
    }
    while (this.mesaDirectivaFormArray.length !== 0) {
      this.mesaDirectivaFormArray.removeAt(0)
    }
    this.mesaDirectiva.forEach(element => {
      this.seedMesaDirectivaFormArray(element);
    });
  }

  removeFromExpositor(item) {
    this.limpiarFormArray();
    for (let i = 0; i < this.listExpositor.length; i++) {
      if (this.listExpositor[i].numeroDocumento === item.numeroDocumento && this.listExpositor[i].tipoDocumento === item.tipoDocumento) {
        this.listExpositor.splice(i, 1);
      }
    }
    while (this.expositorFormArray.length !== 0) {
      this.expositorFormArray.removeAt(0)
    }
    this.listExpositor.forEach(element => {
      this.seedExpositorFormArray(element);
    });
  }

  openDialogBuscarDirectivo(tipoIntegrante): void {
    let context: any;
    let data;
    if (tipoIntegrante == 'mesaDirectiva') {
      context = {
        titulo: "Mesa directiva",
        tipo: 'mesadirectiva'
      };
    }

    if (tipoIntegrante == 'expositor') {
      context = {
        titulo: "Expositores",
        tipo: 'expositor'
      };
    }

    data = {
      titulo: context.titulo,
      tipo: context.tipo,
      button: context.button,
      urlimg: context.urlimg
    };
    const dialogRef = this.dialog.open(DialogBuscarDirectivoComponent, {
      width: "120rem",
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (tipoIntegrante == 'mesaDirectiva') {
        result.forEach(element => {
          this.mesaDirectiva.push(element);
        });
        this.mesaDirectiva = this.borrarObjetosRepetidos(this.mesaDirectiva);
        this.mesaDirectiva.forEach(element => {
          this.seedMesaDirectivaFormArray(element);
        });

      }
      if (tipoIntegrante == 'expositor') {
        result.forEach(element => {
          this.listExpositor.push(element);
        });
        this.listExpositor = this.borrarObjetosRepetidos(this.listExpositor);
        this.listExpositor.forEach(element => {
          this.seedExpositorFormArray(element);
        });
      }
    });
  }

  private borrarObjetosRepetidos(arregloConRepetidos) {
    let sinRepetidos = arregloConRepetidos.filter((valorActual, indiceActual, arreglo) => {
      return arreglo.findIndex(
        valorDelArreglo => JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)
      ) === indiceActual
    });
    return sinRepetidos;
  }

  getDataJson() {
    this.httpClient.get("assets/i18n/es.json").subscribe(data => {
      this.placeholderFechaReunion = data['negocioAsambleasNuevaEntrada'].fecha_reunión;
      this.placeholderHoraReunion = data['negocioAsambleasNuevaEntrada'].hora_reunion;
      this.placeholderLugarReunion = data['negocioAsambleasNuevaEntrada'].lugar_reunion;
      this.placeholderDecisionesTomada = data['negocioAsambleasNuevaEntrada'].decisiones_tomadas;
      this.placeholderTareaPendiente = data['negocioAsambleasNuevaEntrada'].tareas_pendientes;
      this.placeholderResponsable = data['negocioAsambleasNuevaEntrada'].responsable;
      this.placeholderQuorumAprobatorio = data['negocioAsambleasNuevaEntrada'].quorum_aprobatorio;
      this.placeholderNombre = data['negocioAsambleasNuevaEntrada'].nombres;
      this.placeholderRol = data['negocioAsambleasNuevaEntrada'].rol_tercero;
      this.placeholderRolFuncionario = data['negocioAsambleasNuevaEntrada'].rol_funcionario;
      this.placeholderNumeroAsistentes = data['negocioAsambleasNuevaEntrada'].numero_asistentes;
      this.placeholderNumeroRadicado = data['negocioAsambleasNuevaEntrada'].numero_radicado;
      this.placeholderTipoArchivo = data['negocioAsambleasNuevaEntrada'].tipo_archivo;
    })
  }


  handleInputChange(files) {
    let file = files;
    let reader = new FileReader();
    if (!file.type.match('application/pdf') && !file.type.match('application/vnd.ms-powerpoint')) {
      alert('invalid format');
      return;
    }
    reader.onloadend = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }

  _handleReaderLoaded(e) {
    let reader = e.target;
    var base64result = reader.result.substr(reader.result.indexOf(',') + 1);
    this.archivoBase64 = base64result;
  }


  subirArchivo() {
    this.fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
    // this.picked(this.fileUpload, 1);
    this.fileUpload.onchange = () => {
      for (let index = 0; index < this.fileUpload.files.length; index++) {
        const file = this.fileUpload.files[index];
        this.handleInputChange(file); //turn into base64
        this.nombreArchivo = file.name;
        this.files[0] = {
          data: file, state: 'in',
          inProgress: false, progress: 0, canLoad: true, canRetry: false, canCancel: true
        };
        this.filesNull = false;
        this.dynamicForm.patchValue({
          fechaReunion: this.datePipe.transform(this.dateControl.value._d, 'dd/MM/yyyy HH:mm:ss'),
          nombreArchivo: this.nombreArchivo,
          codigoSFC: this.codigoSFC,
          archivo: this.archivoBase64   
        });
      }
    };
    this.fileUpload.click();
    this.guardarBotton = true;

  }

  openDialog(ok, context): void {
    let data;
    if (ok) {
      data = {
        titulo: "Datos guardados",
        contenido: "El registro de la asamblea han sido guardada de manera exitosa.",
        button: "ACEPTAR",
        urlimg: false
      };
    }
    if (!ok) {
      let contenido = "El registro de la asamblea no se pueden guardar. Intente más tarde.";
      if (context != null) {
        contenido = context;
      }
      data = {
        titulo: "Datos no guardados",
        contenido: contenido,
        button: "ACEPTAR",
        urlimg: true
      };
    }
    const dialogRef = this.dialog.open(DialogParametrosSaveComponent, {
      width: "40rem",
      data
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  removeItemFromArr = (arr, item) => {
    return arr.filter(e => e.numeroDocumento !== item);
  };

  limpiarFormArray() {
    let numeroDocumento = "";
    this.dynamicForm.value.mesaDirectiva = this.removeItemFromArr(this.dynamicForm.value.mesaDirectiva, numeroDocumento);
    this.dynamicForm.value.expositor = this.removeItemFromArr(this.dynamicForm.value.expositor, numeroDocumento);
    this.dynamicForm.value.mesaDirectiva = this.borrarObjetosRepetidos(this.dynamicForm.value.mesaDirectiva);
    this.dynamicForm.value.expositor = this.borrarObjetosRepetidos(this.dynamicForm.value.expositor);
  }

  guardarAsamblea() {
    // let numeroDocumento = "";
    /*
    this.dynamicForm.value.fechaReunion = this.dateControl.value._d;
    this.dynamicForm.value.nombreArchivo = this.nombreArchivo;
    this.dynamicForm.value.codigoSFC = this.codigoSFC;
    this.dynamicForm.value.archivo = this.archivoBase64;
    */
   this.dynamicForm.value.archivo = this.archivoBase64;
    this.limpiarFormArray();
    /*
    this.dynamicForm.value.mesaDirectiva = this.removeItemFromArr(this.dynamicForm.value.mesaDirectiva, numeroDocumento);
    this.dynamicForm.value.expositor = this.removeItemFromArr(this.dynamicForm.value.expositor, numeroDocumento);
    this.dynamicForm.value.mesaDirectiva = this.borrarObjetosRepetidos(this.dynamicForm.value.mesaDirectiva);
    this.dynamicForm.value.expositor = this.borrarObjetosRepetidos(this.dynamicForm.value.expositor);
    */
    this._dataService.negociosRegistrarAsamblea(this.dynamicForm.value)
      .subscribe(
        (data: any) => {
          this.uploadResponse = data;
          this.openDialog(true, null);
        },
        (err) => {

          this.fileUpload.inProgress = false;
          this.fileUpload.canRetry = true;
          this.fileUpload.canLoad = false;
          this.openDialog(false, null);
        }
      );

  }

}
