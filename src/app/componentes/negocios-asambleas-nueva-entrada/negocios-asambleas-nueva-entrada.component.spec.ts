import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NegociosAsambleasNuevaEntradaComponent } from './negocios-asambleas-nueva-entrada.component';

describe('NegociosAsambleasNuevaEntradaComponent', () => {
  let component: NegociosAsambleasNuevaEntradaComponent;
  let fixture: ComponentFixture<NegociosAsambleasNuevaEntradaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NegociosAsambleasNuevaEntradaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NegociosAsambleasNuevaEntradaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
