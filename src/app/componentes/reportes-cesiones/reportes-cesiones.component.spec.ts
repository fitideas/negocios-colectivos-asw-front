import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportesCesionesComponent } from './reportes-cesiones.component';

describe('ReportesCesionesComponent', () => {
  let component: ReportesCesionesComponent;
  let fixture: ComponentFixture<ReportesCesionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportesCesionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportesCesionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
