import { Component, OnInit } from '@angular/core';

import { MatDialog } from '@angular/material';

import { DialogNegocioListTitularesComponent } from 'src/app/dialogs/dialog-negocio-list-titulares/dialog-negocio-list-titulares.component';
import { DialogBuscarArchivoComponent } from 'src/app/dialogs/dialog-buscar-archivo/dialog-buscar-archivo.component';


@Component({
  selector: 'app-negocios-carga-archivo',
  templateUrl: './negocios-carga-archivo.component.html',
  styleUrls: ['./negocios-carga-archivo.component.css']
})
/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Marzo 2020
 * @Proposito: clase para cargar archivos a de negocios .
 */
export class NegociosCargaArchivoComponent implements OnInit {

  public codigoNegocio: string;

  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit() {
  }

 public openDialogSubirArchivo() {

  let data;
  data = {
    titulo: "Cargar titulares y cotitulares",
    contenido:"dialogo para subir archivo ",
    buttonBuscar: "Buscar",
    buttonCargar: "Cargar",
    buttonCancelar: "Cancelar",
    urlimg: false    
  };
  const dialogRef = this.dialog.open(DialogBuscarArchivoComponent, {
    width: "52rem",
    data
  });

  dialogRef.afterClosed().subscribe(async resultado => {
    switch (resultado) {
      case "Cargar":
        break;
      case "Cancelar":
    }
  });

 }

  openDialogVerListadoTitulares(): void {
    let data;
    
      data = {
        titulo: "Titulares y cotitulares",
        button: "ACEPTAR",
        urlimg: false,
        codigoSFC: null
      };
 
    const dialogRef = this.dialog.open(DialogNegocioListTitularesComponent, {
      width: "400rem",
      data
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
