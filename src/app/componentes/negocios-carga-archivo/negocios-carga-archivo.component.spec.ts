import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NegociosCargaArchivoComponent } from './negocios-carga-archivo.component';

describe('NegociosCargaArchivoComponent', () => {
  let component: NegociosCargaArchivoComponent;
  let fixture: ComponentFixture<NegociosCargaArchivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NegociosCargaArchivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NegociosCargaArchivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
