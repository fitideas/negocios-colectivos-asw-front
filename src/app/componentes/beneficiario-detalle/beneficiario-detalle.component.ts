import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { MatDialog } from '@angular/material';
import { DialogParametrosSaveComponent } from '../dialog-parametros-save/dialog-parametros-save.component';
import { BeneficiarioDetalle, Documento } from 'src/app/modelo/beneficiario-detalle';
import { BeneficiarioObtenerSolicitudes, BeneficiarioEnviarSolicitudes, camposModificar } from 'src/app/modelo/beneficiario-obtener-solicitudes';
import { DataService } from 'src/app/services/data.service';
import { AuthService } from 'src/app/services/auth.service';

import { DatePipe } from '@angular/common';
import { LoaderService } from 'src/app/core';
import { Utils } from 'src/app/core/utils';
import { Router } from '@angular/router';

@Component({
  selector: 'app-beneficiario-detalle',
  templateUrl: './beneficiario-detalle.component.html',
  styles: ["src/styles.css"],
  providers: [DatePipe]
})

/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Enero 2020
 * @Proposito: clase para mostrar beneficiarios.
 */

export class BeneficiarioDetalleComponent implements OnInit {

  public swRecibeClickDistribucion: boolean = false;
  public banderaRecibeClickFinanciera: boolean = false;
  public beneficiarioDetalleForm: FormGroup;
  public solicitudCambioDynamicForm: FormGroup;
  public infoBeneficiario: BeneficiarioDetalle;
  public beneficiarioObtenerSolicitudes: BeneficiarioObtenerSolicitudes[];
  public beneficiarioEnviarSolicitud: BeneficiarioEnviarSolicitudes;
  public beneficiarioEnviarSolicitudes: BeneficiarioEnviarSolicitudes[] = [];
  public listCampos: camposModificar[] = [];
  public documento: Documento;
  public beneficiario: string;
  public juridica: boolean = false;
  public fechaSolicitud: any = new Date();
  public btEnviar: boolean = true;
  public btAgregarOtro: boolean = true;
  public cantRecibida: number = 0;
  public cantPorEnviar: number = 0;

  public datos_generales_escribe: boolean = false;
  public consultar_vinculado_escribe: boolean = false;
  public datos_generales: boolean = false;
  public negocios_asociados: boolean = false;
  public datos_distribucion: boolean = false;
  public informacion_financiera: boolean = false;
  public historial_modificaciones: boolean = false;
  public tieneNegocio: boolean = false;

  color = 'primary';
  mode = 'indeterminate';
  value = 50;

  constructor(
    public fb: FormBuilder,
    public dialog: MatDialog,
    private dataService: DataService,
    private _authService: AuthService,
    private _util: Utils,
    private _router: Router,
    private _storageData: StorageDataService,
    private datePipe: DatePipe,
    public loaderService: LoaderService,

    private cdref: ChangeDetectorRef
  ) {
    this.beneficiarioDetalleForm = this.fb.group({
      fecha: new FormControl(""),
      ciudad: new FormControl(""),
      oficina: new FormControl(""),
      vinculacion: new FormControl(""),
      titular: new FormControl(""),
      tipoIdentificacion: new FormControl(""),
      numeroIdentificacion: new FormControl(""),
      telefonoContacto: new FormControl(""),
      emailContacto: new FormControl(""),
      paisResidencia: new FormControl(""),
      ciudadResidencia: new FormControl(""),
      direccionCorrespondencia: new FormControl(""),
      nombreCompleto: new FormControl(""),
    });
    this.fechaSolicitud = this.datePipe.transform(this.fechaSolicitud, 'dd/MM/yyyy');
  }

  ngOnInit() {
    this.cargarInfo();
    this.loadPermisos();
  }

  ngAfterContentChecked() {
    this.cdref.detectChanges();
  }

  private loadPermisos() {
    this.datos_generales_escribe = this._util.permiteEscribir(this._authService.getFuncionalidades(), 'datos_generales');
    this.datos_generales = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'datos_generales');
    this.negocios_asociados = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'negocios_asociados');
    this.datos_distribucion = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'datos_distribucion');
    this.informacion_financiera = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'informacion_financiera');
    this.historial_modificaciones = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'historial_modificaciones');
  }

  public cargarInfo() {

    this.solicitudCambioDynamicForm = this.fb.group({
      solicitudCambio: this.fb.array([])
    });
    this.infoBeneficiario = this._storageData.getInfoBeneficiario();
    if (!this.infoBeneficiario || this._storageData.getSwInicio() || this.infoBeneficiario == undefined) {
      this._router.navigate(['accionfiduciaria']);
    }
    
    if (this.infoBeneficiario) {
      this.tieneNegocio = this.infoBeneficiario.tieneNegocio;
    }
    
    this.loadFormGroup();
    this.loadListaCampos();
    this.getObtenerSolicitudes(this.documento);
    this.btEnviar = true;
  }

  public loadListaCampos() {
    let listCampo: camposModificar;

    listCampo = {
      campo: 'telefonoContacto',
      valor: this.infoBeneficiario.telefono,
      descripcion: 'Teléfono de contacto'
    }
    this.listCampos.push(listCampo);
    listCampo = {
      campo: 'emailContacto',
      valor: this.infoBeneficiario.email,
      descripcion: 'Correo electrónico'
    }
    this.listCampos.push(listCampo);
    listCampo = {
      campo: 'paisResidencia',
      valor: this.infoBeneficiario.paisResidencia,
      descripcion: 'País de residencia'
    }
    this.listCampos.push(listCampo);
    listCampo = {
      campo: 'ciudadResidencia',
      valor: this.infoBeneficiario.ciudadResidencia,
      descripcion: 'Ciudad de residencia'
    }
    this.listCampos.push(listCampo);
    listCampo = {
      campo: 'direccionCorrespondencia',
      valor: this.infoBeneficiario.envioCorrespondenciaTipo,
      descripcion: 'Envío de correspondencia'
    }
    this.listCampos.push(listCampo);

    if (this.juridica) {
      listCampo = {
        campo: 'nombreCompleto',
        valor: this.infoBeneficiario.nombreCompleto,
        descripcion: 'Representante legal'
      }
      this.listCampos.push(listCampo);
    }

  }

  public loadFormGroup() {
    this.documento = {
      documento: this.infoBeneficiario.numeroDocumento,
      tipodocumento: this.infoBeneficiario.tipoDocumento.id
    }
    let fechaVinculacion = null;
    let ciudadVinculacion = null;
    let oficinaVinculacion = null;
    let tipoVinculacion = null;
    let naturalezaJuridicaVinculado = null;
    let tipoDocumentoDescripcion = null;
    let telefono = null;
    let email = null;
    let paisResidencia = null;
    let ciudadResidencia = null;
    let envioCorrespondenciaTipo = null;
    let nombreCompleto = null;
    let razonSocial = null;

    if (this.infoBeneficiario.razonSocial && this.infoBeneficiario.razonSocial != null && this.infoBeneficiario.razonSocial.length > 1) {
      razonSocial = this.infoBeneficiario.razonSocial;
    }
    if (this.infoBeneficiario.fechaVinculacion && this.infoBeneficiario.fechaVinculacion != null && this.infoBeneficiario.fechaVinculacion.length > 1) {
      fechaVinculacion = this.infoBeneficiario.fechaVinculacion;
    }
    if (this.infoBeneficiario.ciudadVinculacion && this.infoBeneficiario.ciudadVinculacion != null && this.infoBeneficiario.ciudadVinculacion.length > 1) {
      ciudadVinculacion = this.infoBeneficiario.ciudadVinculacion;
    }
    if (this.infoBeneficiario.oficinaVinculacion && this.infoBeneficiario.oficinaVinculacion != null && this.infoBeneficiario.oficinaVinculacion.length > 1) {
      oficinaVinculacion = this.infoBeneficiario.oficinaVinculacion;
    }
    if (this.infoBeneficiario.tipoVinculacion && this.infoBeneficiario.tipoVinculacion != null && this.infoBeneficiario.tipoVinculacion.length > 1) {
      tipoVinculacion = this.infoBeneficiario.tipoVinculacion;
    }
    if (this.infoBeneficiario.naturalezaJuridicaVinculado && this.infoBeneficiario.naturalezaJuridicaVinculado != null && this.infoBeneficiario.naturalezaJuridicaVinculado.length > 1) {
      naturalezaJuridicaVinculado = this.infoBeneficiario.naturalezaJuridicaVinculado;
    }
    if (this.infoBeneficiario.tipoDocumento.descripcion && this.infoBeneficiario.tipoDocumento.descripcion != null && this.infoBeneficiario.tipoDocumento.descripcion.length > 1) {
      tipoDocumentoDescripcion = this.infoBeneficiario.tipoDocumento.descripcion;
    }    
    if (this.infoBeneficiario.telefono && this.infoBeneficiario.telefono != null && this.infoBeneficiario.telefono.length > 1) {
      telefono = this.infoBeneficiario.telefono;
    }    
    if (this.infoBeneficiario.email && this.infoBeneficiario.email != null && this.infoBeneficiario.email.length > 1) {
      email = this.infoBeneficiario.email;
    }    
    if (this.infoBeneficiario.paisResidencia && this.infoBeneficiario.paisResidencia != null && this.infoBeneficiario.paisResidencia.length > 1 ) {
      paisResidencia = this.infoBeneficiario.paisResidencia;
    }
    if (this.infoBeneficiario.ciudadResidencia && this.infoBeneficiario.ciudadResidencia != null && this.infoBeneficiario.ciudadResidencia.length > 1) {
      ciudadResidencia = this.infoBeneficiario.ciudadResidencia;
    }
    if (this.infoBeneficiario.envioCorrespondenciaTipo && this.infoBeneficiario.envioCorrespondenciaTipo != null && this.infoBeneficiario.envioCorrespondenciaTipo.length > 1) {
      envioCorrespondenciaTipo = this.infoBeneficiario.envioCorrespondenciaTipo;
    }
    if (this.infoBeneficiario.nombreCompleto && this.infoBeneficiario.nombreCompleto != null && this.infoBeneficiario.nombreCompleto.length > 1) {
      nombreCompleto = this.infoBeneficiario.nombreCompleto;
    } 

    this.beneficiario = razonSocial;
    this.beneficiarioDetalleForm.controls.fecha.setValue(fechaVinculacion);
    this.beneficiarioDetalleForm.controls.ciudad.setValue(ciudadVinculacion);
    this.beneficiarioDetalleForm.controls.oficina.setValue(oficinaVinculacion);
    this.beneficiarioDetalleForm.controls.vinculacion.setValue(tipoVinculacion);
    this.beneficiarioDetalleForm.controls.titular.setValue(naturalezaJuridicaVinculado);
    this.beneficiarioDetalleForm.controls.tipoIdentificacion.setValue(tipoDocumentoDescripcion);
    this.beneficiarioDetalleForm.controls.numeroIdentificacion.setValue(this.infoBeneficiario.numeroDocumento);
    this.beneficiarioDetalleForm.controls.telefonoContacto.setValue(telefono);
    this.beneficiarioDetalleForm.controls.emailContacto.setValue(email);
    this.beneficiarioDetalleForm.controls.paisResidencia.setValue(paisResidencia);
    this.beneficiarioDetalleForm.controls.ciudadResidencia.setValue(ciudadResidencia);
    this.beneficiarioDetalleForm.controls.direccionCorrespondencia.setValue(envioCorrespondenciaTipo);
    this.beneficiarioDetalleForm.controls.nombreCompleto.setValue(nombreCompleto);
    if (this.infoBeneficiario.naturalezaJuridicaVinculado.toLowerCase() == 'natural') {
      this.juridica = false;
    } else {
      this.juridica = true;
    }
  }

  getObtenerSolicitudes(documento: Documento) {
    this.dataService
      .getBeneficiarioObtenerSolicitudes(documento)
      .subscribe((data: any) => {
        if (!data) {
          return;
        }
        this.seedSolicitudCambioFormArray(data);
      },
        (err) => {

        }
      );
  }

  seedSolicitudCambioFormArray(item: any) {
    this.beneficiarioObtenerSolicitudes = item;
    this.cantRecibida = this.beneficiarioObtenerSolicitudes.length;
    this.beneficiarioObtenerSolicitudes.forEach(seedDatum => {
      const formGroup = this.createGroup();
      formGroup.patchValue(seedDatum);
      this.solicitudCambioFormArray.push(formGroup);
    });
  }

  getIdSolicitud(i) {
    let swBt = false;
    if (<FormArray>this.solicitudCambioDynamicForm.get('solicitudCambio').value[i].idSolicitud == null) {
      swBt = true;
    } else swBt = false;
    return swBt;
  }


  public createGroup() {
    return this.fb.group({
      idSolicitud: [],
      fechaSolicitud: [this.fechaSolicitud, Validators.required],
      campoAModificar: ['', Validators.required],
      datoNuevo: ['', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(100)]],
      numeroRadicado: ['', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(50)]],
      profesional: [this._authService.getNombreCompletoUsuario(), Validators.required]
    });
  }

  public addToSolicitudCambioFormArray() {
    this.btAgregarOtro = false;
    this.solicitudCambioFormArray.push(this.createGroup());
  }

  public removeFromSolicitudFormArray(index) {
    this.solicitudCambioFormArray.removeAt(index);
    this.cantPorEnviar = this.solicitudCambioFormArray.length;
    if (this.cantRecibida == this.cantPorEnviar) {
      this.btAgregarOtro = true;
    } else {
      this.btAgregarOtro = false;
    }
  }

  get solicitudCambioFormArray() {
    return <FormArray>this.solicitudCambioDynamicForm.get("solicitudCambio");
  }

  openDialog(context): void {
    let data;
    data = {
      titulo: context.titulo,
      contenido: context.contenido,
      button: context.button,
      urlimg: context.urlimg
    };
    const dialogRef = this.dialog.open(DialogParametrosSaveComponent, {
      width: "40rem",
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  public enviarInformacion() {
    let context: any;
    let listCampo: camposModificar;
    this.btEnviar = false;

    this.solicitudCambioDynamicForm.value.solicitudCambio.forEach((element, index) => {

      if (element.idSolicitud == null) {
        listCampo = this.listCampos.find(item => item.campo === element.campoAModificar);
        this.beneficiarioEnviarSolicitud = {
          campoAModificar: listCampo.descripcion,
          datoNuevo: element.datoNuevo,
          fechaSolicitud: element.fechaSolicitud,
          numeroRadicado: element.numeroRadicado,
          profesional: this._authService.getNombreUsuario(),
          nombreCompletoVinculado: this.beneficiario,
          datoAnterior: listCampo.valor,
          numeroDocumentoVinculado: this.infoBeneficiario.numeroDocumento,
          tipoDocumentoVinculado: this.infoBeneficiario.tipoDocumento.id
        }
        this.beneficiarioEnviarSolicitudes.push(this.beneficiarioEnviarSolicitud);
      }
    });

    this.dataService.putBeneficiarioEnviarSolicitud(this.beneficiarioEnviarSolicitudes).subscribe(
      (data: any) => {
        context = {
          titulo: "Solicitud enviada",
          contenido: "La solicitud ha sido entregada exitosamente a Vinculación y SARLAFT.",
          button: "ACEPTAR",
          urlimg: false
        };
        this.openDialog(context);
        this.cargarInfo();
        this.btAgregarOtro = true;
        this.beneficiarioEnviarSolicitudes = [];
      },
      (err: any) => {
        context = {
          titulo: "Solicitud no enviada",
          contenido: "La solicitud no se puede enviar",
          button: "ACEPTAR",
          urlimg: true
        };
        this.openDialog(context);
        this.btEnviar = true;
        this.beneficiarioEnviarSolicitudes = [];
      }
    );
  }

  public showDatosDistribucion() {
    return this._storageData.getActiveDistribucion();
  }

  public showDatosInfoFinanciera() {
    return this._storageData.getActiveFinanciera();
  }

  public showBeneficiarioHistorial() {
    return this._storageData.getActiveBeneficiarioHistorial();
  }

  public showBeneficiarioHistorialPagos() {
    return this._storageData.getBeneficiarioHistorialPagos();
  }

  public showBeneficiarioHistorialModificaciones() {
    return this._storageData.getBeneficiarioHistorialModificaciones();
  }


  public iniciarPestana() {
    this._router.navigate(['accionfiduciaria/vinculados/detalles']);
    this._storageData.setActiveDistribucion(false);
    this._storageData.setActiveFinanciera(false);
    this._storageData.setActiveBeneficiarioHistorial(false);
    this._storageData.setBeneficiarioHistorialPagos(false);
    this._storageData.setBeneficiarioHistoriallModificaciones(false);
  }

  public iniciarPestanaNegocioAsociado() {
    this._storageData.setActiveDistribucion(false);
    this._storageData.setActiveFinanciera(false);
  }

  public iniciarPestanaHistorial() {
    this._storageData.setActiveBeneficiarioHistorial(true);
    this._storageData.setBeneficiarioHistorialPagos(false);
    this._storageData.setBeneficiarioHistoriallModificaciones(false);
  }

  public procesaPropagar(mensaje) {
    this.banderaRecibeClickFinanciera = mensaje;
  }

}
