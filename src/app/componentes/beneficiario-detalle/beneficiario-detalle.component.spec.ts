import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiarioDetalleComponent } from './beneficiario-detalle.component';

describe('BeneficiarioDetalleComponent', () => {
  let component: BeneficiarioDetalleComponent;
  let fixture: ComponentFixture<BeneficiarioDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiarioDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiarioDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
