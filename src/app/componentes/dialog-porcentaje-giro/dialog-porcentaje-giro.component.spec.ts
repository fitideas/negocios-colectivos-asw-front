import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogPorcentajeGiroComponent } from './dialog-porcentaje-giro.component';

describe('DialogPorcentajeGiroComponent', () => {
  let component: DialogPorcentajeGiroComponent;
  let fixture: ComponentFixture<DialogPorcentajeGiroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogPorcentajeGiroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogPorcentajeGiroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
