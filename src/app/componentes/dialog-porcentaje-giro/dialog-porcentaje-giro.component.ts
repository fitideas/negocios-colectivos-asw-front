import { Component, OnInit, Inject } from "@angular/core";
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material";
import {
  FormGroup,
  FormArray,
  FormBuilder,
  Validators
} from "@angular/forms";

export interface DialogData {
  titulo: string;
  contenido: string;
  button: string;
  urlimg: boolean;
  buttonCancelar: string;
  buttonContinuar: string;
  beneficiario: datoGiro[];
}

export interface datoGiro {
  beneficiario: string;
  tipoDocumento: string;
  numeroDocumento: string;
  porcentajeGiro:string,
}

@Component({
  selector: "app-dialog-porcentaje-giro",
  templateUrl: "./dialog-porcentaje-giro.component.html",
  styleUrls: ["./dialog-porcentaje-giro.component.css"]
})


/**
* Autor:Lizeth Patiño
* Proposito: Generación de formulario para realizar distribución de porcentajes de giro y adicionar
porcentaje de distribución a cada beneficiario
* Fecha: 08/04/2020
*/
export class DialogPorcentajeGiroComponent implements OnInit {
  giroForm: FormGroup;
  public dataSource;
  seedGiro: any;
  banderaMensajeError: boolean = false;
  banderaContinuar: boolean = false;
  totalDerechos: any;
  numeroDerechosTitular: any;
  mensajeTres: boolean;
 

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogPorcentajeGiroComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.giroForm = this.fb.group({
      checkParteIgual: [ ],
      porcentajeGiro: this.fb.array([])
    });
  }

  ngOnInit() {
    this.dataSource = this.data["infoParaModalGiros"];
    this.numeroDerechosTitular = sessionStorage.getItem('numeroDerechos');
    this.totalDerechos = sessionStorage.getItem('derechos');
    if (this.banderaContinuar === true) {
      this.PagoGiroFormArray.clear();
  
      this.seedGiroFormArray(this.dataSource);
      this.validarSumatoriaGiro();
    }
    this.seedGiroFormArray(this.dataSource);
    this.validarSumatoriaGiro();
  }

  public seedGiroFormArray(item) {
    this.seedGiro = item;
    this.seedGiro.forEach(seedDatum => {
      const formGroup = this.createGroup();
      formGroup.patchValue(seedDatum);
      this.PagoGiroFormArray.push(formGroup);
    });
  }

  public get PagoGiroFormArray() {
    return <FormArray>this.giroForm.get("porcentajeGiro");
  }

  public createGroup() {
    return this.fb.group({
      beneficiario: ["", [Validators.required]],
      tipoDocumento: ["", [Validators.required]],
      numeroDocumento: ["", [Validators.required]],
      porcentajeGiro: ["", [Validators.required, Validators.min(0), Validators.max(100)]],
      porcentajeDistribucion: ["", []]
    });
  }

  public repartirPorcentajeParteIgual() {
    if (this.dataSource["porcentajeGiro"]) {
      this.mensajeTres=false;
      let repartirPartesIguales = 100 / this.dataSource["porcentajeGiro"].length;
      if (this.giroForm.controls["checkParteIgual"].value) {
        if(this.validarDecimales(repartirPartesIguales)===false){
          this.mensajeTres=true;
        this.dataSource["porcentajeGiro"].forEach(dato => {
          dato.porcentajeGiro = repartirPartesIguales;
        });
        this.PagoGiroFormArray.clear();
        this.seedGiroFormArray(this.dataSource["porcentajeGiro"]);
        this.validarSumatoriaGiro();
      }else{
        this.mensajeTres=true;
        if(this.validarSumatoriaGiro()===true){
          this.mensajeTres=false;
        }
      }
      }
     
    } else if(this.dataSource && this.dataSource.length > 0){
      let repartirPartesIguales = 100 / this.dataSource.length;
      if (this.giroForm.controls["checkParteIgual"].value) {
        if(this.validarDecimales(repartirPartesIguales)===false){
        
          this.dataSource.forEach(dato => {
            dato.porcentajeGiro =  repartirPartesIguales;
          });
          this.PagoGiroFormArray.clear();
          this.seedGiroFormArray(this.dataSource);
          this.validarSumatoriaGiro();
        }
        else{
          this.banderaMensajeError=false;
          this.mensajeTres=true;
        }
        }
      
    }
  }

  public validarSumatoriaGiro() {
    let total = 0;
    this.giroForm.value.porcentajeGiro.forEach(dato => {

      total += parseFloat(dato.porcentajeGiro);

    });
    if (total > 100 || total < 100) {
      this.mensajeTres=false;
      this.banderaMensajeError = true;
    } else {
      this.banderaMensajeError = false;
      this.mensajeTres=false;
      this.dataSource = this.giroForm.value;
    }
    return true;
  }

  public continuar() {
    
    this.banderaContinuar = true;

   
    this.PagoGiroFormArray.clear();
    this.porcentajeDistribucion(this.dataSource.porcentajeGiro);
   
    this.seedGiroFormArray(this.dataSource.porcentajeGiro);
   
    this.validarSumatoriaGiro();
    let enviado = this.dataSource.porcentajeGiro;
 
    this.dialogRef.close(enviado);
  }

  public porcentaje(num) {
    return num / 100;
  }

  public porcentajeDistribucion(item) {
    var elItem = item;
    elItem.forEach(element => {
      
      let transformacionPorcentaje =element.porcentajeGiro/100;
      let resultado1 = transformacionPorcentaje* this.numeroDerechosTitular;
      let casifinal= resultado1*100;
      let final = casifinal/this.totalDerechos;
      element.porcentajeDistribucion =final; 
    });
    return elItem;
  }

  public validarDecimales(num){
    return num % 1 != 0;
  }
}
