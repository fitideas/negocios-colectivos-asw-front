import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { DialogCargarArchivoComponent } from 'src/app/dialogs/dialog-cargar-archivo/dialog-cargar-archivo.component';
import { Utils } from 'src/app/core/utils';
import { AuthService } from 'src/app/services/auth.service';
import { CurrencyPipe } from '@angular/common';
import { SelectionModel } from '@angular/cdk/collections';
import { DialogParametrosSaveComponent } from '../dialog-parametros-save/dialog-parametros-save.component';
import { ModalSincronizacionErrorComponent } from 'src/app/dialogs/modal-sincronizacion-error/modal-sincronizacion-error.component';
import { DialogInfopagoRechazadoComponent } from 'src/app/dialogs/dialog-infopago-rechazado/dialog-infopago-rechazado.component';

export interface ListPagos {
  nombreTitular?: string;
  tipoDocumentoTitular?: string;
  numeroDocumentoTitular?: string;
  porcentajeParticipacionTitular?: string;
  numeroDerechoFiduciarios?: string;
  nombreBeneficiario?: string;
  tipoDocumentoBeneficiario?: string;
  numeroDocumentoBeneficiario?: string;
  porcentajeGiro?: string;
  porcentajeDistribuido?: string;
  valorAGirar?: string;
  tipoPago?: string;
  valorTipoPago?: string;
  subTotalGirados?: string;
  tipoCuenta?: string;
  entidadBancariaFidecomiso?: string;
  codigoEntidadBancaria?: string;
  estadoPago?: string;
  idTipoPago?: string,
  idTipoCuenta?:string,
  idEntidadBancaria?:string,
  numeroCuentaEncargo?:string
  fechaGiro?:string,
  prefijoTipoPago?:string
}

export interface Parametros {
  codigoNegocio: string;
  codigoTipoNegocio: string;
  periodo: any;
}

@Component({
  selector: 'app-negocios-historial-pagos-detalle-beneficiarios',
  templateUrl: './negocios-historial-pagos-detalle-beneficiarios.component.html',
  styleUrls: ['./negocios-historial-pagos-detalle-beneficiarios.component.scss']
})

/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Abril 2020
 * @Proposito: clase para mostrar el historial de pagos distribuidos por beneficiarios.
 */

export class NegociosHistorialPagosDetalleBeneficiariosComponent implements OnInit {


  public submitted: boolean = false;
  public dataNotFound: boolean = null;
  public dataFound: boolean = null;
  public infoNegocio: any;
  public codigoNegocio: string;
  public periodo: any[];
  public tipoNegocio: string;
  public request: Parametros;
  public listError: any = null;
  public swListError: boolean = false;
  public ver_negocios_historial_pagos: boolean = false;

  dataSource: MatTableDataSource<ListPagos>;

  displayedColumns: string[] = [
    'nombreTitular',
    'tipoDocumentoTitular',
    'numeroDocumentoTitular',
    'fechaGiro',
    'porcentajeParticipacionTitular',
    'numeroDerechoFiduciarios',
    'nombreBeneficiario',
    'tipoDocumentoBeneficiario',
    'numeroDocumentoBeneficiario',
    'porcentajeGiro',
    'porcentajeDistribuido',
    'valorAGirar',
    'tipoPago',
    'valorTipoPago',
    'subTotalGirados',
    'tipoCuenta',
    'entidadBancariaFidecomiso',
    'codigoEntidadBancaria',
    'estadoPago',
    'select'
  ];
  selection: any;
  informacionReprocesar: any;
  arrayDatosCambiados: any[];
  codigoTipoNegocioOficial: number=-1;
  codigoTipoCuenta: any;
  check: boolean;

  constructor(
    private _dataService: DataService,
    private _storageData: StorageDataService,
    private _util: Utils,
    private _authService: AuthService,
    private currencyPipe: CurrencyPipe,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.arrayDatosCambiados=[];
    this.loadPermisos();
    this.loadInfoNegocio();
    this.tipoNegocio = this._storageData.getNegocioHistorialPagosIngresoEgresoTipoNegocio();
    this.obtenerIdTipoNegocio(this.tipoNegocio);
    this.periodo = this._storageData.getNegocioHistorialPagosIngresoEgresoPeriodo();
    this.request = {
      codigoNegocio: this.codigoNegocio,
      codigoTipoNegocio: this.tipoNegocio,
      periodo: this.periodo
    };
    this.negocioListHistorialPagosBeneficiarios(this.request);
    // document.getElementById("dataFound").style.display = 'none';
    // document.getElementById("dataFoundBoton").style.display = 'none';
    // document.getElementById("dataNotFound").style.display = 'none';
  }

  private loadPermisos() {
    this.ver_negocios_historial_pagos = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'negocios_historial_pagos');
  }

  private loadInfoNegocio() {
    this.infoNegocio = this._storageData.getInfoNegocioDetalle();
    this.codigoNegocio = this.infoNegocio.codigoSFC;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  private setFormatNumberDecimal(monto, decimal) {
    let valor = parseFloat(monto).toFixed(decimal);
    if (valor == 'NaN') {
      valor = '0';
    }
    monto = this.currencyPipe.transform(valor);
    return monto;
  }

  private negocioListHistorialPagosBeneficiarios(requestBuscar) {
    this.dataSource = null;
    let listPagos: ListPagos[] = new Array();
    let pagos: ListPagos;

    this._dataService
      .negocioListHistorialPagosBeneficiarios(requestBuscar)
      .subscribe((data: any) => {
        if (!data) {
          this.dataNotFound = true;
          this.dataFound = false;
          document.getElementById("dataFound").style.display = 'none';
          document.getElementById("dataFoundBoton").style.display = 'none';
          document.getElementById("dataNotFound").style.display = 'inline';
          return;
        }

        this.dataFound = true;
        this.dataNotFound = false;
        document.getElementById("dataFound").style.display = 'inline';
        document.getElementById("dataFoundBoton").style.display = 'inline';
        document.getElementById("dataNotFound").style.display = 'none';
        listPagos = [];
        data.listaDeBeneficiarios.forEach(element => {
          element.beneficiarios.forEach(item => {
            pagos = {
              nombreTitular: element.nombreTitular,
              tipoDocumentoTitular: element.tipoDocumentoTitular,
              numeroDocumentoTitular: element.numeroDocumentoTitular,
              porcentajeParticipacionTitular: element.porcentajeParticipacionTitular,
              numeroDerechoFiduciarios: element.numeroDerechoFiduciarios,
              nombreBeneficiario: item.nombreBeneficiario,
              tipoDocumentoBeneficiario: item.tipoDocumentoBeneficiario,
              numeroDocumentoBeneficiario: item.numeroDocumentoBeneficiario,
              porcentajeGiro: item.porcentajeGiro + '%',
              porcentajeDistribuido: item.porcentajeDistribuido + '%',
              prefijoTipoPago: item.prefijoTipoPago,
              idEntidadBancaria: item.idEntidadBancaria,
              valorAGirar: this.setFormatNumberDecimal(item.valorAGirar, 2),
              tipoPago: item.idTipoPago,
              valorTipoPago: this.setFormatNumberDecimal(item.valorTipoPago, 2),
              subTotalGirados: this.setFormatNumberDecimal(item.subTotalGirados, 2),
              tipoCuenta: item.idTipoCuenta,
              entidadBancariaFidecomiso: item.entidadBancariaFidecomiso,
              codigoEntidadBancaria: item.codigoEntidadBancaria,
              estadoPago: item.estadoPago,
              numeroCuentaEncargo: item.numeroCuentaEncargo,
              fechaGiro: item.fechaPago
            }
            listPagos.push(pagos);
          });
        });
        this.dataSource = new MatTableDataSource(listPagos);
        this.selection = new SelectionModel<any>(true, []);
      },
        (err) => {
          this.dataNotFound = true;
          this.dataFound = true;
          document.getElementById("dataFound").style.display = 'none';
          document.getElementById("dataFoundBoton").style.display = 'none';
          document.getElementById("dataNotFound").style.display = 'inline';
        }
      );
  }

  public descargarArchivo() {
    this._dataService.descargarHistorialPagosBeneficiarios(this.request)
      .subscribe(
        (data: any) => {
          this.convertirArchivo(data);
          this.openDialog(true, null);

        },
        (err) => {
          this.openDialog(false, err.error);
        }
      );
  }

  convertirArchivo(data): void {
    let blob = new Blob([data], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    });
    let fileUrl = window.URL.createObjectURL(blob);
    if (window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob, fileUrl.split(':')[1]);
    } else {

      window.open(fileUrl);
    }
  }

  openDialog(ok, context): void {
    let data;
    if (ok) {
      data = {
        titulo: "Archivo generado correctamente",
        contenido: "La descarga de informacion sobre el historia de pagos en archivo han sido generada de manera exitosa.",
        button: "ACEPTAR",
        urlimg: false,
        listError: null
      };
    }
    if (!ok) {
      this.listError = null;
      let contenido = "El archivo no se puede descargar. Intentar más tarde.";
      if (context.mensaje != null) {
        contenido = context.mensaje;

        if (context.lista != null) {
          this.swListError = true;
          this.listError = context.lista;
        }
      }
      data = {
        titulo: "Error al descargar archivo",
        contenido: contenido,
        button: "ACEPTAR",
        urlimg: true,
        listError: this.listError
      };
    }
    const dialogRef = this.dialog.open(DialogCargarArchivoComponent, {
      width: "40rem",
      data
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  public seleccionCheck(row){
    this.check=true;
    this.informacionReprocesar=row;
    if(!this.selection.isSelected(row)){
      let dataMensajeGuardado = {
        titulo: "Pagos rechazados",
        buttonAceptar: "ACEPTAR",
        buttonCancelar:"CANCELAR",
        urlimg: false,
        informacionPago:row
      };
      const dialogRef=this.dialog.open(DialogInfopagoRechazadoComponent, {
        width: "106rem",
        data: dataMensajeGuardado
      });

      dialogRef.afterClosed().subscribe(result => {
       let nuevo=this._storageData.getInfoNuevaReproceso();
       let datoAnterior={
        "tipoPago": this.informacionReprocesar.tipoPago,
        "tipoCuenta": this.informacionReprocesar.tipoCuenta,
        "codigoBancario":this.informacionReprocesar.idEntidadBancaria,
        "numeroCuenta":this.informacionReprocesar.numeroCuentaEncargo,
        "fechaGiro": this.informacionReprocesar.fechaGiro
       }
       let datoNuevo={
        "tipoPago": nuevo.tipoPago,
        "tipoCuenta": nuevo.tipoCuenta,
        "codigoBancario":nuevo.codigoEntidadBancaria.toString(),
        "numeroCuenta":nuevo.numeroCuentaEncargo,
        "fechaGiro": nuevo.fechaPago
       }
       let informacionGeneral ={
        "tipoDocumentoTitular":result.tipoDocumentoTitular,
        "numeroDocumentoTitular":result.numeroDocumentoTitular,
        "tipoDocumentoBeneficiario":result.tipoDocumentoBeneficiario,
        "numeroDocumentoBeneficiario": result.numeroDocumentoBeneficiario,
        "datoAnterior":datoAnterior,
        "datoNuevo":datoNuevo
       }

       this.arrayDatosCambiados.push(informacionGeneral);
    });
  }
}

  public checkboxLabel(row){
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  public reprocesarPagos(){
    let objetoAEnviar={
        "codigoNegocio": this.request.codigoNegocio,
        "codigoTipoNegocio": this.codigoTipoNegocioOficial,
        "periodo":  this.request.periodo,
        "listaReprocesoBeneficiarios":this.arrayDatosCambiados
    }
    this._dataService.negociosReprocesarPagos(objetoAEnviar)
      .subscribe(
        (dataFinal: any) => {

          let dataMensajeGuardado = {
            titulo: "Cambios guardados",
            contenido:
              "Los cambios de reprocesar pagos han sido guardados exitosamente.",
            button: "ACEPTAR",
            urlimg: false
          };
          this.dialog.open(DialogParametrosSaveComponent, {
            width: "40rem",
            data: dataMensajeGuardado
          });
          this.request = {
            codigoNegocio: this.codigoNegocio,
            codigoTipoNegocio: this.tipoNegocio,
            periodo: this.periodo
          };
          this.negocioListHistorialPagosBeneficiarios(this.request);
          setTimeout (() => {
            var nameOfFileToDownload = dataFinal["nombreArchivoZip"];
            const byteCharacters = atob(dataFinal["recursoB64"]);
            const byteNumbers = new Array(byteCharacters.length);
            for (let i = 0; i < byteCharacters.length; i++) {
              byteNumbers[i] = byteCharacters.charCodeAt(i);
            }
            const byteArray = new Uint8Array(byteNumbers);
            const blob = new Blob([byteArray], {
              type: dataFinal.tipoArchivo
            });
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(blob, nameOfFileToDownload);
            } else {
              var a = document.createElement("a");
              a.href = URL.createObjectURL(blob);
              a.download = nameOfFileToDownload;
              document.body.appendChild(a);
              a.click();
              document.body.removeChild(a);
            }
         }); 
        },
        (err: any) => {
          let dataMensajeGuardado = {
            titulo: "Error",
            contenido: err.error,
            button: "ACEPTAR",
            urlimg: true
          };
          this.dialog.open(ModalSincronizacionErrorComponent, {
            width: "40rem",
            data: dataMensajeGuardado
          });
        }
      );
  }  

  private obtenerIdTipoNegocio(codigoNegocio) {
    this._dataService
      .getServiceParametrosValoresTipoCompuesto("TIPNEG")
      .subscribe(
        (tiposNegocio: any) => {
          tiposNegocio.forEach(element => {
            if (element['descripcion'] == codigoNegocio) {
              this.codigoTipoNegocioOficial= element.id;
            }
          });
        },
        (err: any) => {
          let dataMensajeGuardado = {
            titulo: "Cambios NO guardados",
            contenido: "Los cambios no han podido ser guardados intente más tarde.",
            button: "ACEPTAR",
            urlimg: true
          };
          this.dialog.open(DialogParametrosSaveComponent, {
            width: "40rem",
            data: dataMensajeGuardado
          });
        }
      );
  }

}

