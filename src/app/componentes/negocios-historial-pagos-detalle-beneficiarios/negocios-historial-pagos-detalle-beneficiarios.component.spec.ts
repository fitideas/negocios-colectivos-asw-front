import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NegociosHistorialPagosDetalleBeneficiariosComponent } from './negocios-historial-pagos-detalle-beneficiarios.component';

describe('NegociosHistorialPagosDetalleBeneficiariosComponent', () => {
  let component: NegociosHistorialPagosDetalleBeneficiariosComponent;
  let fixture: ComponentFixture<NegociosHistorialPagosDetalleBeneficiariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NegociosHistorialPagosDetalleBeneficiariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NegociosHistorialPagosDetalleBeneficiariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
