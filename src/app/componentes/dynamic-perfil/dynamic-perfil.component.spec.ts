import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicPerfilComponent } from './dynamic-perfil.component';

describe('DynamicPerfilComponent', () => {
  let component: DynamicPerfilComponent;
  let fixture: ComponentFixture<DynamicPerfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicPerfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicPerfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
