import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { FormBuilder, FormArray, FormGroup, Validators, FormControl } from "@angular/forms";
import { RolFuncionalidad } from "src/app/modelo/perfil";
import { DataService } from "src/app/services/data.service";
import { StorageDataService } from "src/app/services/storage-data.service";
import { Utils } from 'src/app/core/utils';

interface Funcionalidad {
  idFuncionalidad: string;
  funcionalidad: string;
  permiteEscribir: boolean;
}

interface FuncionalidadGroup {
  idOpcion: number;
  disabled?: boolean;
  opcion: string;
  funcionalidades: Funcionalidad[];
}

@Component({
  selector: "app-dynamic-perfil",
  templateUrl: "./dynamic-perfil.component.html",
  styleUrls: ["./dynamic-perfil.component.css"]
})
/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Marzo 2020
 * @Proposito: clase para mmostrar componentes individuales del perfil .
 */
export class DynamicPerfilComponent implements OnInit {
  @Input() index: number;
  @Input() itemRol: any;
  @Input() editing: boolean;
  @Output() propagar = new EventEmitter<any>();

  dynamicForm: FormGroup;
  menuControl = new FormControl();
  editable: boolean = false; 
  public permisoList = [
    { name: "Selecciona", value: null },
    { name: "VISUALIZA", value: false },
    { name: "ACTUALIZA", value: true }
  ];

  public seedDataPerfil = new Array();

  menuGroup: FuncionalidadGroup[] = [];
  menuOpcion: FuncionalidadGroup;

  funcionalidad: Funcionalidad;
  funcionalidadGroup: Funcionalidad[] = [];

  constructor(
    private fb: FormBuilder,
    private _dataService: DataService,
    private _storageService: StorageDataService,
    public _util: Utils
  ) {}

  ngOnInit() {
    this.loadPermisos();
    
    this.dynamicForm = this.fb.group({
      listaFuncionalidades: this.fb.group({
        funcionalidades: this.fb.array([])
      })
    });
    this.seedDataPerfil[0] = this.itemRol;
    this.seedPerfilFormArray();
  }

  loadPermisos() {
    this._dataService.getPermisosRequest().subscribe(
      (data: any) => {
        if (!data) {
          return;
        }
        data.forEach(opcion => {
          this.funcionalidadGroup = [];
          opcion.funcionalidades.forEach(funcion => {
            this.funcionalidad = {
              funcionalidad: funcion.funcionalidad,
              idFuncionalidad: funcion.idFuncionalidad,
              permiteEscribir: funcion.permiteEscribir
            }           
            this.funcionalidadGroup.push(this.funcionalidad);                       
          });
          this.funcionalidadGroup = this.changeCamelCase(this.funcionalidadGroup); 
          opcion.opcion = this._util.toCamelCase(opcion.opcion);
          this.menuOpcion = {            
            idOpcion: opcion.idOpcion,
            disabled: false,
            opcion: opcion.opcion,
            funcionalidades: this.funcionalidadGroup
          }                    
          this.menuGroup.push(this.menuOpcion);  
          this.menuOpcion = null;
        });
      },
      err => {
      }
    );
  }

  private changeCamelCase(funcionalidadArray) {
    funcionalidadArray.forEach(item => {
      item.funcionalidad = this._util.toCamelCase(item.funcionalidad);
    });
    return funcionalidadArray;
  }

  public seedPerfilFormArray() { 
    this.seedDataPerfil.forEach(item => {
      
      item.listaFuncionalidades.funcionalidades.forEach(itemFuncionalidad => {
        let funcionalidad = this._util.toCamelCase(itemFuncionalidad.funcionalidad);
        this.perfilFormArray.push(
          this.patchValues(
            item.idRol,
            item.rol,
            itemFuncionalidad.idFuncionalidad,
            funcionalidad,
            itemFuncionalidad.permiteEscribir
          )
        );
      });
      const formGroup = this.createPerfilGroup(item.idRol, item.rol);
      formGroup.patchValue(item);
    });
  }

  patchValues(idRol, rol, idFuncionalidad, funcionalidad, permiteEscribir) { 
    return this.fb.group({
      idRol: [idRol],
      rol: [rol],
      idFuncionalidad: [idFuncionalidad],
      funcionalidad: [funcionalidad],
      permiteEscribir: [permiteEscribir]
    });
  }

  public createPerfilGroup(idRol, rol) {
    return this.fb.group({
      idRol: [idRol],
      rol: [rol],
      funcionalidad: ['', Validators.required],
      idFuncionalidad: ['', Validators.required],
      permiteEscribir: ['', Validators.required]
    });
  }

  addToPerfilFormArray(idRol, rol) {
    this.perfilFormArray.push(this.createPerfilGroup(idRol, rol));
  }

  removeFromPerfilFormArray(index, idRol) {
    let rolFuncionalidad: RolFuncionalidad;
    this.perfilFormArray.removeAt(index);
    this._storageService.eliminarRol(idRol);
    if (this.dynamicForm.value.listaFuncionalidades.funcionalidades){
      this.dynamicForm.value.listaFuncionalidades.funcionalidades.forEach(
        item => {
          rolFuncionalidad = {
            idRol: item.idRol,
            idFuncionalidad: item.idFuncionalidad,
            permiteEscribir: item.permiteEscribir
          };          
          this._storageService.setPerfilesRolPermisos(rolFuncionalidad);
          rolFuncionalidad = {};        
        }
      );
    } else {
      rolFuncionalidad = {
        idRol: idRol,
        idFuncionalidad: null,
        permiteEscribir: null
      };
      this._storageService.setPerfilesRolPermisos(rolFuncionalidad);
      rolFuncionalidad = {}; 
    }
  }

  get perfilFormArray() {
    return <FormArray>(
      this.dynamicForm.get("listaFuncionalidades.funcionalidades")
    );
  }
  public almacenarMemoria() {    
    let rolFuncionalidad: RolFuncionalidad;
    this.dynamicForm.value.listaFuncionalidades.funcionalidades.forEach(
      item => {
        rolFuncionalidad = {
          idRol: item.idRol,
          idFuncionalidad: item.idFuncionalidad,
          permiteEscribir: item.permiteEscribir
        };
        this._storageService.setPerfilesRolPermisos(rolFuncionalidad);
      }
    );
  }
}
