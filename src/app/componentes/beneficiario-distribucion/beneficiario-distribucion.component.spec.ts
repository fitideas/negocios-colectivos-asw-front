import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiarioDistribucionComponent } from './beneficiario-distribucion.component';

describe('BeneficiarioDistribucionComponent', () => {
  let component: BeneficiarioDistribucionComponent;
  let fixture: ComponentFixture<BeneficiarioDistribucionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiarioDistribucionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiarioDistribucionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
