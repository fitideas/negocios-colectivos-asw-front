import { Component, OnInit, ComponentFactoryResolver, ViewChild, ViewContainerRef } from '@angular/core';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { DynamicBeneficiarioDistribucionComponent } from '../dynamic-beneficiario-distribucion/dynamic-beneficiario-distribucion.component';
import { BeneficiarioDistribucionGuardar } from 'src/app/modelo/beneficiario-distribucion';
import { DialogParametrosSaveComponent } from '../dialog-parametros-save/dialog-parametros-save.component';
import { MatDialog } from '@angular/material';
import { AuthService } from 'src/app/services/auth.service';
import { Utils } from 'src/app/core/utils';
import { DialogAddCotitularComponent } from 'src/app/dialogs/dialog-add-cotitular/dialog-add-cotitular.component';
import { HttpClient } from '@angular/common/http';
import { DynamicBeneDistRetencionesTipoNegocioComponent } from '../dynamic-bene-dist-retenciones-tipo-negocio/dynamic-bene-dist-retenciones-tipo-negocio.component';

@Component({
  selector: 'app-beneficiario-distribucion',
  templateUrl: './beneficiario-distribucion.component.html',
  styleUrls: ['./beneficiario-distribucion.component.css'],
  styles: ["src/styles.css"],
})


/**
 * @Autor: Jose Reinaldo Garcia
 * @Fecha: Enero 2020
 * @Proposito: clase información de distribución de un vinculado
 */
export class BeneficiarioDistribucionComponent implements OnInit {

  @ViewChild('container', { read: ViewContainerRef, static: false }) container: ViewContainerRef;
  @ViewChild('vistaRetencionTipoNegocio', { read: ViewContainerRef, static: false }) vistaRetencionTipoNegocio: ViewContainerRef;
  private _counter = 1;
  public beneficiario_distribucion_permisos_escribe: boolean = false;
  public componentRef: any;
  public componentRefRetTipoNegocio: any;

  public codigoNegocio: string;
  public nombreNegocio: string;
  public nombreTitular: string;
  public datosDistribucion: any;
  public infoBeneficiario: any;
  public distribucionForm: FormGroup;
  public listTipNatJur: any[] = [];
  public beneficiarioDistribucionGuardar: BeneficiarioDistribucionGuardar;
  public datos_distribucion: boolean = false;
  public datos_distribucion_escribe: boolean = false;
  public placeholderAsesor: string;
  public placeholderRadicadoVinculacion: string;
  public guardarBotton: boolean = false;
  public listRetencionesTiposDeNaturalezaJudica: any[] = [];
  public idTipoNaturaleza: any = null;
  public retencionesTipoNaturaleza: any[] = [];

  constructor(
    public fb: FormBuilder,
    private _storageData: StorageDataService,
    private _dataService: DataService,
    private _authService: AuthService,
    private _util: Utils,
    private componentFactoryResolver: ComponentFactoryResolver,
    public dialog: MatDialog,
    private httpClient: HttpClient
  ) {
    this.distribucionForm = this.fb.group({
      tipoDocumento: new FormControl("", [Validators.required]),
      documento: new FormControl("", [Validators.required]),
      codigoNegocio: new FormControl(""),
      nombreNegocio: new FormControl(""),
      asesor: new FormControl("", [Validators.required]),
      radicadoVinculacion: new FormControl("", [Validators.required]),
      derechos: new FormControl(""),
      porcentajeParticipacion: new FormControl(""),
      tipoTitularidad: new FormControl(""),
      idTipoNaturaleza: new FormControl("", [Validators.required])
    });
  }

  ngOnInit() {
    this.loadInfoDatosDistribucion();
    this.loadTipoNatulezaJuridica();
    this.loadPermisos();
    this.getDataJson();
  }

  getDataJson() {
    this.httpClient.get("assets/i18n/es.json").subscribe(data => {
      this.placeholderAsesor = data['beneficiario_distribucion'].asesor_vinculacion;
      this.placeholderRadicadoVinculacion = data['beneficiario_distribucion'].numero_radicado_vinculacion;
    })
  }

  private loadPermisos() {
    this.datos_distribucion = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'datos_distribucion');
    this.beneficiario_distribucion_permisos_escribe = this._util.permiteEscribir(this._authService.getFuncionalidades(), 'datos_distribucion');
  }

  public bt_changeInput() {
    this.guardarBotton = true;
  }

  public bt_changeButtonSave(tipoNaturaleza) {
    this.idTipoNaturaleza = tipoNaturaleza.value;
    this.guardarBotton = true;
    this.retencionesTipoNaturaleza = [];
    this.removeRetencionesTipoNaturaleza(0);
    this.removeRetencionesTipoNegocio(0);
    this.addRetencionesTipoNaturaleza();
    this.addRetencionesTipoNegocio();
  }

  async getParametrosObtenerRetencionesAsociadas() {
    return this._dataService.getParametrosObtenerRetencionesAsociadas(this.idTipoNaturaleza).toPromise();
  }

  public loadInfoDatosDistribucion() {
    let infoVinculadoNegocio = this._storageData.getInfoDatosDistribucion();
    this.infoBeneficiario = this._storageData.getInfoBeneficiario();
    this.nombreTitular = this.infoBeneficiario.razonSocial;
    this._dataService
      .getBeneficiarioObtenerDatosDistribucionRequest(infoVinculadoNegocio)
      .subscribe((data: any) => {
        this.datosDistribucion = data;
        setTimeout(() => {
          this.codigoNegocio = this.datosDistribucion['codigoNegocio'];
          this.nombreNegocio = this.datosDistribucion['nombreNegocio'];

          this.loadFormGroup();
        });
      },
        (err) => {

        }
      );
  }

  public loadTipoNatulezaJuridica() {
    this.listTipNatJur = [];
    this._dataService
      .getParametrosValoresDominio('TIPNATJUR')
      .subscribe((data: any) => {
        if (!data) {
          return;
        }
        data.forEach(element => {
          this.listTipNatJur.push(element);
        });
        this.addRetencionesTipoNaturaleza();
        this.addRetencionesTipoNegocio();
      },
        (err) => {

        }
      );
  }

  public loadFormGroup() {
    if (this.datosDistribucion['derechos'] == null || !this.datosDistribucion['derechos'] || this.datosDistribucion['derechos'] == 0) {
      this.datosDistribucion['derechos'] = 'NO APLICA'
    }
    this.distribucionForm.controls.tipoDocumento.setValue(this.datosDistribucion['tipoDocumento']);
    this.distribucionForm.controls.documento.setValue(this.datosDistribucion['documento']);
    this.distribucionForm.controls.asesor.setValue(this.datosDistribucion['asesor']);
    this.distribucionForm.controls.radicadoVinculacion.setValue(this.datosDistribucion['radicadoVinculacion']);
    this.distribucionForm.controls.derechos.setValue(this.datosDistribucion['derechos']);
    this.distribucionForm.controls.porcentajeParticipacion.setValue(this.datosDistribucion['porcentajeParticipacion']);
    this.distribucionForm.controls.tipoTitularidad.setValue(this.datosDistribucion['tipoTitularidad']);
    this.distribucionForm.controls.idTipoNaturaleza.setValue(this.datosDistribucion['idTipoNaturaleza']);
  }

  addRetencionesTipoNaturaleza() {
    this.retencionesTipoNaturaleza = [];
    if (this.idTipoNaturaleza == null) {
      this.retencionesTipoNaturaleza = this.datosDistribucion['retencionesTipoNaturaleza'];
      this.crearComponenteNaturalezaRetenciones(this.datosDistribucion['retencionesTipoNaturaleza']);
    }
    if (this.idTipoNaturaleza != null) {
      let respuest = this.getParametrosObtenerRetencionesAsociadas();
      respuest.then((res) => {
        this.retencionesTipoNaturaleza = res;
        this.crearComponenteNaturalezaRetenciones(res);
      });
    }

  }

  crearComponenteNaturalezaRetenciones(retencionesTipoNaturaleza) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(DynamicBeneficiarioDistribucionComponent);
    // add the component to the view
    this.componentRef = this.container.createComponent(componentFactory);
    // pass some data to the component
    this.componentRef.instance.index = this._counter++;
    this.componentRef.instance.listRetenciones = retencionesTipoNaturaleza;
    this.componentRef.instance.idTipoNaturaleza = this.idTipoNaturaleza;
    this.componentRef.instance.editing = this.beneficiario_distribucion_permisos_escribe;
    this.componentRef.instance.messageEvent.subscribe(data => {
      if (data == 'true') {
        this.guardarBotton = true;
      }
    });
  }

  removeRetencionesTipoNaturaleza(key: number) {
    if (this.container.length < 1) return;
    // removing component from container
    this.container.remove(key);
  }

  addRetencionesTipoNegocio(): void {
    // create the component factory
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(DynamicBeneDistRetencionesTipoNegocioComponent);
    // add the component to the view
    this.componentRefRetTipoNegocio = this.vistaRetencionTipoNegocio.createComponent(componentFactory);
    // pass some data to the component
    this.componentRefRetTipoNegocio.instance.index = this._counter++;
    this.componentRefRetTipoNegocio.instance.listRetenciones = this.datosDistribucion['retencionesTipoNegocio'];
    this.componentRefRetTipoNegocio.instance.editing = this.beneficiario_distribucion_permisos_escribe;
    this.componentRefRetTipoNegocio.instance.messageEvent.subscribe(data => {
      if (data == 'true') {
        this.guardarBotton = true;
      }
    });
  }

  removeRetencionesTipoNegocio(key: number) {
    if (this.vistaRetencionTipoNegocio.length < 1) return;
    this.vistaRetencionTipoNegocio.remove(key);
  }

  clear() {
    this.container.clear();
    this.vistaRetencionTipoNegocio.clear();
  }

  destroyComponent() {
    this.componentRef.destroy();
    this.componentRefRetTipoNegocio.destroy();
  }

  openDialog(ok, context): void {
    let data;
    if (ok) {
      data = {
        titulo: "Datos guardados",
        contenido: "Los datos de distribucion asociados al beneficiario han sido guardados de manera exitosa.",
        button: "ACEPTAR",
        urlimg: false
      };
    }
    if (!ok) {
      let contenido = "Los datos de distribucion asociados al beneficiario no se pueden guardar. Intente más tarde.";
      if (context != null) {
        contenido = context;
      }
      data = {
        titulo: "Datos no guardados",
        contenido: contenido,
        button: "ACEPTAR",
        urlimg: true
      };
    }
    const dialogRef = this.dialog.open(DialogParametrosSaveComponent, {
      width: "40rem",
      data
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  private borrarObjetosRepetidos(arregloConRepetidos) {
    let sinRepetidos = arregloConRepetidos.filter((valorActual, indiceActual, arreglo) => {
      return arreglo.findIndex(
        valorDelArreglo => JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)
      ) === indiceActual
    });
    return sinRepetidos;
  }

  private organizarRetencionesTipoNegocio(retencionesEliminadas) {
    let idTipoNegocioRetEliminar = new Array();
    let tipoNegocioRetencion: any;
    let retencionesTipoNegocioEliminar: any = new Array();
    let retencionesArray = new Array();

    retencionesEliminadas.forEach(element => {
      idTipoNegocioRetEliminar.push(element.idTipoNegocio);
    });

    idTipoNegocioRetEliminar = this.borrarObjetosRepetidos(idTipoNegocioRetEliminar);
    idTipoNegocioRetEliminar.forEach(itemIdTipoNegocio => {
      retencionesEliminadas.forEach(item => {
        if (itemIdTipoNegocio == item.idTipoNegocio) {
          retencionesArray.push(item.idRet);
        }
      });
      tipoNegocioRetencion = {
        idTipoNegocio: itemIdTipoNegocio,
        retenciones: retencionesArray
      }
      retencionesTipoNegocioEliminar.push(tipoNegocioRetencion);
      retencionesArray = [];
      tipoNegocioRetencion = null;
    });
    idTipoNegocioRetEliminar = null;
    return retencionesTipoNegocioEliminar;
  }

  removeItemFromArr(arr, item) {
    return arr.filter(function (e) {
      return e !== item;
    });
  };

  public validarRetencionTipoNaturaleza() {
    let nuevasRetenciones: any = new Array();
    let retencionesTipoNaturalezaNuevas = this._storageData.getBeneficiarioDistribucionRentencionesNuevas();
    let retencionesTipoNegocioPorEliminar = this._storageData.getBeneficiariosDistribucionRetencionesEliminar();

    this.retencionesTipoNaturaleza.forEach(element => {
      nuevasRetenciones.push(element.id);
    });
    retencionesTipoNaturalezaNuevas.forEach(element => {
      nuevasRetenciones.push(element);
    });
    retencionesTipoNegocioPorEliminar.forEach(item => {
      nuevasRetenciones = this.removeItemFromArr(nuevasRetenciones, item);
    });
    return nuevasRetenciones;
  }

  public guardarCambios() {
    let retencionesTipoNegocioPorEliminarArray: any = new Array();
    let retencionesTipoNegocioPorEliminar = this._storageData.getBeneficiariosDistribucionRetencionesTipoNegocioEliminar();
    if (retencionesTipoNegocioPorEliminar.length > 0) {
      retencionesTipoNegocioPorEliminarArray = this.organizarRetencionesTipoNegocio(retencionesTipoNegocioPorEliminar);
    }
    this.beneficiarioDistribucionGuardar = {
      codigoNegocio: this.codigoNegocio,
      tipoDocumento: this.distribucionForm.value.tipoDocumento,
      documento: this.distribucionForm.value.documento,
      nombre: this.nombreTitular,
      idTipoNaturaleza: this.distribucionForm.value.idTipoNaturaleza,
      asesor: this.distribucionForm.value.asesor,
      radicadoVinculacion: this.distribucionForm.value.radicadoVinculacion,
      derechos: this.distribucionForm.value.derechos,
      porcentajeParticipacion: this.distribucionForm.value.porcentajeParticipacion,
      tipoTitularidad: this.distribucionForm.value.tipoTitularidad,
      retencionesTipoNaturaleza: this.validarRetencionTipoNaturaleza(),
      retencionesTipoNegocioEliminadas: retencionesTipoNegocioPorEliminarArray
    }
    this._dataService.putBeneficiarioGuardarDatosDistribucion(this.beneficiarioDistribucionGuardar).subscribe(
      (data: any) => {
        this.openDialog(true, null);
        this._storageData.resetBeneficiariosDistribucionRetencionesTipoNegocioEliminar();
        this._storageData.resetBeneficiarioDistribucionRentencionesNuevas();
        this._storageData.resetBeneficiariosDistribucionRetencionesEliminar();
        retencionesTipoNegocioPorEliminarArray = [];
        retencionesTipoNegocioPorEliminar = null;
        this.guardarBotton = false;
      },
      (err) => {
        this.openDialog(false, err.error);
      }
    );
  }

  public administrarCotitular() {
    let data;
    data = {
      titulo: "Asociar cotitular",
      contenido: "¿Qué acción desea realizar?",
      buttonCancelar: "CANCELAR",
      buttonAceptar: "GUARDAR",
      urlimg: false,
      param: {
        documentoTitular: this.distribucionForm.value.documento,
        tipoDocumentoTitular: this.distribucionForm.value.tipoDocumento,
        codigoNegocio: this.codigoNegocio,
        criterioBuscar: null,
        argumento: null
      }
    };
    const dialogRef = this.dialog.open(DialogAddCotitularComponent, {
      width: "250rem",
      data
    });

    dialogRef.afterClosed().subscribe(async result => {
    });
  }

}
