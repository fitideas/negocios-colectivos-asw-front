import { Component, OnInit, ViewChild, Input } from "@angular/core";
import { DataService } from "src/app/services/data.service";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ResponseNegocios } from 'src/app/modelo/response-negocios';
import { ListNegociosPorDerechos } from 'src/app/modelo/list-negocios-por-derechos';
import { environment } from 'src/environments/environment';

export interface Criterio {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-cert-negocios-listar',
  templateUrl: './cert-negocios-listar.component.html',
  styleUrls: ['./cert-negocios-listar.component.scss']
})

/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: junio 2020
 * @Proposito: clase para mostrar las los vinculados para generar certificados
 */

export class CertNegociosListarComponent implements OnInit {

  selectCriterio: Criterio[] = [
    { value: "codigo", viewValue: "Código del negocio" },
    { value: "nombre", viewValue: "Nombre del negocio" }
  ];
  tiposCertificado: any[] = [
    {
      value: "PART",
      viewValue: "Certificado de participación"
    },
    {
      value: "INGRET",
      viewValue: "Certificado de ingresos y retenciones"
    },
    {
      value: "RENTAEXC",
      viewValue: "Certificado de renta exenta"
    }
  ];

  public dataNotFound: boolean = false;
  public dataFound: boolean = false;
  public listNegocio: ListNegociosPorDerechos[] = new Array();
  public buscarNegociosForm: FormGroup;
  public resultsLength = 0;
  public negocios: ResponseNegocios;
  public codSFC: string;

  public verMas = false;
  public negocioSeleccionado: string = "";
  public certificadoSeleccionado: string = "";
  public periodoSeleccionado: string = "";

  public seleccionadosReporte: any[] = [];

  displayedColumns: string[] = [
    "nombre",
    "codigoSFC",
    "codigoInterno",
    "id"
  ];

  dataSource: MatTableDataSource<ListNegociosPorDerechos>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  public page: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPages: number = 1; //Número total de páginas 
  public numElementos: number = 5; //Total de elemento por pagina
  public request: any;

  public listAnoSelect: any[] = [];
  public iniciarPestanaNegocio: number = 0;

  private ELEMENTOS_POR_PAGINA: number = environment.elementosPorPagina;

  constructor(
    private _dataService: DataService,
    private _storageData: StorageDataService,
    public fb: FormBuilder
  ) {
    this.buscarNegociosForm = this.fb.group(
      {
        selectCriterio: new FormControl("", [Validators.required]),
        argumento: new FormControl("", [Validators.required]),
        tiposCertificado: new FormControl("", Validators.required),
        anoGrabable: new FormControl("", Validators.required)
      }
    );
  }

  ngOnInit() {
    this.iniciarPestanaNegocio = this._storageData.getInicializarCertificadosNegocios();
    this.comboAno();
    this.dataNotFound = false;
    this.dataFound = false;
  }

  get f() {
    return this.buscarNegociosForm.controls;
  }

  private comboAno() {
    let d = new Date();
    let n = d.getFullYear();
    let year: any;
    for (let i = n; i >= 1992; i--) {
      year = {
        value: i,
        viewValue: i
      }
      this.listAnoSelect.push(year);
    }
  }

  public limpiarformulario() {
    this.dataSource = null;
    this.dataNotFound = false;
    this.dataFound = false;
    this.seleccionadosReporte = [];
    this.dataSource = null;
    this.buscarNegociosForm.controls['selectCriterio'].setValue('');
    this.buscarNegociosForm.controls['argumento'].setValue('');
    this.buscarNegociosForm.controls['tiposCertificado'].setValue('');
    this.buscarNegociosForm.controls['anoGrabable'].setValue('');
  }

  goToPage(page: number) {
    this.mostraPaginaDataLocal(page);
  }

  loadNegocios(negocios): void {
    this.listNegocio = [];
    negocios.forEach(negocio => {
      this.listNegocio.push(negocio);
    });    
  }

  enviarDatos(): void {
    this.dataNotFound = false;
    this.dataFound = false;
    if (this.buscarNegociosForm.invalid) {
      return;
    }

    if (this.buscarNegociosForm.value.selectCriterio == 'codigo') {
      this.request = {
        codigoNegocio: this.buscarNegociosForm.value.argumento,
        nombreNegocio: "",
        numeroDocumento: "",
        tipoDocumento: "",
        nombreTitular: "",
        anoGrabable: this.periodoSeleccionado,
        tipoCertificado: this.certificadoSeleccionado
      }
    }
    if (this.buscarNegociosForm.value.selectCriterio == 'nombre') {
      this.request = {
        codigoNegocio: "",
        nombreNegocio: this.buscarNegociosForm.value.argumento,
        numeroDocumento: "",
        tipoDocumento: "",
        nombreTitular: "",
        anoGrabable: this.periodoSeleccionado,
        tipoCertificado: this.certificadoSeleccionado
      }
    }
    let requestPaginado: any = this.request;
    requestPaginado.pagina = 1;
    requestPaginado.elementos = this.numElementos;
    this.buscarNegociosList(requestPaginado);
  }


  buscarNegociosList(requestBuscar) {
    this.negocios = null;
    this._dataService.postCertificadosNegociosListRequest(requestBuscar).subscribe((data: any) => {
      if (!data) {
        this.dataNotFound = true;
        this.dataFound = false;
        return;
      } else {
        this.dataFound = true;
        this.dataNotFound = false;
        this.negocios = data;
      }
      setTimeout(() => {
        this.loadNegocios(this.negocios['negocios']);
        let pagina = 1;
        this.mostraPaginaDataLocal(pagina);
      });
    },
      (err) => {
        this.dataNotFound = true;
      }
    );
  }

  mostraPaginaDataLocal(pagina) {
    let registrosPagina: any[] = new Array();
    this.page = pagina;
    this.numElementos = this.ELEMENTOS_POR_PAGINA;
    this.resultsLength = this.listNegocio.length;
    this.totalPages = this.resultsLength / this.numElementos;
    let totalPagina = this.resultsLength % this.numElementos;
    if (totalPagina > 1) {
      this.totalPages = Number(parseInt(String(this.totalPages))) + 1;
    }
    if (this.totalPages < 1) {
      this.totalPages = 1;
    }

    let inicioRegistros = ((pagina - 1) * this.numElementos);
    let ultimoRegistros = pagina * this.numElementos;
    for (let i = inicioRegistros; ((i < this.listNegocio.length) && (i < ultimoRegistros)); i++) {
      registrosPagina.push(this.listNegocio[i]);
    }

    this.dataSource = new MatTableDataSource(registrosPagina);         
    this.dataSource.sort = this.sort;
  }

  mostrarSubbusqueda(codSFC: string, negocio: string) {
    this.verMas = true;
    this.codSFC = codSFC;
    this._storageData.setInicializarCertificadosNegocios(1);
    this.iniciarPestanaNegocio = this._storageData.getInicializarCertificadosNegocios();
    this.negocioSeleccionado = negocio;
  }

  setTipoCertificado(valor: string) {
    this.certificadoSeleccionado = valor;
  }
  setAnoSeleccionado(valor: string) {
    this.periodoSeleccionado = this.buscarNegociosForm.value.anoGrabable;
  }
}
