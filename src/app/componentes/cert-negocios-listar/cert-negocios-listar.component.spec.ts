import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertNegociosListarComponent } from './cert-negocios-listar.component';

describe('CertNegociosListarComponent', () => {
  let component: CertNegociosListarComponent;
  let fixture: ComponentFixture<CertNegociosListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertNegociosListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertNegociosListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
