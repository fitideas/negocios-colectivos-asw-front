import { Component, OnInit, ViewChild, Input } from "@angular/core";
import { DataService } from "src/app/services/data.service";
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table"
import { ListTitulares } from 'src/app/modelo/list-titulares';
import { ResponseTitulares } from 'src/app/modelo/response-titulares';
import { MatDialog } from '@angular/material';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { MensajeEsperaGenericoComponent } from 'src/app/dialogs/mensaje-espera-generico/mensaje-espera-generico.component';
import { ModalSincronizacionErrorComponent } from 'src/app/dialogs/modal-sincronizacion-error/modal-sincronizacion-error.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-cert-negocios-beneficiarios-vinculados',
  templateUrl: './cert-negocios-beneficiarios-vinculados.component.html',
  styleUrls: ['./cert-negocios-beneficiarios-vinculados.component.scss']
})

/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: junio 2020
 * @Proposito: clase para mostrar las los vinculados para generar certificados
 */

export class CertNegociosBeneficiariosVinculadosComponent implements OnInit {

  @Input('codNegocio') codNegocio: string = "";
  @Input('anoGrabable') inAnoGrabable: string = "";
  @Input('tipoCertificado') inTipoCertificado: string = "";
  @Input('nombreNegocio') nombreNegocio: string = "";

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  public dataNotFound: boolean = false;
  public dataFound: boolean = false;
  public listTitulares: ListTitulares[] = new Array();
  
  public titulares: ResponseTitulares;
  public seleccionadosReporte: any[] = [];
  public certificadoSeleccionado: string = "";
  public periodoSeleccionado: string = "";
  selection = new SelectionModel<any>(true, []);
  displayedColumns: string[] = [];
  dataSource: MatTableDataSource<ListTitulares>;
  public page: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPages: number = 1; //Número total de páginas 
  public numElementos: number = 10; //Total de elemento por pagina
  public resultsLength = 0;
  public request: any; 

  public iniciarPestanaNegocio: number = 0;

  private crearDisplayedColums() {
    if (this.certificadoSeleccionado == 'PART') {
      this.displayedColumns = [
        "nombre",
        "tipoDocumento",
        "numeroDocumento",
        "participacion",
        "numeroderechos",
        "id"
      ];
    } else if (this.certificadoSeleccionado == 'INGRET') {
      this.displayedColumns = [
        "nombre",
        "tipoDocumento",
        "numeroDocumento",
        "baseretencion",
        "totalretenido",
        "id"
      ];
    } else if (this.certificadoSeleccionado == 'RENTAEXC') {
      this.displayedColumns = [
        "nombre",
        "tipoDocumento",
        "numeroDocumento",
        "rentaexcenta",
        "id"
      ];
    }
  }

  private ELEMENTOS_POR_PAGINA: number = environment.elementosPorPagina;

  constructor(
    private _dataService: DataService,
    private _storageData: StorageDataService,
    public dialog: MatDialog

  ) {
  }

  ngOnInit() {
    this.iniciarPestanaNegocio = this._storageData.getInicializarCertificadosNegocios();
    this.listTitulares = new Array();
    this.dataNotFound = false;
    this.dataFound = false;
    this.enviarDatosSubConsulta();
    this.crearDisplayedColums();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public Regresar() {
    this._storageData.setInicializarCertificadosNegocios(0);
    this.iniciarPestanaNegocio = this._storageData.getInicializarCertificadosNegocios();    
    this.periodoSeleccionado = null;
    this.certificadoSeleccionado = null;
  }

  goToPage(page: number) {
    this.mostraPaginaDataLocal(page);
  }

  loadNegocios(titulares): void {
    this.listTitulares = [];

    titulares.forEach(t => {
      this.listTitulares.push(t);
    });
  }

  enviarDatosSubConsulta() {
    this.periodoSeleccionado = this.inAnoGrabable;
    this.certificadoSeleccionado = this.inTipoCertificado;
    this.request = {
      codigoNegocio: this.codNegocio,
      nombreNegocio: "",
      numeroDocumento: "",
      tipoDocumento: "",
      nombreTitular: "",
      anoGrabable: this.periodoSeleccionado,
      tipoCertificado: this.certificadoSeleccionado
    }

    let requestPaginado: any = this.request;
    requestPaginado.pagina = 1;
    requestPaginado.elementos = this.numElementos;
    this.buscarTitularesList(requestPaginado);
  }

  buscarTitularesList(requestBuscar) {
    this.titulares = null;
    this._dataService.postCertificadosTitularesListRequest(requestBuscar)
      .subscribe((data: any) => {
        if (!data || data.totalTitulares == 0) {
          this.dataNotFound = true;
          this.dataFound = false;
          return;
        } else {
          this.dataFound = true;
          this.dataNotFound = false;
          this.titulares = data;
        }
        setTimeout(() => {
          this.loadNegocios(this.titulares['titulares']);
          let pagina = 1;
          this.mostraPaginaDataLocal(pagina);
        });
      },
        (err) => {
          this.dataNotFound = true;
        }
      );
  }

  mostraPaginaDataLocal(pagina) {
    let registrosPagina: any[] = new Array();
    this.page = pagina;
    this.numElementos = this.ELEMENTOS_POR_PAGINA;
    this.resultsLength =this.listTitulares.length;
    this.totalPages = this.resultsLength / this.numElementos;
    let totalPagina = this.resultsLength % this.numElementos;
    if (totalPagina > 1) {
      this.totalPages = Number(parseInt(String(this.totalPages))) + 1;
    }
    if (this.totalPages < 1) {
      this.totalPages = 1;
    }

    let inicioRegistros = ((pagina - 1) * this.numElementos);
    let ultimoRegistros = pagina * this.numElementos;
    for (let i = inicioRegistros; ((i < this.listTitulares.length) && (i < ultimoRegistros)); i++) {
      registrosPagina.push(this.listTitulares[i]);
    }

    this.dataSource = new MatTableDataSource(registrosPagina);         
    this.dataSource.sort = this.sort;
  }

  tipoReporte(claveTipo) {
    if (claveTipo == this.certificadoSeleccionado) {
      return true;
    } else {
      return false;
    }
  }

  administrarEscogidos(row) {
    if (this.seleccionadosReporte.includes(row)) {
      this.seleccionadosReporte
        = this.seleccionadosReporte.filter(item => item != row);
    } else {
      this.seleccionadosReporte.push(row);
    }
  }

  seleccionarTodos(event) {
    this.seleccionadosReporte = [];
    if (event.checked) {
      this.seleccionadosReporte = this.listTitulares;
    }
    this.selected();
  }

  selected() {
    if (this.seleccionadosReporte.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    return numSelected;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));

  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row) {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  verificarSeleccion() {
    this.request = [];
    this.seleccionadosReporte.forEach(element => {
      this.request.push({
        codigoNegocio: element.codSFC,
        tipoDocumento: element.tipoDocumento,
        numeroDocumento: element.numeroDocumento,
        anoGrabable: this.periodoSeleccionado
      })
    });
    this.generarArchivo(this.certificadoSeleccionado, this.request);
  }

  private generarArchivo(paramTipo: string, request) {
    this._dataService
      .generarCertificado(paramTipo, request)
      .subscribe((informacionArchivo: any) => {
        let data;
        data = {
          titulo: "Generando certificados",
          contenido: "Se esta generando el certificado del vinculado."
        };
        const cargaDialog = this.dialog.open(MensajeEsperaGenericoComponent, {
          width: "46rem",
          data
        });
        setTimeout(() => {
          var nameOfFileToDownload = informacionArchivo["nombreArchivoZip"];
          const byteCharacters = atob(informacionArchivo["recursoB64"]);
          const byteNumbers = new Array(byteCharacters.length);
          for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
          }
          const byteArray = new Uint8Array(byteNumbers);
          const blob = new Blob([byteArray], {
            type: informacionArchivo.tipoArchivo
          });
          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob, nameOfFileToDownload);
          } else {
            var a = document.createElement("a");
            a.href = URL.createObjectURL(blob);
            a.download = nameOfFileToDownload;
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
            cargaDialog.close();
          }
        }, 4000);

      },
        (error) => {
          let data = {
            titulo: "Error en la generación del archivo",
            contenido: "Ha ocurrido un error en la generación del archivo",
            buttonAceptar: "ACEPTAR",
            urlimg: true
          };
          //se usa este modal ya que  permite mostrar mensajes de error
          //genericos
          this.dialog.open(ModalSincronizacionErrorComponent, {
            width: "46rem",
            data
          });
        });
  }
}
