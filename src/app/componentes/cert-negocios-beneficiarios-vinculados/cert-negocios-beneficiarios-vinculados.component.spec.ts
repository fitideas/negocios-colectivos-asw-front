import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertNegociosBeneficiariosVinculadosComponent } from './cert-negocios-beneficiarios-vinculados.component';

describe('CertNegociosBeneficiariosVinculadosComponent', () => {
  let component: CertNegociosBeneficiariosVinculadosComponent;
  let fixture: ComponentFixture<CertNegociosBeneficiariosVinculadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertNegociosBeneficiariosVinculadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertNegociosBeneficiariosVinculadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
