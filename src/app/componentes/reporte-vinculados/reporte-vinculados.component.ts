import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, FormBuilder } from '@angular/forms';
import {  MatDialog } from '@angular/material';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Utils } from 'src/app/core/utils';
import { AuthService } from 'src/app/services/auth.service';
import { ModalSincronizacionErrorComponent } from 'src/app/dialogs/modal-sincronizacion-error/modal-sincronizacion-error.component';


export interface CriterioList {
  value: string;
  viewValue: string;
  active: boolean;
}

@Component({
  selector: 'app-reporte-vinculados',
  templateUrl: './reporte-vinculados.component.html',
  styleUrls: ['./reporte-vinculados.component.scss']
})
export class ReporteVinculadosComponent implements OnInit {
  public buscarPagosDynamicForm: FormGroup;
  get filtrosFormArray() {
    
    return <FormArray>this.buscarPagosDynamicForm.get("filtros");
  }
  public isFecha: boolean = false;
  public isTipoNegocio: boolean = false;
  public isNegocioAsociado: boolean = false;
  public isUsuario: boolean = false;
  public isTodo: boolean = false;
  public swAddFiltros = true;
  public request: any;


  listSelectCriterio: CriterioList[] = [    
    { value: "fecha", viewValue: "Fecha", active: true },
    { value: "usuarios", viewValue: "Usuario que realiza cambio", active: true }
  ];
  listSelectCriterioNuevo: CriterioList[] = [];

  public arrayFiltro: any[] = [];
  public nuevoItem: boolean = false;
  public listFechaRango: any[] = [];
  public listUsuario: any[] = [];
  public total:any;
  public mensual:any;
  public anual:any;

  public filtroEscogido :boolean =false;
  constructor(public fb: FormBuilder,
    private _dataService: DataService,
    private _storageData: StorageDataService,
    private _util: Utils,
    private _authService: AuthService,
    public dialog: MatDialog) {
  }

  ngOnInit(): void {    
    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarPagosDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();

  }

  createGroup() {
    return this.fb.group({
      selectCriterio: new FormControl("", []),
      argumento: new FormControl("", []),
    });
  }


  addToFiltrosFormArray() {
    this.filtrosFormArray.push(this.createGroup());
    if (this.filtrosFormArray.length == 2) {
      this.swAddFiltros = false;
    }
    this.nuevoItem = true;
  } 

  checkFiltros() {
    if (this.filtrosFormArray.length == 1) {
      return false
    }
    return true;
  }

  removeFromFiltrosFormArray(index, itemFiltro) {
    this.filtrosFormArray.removeAt(index);
    
    if (itemFiltro.value.selectCriterio=="usuarios"){
      this.listUsuario =[];
    }else if (itemFiltro.value.selectCriterio=="fecha"){
      this.listFechaRango=[];
    }
    this.isFiltroEscogido();
   //controla el boton mas
    if (this.filtrosFormArray.length < 2) {
      this.swAddFiltros = true;
    }

    //remueve un elemento del array de filtros
    this.arrayFiltro = this.arrayFiltro.filter(item => {
      return (item !== itemFiltro.value.selectCriterio);
    });

    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == itemFiltro.value.selectCriterio) {
        element.active = true;
      }
    });
  }
  buscarListaXCriterioSeleccionado(eventSeleccionado, ind) {
    this.isFecha = false;
    this.isNegocioAsociado = false;
    this.isTodo = true;
    this.isTipoNegocio = false;
    this.isUsuario = false;
    if (eventSeleccionado.value == 'todo') {
      this.isTodo = true;
    }
    if (eventSeleccionado.value == 'tipoNegocio') {
      this.arrayFiltro[ind] = 'tipoNegocio';
      this.isFecha = false;
      this.isNegocioAsociado = false;
      this.isTodo = false;
      this.isTipoNegocio = true;
      this.isUsuario = false;
    }
    if (eventSeleccionado.value == 'fecha') {
      this.arrayFiltro[ind] = 'fecha';
      this.isFecha = true;
      this.isNegocioAsociado = false;
      this.isTodo = false;
      this.isTipoNegocio = false;
      this.isUsuario = false;
    }
    if (eventSeleccionado.value == 'negocios') {
      this.arrayFiltro[ind] = 'negocios';
      this.isFecha = false;
      this.isNegocioAsociado = true;
      this.isTodo = false;
      this.isTipoNegocio = false;
      this.isUsuario = false;
    }
    if (eventSeleccionado.value == 'usuarios') {
      this.arrayFiltro[ind] = 'usuarios';
      this.isFecha = false;
      this.isNegocioAsociado = false;
      this.isTodo = false;
      this.isTipoNegocio = false;
      this.isUsuario = true;
    }

    //desaparece el criterio que se selecciono
    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == eventSeleccionado.value) {
        element.active = false;
      }
      if (!this.isTodo) {
        if (element.value == 'todo') {
          element.active = false;
        }
      }
    });
  }

  isFiltroEscogido() {
    if (this.listFechaRango.length == 0
      && this.listUsuario.length == 0){
      this.filtroEscogido = false;
    } else {
      this.filtroEscogido = true;

    }
  }
  getDateRange(range) {
    
    this.listFechaRango = range;
    this.isFiltroEscogido();
  }

  getUsuario(usuario) {
    this.listUsuario = usuario;
    this.isFiltroEscogido();
  }

  limpiarformulario() {
    this.listFechaRango = [];
    this.listUsuario = [];
    this.arrayFiltro = [];
    this.filtrosFormArray.clear();
    this.filtrosFormArray.reset([]);
    this.total = null;
    this.mensual = null;
    this.anual = null;
    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarPagosDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();
    this.swAddFiltros = true;
    this.listSelectCriterioNuevo.forEach(element => {
      element.active = true;
    });   
  }

  enviarDatos(listar): void {
    this.request = {

      rangoFecha: this.listFechaRango,
      solicitadoPor: this.listUsuario,

    }
    this._dataService.postServiceReporteSolicitudesCambios(this.request)
    .subscribe(data=>{
      this.total=data.compiladoTotal;
      this.mensual=data.compiladoMensual;
      this.anual=data.compiladoAnual;

    },   (error)=>{
      let data = {
        titulo: "Error realizando la consulta ",
        contenido: "Ha ocurrido un error en la consulta",
        buttonAceptar: "ACEPTAR",
        urlimg: true
      };
  
      //se usa este modal ya que  permite mostrar mensajes de error
      //genericos
      this.dialog.open(ModalSincronizacionErrorComponent, {
        width: "46rem",
        data
      });
    });
  }
}
