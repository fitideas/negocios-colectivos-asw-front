import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteVinculadosComponent } from './reporte-vinculados.component';

describe('ReporteVinculadosComponent', () => {
  let component: ReporteVinculadosComponent;
  let fixture: ComponentFixture<ReporteVinculadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteVinculadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteVinculadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
