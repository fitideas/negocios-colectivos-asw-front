import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NegocioEquipoComponent } from './negocio-equipo.component';

describe('NegocioEquipoComponent', () => {
  let component: NegocioEquipoComponent;
  let fixture: ComponentFixture<NegocioEquipoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NegocioEquipoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NegocioEquipoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
