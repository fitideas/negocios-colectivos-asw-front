import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatPaginator, MatSort, MatDialog } from "@angular/material";
import { Criterio } from 'src/app/componentes/beneficiario/beneficiario.component';
import { Validators, FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { DialogParametrosSaveComponent } from 'src/app/componentes/dialog-parametros-save/dialog-parametros-save.component';
import { ListEquipo, ParamBusquedaEq } from 'src/app/modelo/list-equipo';

export interface DialogData {
  titulo: string,
  contenido: string
  urlimg: boolean,
  buttonCancelar: string,
  buttonAceptar: string,
  param: any
}

@Component({
  selector: 'app-negocio-equipo',
  templateUrl: './negocio-equipo.component.html',
  styleUrls: ['./negocio-equipo.component.css']
})
/**
 * @Autor: Lizeth Patino
 * @Fecha: Marzo 2020
 * @Proposito: clase para gestion de equipos de negocios .
 */
export class NegocioEquipoComponent implements OnInit {

  displayedColumns: string[] = [
    "name",
    "documentNumber",
    "rol",
    "id"
  ];
  dataSource: MatTableDataSource<ListEquipo>;
  selection = new SelectionModel<ListEquipo>(true, []);

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  public buscarEquipoForm: FormGroup;
  public dataNotFound: boolean = false;
  public dataFound: boolean = false;
  public listEquipo: ListEquipo[] = new Array();
  private listEquipoNuevos: ListEquipo[] = new Array();
  public resultsLength = 0;
  public guardarBotton = false;
  theCheckbox = false;

  selectCriterio: Criterio[] = [
    { value: "documento", viewValue: "Número de identificación" },
    { value: "nombre", viewValue: "Nombres y/o apellidos" }
  ];

  public page: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPages: number = 1; //Número total de páginas 
  public numElementos: number = 10; //Total de elemento por pagina

  constructor(
    public fb: FormBuilder,
    private _dataService: DataService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<NegocioEquipoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.buscarEquipoForm = this.fb.group({
      selectCriterio: new FormControl("", [Validators.required]),
      argumento: new FormControl("", Validators.required)
    });
  }

  ngOnInit() {
    this.dataNotFound = false;
    this.dataFound = false;
    this.serviceLoadCotitularList(this.data.param);
  }

  public resetTables() {
    this.dataNotFound = false;
    this.dataFound = false;
    this.buscarEquipoForm.controls['argumento'].setValue('');
  }

  get f() {
    return this.buscarEquipoForm.controls;
  }

  private serviceLoadCotitularList(param: ParamBusquedaEq): void {
    this._dataService.getObtenerEquiposDeNegocio(param)
      .subscribe((result: any) => {
        if (!result) {
          this.dataNotFound = true;
          this.dataFound = false;
          return;
        }
        this.dataFound = true;
        this.dataNotFound = false;
        this.loadCotitulares(result);
        let pagina = 1;
        this.mostraPaginaDataLocal(pagina);
        // this.dataSource = new MatTableDataSource(this.listEquipo);
        // this.resultsLength = this.listEquipo.length;
        // this.dataSource.paginator = this.paginator;
        // this.dataSource.sort = this.sort;
      },
        (err) => {
          this.dataNotFound = true;
        }
      );
  }

  private buscarObjetoCotitular(array, valor) {
    return array.filter(function (e) {
      if (e.documentoCotitular == valor.documentoCotitular) {
        e.asociado = valor.asociado;
      }
      return e.documentoCotitular == valor.documentoCotitular;
    });
  }
  public buscarCotitular(array, valor) {
    let objeto = this.buscarObjetoCotitular(array, valor);
    return (objeto.length > 0) ? true : false;
  }

  public agragarCambios(row?: ListEquipo): void {
    if (this.listEquipoNuevos.length > 0) {
      if (!this.buscarCotitular(this.listEquipoNuevos, row)) {
        this.listEquipoNuevos.push(row);
      }
    } else {
      this.listEquipoNuevos.push(row);
    }
    this.guardarBotton = true;
  }

  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public loadCotitulares(cotitulares: any): void {
    this.listEquipo = [];
    let cotitular: ListEquipo;

    cotitulares.forEach((item) => {
      cotitular = {
        nombreCompleto: item.nombreCompleto,
        numeroDocumento: item.numeroDocumento,
        rol: item.rol,
        esDelEquipo: item.esDelEquipo
      };
      this.listEquipo.push(cotitular);
    });
  }

  enviarDatos(): void {
    this.dataNotFound = false;
    this.dataFound = false;   
    if (this.buscarEquipoForm.invalid) {
      return;
    }
    let requestBuscar: ParamBusquedaEq;
    if (this.buscarEquipoForm.value.selectCriterio) {
      requestBuscar = {
        codigonegocio: this.data.param.codigonegocio,
        argumento: this.buscarEquipoForm.value.argumento,
        criterioBuscar: this.buscarEquipoForm.value.selectCriterio
      }
    }
    this.serviceLoadCotitularList(requestBuscar);
  }

  guardarEquipo(){
   
  }  

  openDialog(ok, context): void {
    let data;
    if (ok) {
      data = {
        titulo: "Datos guardados",
        contenido: "Las acciones sobre Cotitulares han sido guardadas de manera exitosa.",
        button: "ACEPTAR",
        urlimg: false
      };
    }
    if (!ok) {
      let contenido = "Las acciones sobre Cotitulares no se pueden guardar. Intente más tarde.";
      if (context != null) {
        contenido = context;
      }
      data = {
        titulo: "Datos no guardados",
        contenido: contenido,
        button: "ACEPTAR",
        urlimg: true
      };
    }
    const dialogRef = this.dialog.open(DialogParametrosSaveComponent, {
      width: "40rem",
      data
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  mostraPaginaDataLocal(pagina) {
    let registrosPagina: any[] = new Array();
    this.page = pagina;
    this.numElementos = 10;
    this.resultsLength = this.listEquipo.length;
    this.totalPages = this.resultsLength / this.numElementos;
    let totalPagina = this.resultsLength % this.numElementos;
    if (totalPagina > 1) {
      this.totalPages = Number(parseInt(String(this.totalPages))) + 1;
    }
    if (this.totalPages < 1) {
      this.totalPages = 1;
    }   

    let inicioRegistros = ((pagina - 1) * this.numElementos);
    let ultimoRegistros = pagina * this.numElementos;
    for (let i = inicioRegistros; ((i < this.listEquipo.length) && (i < ultimoRegistros)); i++) {
      registrosPagina.push(this.listEquipo[i]);
    }

    this.dataSource = new MatTableDataSource(registrosPagina);         
    this.dataSource.sort = this.sort;
  }

  goToPage(page: number) {
    this.mostraPaginaDataLocal(page);
  }
  
}
