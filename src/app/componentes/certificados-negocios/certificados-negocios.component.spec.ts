import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificadosNegociosComponent } from './certificados-negocios.component';

describe('CertificadosNegociosComponent', () => {
  let component: CertificadosNegociosComponent;
  let fixture: ComponentFixture<CertificadosNegociosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertificadosNegociosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificadosNegociosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
