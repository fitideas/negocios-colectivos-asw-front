import { Component, OnInit, ViewChild, Input } from "@angular/core";
import { DataService } from "src/app/services/data.service";
import { IconService } from "src/app/services/icon.service";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { StorageDataService } from 'src/app/services/storage-data.service';
import { AuthService } from 'src/app/services/auth.service';
import { Utils } from 'src/app/core/utils';
import { ResponseNegocios } from 'src/app/modelo/response-negocios';
import { Router } from '@angular/router';
import { ListNegociosPorDerechos } from 'src/app/modelo/list-negocios-por-derechos';
import { MatDialog } from '@angular/material';
import { CertififcadosVerficarNegociosComponent } from '../certififcados-verficar-negocios/certififcados-verficar-negocios.component';
import {SelectionModel} from '@angular/cdk/collections';

export interface Criterio {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-certificados-negocios',
  templateUrl: './certificados-negocios.component.html',
  styleUrls: ['./certificados-negocios.component.css']
})

/**
 * @Autor: Fabian Camilo Herrera
 * @Fecha: mayo 2020
 * @Proposito: clase para mostrar las los negocios para generar certificados
 */

export class CertificadosNegociosComponent implements OnInit {
  selectCriterio: Criterio[] = [
    { value: "codigo", viewValue: "Código del negocio" },
    { value: "nombre", viewValue: "Nombre del negocio" }
  ];
  tiposCertificado:any[]= [
    {
      value:"PART",
      viewValue:"Certificado de participacion"
    },
    {
      value:"INGRET",
      viewValue:"Certificado de ingresos y retenciones"
    },
    {
      value:"RENTAEXC",
      viewValue:"Certificado de renta exenta"
    } 
  ];

  public dataNotFound: boolean = false;
  public dataFound: boolean = false;
  public listNegocio: ListNegociosPorDerechos[] = new Array();
  selection = new SelectionModel<any>(true, []);
  public buscarNegociosForm : FormGroup;
  public submitted: boolean = false;
  public resultsLength = 0;
  public negocios: ResponseNegocios;
  public negocioDetalle:string;
  public consultar_vinculado: boolean = false; 
  public datos_generales: boolean = false;
  public negocios_asociados: boolean = false; 
  public historial_modificaciones: boolean = false; 
  public verMas =false;
  public negocioSeleccionado:string ="";
  public certificadoSeleccionado:string ="";
  public periodoSeleccionado:string="";

  public seleccionadosReporte :any[]=[];

  displayedColumns: string[] = [
    "nombre",
    "codigoSFC",
    "codigoInterno",
    "id"
  ];

  dataSource: MatTableDataSource<ListNegociosPorDerechos>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;


  public page: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPages: number = 1; //Número total de páginas 
  public numElementos: number = 5; //Total de elemento por pagina
  public request: any; 


  @Input() subBusqueda: boolean=false;
  @Input('tipoDocumento') tipoDocumento: string="";
  @Input('numeroDocumento') numeroDocumento: string="";
  @Input('anoGrabable') inAnoGrabable: string="";
  @Input('tipoCertificado') inTipoCertificado: string="";
  public listAnoSelect: any[] = [];
  public iniciarPestanaNegocio: number = 0;
  public iniciarPestanaVinculado: number = 0;  
  
  constructor(
    private _dataService: DataService,
    private _iconService: IconService,
    private _storageData: StorageDataService,
    private _authService: AuthService,
    private dialog: MatDialog,
    private _router: Router,
    public fb: FormBuilder,
    public _util: Utils
  ) { 
    this.buscarNegociosForm = this.fb.group(
      {
        selectCriterio: new FormControl("", [Validators.required]),
        argumento: new FormControl("", [Validators.required]),
        tiposCertificado: new FormControl("", Validators.required ),
        anoGrabable: new FormControl("", Validators.required)
      }
    )

  }

  ngOnInit() {
    this.iniciarPestanaVinculado = this._storageData.getInicializarCertificadosVinculados();
    this.iniciarPestanaNegocio = this._storageData.getInicializarCertificadosNegocios();
    this.comboAno();
    this.dataNotFound = false;
    this.dataFound = false;
    this.enviarDatosSubConsulta();
  }

  get f() {
    return this.buscarNegociosForm.controls;
  }

  private comboAno() {
    let d = new Date();
    let n = d.getFullYear();
    let year: any;    
    for (let i = n; i >= 1992; i--) {
      year = {
        value: i,
        viewValue: i
      }
      this.listAnoSelect.push(year);
    }
  }

  public resetTables() {
    this.dataNotFound = false;
    this.listNegocio = null;
    this.seleccionadosReporte=[];
    this.buscarNegociosForm.controls['argumento'].setValue('');
  }

  public Regresar(){
    this._storageData.setInicializarCertificadosVinculados(1);
    this.iniciarPestanaVinculado = this._storageData.getInicializarCertificadosVinculados();
    this._storageData.setInicializarCertificadosNegocios(0);
    this.iniciarPestanaNegocio = this._storageData.getInicializarCertificadosVinculados(); 
    this.verMas = null;
    this.negocioDetalle = null;
    this.periodoSeleccionado = null;
    this.certificadoSeleccionado = null;
  }
  
  public limpiarformulario() {    
    this.dataSource = null;
    this.dataNotFound = false;
    this.dataFound = false;
    this.seleccionadosReporte = [];
    this.dataSource = null;
    this.buscarNegociosForm.controls['selectCriterio'].setValue('');
    this.buscarNegociosForm.controls['argumento'].setValue('');
    this.buscarNegociosForm.controls['tiposCertificado'].setValue('');
    this.buscarNegociosForm.controls['anoGrabable'].setValue('');
  }

  goToPage(page: number) {
    this.page = page;
    let requestPaginado: any = this.request;
    requestPaginado.pagina = page;
    requestPaginado.elementos = this.numElementos;
    this.buscarNegociosList(requestPaginado);
  }
 
   
 
   loadnegocios(negocios): void {
     this.listNegocio = [];
     negocios.forEach(negocio => {
       this.listNegocio.push(negocio);
     });
   }
  
  enviarDatos(): void {
    this.dataNotFound = false;
    this.dataFound = false;
    if (this.buscarNegociosForm.invalid) {
      return;
    }

    if (this.buscarNegociosForm.value.selectCriterio=='codigo'){
      this.request = {
        codigoNegocio: this.buscarNegociosForm.value.argumento,
        nombreNegocio: "",
        numeroDocumento:this.numeroDocumento,
        tipoDocumento:this.tipoDocumento,
        nombreTitular:"",
        anoGrabable:this.periodoSeleccionado,
        tipoCertificado:this.certificadoSeleccionado
        }
    }
    if (this.buscarNegociosForm.value.selectCriterio=='nombre'){
      this.request = {
        codigoNegocio:"",
        nombreNegocio: this.buscarNegociosForm.value.argumento,
        numeroDocumento:this.numeroDocumento,
        tipoDocumento:this.tipoDocumento,
        nombreTitular:"",
        anoGrabable:this.periodoSeleccionado,
        tipoCertificado:this.certificadoSeleccionado
        }
    }    
    let requestPaginado: any = this.request;
    requestPaginado.pagina = 1;
    requestPaginado.elementos = this.numElementos;
    this.buscarNegociosList(requestPaginado);
  }

  
  buscarNegociosList(requestBuscar) {
    this.negocios = null;
    this._dataService.postCertificadosNegociosListRequest(requestBuscar).subscribe((data: any) => {
      if (!data) {
          this.dataNotFound = true;
          this.dataFound = false;
          return;
        }else{
        this.dataFound = true;
        this.dataNotFound = false;
        this.negocios = data;
       
        }
      setTimeout(() => {
         this.loadnegocios(this.negocios['negocios']);
         this.dataSource = new MatTableDataSource(this.listNegocio);
         this.resultsLength = data.totalNegocios;
         // this.totalPages = Math.round(this.resultsLength / this.numElementos);
         // this.totalPages = (this.totalPages < 1) ? 1 : this.totalPages;
         this.totalPages = this.resultsLength / this.numElementos;          
          let totalPagina = this.resultsLength % this.numElementos;
          if (totalPagina > 1) {
            this.totalPages = Number(parseInt(String(this.totalPages))) + 1;
          }
          if (this.totalPages < 1) {
            this.totalPages = 1;
          } 
         this.dataSource.paginator = this.paginator;
         this.dataSource.sort = this.sort;
        });
      },
        (err) => {
          this.dataNotFound = true;
        }
      );
  }

  mostrarSubbusqueda(codSFC:string, negocio:string){
    this.verMas =true;
    this.negocioDetalle = codSFC;
    this._storageData.setInicializarCertificadosNegocios(2);    
    this.iniciarPestanaNegocio = this._storageData.getInicializarCertificadosNegocios();
    this.negocioSeleccionado= negocio;
  }

  enviarDatosSubConsulta(){
    if (this.subBusqueda){
      this.periodoSeleccionado=this.inAnoGrabable;
      this.certificadoSeleccionado =this.inTipoCertificado;
      this.buscarNegociosForm.controls["anoGrabable"].setValue(this.periodoSeleccionado);
      this.buscarNegociosForm.controls["tiposCertificado"].setValue(this.certificadoSeleccionado);
      this.request = {
        codigoNegocio:"",
        nombreNegocio: "",
        numeroDocumento:this.numeroDocumento,
        tipoDocumento:this.tipoDocumento,
        nombreTitular:"",
        anoGrabable:this.periodoSeleccionado,
        tipoCertificado:this.certificadoSeleccionado
        }
     
    let requestPaginado: any = this.request;
    requestPaginado.pagina = 1;
    requestPaginado.elementos = this.numElementos;
    this.buscarNegociosList(requestPaginado);
      }
  }
  
  administrarEscogidos(row){
    if(this.seleccionadosReporte.includes(row)){
      this.seleccionadosReporte 
        = this.seleccionadosReporte.filter(item => item!=row);
    }else{
      this.seleccionadosReporte.push(row);
    }  }

    seleccionarTodos(){
      this.seleccionadosReporte=this.listNegocio;
      this.selected(); 
    }

    
  selected(){
    if (this.seleccionadosReporte.length>0){
      return true;
    }else{
      return false;
    }
  }

  verificarSeleccion(){
  let  data = {
    seleccionadosReporte :this.seleccionadosReporte,
    periodoSeleccionado:this.periodoSeleccionado,
    certificadoSeleccionado:this.certificadoSeleccionado
  }

    this.dialog.open(CertififcadosVerficarNegociosComponent,{
      width: "90rem",
    data
    
  });
 // this.generarArchivo();

  }

  
  setTipoCertificado(valor:string){
    this.certificadoSeleccionado=valor;
  }
  setAnoSeleccionado(valor:string){
    this.periodoSeleccionado=this.buscarNegociosForm.value.anoGrabable;
  }
   /** Whether the number of selected elements matches the total number of rows. */
   isAllSelected() {
    const numSelected = this.selection.selected.length;
    return numSelected;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
      
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row){
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }
}
