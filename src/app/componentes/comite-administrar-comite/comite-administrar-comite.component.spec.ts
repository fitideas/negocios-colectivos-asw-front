import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComiteAdministrarComiteComponent } from './comite-administrar-comite.component';

describe('ComiteAdministrarComiteComponent', () => {
  let component: ComiteAdministrarComiteComponent;
  let fixture: ComponentFixture<ComiteAdministrarComiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComiteAdministrarComiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComiteAdministrarComiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
