import { Component, OnInit, Inject } from '@angular/core';
import { MatTableDataSource, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { DialogTwoButtonsComponent } from '../dialog-two-buttons/dialog-two-buttons.component';
import { ComiteAdicionarMiembroComponent } from '../comite-adicionar-miembro/comite-adicionar-miembro.component';
import { DialogParametrosSaveComponent } from '../dialog-parametros-save/dialog-parametros-save.component';

export interface DialogData {
  titulo: string,
  contenido: string
  urlimg: boolean,
  buttonCancelar: string,
  buttonAceptar: string,
}

@Component({
  selector: 'app-comite-administrar-comite',
  templateUrl: './comite-administrar-comite.component.html',
  styleUrls: ['./comite-administrar-comite.component.css']
})

/**
* Autor:Lizeth Patiño
* Proposito: Administración de miembros de un comité
* Fecha: 11/04/2020
*/

export class ComiteAdministrarComiteComponent implements OnInit {
  
  displayedColumns: string[] = [
    "inhabilitar",
    "name",
    "typedocument",
    "documentNumber",
     "id"
  ];

  dataSource: MatTableDataSource<any>;
  codigoSFC: string;
  datosParaBack: any[];

  constructor(
  private _dataService: DataService,
  public dialog: MatDialog,
  private _storageData: StorageDataService,
  @Inject(MAT_DIALOG_DATA) public data: DialogData)
  { }

  ngOnInit() {
    this.codigoSFC=this._storageData.getcodigoSFCNegocio();
    this._dataService.getObtenerInformacionComite(this.codigoSFC).subscribe((dataResultado: any) => {
      this.dataSource = new MatTableDataSource(dataResultado);
    });
  }


  public inactivarMiembro(index){
    this.dataSource.filteredData[index].estado="INACTIVO"
    this.datosParaBack=this.dataSource.filteredData;
    let data;
    data = {
      titulo: "Eliminar miembro",
      contenido:
        "Se esta intentando eliminar un miembro ¿Está seguro?",
      buttonContinuar: "Continuar",
      buttonCancelar: "Cancelar",
      urlimg: true,
    };
    const dialogRef = this.dialog.open(DialogTwoButtonsComponent, {
      width: "42rem",
      data
    });
    dialogRef.afterClosed().subscribe(async resultado => {
      if (resultado == "Continuar") {
        this._dataService.putGuardarInformacpionMiembroComite(this.datosParaBack[index])
          .subscribe((data: any) => {
            let dataMensajeGuardado = {
              titulo: "Cambios guardados",
              contenido:
                "Los cambios de miembro de comité han sido guardados exitosamente.",
              button: "ACEPTAR",
              urlimg: false
            };
            this.dialog.open(DialogParametrosSaveComponent, {
              width: "40rem",
              data: dataMensajeGuardado
            });
           },
           (err: any) => {
            let dataMensajeGuardado = {
              titulo: "Cambios NO guardados",
              contenido: "Los cambios no han podido ser guardados intente más tarde.",
              button: "ACEPTAR",
              urlimg: true
            }
            this.dialog.open(DialogParametrosSaveComponent, {
              width: "40rem",
              data: dataMensajeGuardado
            });            
          });
      }
    });
  }

  public completarDatosComite(informacionMiembroSeleccionado){
    let data;
    data = {
      buttonCancelar: "CANCELAR",
      buttonAceptar: "ACEPTAR",
      informacion:informacionMiembroSeleccionado,
      urlimg: true,
    };
    const dialogoAdicionar=this.dialog.open(ComiteAdicionarMiembroComponent, {
      width: "77rem",
      data
    });
    dialogoAdicionar.afterClosed().subscribe(async resultado => {
      switch (resultado) {
        case "ACEPTAR":
          this._dataService.getObtenerInformacionComite(this.codigoSFC).subscribe((dataResultado: any) => {
            this.dataSource = new MatTableDataSource(dataResultado);
          });
          break;
        case "CANCELAR":
      }
    }); 
  }

}
