import { Component, OnInit } from '@angular/core';
import { Utils } from 'src/app/core/utils';
import { AuthService } from 'src/app/services/auth.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.scss']

  
})
export class ReportesComponent implements OnInit {
  public ver_reportes_viculados:boolean =false;
  public ver_reportes_negocios:boolean =false;
  public ver_reportes_pagos:boolean =false;

  constructor(   private _util: Utils,
    private _authService: AuthService,
    private _storageData: StorageDataService,
    private _router: Router
    ) { }

  ngOnInit(): void {
    if (this._storageData.getSwInicio()) {
      this._router.navigate(['accionfiduciaria']);
    }
    this.loadPermisos();
    
  }

  private loadPermisos() {
    this.ver_reportes_viculados = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'reportes_informe_vinculados');
    this.ver_reportes_negocios = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'reportes_informe_negocios');
    this.ver_reportes_pagos = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'reportes_informe_pagos');

  } 
}
