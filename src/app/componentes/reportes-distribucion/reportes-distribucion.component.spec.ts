import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportesDistribucionComponent } from './reportes-distribucion.component';

describe('ReportesDistribucionComponent', () => {
  let component: ReportesDistribucionComponent;
  let fixture: ComponentFixture<ReportesDistribucionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportesDistribucionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportesDistribucionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
