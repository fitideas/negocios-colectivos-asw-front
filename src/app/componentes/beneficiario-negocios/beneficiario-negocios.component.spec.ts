import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiarioNegociosComponent } from './beneficiario-negocios.component';

describe('BeneficiarioNegociosComponent', () => {
  let component: BeneficiarioNegociosComponent;
  let fixture: ComponentFixture<BeneficiarioNegociosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiarioNegociosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiarioNegociosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
