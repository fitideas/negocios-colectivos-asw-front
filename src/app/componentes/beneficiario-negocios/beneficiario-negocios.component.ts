import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Documento, ParamDatosNegocio, BeneficiarioDetalle } from 'src/app/modelo/beneficiario-detalle';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { AuthService } from 'src/app/services/auth.service';
import { Utils } from 'src/app/core/utils';
import { Router } from '@angular/router';

export interface NegocioData {
  codigo: string;
  nombre: string;
  distribucion: string;
  financiera: string;
}

@Component({
  selector: 'app-beneficiario-negocios',
  templateUrl: './beneficiario-negocios.component.html',
  styleUrls: ['./beneficiario-negocios.component.scss']
})
/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Marzo 2020
 * @Proposito: clase para mostrar los negocios asociados al beneficiarios.
 */
export class BeneficiarioNegociosComponent implements OnInit {

  displayedColumns: string[] = ['linknegocio','codigo', 'nombre', 'distribucion', 'financiera'];
  dataSource: MatTableDataSource<NegocioData>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  public showDistribucion: boolean = false;
  public infoBeneficiario: BeneficiarioDetalle;
  public datos_distribucion: boolean = false;
  public informacion_financiera: boolean = false

  @Output() propagarClickFinaciera = new EventEmitter<boolean>();
  public documento: Documento;
  public infoVinculadoNegocio: ParamDatosNegocio;
  public dataNotFound: boolean = false;
  public dataFound: boolean = false;
  public detalleNegocio: boolean = false;
  public resultsLength = 0;
  public negocioData: NegocioData;
  public listNegocios: NegocioData[] = new Array();

  public page: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPages: number = 1; //Número total de páginas 
  public numElementos: number = 10; //Total de elemento por pagina
  public request: any;

  constructor(
    private _dataService: DataService,
    private _storageData: StorageDataService,
    private _authService: AuthService,
    private _router: Router,
    private _util: Utils
  ) {
    this.dataSource = new MatTableDataSource();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.loadInfoNegocio();
    this.loadPermisos();
  }

  private loadPermisos() {
    this.datos_distribucion = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'datos_distribucion');
    this.informacion_financiera = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'informacion_financiera');
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  private loadInfoNegocio() {
    this.infoBeneficiario = this._storageData.getInfoBeneficiario();
    this.documento = {
      documento: this.infoBeneficiario.numeroDocumento,
      tipodocumento: this.infoBeneficiario.tipoDocumento.id
    }

    this._dataService
      .getBeneficiarioObtenerNegociosListRequest(this.documento)
      .subscribe((data: any) => {
        if (!data) {
          this.dataNotFound = true;
          this.dataFound = false;
          this.detalleNegocio = false;
          return;
        }
        this.dataFound = true;
        this.dataNotFound = false;
        this.detalleNegocio = false;

        setTimeout(() => {
          this.loadNegocios(data);
        });
      },
        (err) => {
          this.dataNotFound = true;
        }
      );
  }

  private loadNegocios(Listnegocios): void {
    this.listNegocios = [];

    Listnegocios.negocios.forEach(item => {
      this.negocioData = {
        codigo: item.codigo,
        nombre: item.nombre,
        distribucion: null,
        financiera: null
      }
      this.listNegocios.push(this.negocioData);
    });
    let pagina = 1;
    this.mostraPaginaDataLocal(pagina);
  }  

  public vinculoDetalleNegocio(codigoNegocio) {
     this._dataService.getObtenerInformacionNegocio(codigoNegocio)
     .subscribe((response: any) => {
       if (!response) {
         this.dataNotFound = true;
         this.dataFound = false;
         return;
       }
       this.dataFound = false;
       this.dataNotFound = false;
       this._storageData.setEstadoProyecto(response.completo);
       this._storageData.setInfoNegocioDetalle(response);
       this._router.navigate(['accionfiduciaria/negocios/detalles']);
     },
       (err) => {
         this.dataNotFound = true;
       }
     );
  }

  public getInfoDistribucion(codigo) {
    this.showDistribucion = true;
    this.infoVinculadoNegocio = {
      codigo: codigo,
      documento: this.documento.documento,
      tipodocumento: this.documento.tipodocumento
    }
    this._storageData.setInfoDatosDistribucion(this.infoVinculadoNegocio);
    this._storageData.setActiveDistribucion(true);
    this._storageData.setActiveFinanciera(false);    
  }

  public getInfoFinanciera(codigo, nombre) {
    this._storageData.setActiveFinanciera(true);
    this._storageData.setActiveDistribucion(false);
    this.infoVinculadoNegocio = {
      codigo: codigo,
      documento: this.documento.documento,
      tipodocumento: this.documento.tipodocumento
    }
    var informacionNegocio: any = {
      codigoNegocio: this.infoVinculadoNegocio.codigo,
      nombreNegocio: nombre,
      documento: this.infoVinculadoNegocio.documento,
      tipoDocumento: this.infoVinculadoNegocio.tipodocumento
    }
    this._storageData.setPDatosGenerales(informacionNegocio);
  }

  mostraPaginaDataLocal(pagina) {
    let registrosPagina: NegocioData[] = new Array();
    this.page = pagina;
    this.numElementos = 10;
    this.resultsLength = this.listNegocios.length;
    this.totalPages = this.resultsLength / this.numElementos;
    let totalPagina = this.resultsLength % this.numElementos;
    if (totalPagina > 1) {
      this.totalPages = Number(parseInt(String(this.totalPages))) + 1;
    }
    if (this.totalPages < 1) {
      this.totalPages = 1;
    }   

    let inicioRegistros = ((pagina - 1) * this.numElementos);
    let ultimoRegistros = pagina * this.numElementos;
    for (let i = inicioRegistros; ((i < this.listNegocios.length) && (i < ultimoRegistros)); i++) {
      registrosPagina.push(this.listNegocios[i]);
    }

    this.dataSource = new MatTableDataSource(registrosPagina);         
    this.dataSource.sort = this.sort;
  }

  goToPage(page: number) {
    this.mostraPaginaDataLocal(page);
  }
}

