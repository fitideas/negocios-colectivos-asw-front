import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogReemplazarParametrosComponent } from './dialog-reemplazar-parametros.component';

describe('DialogReemplazarParametrosComponent', () => {
  let component: DialogReemplazarParametrosComponent;
  let fixture: ComponentFixture<DialogReemplazarParametrosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogReemplazarParametrosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogReemplazarParametrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
