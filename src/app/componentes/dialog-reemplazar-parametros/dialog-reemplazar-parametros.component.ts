import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';

export interface DialogData {
  titulo: string,
  contenido: string,
  parametroActualLabel: string,
  parametroReemplazoLabel: string,
  parametroActual: any,
  parametroReemplazo: string,
  buttonCancelar: string,
  buttonReemplazar: string,
  urlimg: boolean
}
@Component({
  selector: 'app-dialog-reemplazar-parametros',
  templateUrl: './dialog-reemplazar-parametros.component.html',
  styleUrls: ['./dialog-reemplazar-parametros.component.css']
})
/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Marzo 2020
 * @Proposito: clase para dialog modal para reemplazar parametros
 */
export class DialogReemplazarParametrosComponent {

  public listParametrosReemplazo: object[] = new Array();
  private parametroEliminar: string = null;
  private parametroEliminarId: number = null;
  public formReemplazar: FormGroup;
  public btReemplazarDisable: boolean = true;
  constructor(
    public dialogRef: MatDialogRef<DialogReemplazarParametrosComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private _dataService: DataService
  ) {
    this.parametroEliminar = (data.parametroActual.rol) ? data.parametroActual.rol : data.parametroActual.descripcion;
    this.parametroEliminarId = (data.parametroActual.idRol) ? data.parametroActual.idRol : data.parametroActual.id;

    this.createForm();
    this._dataService.getParametrosValoresDominio(data.parametroReemplazo).subscribe(
      (response) => {
        response.forEach(element => {
          let elementId = (element.idRol) ? element.idRol : element.id;
          if (this.parametroEliminarId != elementId) {
            this.listParametrosReemplazo.push(element);
          }
          if (this.listParametrosReemplazo.length > 0) {
            this.btReemplazarDisable = false;
          } else {
            this.btReemplazarDisable = true;
          }
        });
      },
      (err) => {
      }
    );
  }

  private createForm() {
    this.formReemplazar = new FormGroup({
      parametroAEliminar: new FormControl(this.parametroEliminar, [Validators.required]),
      parametroReemplazo: new FormControl('', [Validators.required]),
    });
  }

  public reemplarzar() {
    this.dialogRef.close(this.formReemplazar.value.parametroReemplazo);
  }
}
