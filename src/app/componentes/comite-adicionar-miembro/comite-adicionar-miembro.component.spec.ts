import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComiteAdicionarMiembroComponent } from './comite-adicionar-miembro.component';

describe('ComiteAdicionarMiembroComponent', () => {
  let component: ComiteAdicionarMiembroComponent;
  let fixture: ComponentFixture<ComiteAdicionarMiembroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComiteAdicionarMiembroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComiteAdicionarMiembroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
