import { Component, OnInit, Inject } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  DateAdapter,
  MAT_DATE_LOCALE,
  MAT_DATE_FORMATS,
  MatDialog,
} from "@angular/material";
import { DataService } from 'src/app/services/data.service';
import { DatePipe } from '@angular/common';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { MY_FORMATS } from 'src/app/datepickers/datepicker-mes-anio/datepicker-mes-anio.component';
import { DialogParametrosSaveComponent } from '../dialog-parametros-save/dialog-parametros-save.component';

export interface DialogData {
  dataDeAlla: {};
  buttonCancelar: string,
  buttonAceptar: string,
}

@Component({
  selector: 'app-comite-adicionar-miembro',
  templateUrl: './comite-adicionar-miembro.component.html',
  styleUrls: ['./comite-adicionar-miembro.component.css'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})


/**
* Autor:Lizeth Patiño
* Proposito: Formulario para adicionar rol, radicado de comisión y fecha de vigencia de un miembro
de comité.
* Fecha: 12/04/2020
*/

export class ComiteAdicionarMiembroComponent implements OnInit {

  miembroForm: FormGroup;
  guardandoDataProvisional: any;
  roles: any;
  fechaMinimaIngreso = new Date();

  constructor( 
    public dialog: MatDialog,
    private datePipe: DatePipe,
    private _dataService: DataService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<ComiteAdicionarMiembroComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
   
  }

  public crearFormulario(){
    this.miembroForm = this.fb.group({
      codigoSFC: [ ],
      tipoDocumento: [ ],
      numDocumento: [ ],
      nombre: [ ],
      estado: [],
      rol: ["", Validators.compose([Validators.required])],
      fecVigenciaRol: ["",Validators.compose([Validators.required])],
      radicado: ["",Validators.compose([Validators.required]) ],
      agregado: [true]
    });
  }

  ngOnInit() {
    this.crearFormulario();
    if(this.data){
      this.llenarInformacionFormulario(this.data['informacion']);   
  }
}

  public llenarInformacionFormulario(dataParaElForm){
    this.llamarServicios();
    this.miembroForm.get('nombre').setValue(dataParaElForm.nombre);
    this.miembroForm.get('tipoDocumento').setValue(dataParaElForm.tipoDocumento);
    this.miembroForm.get('numDocumento').setValue(dataParaElForm.numDocumento);
    this.miembroForm.get('rol').setValue(dataParaElForm.rol);
    this.miembroForm.get('fecVigenciaRol').setValue(this.transformarFecha(dataParaElForm.fecVigenciaRol));
    this.miembroForm.get('radicado').setValue(dataParaElForm.radicado);
    this.miembroForm.get('estado').setValue(dataParaElForm.estado);
    this.miembroForm.get('codigoSFC').setValue(dataParaElForm.codigoSFC);
  }  

  public llamarServicios(){
    this._dataService
    .getParametrosValoresDominio("ROLTER")
    .subscribe((rolesTercero: any) => {
      this.roles = rolesTercero;
    });
  }

  public transformarFecha(dateString) {
    let dateParts: any;
    let dateObject: any = null;
    if (dateString != null) {
      dateParts = dateString.split("/");
      dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    }
    return dateObject;
  }

  public adicionarMiembro(){
    this.miembroForm.value['fecVigenciaRol']=this.datePipe.transform(this.miembroForm.value['fecVigenciaRol'], 'dd/MM/yyyy');
    if(this.miembroForm.value['estado']=='' || this.miembroForm.value['estado']==null || this.miembroForm.value['estado']=='INACTIVO' ){
      this.miembroForm.value['estado']="ACTIVO";
    }
    this._dataService.putGuardarInformacpionMiembroComite(this.miembroForm.value)
    .subscribe((data: any) => {
      let dataMensajeGuardado = {
        titulo: "Cambios guardados",
        contenido:
          "Los cambios de adición de miembro de comité han sido guardados exitosamente.",
        button: "ACEPTAR",
        urlimg: false
      };
      this.dialog.open(DialogParametrosSaveComponent, {
        width: "40rem",
        data: dataMensajeGuardado
      });
     },
     (err: any) => {
       let dataMensajeGuardado = {
        titulo: "Cambios NO guardados",
        contenido: "Los cambios no han podido ser guardados intente más tarde.",
        button: "ACEPTAR",
        urlimg: true
      };
      this.dialog.open(DialogParametrosSaveComponent, {
        width: "40rem",
        data: dataMensajeGuardado
      });
     } 
   );
   
  }

}
