import { Component, OnInit } from '@angular/core';
import { Utils } from 'src/app/core/utils';
import { AuthService } from 'src/app/services/auth.service';
import { StorageDataService } from 'src/app/services/storage-data.service';

@Component({
  selector: 'app-negocio-historial',
  templateUrl: './negocio-historial.component.html',
  styleUrls: ['./negocio-historial.component.css']
})
export class NegocioHistorialComponent implements OnInit {

  public ver_historial_modificaciones: boolean = false;
  public historial_modificaciones: boolean = false;

  constructor(
    public _util: Utils,
    private _authService: AuthService,
    private _storageData: StorageDataService
  ) { }

  ngOnInit() {
    this.loadPermisos();   
  }

  private loadPermisos() {
    this.ver_historial_modificaciones = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'historial_modificaciones');    
  }

  public verHistorialModificaciones() {
    this._storageData.setActiveNegocioHistorial(false);    
    this._storageData.setNegocioHistoriallModificaciones(true);
    this.historial_modificaciones = true; 
  }


}
