import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NegocioHistorialComponent } from './negocio-historial.component';

describe('NegocioHistorialComponent', () => {
  let component: NegocioHistorialComponent;
  let fixture: ComponentFixture<NegocioHistorialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NegocioHistorialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NegocioHistorialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
