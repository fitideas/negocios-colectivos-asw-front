
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, FormBuilder } from '@angular/forms';
import {  MatDialog } from '@angular/material';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Utils } from 'src/app/core/utils';
import { AuthService } from 'src/app/services/auth.service';
import { ModalSincronizacionErrorComponent } from 'src/app/dialogs/modal-sincronizacion-error/modal-sincronizacion-error.component';


export interface CriterioList {
  value: string;
  viewValue: string;
  active: boolean;
}
@Component({
  selector: 'app-reportes-proyectos',
  templateUrl: './reportes-proyectos.component.html',
  styleUrls: ['./reportes-proyectos.component.scss']
})

/**
 * @Autor: Fabian Herrera
 * @Fecha: Abril 2020
 * @Proposito: clase para consultar información de los estados de los proyectos .
 */
export class ReportesProyectosComponent implements OnInit {

  public buscarPagosDynamicForm: FormGroup;
  isResponsableNegocio: boolean;
  listFinalResponsableNegocio: any[];
  get filtrosFormArray() {
    return <FormArray>this.buscarPagosDynamicForm.get("filtros");
  }
  public isFecha: boolean = false;
  public isTipoNegocio: boolean = false;
  public isNegocioAsociado: boolean = false;
  public isUsuario: boolean = false;
  public isTodo: boolean = false;
  public swAddFiltros = true;
  public request: any;


  listSelectCriterio: CriterioList[] = [
    //{ value: "todo", viewValue: "Sin Filtros", active: true },
    { value: "tipoNegocio", viewValue: "Tipo de Negocio", active: true },
    { value: "fecha", viewValue: "Fecha", active: true },
    { value: "responsablesNegocio", viewValue: "Responsables de negocio", active: true },
    //{ value: "usuarios", viewValue: "Usuario que realiza cambio", active: true },
    { value: "negocios", viewValue: "Negocios", active: true }
  ];
  listSelectCriterioNuevo: CriterioList[] = [];

  public arrayFiltro: any[] = [];
  public nuevoItem: boolean = false;
  public listFechaRango: any[] = [];
  public listUsuario: any[] = [];
  public listNegocio: any[] = [];
  public listTipoNegocio: any[] = [];
  public listResponsableNegocio: any[] = [];


  public proyectosActivos: any;
  public proyectosEnLiquidacion: any;
  public proyectosLiquidados: any;
  public cambiosProyecto: any;
  public cambiosMensual: any;
  public cambiosAnual: any;
  public avaluoInmuebles: any;
  public valorPrediales: any;
  public valorPredialesMensual: any;
  public valorPredialesAnual: any;
  public valorValorizacion: any;
  public valorValorizacionMensual: any;
  public valorValorizacionAnual: any;

  public filtroEscogido:boolean =false;
  constructor(public fb: FormBuilder,
    private _dataService: DataService,
    private _storageData: StorageDataService,
    private _util: Utils,
    private _authService: AuthService,
    public dialog: MatDialog) {
  }

  createGroup() {
    return this.fb.group({
      selectCriterio: new FormControl("", []),
      argumento: new FormControl("", []),
    });
  }


  addToFiltrosFormArray() {
    this.filtrosFormArray.push(this.createGroup());
    if (this.filtrosFormArray.length == 4) {
      this.swAddFiltros = false;
    }
    this.nuevoItem = true;
  }

  ngOnInit(): void {
    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarPagosDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();
  }

  checkFiltros() {
    if (this.filtrosFormArray.length == 1) {
      return false
    }
    return true;
  }

  removeFromFiltrosFormArray(index, itemFiltro) {
    this.filtrosFormArray.removeAt(index);


    if (itemFiltro.value.selectCriterio == "usuarios") {
      this.listUsuario = [];
    } else if (itemFiltro.value.selectCriterio == "fecha") {
      this.listFechaRango = [];
    } else
      if (itemFiltro.value.selectCriterio == "tipoNegocio") {
        this.listTipoNegocio = [];
      } else if (itemFiltro.value.selectCriterio == "negocios") {
        this.listNegocio = [];
      }else if (itemFiltro.value.selectCriterio == "responsablesNegocio") {
        this.listResponsableNegocio = [];
      }  
      this.isFiltroEscogido();

    //controla el boton mas
    if (this.filtrosFormArray.length < 5) {
      this.swAddFiltros = true;
    }

    //remueve un elemento del array de filtros
    this.arrayFiltro = this.arrayFiltro.filter(item => {
      return (item !== itemFiltro.value.selectCriterio);
    });
    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == itemFiltro.value.selectCriterio) {
        element.active = true;
      }
    });


  }
  buscarListaXCriterioSeleccionado(eventSeleccionado, ind) {
    this.isFecha = false;
    this.isNegocioAsociado = false;
    this.isTodo = true;
    this.isResponsableNegocio=false;
    this.isTipoNegocio = false;
    this.isUsuario = false;
    if (eventSeleccionado.value == 'todo') {
      this.isTodo = true;
    }
    if (eventSeleccionado.value == 'tipoNegocio') {
      this.arrayFiltro[ind] = 'tipoNegocio';
      this.isFecha = false;
      this.isResponsableNegocio=false;
      this.isNegocioAsociado = false;
      this.isTodo = false;
      this.isTipoNegocio = true;
      this.isUsuario = false;
    }
    if (eventSeleccionado.value == 'fecha') {
      this.arrayFiltro[ind] = 'fecha';
      this.isFecha = true;
      this.isResponsableNegocio=false;
      this.isNegocioAsociado = false;
      this.isTodo = false;
      this.isTipoNegocio = false;
      this.isUsuario = false;
    }
    if (eventSeleccionado.value == 'negocios') {
      this.arrayFiltro[ind] = 'negocios';
      this.isFecha = false;
      this.isResponsableNegocio=false;
      this.isNegocioAsociado = true;
      this.isTodo = false;
      this.isTipoNegocio = false;
      this.isUsuario = false;
    }
    if (eventSeleccionado.value == 'usuarios') {
      this.arrayFiltro[ind] = 'usuarios';
      this.isFecha = false;
      this.isResponsableNegocio=false;
      this.isNegocioAsociado = false;
      this.isTodo = false;
      this.isTipoNegocio = false;
      this.isUsuario = true;
    }
    if (eventSeleccionado.value == 'responsablesNegocio') {
      this.arrayFiltro[ind] = 'responsablesNegocio';
      this.isFecha = false;
      this.isResponsableNegocio=true;
      this.isNegocioAsociado = false;
      this.isTodo = false;
      this.isTipoNegocio = false;
      this.isUsuario = false;
    }
    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == eventSeleccionado.value) {
        element.active = false;
      }
      if (!this.isTodo) {
        if (element.value == 'todo') {
          element.active = false;
        }
      }
    });
  }

  isFiltroEscogido() {
    if (this.listFechaRango.length == 0
      && this.listUsuario.length == 0
      && this.listTipoNegocio.length == 0
      && this.listResponsableNegocio.length == 0
      && this.listNegocio.length == 0) {
      this.filtroEscogido = false;
    } else {
      this.filtroEscogido = true;

    }

  }

  getDateRange(range) {
   
    this.listFechaRango = range;
    this.isFiltroEscogido();
  }

  getUsuario(usuario) {
    
    this.listUsuario = usuario;
    this.isFiltroEscogido();
  }

  getNegocios(negocio) {
    this.listNegocio = negocio;
    this.isFiltroEscogido();
  }
  getTipoNegocio(tipoN) {
    
    this.listTipoNegocio = tipoN;
    this.isFiltroEscogido();
  }

  getResponsableNegocio(responsables) {   
    this.listResponsableNegocio = responsables;
    this.isFiltroEscogido();
  }

  limpiarformulario() {
    this.arrayFiltro = [];
    this.filtrosFormArray.clear();
    this.filtrosFormArray.reset([]);
    this.listResponsableNegocio=[];
    this.listFinalResponsableNegocio=[];
    this.listFechaRango=[];
    this.listTipoNegocio=[];
    this.listNegocio=[];
    this.listUsuario=[];
    this.proyectosActivos = null;
    this.proyectosEnLiquidacion = null;
    this.proyectosLiquidados = null;
    this.cambiosProyecto = null;
    this.cambiosMensual = null;
    this.cambiosAnual = null;
    this.avaluoInmuebles = null;
    this.valorPrediales = null;
    this.valorPredialesMensual = null;
    this.valorPredialesAnual = null;
    this.valorValorizacion = null;
    this.valorValorizacionMensual = null;
    this.valorValorizacionAnual = null;

    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarPagosDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();
    this.swAddFiltros = true;
    this.listSelectCriterioNuevo.forEach(element => {
      element.active = true;
    });
  }

  enviarDatos(listar): void {
    this.listFinalResponsableNegocio=[];
    this.listResponsableNegocio.forEach(numeroDoc => {
      this.listFinalResponsableNegocio.push(numeroDoc.numeroDocumento);
    });
    this.request = {
      responsablesNegocio:   this.listFinalResponsableNegocio,
      rangoFecha: this.listFechaRango,
      tipoNegocio: this.listTipoNegocio,
      negocios: this.listNegocio,
      solicitadoPor:this.listUsuario


    }
 
    this._dataService.postServiceReporteModificacionesNegocio(this.request)
      .subscribe(data => {
        
        this.proyectosActivos = data.proyectosActivos;
        this.proyectosEnLiquidacion = data.proyectosEnLiquidacion;
        this.proyectosLiquidados = data.proyectosLiquidados;
        this.cambiosProyecto = data.cambiosProyecto;
        this.cambiosMensual = data.cambiosMensual;
        this.cambiosAnual = data.cambiosAnual;
        this.avaluoInmuebles = data.avaluoInmuebles;
        this.valorPrediales = data.valorPrediales;
        this.valorPredialesMensual = data.valorPredialesMensual;
        this.valorPredialesAnual = data.valorPredialesAnual;
        this.valorValorizacion = data.valorValorizacion;
        this.valorValorizacionMensual = data.valorValorizacionMensual;
        this.valorValorizacionAnual = data.valorValorizacionAnual;
      },   (error)=>{
        let data = {
          titulo: "Error realizando la consulta ",
          contenido: "Ha ocurrido un error en la consulta",
          buttonAceptar: "ACEPTAR",
          urlimg: true
        };
    
        //se usa este modal ya que  permite mostrar mensajes de error
        //genericos
        this.dialog.open(ModalSincronizacionErrorComponent, {
          width: "46rem",
          data
        });
      });
  }
}
