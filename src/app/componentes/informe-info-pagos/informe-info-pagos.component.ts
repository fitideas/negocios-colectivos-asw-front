import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormArray, FormControl, FormBuilder } from '@angular/forms';
import { MatTableDataSource, MatDialog, MatPaginator, MatSort } from '@angular/material';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Utils } from 'src/app/core/utils';
import { AuthService } from 'src/app/services/auth.service';
import { DatePipe } from '@angular/common';

export interface CriterioList {
  value: string;
  viewValue: string;
  active: boolean;
}

export interface Parametros {
  codigoNegocio: any[];
  rangoFecha: any[];
  codigoTipoNegocio: any[];
}

export interface ListPagos {
  periodoPago?: string;
  tipoNegocio?: string;
  encargoDebitado?: string;
  valorIngreso?: string;
  valorDistribuido?: string;
  verMas?: string;
}

@Component({
  selector: 'app-informe-info-pagos',
  templateUrl: './informe-info-pagos.component.html',
  styleUrls: ['./informe-info-pagos.component.css']
})

/**
 * @Autor: Lizeth Patiño Manrique
 * @Fecha: 26 de abril de 2020
 * @Proposito: clase para visualizar la información de pagos de la aplicación basados
 * en filtros de tipo de negocio, negocio y rangos de fecha de
 * solicitud.
 */

export class InformeInfoPagosComponent implements OnInit {

  public buscarPagosDynamicForm: FormGroup;
  usuario: boolean;
  negocio: boolean;
  listUsuario: any;
  ingresos: string;
  distribuciones: string;
  retenciones: string;
  obligaciones: string;
  fechaFinal: any;
  fechaInicial: any;
  resultadoTotal: any;
  get filtrosFormArray() {
    return <FormArray>this.buscarPagosDynamicForm.get("filtros");
  }
  public submitted: boolean = false;
  public dataNotFound: boolean = null;
  public dataFound: boolean = null;
  public detalleNegocio: boolean = false;

  public isFecha: boolean = false;
  public isTipoNegocio: boolean = false;
  public isNumeroEncargo: boolean = false;
  public isTodo: boolean = false;
  public swAddFiltros = true;

  public arrayFiltro: any[] = [];
  public nuevoItem: boolean = false;

  public listFechaRango: any[] = [];
  public listNegocio: any[] = [];
  public listTipoNegocio: any[] = [];
  public request: Parametros;
  public codigoNegocio: any[]=[];
  public ver_negocios_historial_pagos: boolean = false;

  listSelectCriterio: CriterioList[] = [
    { value: "todo", viewValue: "Sin Filtros", active: true },
    { value: "tipoNegocio", viewValue: "Tipo de Negocio", active: true },
    { value: "fecha", viewValue: "Fecha de solicitud de cambio", active: true },
    { value: "negocio", viewValue: "Negocio", active: true }
  ];

  listSelectCriterioNuevo: CriterioList[] = [];



  dataSource: MatTableDataSource<ListPagos>;
  public resultsLength: number = 0;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  public listError: any = null;
  public swListError: boolean = false;
  public infoNegocio: any;


  constructor(
    public fb: FormBuilder,
    private _dataService: DataService,
    private _storageData: StorageDataService,
    private _util: Utils,
    private datePipe: DatePipe,
    private _authService: AuthService,
    public dialog: MatDialog
  ) {

  }

  ngOnInit() {
    this.loadPermisos();
    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarPagosDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();
    this.resultsLength = 0;
    this.enviarDatos();
  }

  private loadPermisos() {
   // this.ver_negocios_historial_pagos = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'negocios_historial_pagos');
  }

  

  checkFiltros() {
    if (this.filtrosFormArray.length == 1) {
      return false
    }
    return true;
  }

  limpiarformulario() {

    //Filtros limpiados
    this.listNegocio=[];
     this.listFechaRango=[];
    this.listTipoNegocio=[];

    this.arrayFiltro = [];
    this.filtrosFormArray.clear();
    this.filtrosFormArray.reset([]);
    this.dataSource = null;
    this.dataNotFound = false;
    this.dataFound = false;
    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarPagosDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();
    this.resultsLength = 0;
    this.swAddFiltros = true;
    this.listSelectCriterioNuevo.forEach(element => {
      element.active = true;
    });   
  }

  buscarListaXCriterioSeleccionado(eventSeleccionado, ind) {
    this.isFecha = false;
    this.isTipoNegocio = false;
    this.negocio=false;
    this.isTodo = true;
    if (eventSeleccionado.value == 'todo') {
      this.isTodo = true;
    }
    if (eventSeleccionado.value == 'tipoNegocio') {
      this.arrayFiltro[ind] = 'tipoNegocio';
      this.isTipoNegocio = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }
    if (eventSeleccionado.value == 'fecha') {
      this.arrayFiltro[ind] = 'fecha';
      this.isFecha = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }
    if (eventSeleccionado.value == 'negocio') {
      this.arrayFiltro[ind] = 'negocio';
      this.negocio = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }

    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == eventSeleccionado.value) {
        element.active = false;
      }
      if (!this.isTodo) {
        if (element.value == 'todo') {
          element.active = false;
        }
      }
    });
  }

  getDateRangeFin(range) {
    this.fechaFinal=this.datePipe.transform(range, '01/MM/yyyy')
  }

  

  getDateRange(range) {
    this.fechaInicial=this.datePipe.transform(range, '01/MM/yyyy')
  }

  
  getTipoNegocio(tipoNegocio) {
    this.listTipoNegocio = tipoNegocio;
  }

  getNegocio(negocio) {
    this.listNegocio = negocio;
  }

  getNegocios(negocio) {
    //solo se necesita el cod_sfc    
    this.listNegocio = negocio;
  }

  createGroup() {
    return this.fb.group({
      selectCriterio: new FormControl("", []),
      argumento: new FormControl("", []),
    });
  }

  addToFiltrosFormArray() {
    this.filtrosFormArray.push(this.createGroup());
    if (this.filtrosFormArray.length == 3) {
      this.swAddFiltros = false;
    }
    this.nuevoItem = true;
  }

  removeFromFiltrosFormArray(index, itemFiltro) {
    this.filtrosFormArray.removeAt(index);

    this.arrayFiltro = this.arrayFiltro.filter(item => {
      return (item !== itemFiltro.value.selectCriterio);
    });

    if (this.filtrosFormArray.length < 4) {
      this.swAddFiltros = true;
    }

    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == itemFiltro.value.selectCriterio) {
        element.active = true;
      }
    });
  }


  public enviarDatos( ): void {
    this.listFechaRango=[];
    this.dataNotFound = false;
    this.dataFound = false;
    this.submitted = true;
    this.detalleNegocio = false;
    this.listFechaRango.push(this.fechaInicial);
    this.listFechaRango.push(this.fechaFinal);
    if(this.fechaInicial==null || this.fechaFinal==null){
      this.listFechaRango=[];
    }
    this.request = {
      codigoNegocio: this.listNegocio,
      rangoFecha: this.listFechaRango,
      codigoTipoNegocio: this.listTipoNegocio
    }
    this.listFechaRango=[];
    this._dataService.postConsultarCifrasPago(this.request)
    .subscribe(data=>{
      this.resultadoTotal=data;
        this.ingresos= this.resultadoTotal.total;
        this.obligaciones= this.resultadoTotal.totalObligaciones;
        this.distribuciones= this.resultadoTotal.totalDistribucion;
        this.retenciones= this.resultadoTotal.totalRetenciones;
      });
  }
}
