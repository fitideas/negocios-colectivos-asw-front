import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformeInfoPagosComponent } from './informe-info-pagos.component';

describe('InformeInfoPagosComponent', () => {
  let component: InformeInfoPagosComponent;
  let fixture: ComponentFixture<InformeInfoPagosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformeInfoPagosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformeInfoPagosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
