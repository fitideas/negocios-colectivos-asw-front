import { Component, OnInit } from '@angular/core';
import { Utils } from 'src/app/core/utils';
import { AuthService } from 'src/app/services/auth.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-certificados',
  templateUrl: './certificados.component.html',
  styleUrls: ['./certificados.component.css']
})

/**
 * @Autor: Fabian Camilo Herrera
 * @Fecha: mayo 2020
 * @Proposito: clase para mostrar las opciones de generación de certificados
 */
export class CertificadosComponent implements OnInit {
  public ver_certificados_viculados :boolean = false;
  public ver_certificados_negocios : boolean = false;
  /*public verMas: boolean = false;
  public pnegocioDetalle: string = "";
  public periodoSeleccionado: string = "";
  public certificadoSeleccionado: string = "";
  */
  public iniciarPestanaNegocio: number = 0;
  public iniciarPestanaVinculado: number = 0;

  constructor(
    private _util: Utils,
    private _authService: AuthService,
    private _storageData: StorageDataService,
    private _router: Router
  ) { }

  ngOnInit() {
    if (this._storageData.getSwInicio()) {
      this._router.navigate(['accionfiduciaria']);
    }
    this.loadPermisos();
  }

  private loadPermisos() {
    this.ver_certificados_viculados = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'certificacion_vinculados');
    this.ver_certificados_negocios = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'certificacion_negocios');
  }
  public iniciarPestanaVinculados() {    
    this._storageData.setInicializarCertificadosVinculados(0);
    this.iniciarPestanaVinculado = this._storageData.getInicializarCertificadosVinculados();   
    /*this.verMas = false;
    this.pnegocioDetalle = "";
    this.periodoSeleccionado = "";
    this.certificadoSeleccionado = "";
    */    
  } 
  public iniciarPestanaNegocios() {    
    this._storageData.setInicializarCertificadosNegocios(0);
    this.iniciarPestanaNegocio = this._storageData.getInicializarCertificadosNegocios();
    /*this.verMas = false;
    this.pnegocioDetalle = "";
    this.periodoSeleccionado = "";
    this.certificadoSeleccionado = "";
    */
  } 
}
