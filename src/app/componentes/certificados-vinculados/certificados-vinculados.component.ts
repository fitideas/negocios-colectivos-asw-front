import { Component, OnInit, ViewChild, Input } from "@angular/core";
import { DataService } from "src/app/services/data.service";
import {SelectionModel} from '@angular/cdk/collections';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table"
import { Utils } from 'src/app/core/utils';
import { ListTitulares } from 'src/app/modelo/list-titulares';
import { ResponseTitulares } from 'src/app/modelo/response-titulares';
import { MatDialog } from '@angular/material';
import { CertificadosVerificarVinculadosComponent } from '../certificados-verificar-vinculados/certificados-verificar-vinculados.component';
import { StorageDataService } from 'src/app/services/storage-data.service';


export interface Criterio {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-certificados-vinculados',
  templateUrl: './certificados-vinculados.component.html',
  styleUrls: ['./certificados-vinculados.component.css']
})



/**
 * @Autor: Fabian Camilo Herrera
 * @Fecha: mayo 2020
 * @Proposito: clase para mostrar las los vinculados para generar certificados
 */

export class CertificadosVinculadosComponent implements OnInit {
  selectCriterio: Criterio[] = [
    { value: "numeroIdentificacion", viewValue: "Número de Identificacion" },
    { value: "nombre", viewValue: "Nombre/Apellidos" }
  ];
  tiposCertificado: any[] = [
    {
      value: "PART",
      viewValue: "Certificado de participacion"
    },
    {
      value: "INGRET",
      viewValue: "Certificado de ingresos y retenciones"
    },
    {
      value: "RENTAEXC",
      viewValue: "Certificado de renta exenta"
    }
  ];
  public dataNotFound: boolean = false;
  public dataFound: boolean = false;
  public listTitulares: ListTitulares[] = new Array();

  public buscarNegociosForm: FormGroup;
  public submitted: boolean = false;
  public resultsLength = 0;
  public titulares: ResponseTitulares;
  public negocioDetalle: boolean = false;
  public consultar_vinculado: boolean = false;
  public datos_generales: boolean = false;
  public negocios_asociados: boolean = false;
  public historial_modificaciones: boolean = false;

  public nombreSeleccionado: string = "";
  public tipoDocumentoSeleccionado: string = "";
  public numeroDocumentoSeleccionado: string = "";
  public verMas: boolean = false;

  public seleccionadosReporte: any[] = [];
  public certificadoSeleccionado: string = "";
  public periodoSeleccionado: string = "";
  selection = new SelectionModel<any>(true, []);
  displayedColumns: string[] = [
    "nombre",
    "tipoDocumento",
    "numeroDocumento",
    "id"
  ];

  dataSource: MatTableDataSource<ListTitulares>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;


  public page: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPages: number = 1; //Número total de páginas 
  public numElementos: number = 4; //Total de elemento por pagina
  public request: any;

  @Input() subBusqueda: boolean = false;
  @Input('codNegocio') codNegocio: string = "";
  @Input('anoGrabable') inAnoGrabable: string = "";
  @Input('tipoCertificado') inTipoCertificado: string = "";

  public listAnoSelect: any[] = [];  
  public iniciarPestanaVinculado: number = 0;
  public iniciarPestanaNegocio: number = 0;

  constructor(
    private _dataService: DataService,
    private _storageData: StorageDataService,
    public dialog: MatDialog,
    public fb: FormBuilder,
    public _util: Utils
  ) {
    this.buscarNegociosForm = this.fb.group(
      {
        selectCriterio: new FormControl("", [Validators.required]),
        argumento: new FormControl("", [Validators.required]),
        tiposCertificado: new FormControl("", Validators.required),
        anoGrabable: new FormControl("", Validators.required)
      }
    )
  }

  ngOnInit() {
    this.iniciarPestanaVinculado = this._storageData.getInicializarCertificadosVinculados();
    this.iniciarPestanaNegocio = this._storageData.getInicializarCertificadosNegocios();
    this.comboAno();
    this.listTitulares = new Array();
    this.dataNotFound = false;
    this.dataFound = false;
    this.enviarDatosSubConsulta();
  }

  get f() {
    return this.buscarNegociosForm.controls;
  }

  private comboAno() {
    let d = new Date();
    let n = d.getFullYear();
    let year: any;    
    for (let i = n; i >= 1992; i--) {
      year = {
        value: i,
        viewValue: i
      }
      this.listAnoSelect.push(year);
    }
  }

  public resetTables() {
    this.dataNotFound = false;
    this.seleccionadosReporte = [];
    this.buscarNegociosForm.controls['argumento'].setValue('');
  }

  public Regresar(){
    this._storageData.setInicializarCertificadosNegocios(1);
    this.iniciarPestanaNegocio = this._storageData.getInicializarCertificadosNegocios();
    this._storageData.setInicializarCertificadosVinculados(0);
    this.iniciarPestanaVinculado = this._storageData.getInicializarCertificadosVinculados();
    this.verMas = null;
    this.tipoDocumentoSeleccionado = null;
    this.numeroDocumentoSeleccionado = null;
    this.periodoSeleccionado = null;
    this.certificadoSeleccionado = null;
  }

  public limpiarformulario() {    
    this.dataSource = null;
    this.dataNotFound = false;
    this.dataFound = false;
    this.seleccionadosReporte = [];
    this.dataSource = null;
    this.buscarNegociosForm.controls['selectCriterio'].setValue('');
    this.buscarNegociosForm.controls['argumento'].setValue('');
    this.buscarNegociosForm.controls['tiposCertificado'].setValue('');
    this.buscarNegociosForm.controls['anoGrabable'].setValue('');    
  }
  goToPage(page: number) {
    this.page = page;
    let requestPaginado: any = this.request;
    requestPaginado.pagina = page;
    requestPaginado.elementos = this.numElementos;
    this.buscarTitularesList(requestPaginado);
  }

  loadnegocios(titulares): void {
    this.listTitulares = [];

    titulares.forEach(t => {
      this.listTitulares.push(t);
    });
  }

  enviarDatos(): void {
    this.dataNotFound = false;
    this.dataFound = false;
    if (this.buscarNegociosForm.invalid) {
      return;
    }

    if (this.buscarNegociosForm.value.selectCriterio) {
      if (this.buscarNegociosForm.value.selectCriterio == 'numeroIdentificacion') {
        this.request = {
          codigoNegocio: this.codNegocio,
          nombreNegocio: "",
          numeroDocumento: this.buscarNegociosForm.value.argumento,
          tipoDocumento: "",
          nombreTitular: "",
          anoGrabable: this.periodoSeleccionado,
          tipoCertificado: this.certificadoSeleccionado

        }
      }
      if (this.buscarNegociosForm.value.selectCriterio == 'nombre') {
        this.request = {
          codigoNegocio: this.codNegocio,
          nombreNegocio: "",
          numeroDocumento: "",
          tipoDocumento: "",
          nombreTitular: this.buscarNegociosForm.value.argumento,
          anoGrabable: this.periodoSeleccionado,
          tipoCertificado: this.certificadoSeleccionado
        }
      }
    }

    let requestPaginado: any = this.request;
    requestPaginado.pagina = 1;
    requestPaginado.elementos = this.numElementos;
    this.buscarTitularesList(requestPaginado);
  }

  enviarDatosSubConsulta() {
    if (this.subBusqueda) {
      this.periodoSeleccionado = this.inAnoGrabable;
      this.certificadoSeleccionado = this.inTipoCertificado;
      this.buscarNegociosForm.controls["anoGrabable"].setValue(this.periodoSeleccionado);
      this.buscarNegociosForm.controls["tiposCertificado"].setValue(this.certificadoSeleccionado);
      this.request = {
        codigoNegocio: this.codNegocio,
        nombreNegocio: "",
        numeroDocumento: "",
        tipoDocumento: "",
        nombreTitular: "",
        anoGrabable: this.periodoSeleccionado,
        tipoCertificado: this.certificadoSeleccionado
      }

      let requestPaginado: any = this.request;
      requestPaginado.pagina = 1;
      requestPaginado.elementos = this.numElementos;
      this.buscarTitularesList(requestPaginado);
    }
  }

  buscarTitularesList(requestBuscar) {
    this.titulares = null;
    this._dataService.postCertificadosTitularesListRequest(requestBuscar)
      .subscribe((data: any) => {
        if (!data || data.totalTitulares == 0) {
          this.dataNotFound = true;
          this.dataFound = false;
          return;
        } else {
          this.dataFound = true;
          this.dataNotFound = false;
          this.titulares = data;
        }
        setTimeout(() => {
          this.loadnegocios(this.titulares['titulares']);
          this.dataSource = new MatTableDataSource(this.listTitulares);
          this.resultsLength = data.totalTitulares;
          this.totalPages = this.resultsLength / this.numElementos;
          let totalPagina = this.resultsLength % this.numElementos;
          if (totalPagina > 1) {
            this.totalPages = Number(parseInt(String(this.totalPages))) + 1;
          }
          if (this.totalPages < 1) {
            this.totalPages = 1;
          }
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        });
      },
        (err) => {
          this.dataNotFound = true;
        }
      );
  }

  mostrarSubbusqueda(nombre: string, tipoDoc: string, numeroDoc: string) {       
    this.verMas = true;
    this.nombreSeleccionado = nombre;
    this.tipoDocumentoSeleccionado = tipoDoc;
    this.numeroDocumentoSeleccionado = numeroDoc;    
    this._storageData.setInicializarCertificadosVinculados(2);    
    this.iniciarPestanaVinculado = this._storageData.getInicializarCertificadosVinculados();
  }

  administrarEscogidos(row) {
    if (this.seleccionadosReporte.includes(row)) {
      this.seleccionadosReporte
        = this.seleccionadosReporte.filter(item => item != row);
    } else {
      this.seleccionadosReporte.push(row);
    }
  }

  seleccionarTodos(){
    this.seleccionadosReporte=this.listTitulares;
    this.selected();
    
  }
 

  selected() {
    if (this.seleccionadosReporte.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  verificarSeleccion() {
    let data = {
      seleccionadosReporte: this.seleccionadosReporte,
      periodoSeleccionado: this.periodoSeleccionado,
      certificadoSeleccionado: this.certificadoSeleccionado
    }
    this.dialog.open(CertificadosVerificarVinculadosComponent, {
      width: "90rem",
      data
    });
  }
  setTipoCertificado(valor: string) {
    this.certificadoSeleccionado = valor;
  }
  setAnoSeleccionado(valor: string) {
    this.periodoSeleccionado = this.buscarNegociosForm.value.anoGrabable;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    return numSelected;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
      
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row){
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }
}
