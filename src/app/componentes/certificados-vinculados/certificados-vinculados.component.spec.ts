import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificadosVinculadosComponent } from './certificados-vinculados.component';

describe('CertificadosVinculadosComponent', () => {
  let component: CertificadosVinculadosComponent;
  let fixture: ComponentFixture<CertificadosVinculadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertificadosVinculadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificadosVinculadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
