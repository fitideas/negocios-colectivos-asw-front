import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertififcadosVerficarNegociosComponent } from './certififcados-verficar-negocios.component';

describe('CertififcadosVerficarNegociosComponent', () => {
  let component: CertififcadosVerficarNegociosComponent;
  let fixture: ComponentFixture<CertififcadosVerficarNegociosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertififcadosVerficarNegociosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertififcadosVerficarNegociosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
