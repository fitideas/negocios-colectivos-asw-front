import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogThreeButtonsComponent } from './dialog-three-buttons.component';

describe('DialogThreeButtonsComponent', () => {
  let component: DialogThreeButtonsComponent;
  let fixture: ComponentFixture<DialogThreeButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogThreeButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogThreeButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
