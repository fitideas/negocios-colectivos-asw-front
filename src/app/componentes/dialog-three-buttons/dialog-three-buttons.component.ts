import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

export interface DialogData {
  titulo: string,
  contenido: string,  
  buttonCancelar: string,
  buttonEliminar: string,
  buttonReemplazar: string,
  urlimg: boolean
}

@Component({
  selector: 'app-dialog-three-buttons',
  templateUrl: './dialog-three-buttons.component.html',
  styleUrls: ['./dialog-three-buttons.component.css']
})
/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Marzo 2020
 * @Proposito: clase para mostrar dialogo de 3 botones  .
 */
export class DialogThreeButtonsComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogThreeButtonsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
  }
}
