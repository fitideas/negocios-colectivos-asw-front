import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'app-form-array-multiple',
  templateUrl: './form-array-multiple.component.html',
  styleUrls: ['./form-array-multiple.component.css']
})
/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Marzo 2020
 * @Proposito: clase usar varios forms dinamicos unidos.
 */
export class FormArrayMultipleComponent implements OnInit {

  form: FormGroup;

  foods: any[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];
  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      'tipoNegocios': this.fb.array([
        this.initTipoNegocios()
      ])
    });
    this.form.valueChanges.subscribe(data => this.validateForm());
    this.validateForm();
  }

  initTipoNegocios() {
    return this.fb.group({
      //  ---------------------forms fields on level Tipos de Negocios ------------------------
      'XtipoNegocios': ['Select Tipos Negocio', [Validators.required]],
      // ------------------------ Retenciones ---------------------------------------------
      'retenciones': this.fb.array([
        this.initRetenciones()
      ]),
      // ----------------------- Gastos ----------------------------------------------
      'gastos': this.fb.array([
        this.initGastos()
      ])
    });
  }

  initRetenciones() {
    return this.fb.group({
      //  ---------------------forms fields on y level retenciones------------------------
      'campo1retencion': ['RetencionDescripcion', [Validators.required, Validators.pattern('[0-9]{3}')]],
    })
  }

  initGastos() {
    return this.fb.group({
      //  ---------------------forms fields on y level gastos ------------------------
      'campo1gastos': ['Gasto 1', [Validators.required, Validators.pattern('[0-9]{3}')]],
    })
  }

  addTipoNegocio() {
    const control = <FormArray>this.form.controls['tipoNegocios'];
    control.push(this.initTipoNegocios());
  }


  addRetenciones(ix) {
    const control = (<FormArray>this.form.controls['tipoNegocios']).at(ix).get('retenciones') as FormArray;
    control.push(this.initRetenciones());
  }

  addGastos(ix) {
    const control = (<FormArray>this.form.controls['tipoNegocios']).at(ix).get('gastos') as FormArray;
    control.push(this.initGastos());
  }

  deleteTipoNegocio(index) {
    (<FormArray>this.form.controls['tipoNegocios']).removeAt(index);
  }

  deleteRetenciones(idxtipNeg, idxRet) {    
    let retencionesA = (<FormArray>this.form.controls['tipoNegocios']).at(idxtipNeg) as FormArray;
    retencionesA.controls['retenciones'].removeAt(idxRet);
  }

  deleteGastos(idxtipNeg, idxGasto) {
    let gastosA = (<FormArray>this.form.controls['tipoNegocios']).at(idxtipNeg) as FormArray;
    gastosA.controls['gastos'].removeAt(idxGasto);
  }


  formErrors = {
    tipoNegocios: this.tipoNegociosErrors()
  };


  tipoNegociosErrors() {
    return [{
      //  ---------------------forms errors on x level ------------------------
      XtipoNegocios: '',
      // ---------------------------------------------------------------------
      'retenciones': this.retencionesErrors(),
      'gastos': this.gastosErrors()

    }]

  }

  retencionesErrors() {
    return [{
      //  ---------------------forms errors on y level ------------------------
      campo1retencion: '',
      campo2retencion: '',
      // ----------------------------------------------------------------------   
    }]
  }

  gastosErrors() {
    return [{
      //  ---------------------forms errors on y level ------------------------
      campo1gastos: '',      
      campo2gastos: '',
      // ----------------------------------------------------------------------   
    }]
  }
  


  validationMessages = {
    tipoNegocios: {
      tipoNegocios: {
        required: 'X is required.',
        pattern: 'X must be 3 characters long.'

      },
      retenciones: {
        campo1retencion: {
          required: 'campo1retencion is required.',
          pattern: 'campo1retencion must be 3 characters long.'
        },
        campo2retencion: {
          required: 'campo2retencion is required.',
          pattern: 'campo2retencion must be 3 characters long.'
        }
      },
      gastos: {
        campo1gastos: {
          required: 'campo1gastos is required.',
          pattern: 'campo1gastos must be 3 characters long.'
        },
        campo2gastos: {
          required: 'campo2gastos is required.',
          pattern: 'campo2gastos must be 3 characters long.'
        }
      }
    }
  };

  // form validation
  validateForm() {
    this.validateTipoNegocios();
  }
  validateTipoNegocios() {
    let tipoNegociosA = <FormArray>this.form['controls'].tipoNegocios;
    this.formErrors.tipoNegocios = [];
    let x = 1;
    while (x <= tipoNegociosA.length) {
      this.formErrors.tipoNegocios.push({
        XtipoNegocios: '',
        retenciones: [{
          campo1retencion: '',
          campo2retencion: ''
        }],
        gastos: [{
          campo1gastos: '',
          campo2gastos: ''
        }]
      });
      let X = <FormGroup>tipoNegociosA.at(x - 1);
      for (let field in X.controls) {
        let input = X.get(field);
        if (input.invalid && input.dirty) {
          for (let error in input.errors) {
            this.formErrors.tipoNegocios[x - 1][field] = this.validationMessages.tipoNegocios[field][error];
          }
        }
      }
      this.validateRetenciones(x);
      this.validateGastos(x);
      x++;
    }
  }

  validateRetenciones(x) {
    let retencionesA = (<FormArray>this.form.controls['tipoNegocios']).at(x - 1).get('retenciones') as FormArray;
    this.formErrors.tipoNegocios[x - 1].retenciones = [];
    let y = 1;
    while (y <= retencionesA.length) {
      this.formErrors.tipoNegocios[x - 1].retenciones.push({
        campo1retencion: '',
        campo2retencion: ''
      });
      let Y = <FormGroup>retencionesA.at(y - 1);
      for (let field in Y.controls) {
        let input = Y.get(field);
        if (input.invalid && input.dirty) {
          for (let error in input.errors) {
            this.formErrors.tipoNegocios[x - 1].retenciones[y - 1][field] = this.validationMessages.tipoNegocios.retenciones[field][error];

          }

        }
      }
      y++;
    }
  }

  validateGastos(x) {
    let gastosA = (<FormArray>this.form.controls['tipoNegocios']).at(x - 1).get('gastos') as FormArray;
    this.formErrors.tipoNegocios[x - 1].gastos = [];
    let z = 1;
    while (z <= gastosA.length) {
      this.formErrors.tipoNegocios[x - 1].gastos.push({
        campo1gastos: '',
        campo2gastos: ''
      });
      let Z = <FormGroup>gastosA.at(z - 1);
      for (let field in Z.controls) {
        let input = Z.get(field);
        if (input.invalid && input.dirty) {
          for (let error in input.errors) {
            this.formErrors.tipoNegocios[x - 1].gastos[z - 1][field] = this.validationMessages.tipoNegocios.gastos[field][error];

          }
        }
      }
      z++;
    }
  }

}
