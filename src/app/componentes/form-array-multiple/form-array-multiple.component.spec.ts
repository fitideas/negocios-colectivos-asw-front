import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormArrayMultipleComponent } from './form-array-multiple.component';

describe('FormArrayMultipleComponent', () => {
  let component: FormArrayMultipleComponent;
  let fixture: ComponentFixture<FormArrayMultipleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormArrayMultipleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormArrayMultipleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
