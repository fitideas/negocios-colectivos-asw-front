import { Component, OnInit, ViewChild, Input } from "@angular/core";
import { DataService } from "src/app/services/data.service";
import { IconService } from "src/app/services/icon.service";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { ListBeneficiarios } from "src/app/modelo/list-beneficiarios";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { StorageDataService } from 'src/app/services/storage-data.service';
import { ResponseBeneficiarios } from 'src/app/modelo/response-beneficiarios';
import { AuthService } from 'src/app/services/auth.service';
import { Utils } from 'src/app/core/utils';
import { Router } from '@angular/router';

export interface Criterio {
  value: string;
  viewValue: string;
}

@Component({
  selector: "app-beneficiario",
  templateUrl: "./beneficiario.component.html",
  styleUrls: ['./beneficiario.component.scss']
})

/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Enero 2020
 * @Proposito: clase para la busqueda de beneficiarios por nombre o por documento.
 */

export class BeneficiarioComponent implements OnInit {
  public buscarBeneficiarioForm: FormGroup;
  public submitted: boolean = false;
  public dataNotFound: boolean = false;
  public dataFound: boolean = false;
  public detalleBeneficiario: boolean = false;
  public listBeneficiario: ListBeneficiarios[] = new Array();
  public resultsLength = 0;
  public beneficiarios: ResponseBeneficiarios;
  public errorServer: null;
  public consultar_vinculado: boolean = false;
  public datos_generales: boolean = false;
  public negocios_asociados: boolean = false;
  // public informacion_financiera: boolean = false; 
  public historial_modificaciones: boolean = false;

  @Input('childToMaster') reloadVinculado: boolean;

  displayedColumns: string[] = [
    "name",
    "documentType",
    "documentNumber",
    "id"
  ];
  dataSource: MatTableDataSource<ListBeneficiarios>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  selectCriterio: Criterio[] = [
    { value: "documento", viewValue: "Número de identificación" },
    { value: "nombre", viewValue: "Nombres y/o apellidos" }
  ];

  public page: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPages: number = 1; //Número total de páginas 
  public numElementos: number = 10; //Total de elemento por pagina
  public request: any;

  constructor(
    private _dataService: DataService,
    private _iconService: IconService,
    private _storageData: StorageDataService,
    private _authService: AuthService,        
    private _router: Router,
    private _util: Utils,
    public fb: FormBuilder
  ) {
    this.buscarBeneficiarioForm = this.fb.group({
      selectCriterio: new FormControl("", [Validators.required]),
      argumento: new FormControl("", Validators.required)
    });
  }

  ngOnInit() {

    this._iconService.registerIcons();
    this.listBeneficiario = new Array();
    this.dataNotFound = false;
    this.dataFound = false;
    this.detalleBeneficiario = false;
    this.loadPermisos();
    if (this._storageData.getSwInicio()) {
      this._router.navigate(['accionfiduciaria']);
    }

  }

  private loadPermisos() {
    this.consultar_vinculado = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'consultar_vinculado');
    this.datos_generales = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'datos_generales');
    this.negocios_asociados = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'negocios_asociados');
    this.historial_modificaciones = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'historial_modificaciones');
    this._storageData.setPDatosGenerales(this.datos_generales);
  }

  public resetTables() {
    this.dataNotFound = false;
    this.dataFound = false;
    this.detalleBeneficiario = false;
    this.beneficiarios = null;
    this.buscarBeneficiarioForm.controls['argumento'].setValue('');
  }

  public verPestanas() {
    if (this.datos_generales || this.negocios_asociados || this.historial_modificaciones) {
      return true;
    }
    return false;
  }

  get f() {
    return this.buscarBeneficiarioForm.controls;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  enviarDatos(): void {
    this.dataNotFound = false;
    this.dataFound = false;
    this.submitted = true;
    this.detalleBeneficiario = false;
    if (this.buscarBeneficiarioForm.invalid) {
      return;
    }

    if (this.buscarBeneficiarioForm.value.selectCriterio) {
      this.request = {
        argumento: this.buscarBeneficiarioForm.value.argumento,
        criterioBuscar: this.buscarBeneficiarioForm.value.selectCriterio
      }
    }
    let requestPaginado: any = this.request;
    requestPaginado.pagina = 1;
    requestPaginado.elementos = this.numElementos;
    this.buscarBeneficiarioList(this.request);
  }

  getBeneficiarioDetalle(documentNumber) {
    this._dataService
      .getBeneficiarioDetailsRequest(documentNumber)
      .subscribe((data: any) => {
        if (!data) {
          this.dataNotFound = true;
          this.dataFound = false;
          this.detalleBeneficiario = false;
          return;
        }
        this.dataFound = false;
        this.dataNotFound = false;
        this.detalleBeneficiario = true;
        this._storageData.setInfoBeneficiario(data);
        this._router.navigate(['accionfiduciaria/vinculados/detalles']);
      },
        (err) => {
        
            this.dataFound=false;
            //this.dataNotFound = true;
            this.errorServer = err.error;   

        }
      );
  }

  goToPage(page: number) {
    this.page = page;
    let requestPaginado: any = this.request;
    requestPaginado.pagina = page;
    requestPaginado.elementos = this.numElementos;
    this.buscarBeneficiarioList(requestPaginado);
  }

  buscarBeneficiarioList(requestBuscar) {
    this.beneficiarios = null;
    this._dataService
      .getBeneficiarioListRequest(requestBuscar)
      .subscribe((data: any) => {
        if (!data) {
          this.dataNotFound = true;
          this.dataFound = false;
          this.detalleBeneficiario = false;
          return;
        }
        this.dataFound = true;
        this.dataNotFound = false;
        this.detalleBeneficiario = false;
        this.beneficiarios = data;

        setTimeout(() => {
          this.loadBeneficiarios(this.beneficiarios['beneficiarios']);
          this.dataSource = new MatTableDataSource(this.listBeneficiario);

          this.resultsLength = data.totalbeneficiarios;
          // this.totalPages = Math.round(this.resultsLength / this.numElementos);
          // this.totalPages = (this.totalPages < 1) ? 1 : this.totalPages;

          this.totalPages = this.resultsLength / this.numElementos;          
          let totalPagina = this.resultsLength % this.numElementos;
          if (totalPagina > 1) {
            this.totalPages = Number(parseInt(String(this.totalPages))) + 1;
          }
          if (this.totalPages < 1) {
            this.totalPages = 1;
          }   

          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        });
      },
        (err) => {
          this.dataNotFound = true;
        }
      );
  }

  loadBeneficiarios(beneficiarios): void {
    this.listBeneficiario = [];

    beneficiarios.forEach(beneficiario => {
      this.listBeneficiario.push(beneficiario);
    });
  }
}
