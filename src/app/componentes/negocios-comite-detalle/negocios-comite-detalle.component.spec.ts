import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NegociosComiteDetalleComponent } from './negocios-comite-detalle.component';

describe('NegociosComiteDetalleComponent', () => {
  let component: NegociosComiteDetalleComponent;
  let fixture: ComponentFixture<NegociosComiteDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NegociosComiteDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NegociosComiteDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
