import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import moment from 'moment';
import { FileUploadModel } from 'src/app/modelo/list-cotitulares';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Utils } from 'src/app/core/utils';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-negocios-comite-detalle',
  templateUrl: './negocios-comite-detalle.component.html',
  styleUrls: ['./negocios-comite-detalle.component.css']
})

/**
* Autor:Jhon Rincon
* Proposito: adición de comités
de comité.
* Fecha: Abril 2020
*/
export class NegociosComiteDetalleComponent implements OnInit {

  public placeholderFechaReunion: string;
  public placeholderHoraReunion: string;
  public placeholderLugarReunion: string;
  public placeholderQuorumAprobatorio: string;
  public placeholderResponsable: string;
  public placeholderDecisionesTomada: string;
  public placeholderTareaPendiente: string;
  public placeholderNombre: string;
  public placeholderRol: string;
  public placeholderRolFuncionario: string;
  public placeholderNumeroAsistentes: string;
  public placeholderNumeroRadicado: string;
  public placeholderTipoArchivo: string;
  public dynamicForm: FormGroup;
  public seedDataDecisionTomada: any[];
  public seedDataTareaPendiente: any[];
  public seedDataMesaDirectiva: any[];
  public seedDataExpositor: any[];
  public mesaDirectiva: any[];
  public listExpositor: any[];
  public date: moment.Moment;
  public dateControl = new FormControl(moment());
  public files: Array<FileUploadModel> = [];
  public filesNull = true;
  uploadResponse = { status: '', message: '', filePath: '' };
  public fileUpload: any;
  public nombreArchivo: string;
  public guardarBotton = false;
  private codigoSFC: string = '';
  public asamblea_agregar: boolean = false;
  public fechaLimite = new Date();
  public infoComite: any;
  //base64s
  archivoBase64: string;
  checked: boolean;
  asistentes = [];

  @Input() accept = 'application/pdf, application/vnd.ms-powerpoint';
  @Output() complete = new EventEmitter<string>();

  @ViewChild('picker', { static: false }) picker: any;
  tiposDocumento: any;
  placeholdernumeroDocumento: any;
  placeholderAsistentes: any;
  displayedColumns: string[] = [
    "name"
  ];

  dataSource: MatTableDataSource<any>;
  constructor(
    public fb: FormBuilder,
    private httpClient: HttpClient,
    private datePipe: DatePipe,
    public dialog: MatDialog,
    public _dataService: DataService,
    private _storageData: StorageDataService,
    public _util: Utils,
    private _authService: AuthService
  ) {
  }

  ngOnInit() {
    this.codigoSFC = this._storageData.getcodigoSFCNegocio();
    this.getDataJson();
    this.date = null;
    this.dynamicForm = this.fb.group({
      decisionTomada: this.fb.array([]),
      tareaPendiente: this.fb.array([]),
      asistentes: this.fb.array([]),
      codigoSFC: new FormControl(""),
      temasTratados: new FormControl(""),
      fechaHoraReunion: new FormControl(""),
      direccionReunion: new FormControl(""),
      ordenDia: new FormControl(""),
      nombreArchivo: new FormControl(""),
      archivo: new FormControl(""),
      numeroRadicado: new FormControl("", [Validators.required, Validators.maxLength(30)])
    });


    this.loadPermisos();
    this.cargarServicio();
  }

  private cargarServicio() {
    let infoReunion = this._storageData.getInfoReunion();
    this._dataService.getObtenerInfoComite(infoReunion.codigoSFC, infoReunion.fecha, infoReunion.numeroRadicado)
      .subscribe((data: any) => {
        if (!data) {
          return;
        }
        this.infoComite = data;
        this.loadDetalle();
      },
        (err) => {
        }
      );
  }

  loadDetalle() {
    this.codigoSFC = this.infoComite.codigoSFC;
    this.dynamicForm.get('codigoSFC').setValue(this.infoComite.codigoSFC);
    this.dynamicForm.get('fechaHoraReunion').setValue(this.infoComite.fechaHoraReunion);
    this.dynamicForm.get('direccionReunion').setValue(this.infoComite.direccionReunion);
    this.dynamicForm.get('ordenDia').setValue(this.infoComite.ordenDia);
    this.dynamicForm.get('nombreArchivo').setValue(this.infoComite.nombreArchivo);
    this.filesNull = false;
    this.dynamicForm.get('numeroRadicado').setValue(this.infoComite.numeroRadicado);
    this.dynamicForm.get('archivo').setValue(this.infoComite.archivo);
    this.dynamicForm.get('temasTratados').setValue(this.infoComite.temasTratados);

    this.infoComite.decisiones.filter(element => element.tipo === true).forEach(elemento => {
      var responsablesDecision = [];
      elemento.responsables.forEach(responsable => {
        responsablesDecision.push(this.crearPersonaResponsableResult(responsable.nomResponsable, responsable.tipoDocumento, responsable.numeroDocumento));
      });
      this.decisionTomadaFormArray.push(this.createGroupResult(elemento.descripcion, responsablesDecision));
    });

    this.infoComite.decisiones.filter(element => element.tipo === false).forEach(elemento => {
      var responsablesPendientes = [];
      elemento.responsables.forEach(responsable => {
        responsablesPendientes.push(this.crearPersonaResponsableResult(responsable.nomResponsable, responsable.tipoDocumento, responsable.numeroDocumento));
      });
      this.tareaPendienteFormArray.push(this.createGroupResult(elemento.descripcion, responsablesPendientes));
    });

    this.dataSource = new MatTableDataSource(this.infoComite.asistentes);
  }

  private loadPermisos() {
    this.asamblea_agregar = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'asamblea_agregar');
  }

  closePicker() {
    this.picker.cancel();
  }

  public guardarAsistente(asistente) {
    let asistent = {
      nombreAsistente: asistente.nombre,
      tipoDocumento: asistente.tipoDocumento,
      numeroDocumento: asistente.numDocumento
    }
    this.asistentes.push(asistent);
    this.asistentes = this.borrarObjetosRepetidos(this.asistentes);
  }

  private borrarObjetosRepetidos(arregloConRepetidos) {
    let sinRepetidos = arregloConRepetidos.filter((valorActual, indiceActual, arreglo) => {
      return arreglo.findIndex(
        valorDelArreglo => JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)
      ) === indiceActual
    });
    return sinRepetidos;
  }

  public cargarServiciosDominio() {
    this._dataService
      .getParametrosValoresDominio("TIP_DOC")
      .subscribe((tiposDocumento: any) => {
        this.tiposDocumento = tiposDocumento;
      });
    this._dataService.getObtenerInformacionComite(this.codigoSFC).subscribe((dataResultado: any) => {
      this.dataSource = new MatTableDataSource(dataResultado);
    });
  }

  public addResponsable(ix) {
    const control = (<FormArray>this.dynamicForm.controls["decisionTomada"])
      .at(ix)
      .get("responsables") as FormArray;
    control.push(this.crearPersonaResponsable());
  }

  public addResponsableTarea(ix) {
    const control = (<FormArray>this.dynamicForm.controls["tareaPendiente"])
      .at(ix)
      .get("responsables") as FormArray;
    control.push(this.crearPersonaResponsable());
  }

  get decisionTomadaFormArray() {
    return <FormArray>this.dynamicForm.get("decisionTomada");
  }

  get tareaPendienteFormArray() {
    return <FormArray>this.dynamicForm.get("tareaPendiente");
  }

  public createGroup() {
    return this.fb.group({
      descripcion: ['', []],
      responsables: this.fb.array([this.crearPersonaResponsable()])
    });
  }

  public createGroupResult(descripcionInfo, responsablesInfo) {
    return this.fb.group({
      descripcion: [descripcionInfo, []],
      responsables: this.fb.array(responsablesInfo)
    });
  }

  public crearPersonaResponsable() {
    return this.fb.group({
      nomResponsable: ["", Validators.compose([])],
      tipoDocumento: ["", Validators.compose([])],
      numeroDocumento: ["", [Validators.required, Validators.maxLength(30), Validators.minLength(4)]]
    });
  }

  public crearPersonaResponsableResult(nombre, tipoDocumento, numeroDocumento) {
    return this.fb.group({
      nomResponsable: [nombre],
      tipoDocumento: [tipoDocumento],
      numeroDocumento: [numeroDocumento]
    });
  }

  getDataJson() {
    this.httpClient.get("assets/i18n/es.json").subscribe(data => {
      this.placeholderFechaReunion = data['comiteNuevaEntrada'].fecha_reunión;
      this.placeholderHoraReunion = data['comiteNuevaEntrada'].hora_reunion;
      this.placeholderLugarReunion = data['comiteNuevaEntrada'].lugar_reunion;
      this.placeholderDecisionesTomada = data['comiteNuevaEntrada'].decisiones_tomadas;
      this.placeholderTareaPendiente = data['comiteNuevaEntrada'].tareas_pendientes;
      this.placeholderResponsable = data['comiteNuevaEntrada'].responsable;
      this.placeholdernumeroDocumento = data['comiteNuevaEntrada'].numeroDocumento;
      this.placeholderQuorumAprobatorio = data['comiteNuevaEntrada'].quorum_aprobatorio;
      this.placeholderNombre = data['comiteNuevaEntrada'].nombres;
      this.placeholderRol = data['comiteNuevaEntrada'].rol_tercero;
      this.placeholderRolFuncionario = data['comiteNuevaEntrada'].rol_funcionario;
      this.placeholderNumeroAsistentes = data['comiteNuevaEntrada'].numero_asistentes;
      this.placeholderNumeroRadicado = data['comiteNuevaEntrada'].numero_radicado;
      this.placeholderTipoArchivo = data['comiteNuevaEntrada'].tipo_archivo;
      this.placeholderAsistentes = data['comiteNuevaEntrada'].asistentes;
    })
  }

  descargarArchivo() {
    const enlace = document.createElement("a");
    enlace.href = 'data:application/octet-stream;base64,' + this.dynamicForm.value.archivo;
    enlace.download = this.dynamicForm.value.nombreArchivo;
    enlace.click();
  }
}
