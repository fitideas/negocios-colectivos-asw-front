import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComiteNuevaEntradaComponent } from './comite-nueva-entrada.component';

describe('ComiteNuevaEntradaComponent', () => {
  let component: ComiteNuevaEntradaComponent;
  let fixture: ComponentFixture<ComiteNuevaEntradaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComiteNuevaEntradaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComiteNuevaEntradaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
