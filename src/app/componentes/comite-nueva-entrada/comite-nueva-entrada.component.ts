import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { trigger, state, transition, animate, style } from '@angular/animations';
import { FileUploadModel } from 'src/app/modelo/list-cotitulares';
import { MatDialog,MatTableDataSource } from '@angular/material';
import { DataService } from 'src/app/services/data.service';
import { DialogParametrosSaveComponent } from '../dialog-parametros-save/dialog-parametros-save.component';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { AuthService } from 'src/app/services/auth.service';
import { Utils } from 'src/app/core/utils';
import { DialogTwoButtonsComponent } from '../dialog-two-buttons/dialog-two-buttons.component';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-comite-nueva-entrada',
  templateUrl: './comite-nueva-entrada.component.html',
  styleUrls: ['./comite-nueva-entrada.component.css'],
  animations: [
    trigger('fadeInOut', [
      state('in', style({ opacity: 100 })),
      transition('* => void', [
        animate(300, style({ opacity: 0 }))
      ])
    ])
  ]
})

/**
 * @Autor: Lizeth Patiño Manrique
 * @Fecha: 19/04/2020
 * @Proposito: clase para realizar el registro de un comité.
 */
export class ComiteNuevaEntradaComponent implements OnInit {
  public placeholderFechaReunion: string;
  public placeholderHoraReunion: string;
  public placeholderLugarReunion: string;
  public placeholderQuorumAprobatorio: string;
  public placeholderResponsable: string;
  public placeholderDecisionesTomada: string;
  public placeholderTareaPendiente: string;
  public placeholderNombre: string;
  public placeholderRol: string;
  public placeholderRolFuncionario: string;
  public placeholderNumeroAsistentes: string;
  public placeholderNumeroRadicado: string;
  public placeholderTipoArchivo: string;


  public dynamicForm: FormGroup;
  public seedDataDecisionTomada: any[];
  public seedDataTareaPendiente: any[];
  public seedDataMesaDirectiva: any[];
  public seedDataExpositor: any[];
  public mesaDirectiva: any[];
  public listExpositor: any[];
  public date: moment.Moment;
  public dateControl = new FormControl(moment());
  public files: Array<FileUploadModel> = [];
  public filesNull = true;
  uploadResponse = { status: '', message: '', filePath: '' };
  public fileUpload: any;
  public nombreArchivo: string;
  public guardarBotton = false;
  private codigoSFC: string = '';
  public asamblea_agregar: boolean = false;
  public fechaLimite = new Date();
  //base64s
  archivoBase64: string;
  checked: boolean;
  asistentes = [];

  @Input() accept = 'application/pdf, application/vnd.ms-powerpoint';
  /** Allow you to add handler after its completion. Bubble up response text from remote. */
  @Output() complete = new EventEmitter<string>();

  @ViewChild('picker', { static: false }) picker: any;
  tiposDocumento: any;
  placeholdernumeroDocumento: any;
  placeholderAsistentes: any;
  displayedColumns: string[] = [
    "select",
    "name",
  ];

  dataSource: MatTableDataSource<any>;
  asistentesArray: any;
 


  constructor(
    public fb: FormBuilder,
    private httpClient: HttpClient,
    private datePipe: DatePipe,
    public dialog: MatDialog,
    public _dataService: DataService,
    private _storageData: StorageDataService,
    public _util: Utils,
    private _authService: AuthService
  ) {
  }

  ngOnInit() {
    this.codigoSFC = this._storageData.getcodigoSFCNegocio();
    this.cargarServiciosDominio();
    this.getDataJson();
    this.date = null;
    this.dynamicForm = this.fb.group({
      decisionTomada: this.fb.array([]),
      tareaPendiente: this.fb.array([]),
      asistentes: new FormControl(""),
      codigoSFC: new FormControl(""),
      temasTratados:new FormControl("", [Validators.required]),
      fechaHoraReunion: new FormControl("", [Validators.required]),
      direccionReunion: new FormControl("", [Validators.required]),
      ordenDia: new FormControl("", [Validators.required]),
      nombreArchivo: new FormControl("", [Validators.required]),
      archivo: new FormControl(""),
      numeroRadicado: new FormControl("",[Validators.required, Validators.maxLength(30)])
    });
  

    // this.addToDecisionTomadaFormArray();
    // this.addToTareaPendienteFormArray();
    //this.addAsistentesFormArray();
    this.loadPermisos();

  }

  private loadPermisos() {
    this.asamblea_agregar = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'asamblea_agregar');
  }

  closePicker() {
    this.picker.cancel();
  }

 public guardarAsistente(asistente, checked){
  if (checked) {
    let asistent={
      nombreAsistente:asistente.nombre,
      tipoDocumento: asistente.tipoDocumento,
      numeroDocumento: asistente.numDocumento
    }
   this.asistentes.push(asistent);
  }
  else {    
    for (var i =0; i < this.asistentes.length; i++){
      if (this.asistentes[i].tipoDocumento === asistente.tipoDocumento && this.asistentes[i].numeroDocumento === asistente.numDocumento) {
          this.asistentes.splice(i,1);
      }
   }
  } 
 }


 private borrarObjetosRepetidos(arregloConRepetidos) {
  let sinRepetidos = arregloConRepetidos.filter((valorActual, indiceActual, arreglo) => {
    return arreglo.findIndex(
      valorDelArreglo => JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)
    ) === indiceActual
  });
  return sinRepetidos;
}
  
  public cargarServiciosDominio(){
    this._dataService
    .getParametrosValoresDominio("TIP_DOC")
    .subscribe((tiposDocumento: any) => {
      this.tiposDocumento = tiposDocumento;
    });
    this._dataService.getObtenerInformacionComite(this.codigoSFC).subscribe((dataResultado: any) => {
      this.asistentesArray=[];
      dataResultado.forEach(asistente => {
        if(asistente['agregado']===true){
          this.asistentesArray.push(asistente);
          this.dataSource = new MatTableDataSource(this.asistentesArray);
        }
      });
     
      
    });
  }

  public deleteresponsable(idxtipNeg, idxGasto) {
    let data;
    data = {
      titulo: "Eliminar Responsable",
      contenido: "Se esta intentando eliminar un responsable ¿Está seguro?",
      buttonContinuar: "Continuar",
      buttonCancelar: "Cancelar",
      urlimg: true
    };
    const dialogRef = this.dialog.open(DialogTwoButtonsComponent, {
      width: "42rem",
      data
    });

    dialogRef.afterClosed().subscribe(async resultado => {
      switch (resultado) {
        case "Continuar":
          let gastosA = (<FormArray>this.dynamicForm.controls["decisionTomada"]).at(
            idxtipNeg
          ) as FormArray;
          gastosA.controls["responsable"].removeAt(idxGasto);
          break;
        case "Cancelar":
      }
    });
  }

  public addResponsable(ix) {
    const control = (<FormArray>this.dynamicForm.controls["decisionTomada"])
      .at(ix)
      .get("responsables") as FormArray;
    control.push(this.crearPersonaResponsable());
  }

  public addResponsableTarea(ix) {
    const control = (<FormArray>this.dynamicForm.controls["tareaPendiente"])
      .at(ix)
      .get("responsables") as FormArray;
    control.push(this.crearPersonaResponsable());
  }


  get decisionTomadaFormArray() {
    return <FormArray>this.dynamicForm.get("decisionTomada");
  }

  get tareaPendienteFormArray() {
    return <FormArray>this.dynamicForm.get("tareaPendiente");
  }

  public createGroup() {
    return this.fb.group({
      descripcion: ['', Validators.compose([Validators.required])],
      responsables: this.fb.array([this.crearPersonaResponsable()])
    });
  }

  public crearPersonaResponsable(){
    return this.fb.group({
      nomResponsable: ["", Validators.compose([Validators.required])],
      tipoDocumento: ["", Validators.compose([Validators.required])],
      numeroDocumento: ["", Validators.compose([Validators.required, Validators.maxLength(30), Validators.minLength(4)])]
    });
  }

  addToDecisionTomadaFormArray() {
    this.decisionTomadaFormArray.push(this.createGroup());
  }

  public removeFromDecisionTomadaFormArray(index) {
    let data;
    data = {
      titulo: "Eliminar Decisión tomada",
      contenido: "Se esta intentando eliminar una decisión tomada ¿Está seguro?",
      buttonContinuar: "Continuar",
      buttonCancelar: "Cancelar",
      urlimg: true
    };
    const dialogRef = this.dialog.open(DialogTwoButtonsComponent, {
      width: "42rem",
      data
    });

    dialogRef.afterClosed().subscribe(async resultado => {
      switch (resultado) {
        case "Continuar":
          this.decisionTomadaFormArray.removeAt(index);
          break;
        case "Cancelar":
      }
    });
   
  }

  addToTareaPendienteFormArray() {
    this.tareaPendienteFormArray.push(this.createGroup());
  }

  removeFromTareaPendienteFormArray(index) {
    let data;
    data = {
      titulo: "Eliminar tarea pendiente",
      contenido: "Se esta intentando eliminar una tarea pendiente ¿Está seguro?",
      buttonContinuar: "Continuar",
      buttonCancelar: "Cancelar",
      urlimg: true
    };
    const dialogRef = this.dialog.open(DialogTwoButtonsComponent, {
      width: "42rem",
      data
    });

    dialogRef.afterClosed().subscribe(async resultado => {
      switch (resultado) {
        case "Continuar":
          this.tareaPendienteFormArray.removeAt(index);
          break;
        case "Cancelar":
      }
    });
    
  }


  getDataJson() {
    this.httpClient.get("assets/i18n/es.json").subscribe(data => {
      this.placeholderFechaReunion = data['comiteNuevaEntrada'].fecha_reunión;
      this.placeholderHoraReunion = data['comiteNuevaEntrada'].hora_reunion;
      this.placeholderLugarReunion = data['comiteNuevaEntrada'].lugar_reunion;
      this.placeholderDecisionesTomada = data['comiteNuevaEntrada'].decisiones_tomadas;
      this.placeholderTareaPendiente = data['comiteNuevaEntrada'].tareas_pendientes;
      this.placeholderResponsable = data['comiteNuevaEntrada'].responsable;
      this.placeholdernumeroDocumento = data['comiteNuevaEntrada'].numeroDocumento;
      this.placeholderQuorumAprobatorio = data['comiteNuevaEntrada'].quorum_aprobatorio;
      this.placeholderNombre = data['comiteNuevaEntrada'].nombres;
      this.placeholderRol = data['comiteNuevaEntrada'].rol_tercero;
      this.placeholderRolFuncionario = data['comiteNuevaEntrada'].rol_funcionario;
      this.placeholderNumeroAsistentes = data['comiteNuevaEntrada'].numero_asistentes;
      this.placeholderNumeroRadicado = data['comiteNuevaEntrada'].numero_radicado;
      this.placeholderTipoArchivo = data['comiteNuevaEntrada'].tipo_archivo;
      this.placeholderAsistentes = data['comiteNuevaEntrada'].asistentes;
    })
  }


  handleInputChange(files) {
    let file = files;
    let reader = new FileReader();
    if (!file.type.match('application/pdf') && !file.type.match('application/vnd.ms-powerpoint')) {
      alert('invalid format');
      return;
    }
    reader.onloadend = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }

  _handleReaderLoaded(e) {
    let reader = e.target;
    var base64result = reader.result.substr(reader.result.indexOf(',') + 1);
    this.archivoBase64 = base64result;
  }


  subirArchivo() {
    this.fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
    this.fileUpload.onchange = () => {
      for (let index = 0; index < this.fileUpload.files.length; index++) {
        const file = this.fileUpload.files[index];
        this.handleInputChange(file); //turn into base64
        this.nombreArchivo = file.name;
        this.files[0] = {
          data: file, state: 'in',
          inProgress: false, progress: 0, canLoad: true, canRetry: false, canCancel: true
        };
        this.filesNull = false;
        this.dynamicForm.patchValue({
          fechaHoraReunion: this.datePipe.transform(this.dateControl.value._d, 'dd/MM/yyyy HH:mm:ss'),
          nombreArchivo: this.nombreArchivo,
          codigoSFC: this.codigoSFC,
          archivo: this.archivoBase64
        });
      }
    };
    this.fileUpload.click();
    this.guardarBotton = true;

  }

  openDialog(ok, context): void {
    let data;
    if (ok) {
      data = {
        titulo: "Datos guardados",
        contenido: "El registro del comité han sido guardada de manera exitosa.",
        button: "ACEPTAR",
        urlimg: false
      };
    }
    if (!ok) {
      let contenido = "El registro del comité no se pueden guardar. Intente más tarde.";
      if (context != null) {
        contenido = context;
      }
      data = {
        titulo: "Datos no guardados",
        contenido: contenido,
        button: "ACEPTAR",
        urlimg: true
      };
    }
    const dialogRef = this.dialog.open(DialogParametrosSaveComponent, {
      width: "40rem",
      data
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  removeItemFromArr = (arr, item) => {
    return arr.filter(e => e.numeroDocumento !== item);
  };


  public guardarComite() {
    this.dynamicForm.patchValue({
      fechaHoraReunion: this.datePipe.transform(this.dateControl.value._d, 'dd/MM/yyyy HH:mm:ss'),
      nombreArchivo: this.nombreArchivo,
      codigoSFC: this.codigoSFC,
      archivo: this.archivoBase64,
      asistentes: this.asistentes
    });
    this._dataService.negociosRegistrarComite(this.dynamicForm.value)
      .subscribe(
        (data: any) => {
          this.uploadResponse = data;
          this.openDialog(true, null);
        },
        (err) => {

          this.fileUpload.inProgress = false;
          this.fileUpload.canRetry = true;
          this.fileUpload.canLoad = false;
          this.openDialog(false, null);
        }
      );

  }

}
