import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, FormArray, Validators } from "@angular/forms";
import {
  MatDialog
} from "@angular/material";
import { FormControl } from "@angular/forms";

import { takeUntil } from "rxjs/operators";
import { ReplaySubject, Subject } from "rxjs";
import { DataService } from "src/app/services/data.service";
import { ParametroSimple, Valores } from "src/app/modelo/parametro-simple";
import {
  ParametroCompuesto,
  RolTerceroComite,
  Compuesto,
  ValoresCompuesto,
  DetalleValorCompuesto,
  IdRolTercerosComite
} from "src/app/modelo/parametro-compuesto";
import { DialogParametrosSaveComponent } from "../dialog-parametros-save/dialog-parametros-save.component";
import { AuthService } from 'src/app/services/auth.service';
import { Utils } from 'src/app/core/utils';
import { DialogThreeButtonsComponent } from '../dialog-three-buttons/dialog-three-buttons.component';
import { DialogReemplazarParametrosComponent } from '../dialog-reemplazar-parametros/dialog-reemplazar-parametros.component';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Router } from '@angular/router';
import { CurrencyPipe } from '@angular/common';

interface Retencion {
  id: string;
  name: string;
  value: string;
}

@Component({
  selector: "app-parametros",
  templateUrl: "./parametros.component.html",
  styles: ["src/styles.css"],
  styleUrls: ['./parametros.component.scss']
})
/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Marzo 2020
 * @Proposito: clase para gestionar parametros de sistema.
 */
export class ParametrosComponent implements OnInit {

  /** control for the MatSelect filter keyword multi-selection */
  public retencionMultiFilterCtrl: FormControl = new FormControl();
  public crearCosto = [];
  public crearRetencion = [];
  public crearNegocio = [];
  public crearFechasClaves = [];
  public crearRoles = [];

  public checked = false;
  public indeterminate = false;
  public labelPosition = "after";
  public disabled = false;

  public unidadCostoList = [
    { name: "Selecciona", value: null },
    { name: "%", value: '%' },
    { name: "COP", value: "COP" }
  ];
  public unidadRetencionList = [
    { name: "Selecciona", value: null },
    { name: "%", value: '%' },
    { name: "COP", value: "COP" }
  ];

  public seedDataRetencion: Valores[];
  public seedDataCosto: Valores[];
  public seedDataFechaClave: Valores[];
  public seedDataNegocio: ParametroCompuesto[] = [];
  public seedDataNaturalezaJuridica: ParametroCompuesto[] = [];
  public seedDataTipoGasto: ParametroCompuesto[] = [];
  public seedDataRol: RolTerceroComite[] = [];
  public retencionesMulti: Retencion[] = [];
  public retencionSent: Retencion[] = [];
  public dynamicForm: FormGroup;
  public dynamicFormNegocios: FormGroup;
  public dynamicFormTercero: FormGroup;
  public idRolTercero: IdRolTercerosComite;
  public filteredRetencionesMulti: ReplaySubject<
    Retencion[]
  > = new ReplaySubject<Retencion[]>(1);
  private _onDestroy = new Subject<void>();

  public parametros_generales: boolean = false;
  public tipo_negocio_gasto: boolean = false;
  public roles_terceros: boolean = false;
  public parametros_generales_escribe: boolean = false;
  public roles_terceros_escribe: boolean = false;
  public tipo_negocio_gasto_escribe: boolean = false;
  public cantidadNaturezaRepetida: boolean = false;
  public cantidadRetencionRepetida: boolean = false;
  public cantidadTipoNegocioRepetida: boolean = false;
  public cantidadTipoGastoRepetida: boolean = false;
  
  constructor(
    private fb: FormBuilder,
    private _dataService: DataService,
    public dialog: MatDialog,
    private _authService: AuthService,
    public _util: Utils,
    private _router: Router,
    private _storageService: StorageDataService,
    private currencyPipe: CurrencyPipe,
  ) { }

  ngOnInit() {
    if (this._storageService.getSwInicio()) {
      this._router.navigate(['accionfiduciaria']);
    }
    this.loadPermisos();
    this.loadInfo();

    this.dynamicForm = this.fb.group({
      costosPago: this.fb.array([]),
      retenciones: this.fb.array([]),
      fechasClave: this.fb.array([])
    });

    this.dynamicFormNegocios = this.fb.group({
      negocios: this.fb.array([]),
      naturalezaJuridica: this.fb.array([]),
      tipoGasto: this.fb.array([])
    });

    this.dynamicFormTercero = this.fb.group({
      roles: this.fb.array([])
    });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private resetInfo() {
    this.seedDataRetencion = [];
    this.seedDataCosto = [];
    this.seedDataFechaClave = [];
    this.seedDataNegocio = [];
    this.seedDataNaturalezaJuridica = [];
    this.seedDataTipoGasto = [];
    this.seedDataRol = [];

    this.costosPagoFormArray.clear();
    this.retencionFormArray.clear();
    this.fechaClaveFormArray.clear();
    this.negocioFormArray.clear();
    this.naturalezaJuridicaFormArray.clear();
    this.tipoGastoFormArray.clear();
    this.rolFormArray.clear();

    this.costosPagoFormArray.reset();
    this.retencionFormArray.reset();
    this.fechaClaveFormArray.reset();
    this.negocioFormArray.reset();
    this.naturalezaJuridicaFormArray.reset();
    this.tipoGastoFormArray.reset();
    this.rolFormArray.reset();

  }
  private obnenerDominioRoles() {
    let tipoDominio = 'ROL';
    let idComite = null;
    let idTercero = null;
    this._dataService.getParametrosValoresDominio(tipoDominio).subscribe(
      (data: any) => {
        if (!data) {
          return;
        }       
        data.forEach(item => {
          if (item.descripcion == 'tercero'){
            idTercero = item.id;
          }
          if (item.descripcion == 'comite') {
            idComite = item.id;
          }
         this.idRolTercero = {
            idComite: idComite,
            idTercero: idTercero
          }          
        });
      },
      err => {
      }
    );
  }
  private loadInfo() {
    this.obtenerRetenciones();
    this.obnenerDominioRoles();    
    this._dataService.getParametrosSimplesRequest().subscribe(
      (data: any) => {
        if (!data) {
          return;
        }
        data.forEach(item => {
          if (item.codigo == "RET") {
            this.seedRetencionFormArray(item.valores);
          }
          if (item.codigo == "COSMEDPAG") {
            this.seedCostoPagosFormArray(item.valores);
          }
          if (item.codigo == "FECCLA") {
            this.seedDataFechaClaveFormArray(item.valores);
          }
        });
      },
      err => {
      }
    );

    this._dataService.getParametrosCompuestosRequest().subscribe(
      (data: any) => {
        if (!data) {
          return;
        }
        data.forEach(item => {
          if (item.codigo == "TIPNEG") {
            this.seedDataNegocio = this.seedFormArray(item);
            this.seedNegociosFormArray();
          }
          if (item.codigo == "TIPNATJUR") {
            this.seedDataNaturalezaJuridica = this.seedFormArray(item);
            this.seedNaturalezaJuridicaFormArray();
          }
          if (item.codigo == "TIPGAS") {
            this.seedDataTipoGasto = this.seedFormArray(item);
            this.seedTipoGastoFormArray();
          }
          if (item.codigo == "ROLTER") {
            this.seedDataRol = this.seedFormArrayRol(item);
            this.seedDataRolFormArray();
          }
          this.filteredRetencionesMulti.next(this.retencionesMulti.slice());
          this.retencionMultiFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
              this.filterRetencionesMulti();
            });
        });
      },
      (err: any) => {
      }
    );
  }

  public onClickResetLoadInfo() {
    this.resetInfo();
    this.loadInfo();
  }

  private loadPermisos() {
    this.parametros_generales = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'parametros_generales');
    this.roles_terceros = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'roles_terceros');
    this.tipo_negocio_gasto = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'tipo_negocio_gasto');    
    this.parametros_generales_escribe = this._util.permiteEscribir(this._authService.getFuncionalidades(), 'parametros_generales');
    this.roles_terceros_escribe = this._util.permiteEscribir(this._authService.getFuncionalidades(), 'roles_terceros');
    this.tipo_negocio_gasto_escribe = this._util.permiteEscribir(this._authService.getFuncionalidades(), 'tipo_negocio_gasto');
  }

  private filterRetencionesMulti() {
    if (!this.retencionesMulti) {
      return;
    }
    // get the search keyword
    let search = this.retencionMultiFilterCtrl.value;
    if (!search) {
      this.filteredRetencionesMulti.next(this.retencionesMulti.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filtro de rentenciones
    this.filteredRetencionesMulti.next(
      this.retencionesMulti.filter(
        retencion => retencion.name.toLowerCase().indexOf(search) > -1
      )
    );
  }

  comparer(o1: any, o2: any): boolean {
    return o1 && o2 ? (o1.name === o2.descripcion && o2.valor == "true") : false;
  }

  seedCostoPagosFormArray(item: any) {
    this.seedDataCosto = null;
    this.seedDataCosto = item;
    this.seedDataCosto.forEach(seedDatum => {
      if (seedDatum.unidad == 'COP') {
        seedDatum.valor = this.currencyPipe.transform(seedDatum.valor);
      }
      const formGroup = this.createGroup();
      formGroup.patchValue(seedDatum);
      this.costosPagoFormArray.push(formGroup);
    });
  }

  seedRetencionFormArray(item: any) {
    this.seedDataRetencion = null;
    this.seedDataRetencion = item;
    this.seedDataRetencion.forEach(seedDatum => {
      if (seedDatum.unidad == 'COP') {
        seedDatum.valor = this.currencyPipe.transform(seedDatum.valor);
      }
      const formGroup = this.createGroup();
      formGroup.patchValue(seedDatum);
      this.retencionFormArray.push(formGroup);
    });
  }

  seedDataFechaClaveFormArray(item: any) {
    this.seedDataFechaClave = null;
    this.seedDataFechaClave = item;
    this.seedDataFechaClave.forEach(seedDatum => {
      const formGroup = this.createGroup();
      formGroup.patchValue(seedDatum);
      this.fechaClaveFormArray.push(formGroup);
    });
  }

  seedDataRolFormArray() {
    this.seedDataRol.forEach(seedDatum => {
      const formGroup = this.createRolGroup();
      formGroup.patchValue(seedDatum);
      this.rolFormArray.push(formGroup);
      this.idRolTercero = {
        idComite: seedDatum.idComite,
        idTercero: seedDatum.idTercero
      }
    });
  }

  compareObj(retencion) {
    let sw = false;
    this.retencionesMulti.forEach(element => {
      if (retencion.descripcion == element.name) {
        sw = true;
      }
    });
    return sw;
  }

  seedFormArrayRol(item: any) {
    let auxData: RolTerceroComite[] = [];
    item.valores.forEach(function (seedData, index) {
      let tercer = null;
      let comite = null;
      let idTercero = null;
      let idComite = null;
      seedData.valores.forEach(element => {
        if (element.descripcion == "tercero") {
          tercer = element.valor == "true" ? true : false;
          idTercero = element.id;
        }
        if (element.descripcion == "comite") {
          comite = element.valor == "true" ? true : false;
          idComite = element.id;
        }
      });
      auxData[index] = {
        rol: seedData.descripcion,
        tercero: tercer,
        comite: comite,
        idTercero: idTercero,
        idComite: idComite,
        idRol: seedData.id
      };
    });
    return auxData;
  }

  obtenerRetenciones() {
    let auxRet: Retencion;
    this.retencionesMulti = [];
    this._dataService.getParametrosValoresDominio('RET').subscribe(
      (data: any) => {
        if (data) {
          data.forEach(element => {
            if (this.retencionesMulti == [] || !this.compareObj(element)) {
              auxRet = {
                name: element.descripcion,
                value: null,
                id: element.id
              };
              this.retencionesMulti.push(auxRet);
              this.retencionSent.push(auxRet);
            }
          });
        }
      },
      (error: any) => { }
    );
  }

  seedFormArray(item: any) {
    let auxData: ParametroCompuesto[] = [];

    item.valores.forEach(seedDatum => {
      item.valores.forEach(function (seedData, index) {
        auxData[index] = {
          id: seedData.id,
          descripcion: seedData.descripcion,
          valores: seedData.valores
        };
      });
    });
    return auxData;
  }

  seedNegociosFormArray() {
    this.seedDataNegocio.forEach(seedDatum => {
      const formGroup = this.createCompuestoGroup();
      formGroup.patchValue(seedDatum);
      let valor = this.patchValues(seedDatum.id, seedDatum.descripcion, seedDatum.valores);
      this.negocioFormArray.push(valor);
    });
  }

  patchValues(idNegocio, negocioValue, retencionMultiCtrlValue) {
    return this.fb.group({
      id: [idNegocio],
      descripcion: [negocioValue],
      retencionMultiCtrl: [retencionMultiCtrlValue]
    });
  }

  seedNaturalezaJuridicaFormArray() {
    this.seedDataNaturalezaJuridica.forEach(seedDatum => {
      const formGroup = this.createCompuestoGroup();
      formGroup.patchValue(seedDatum);
      let valor = this.patchValues(seedDatum.id, seedDatum.descripcion, seedDatum.valores);
      this.naturalezaJuridicaFormArray.push(valor);
    });
  }

  seedTipoGastoFormArray() {
    this.seedDataTipoGasto.forEach(seedDatum => {
      const formGroup = this.createCompuestoGroup();
      formGroup.patchValue(seedDatum);
      let valor = this.patchValues(seedDatum.id, seedDatum.descripcion, seedDatum.valores);
      this.tipoGastoFormArray.push(valor);
    });
  }

  createGroup() {
    return this.fb.group({
      descripcion: ['', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(100)]],
      unidad: [],
      valor: ['', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(50)]],
      id: []
    });
  }

  createRolGroup() {
    return this.fb.group({
      rol: ['', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(100)]],
      tercero: [],
      comite: [],
      idTercero: [],
      idComite: [],
      idRol: []
    });
  }

  createCompuestoGroup() {
    return this.fb.group({
      id: [],
      descripcion: ['', Validators.required],
      retencionMultiCtrl: new FormControl()
    });
  }

  addToCostosPagoFormArray() {
    this.costosPagoFormArray.push(this.createGroup());
  }

  addToRentencionFormArray() {
    this.retencionFormArray.push(this.createGroup());
  }

  addToFechaClaveFormArray() {
    this.fechaClaveFormArray.push(this.createGroup());
  }

  addToRolFormArray() {
    this.rolFormArray.push(this.createRolGroup());
  }

  addToNegocioFormArray() {
    this.negocioFormArray.push(this.createCompuestoGroup());
  }

  addToNaturalezaJuridicaFormArray() {
    this.naturalezaJuridicaFormArray.push(this.createCompuestoGroup());
  }

  addToTipoGastoFormArray() {
    this.tipoGastoFormArray.push(this.createCompuestoGroup());
  }

  removeFromRetencionFormArray(index, itemRetencion) {
    if (itemRetencion.descripcion != "RETEFUENTE") {
      if (itemRetencion.id == null) {
        this.retencionFormArray.removeAt(index);
      } else {
        this.openDialogEliminar('retenciones', index, itemRetencion);
      }
      this.cantidadRetencionRepetida = false;
    }
  }

  removeFromFechaClaveFormArray(index, itemFechaClave) {
    if (itemFechaClave.id == null) {
      this.fechaClaveFormArray.removeAt(index);
    } else {
      this.openDialogEliminar('fechasClave', index, itemFechaClave);
    }
  }

  removeFromRolFormArray(index, itemRol) {
    if (itemRol.idRol == null) {
      this.rolFormArray.removeAt(index);
    } else {
      this.openDialogEliminar('roles', index, itemRol);
    }
  }

  removeFromNegocioFormArray(index, itemNegocio) {
    this.cantidadTipoNegocioRepetida = false;
    if (itemNegocio.id == null) {
      this.negocioFormArray.removeAt(index);
    } else {
      let parametroReemplazo = 'TIPNEG';
      this.openDialogReemplazar(itemNegocio, parametroReemplazo, index);
    }
  }

  removeFromNaturalezaJuridicaFormArray(index, itemNaturaleza) {
    this.cantidadNaturezaRepetida = false;
    if (itemNaturaleza.id == null) {
      this.naturalezaJuridicaFormArray.removeAt(index);
    }
    else {
      if ((itemNaturaleza.descripcion != "NATURAL") && (itemNaturaleza.descripcion != "JURIDICA")) {
      let parametroReemplazo = 'TIPNATJUR';
      this.openDialogReemplazar(itemNaturaleza, parametroReemplazo, index);
      }
    }
  }

  removeFromTipoGastoFormArray(index, itemTipoGasto) {
    this.cantidadTipoGastoRepetida = false;
    if (itemTipoGasto.id == null) {
      this.tipoGastoFormArray.removeAt(index);
    }
    else {
      this.openDialogEliminar('tipoGasto', index, itemTipoGasto);
    }
  }

  validarRetencionDescripcion(descripcion) {
    let cantidadRepetido = 0;
    this.retencionFormArray.value.forEach(element => {
      if (element.descripcion == descripcion) {
        cantidadRepetido++;
      }
    });
    if (cantidadRepetido > 1) {
      this.cantidadRetencionRepetida = true; 
    } else {
      this.cantidadRetencionRepetida = false;
    }
  }

  validarTipoGastoDescripcion(descripcion) {
    let cantidadRepetido = 0;
    this.tipoGastoFormArray.value.forEach(element => {
      if (element.descripcion == descripcion) {
        cantidadRepetido++;
      }
    });
    if (cantidadRepetido > 1) {
      this.cantidadTipoGastoRepetida = true; 
    } else {
      this.cantidadTipoGastoRepetida = false;
    }
  }
  
  validarTipoNegocioDescripcion(descripcion) {
    let cantidadRepetido = 0;
    this.negocioFormArray.value.forEach(element => {
      if (element.descripcion == descripcion) {
        cantidadRepetido++;
      }
    });
    if (cantidadRepetido > 1) {
      this.cantidadTipoNegocioRepetida = true; 
    } else {
      this.cantidadTipoNegocioRepetida = false;
    }
  }

  validarNaturalezaDescripcion(descripcion) {
    let cantidadRepetido = 0;
    this.naturalezaJuridicaFormArray.value.forEach(element => {
      if (element.descripcion == descripcion) {
        cantidadRepetido++;
      }
    });
    if (cantidadRepetido > 1) {
      this.cantidadNaturezaRepetida = true; 
    } else {
      this.cantidadNaturezaRepetida = false;
    }
  }

  get costosPagoFormArray() {
    return <FormArray>this.dynamicForm.get("costosPago");
  }

  get retencionFormArray() {
    return <FormArray>this.dynamicForm.get("retenciones");
  }

  get fechaClaveFormArray() {
    return <FormArray>this.dynamicForm.get("fechasClave");
  }

  get rolFormArray() {
    return <FormArray>this.dynamicFormTercero.get("roles");
  }

  get negocioFormArray() {
    return <FormArray>this.dynamicFormNegocios.get("negocios");
  }

  get naturalezaJuridicaFormArray() {
    return <FormArray>this.dynamicFormNegocios.get("naturalezaJuridica");
  }

  get tipoGastoFormArray() {
    return <FormArray>this.dynamicFormNegocios.get("tipoGasto");
  }

  public getIdCostoPago(i) {
    let swBt = false;
    if (<FormArray>this.dynamicForm.get("costosPago").value[i].id == null) {
      swBt = true;
    } else swBt = false;
    return swBt;
  }

  public getIdRetenciones(i) {
    let swBt = false;
    if (<FormArray>this.dynamicForm.get("retenciones").value[i].id == null) {
      swBt = true;
    } else swBt = false;
    return swBt;
  }

  public getIdFechasClave(i) {
    let swBt = false;
    if (<FormArray>this.dynamicForm.get("fechasClave").value[i].id == null) {
      swBt = true;
    } else swBt = false;
    return swBt;
  }

  public getIdNaturalezaJuridica(i) {
    let swBt = false;
    if (<FormArray>this.dynamicFormNegocios.get("naturalezaJuridica").value[i].id == null) {
      swBt = true;
    } else swBt = false;
    return swBt;
  }

  public openDialogEliminar(nameFormArray, index, element): any {
    let parametroReemplazo: string = null;
    let data;
    data = {
      titulo: "¿Eliminar parámetro?",
      contenido: "¿Qué acción desea realizar?",
      buttonCancelar: "CANCELAR",
      buttonEliminar: "ELIMINAR",
      buttonReemplazar: "REEMPLAZAR",
      urlimg: false
    };
    const dialogRef = this.dialog.open(DialogThreeButtonsComponent, {
      width: "55rem",
      data
    });

    dialogRef.afterClosed().subscribe(async result => {
      switch (result) {
        case 'ELIMINAR':
          if (nameFormArray == 'retenciones') {
            let param = {
              idEliminar: element.id,
              idReemplazar: null
            }
            this._dataService.deleteParametros(param).subscribe(
              response => {
                this.openDialog(true, null, 'eliminar');
                this.onClickResetLoadInfo();
              },
              err => {
                this.openDialog(false, err.error, 'guardar');
              }
            );
          }
          if (nameFormArray == 'fechasClave') {
            let param = {
              idEliminar: element.id,
              idReemplazar: null
            }
            this._dataService.deleteParametros(param).subscribe(
              response => {
                this.openDialog(true, null, 'eliminar');
                this.fechaClaveFormArray.removeAt(index);
                this.onClickResetLoadInfo();
              },
              err => {
                this.openDialog(false, err.error, 'guardar');
              }
            );
          }

          if (nameFormArray == 'roles') {
            let param = {
              idEliminar: element.idRol,
              idReemplazar: null
            }
            this._dataService.deleteParametros(param).subscribe(
              response => {
                this.openDialog(true, null, 'eliminar');
                this.rolFormArray.removeAt(index);
                this.onClickResetLoadInfo();
              },
              err => {
                this.openDialog(false, err.error, 'guardar');
              }
            );
          }
          if (nameFormArray == 'tipoGasto') {
            let param = {
              idEliminar: element.id,
              idReemplazar: null
            }
            this._dataService.deleteParametros(param).subscribe(
              response => {
                this.openDialog(true, null, 'eliminar');
                this.tipoGastoFormArray.removeAt(index);
                this.onClickResetLoadInfo();
              },
              err => {
                this.openDialog(false, err.error, 'guardar');
              }
            );
          }

          break;
        case 'REEMPLAZAR':
          if (nameFormArray == 'retenciones') {
            parametroReemplazo = 'RET';
            this.openDialogReemplazar(element, parametroReemplazo, index);
          }
          if (nameFormArray == 'fechasClave') {
            parametroReemplazo = 'FECCLA';
            this.openDialogReemplazar(element, parametroReemplazo, index);
          }
          if (nameFormArray == 'roles') {
            parametroReemplazo = 'ROLTER';
            this.openDialogReemplazar(element, parametroReemplazo, index);
          }
          if (nameFormArray == 'tipoGasto') {
            parametroReemplazo = 'TIPGAS';
            this.openDialogReemplazar(element, parametroReemplazo, index);
          }
          break;
        default:
          return;
      }
    });
  }

  public openDialogReemplazar(parametroActual, codigoParametroReemplazo, index) {
    let data;
    data = {
      titulo: "Parámetro a reemplazar",
      contenido: "El parámetro indicado debe ser reemplazado en los negocios asignados.",
      parametroActualLabel: "Parámetro a eliminar",
      parametroReemplazoLabel: "Parámetro de reemplazo",
      parametroActual: parametroActual,
      parametroReemplazo: codigoParametroReemplazo,
      buttonCancelar: "CANCELAR",
      buttonReemplazar: "REEMPLAZAR",
      urlimg: false
    };
    const dialogRef = this.dialog.open(DialogReemplazarParametrosComponent, {
      width: "68rem",
      data
    });

    dialogRef.afterClosed().subscribe(result => {
      let parametroEliminarId = (parametroActual.id) ? parametroActual.id : parametroActual.idRol;
      let param = {
        idEliminar: parametroEliminarId,
        idReemplazar: result
      }
      if (result != 'CANCELAR') {
        this._dataService.deleteParametros(param).subscribe(
          response => {
            if (codigoParametroReemplazo == 'COSMEDPAG') {
              this.costosPagoFormArray.removeAt(index);
            }
            if (codigoParametroReemplazo == 'RET') {
              this.retencionFormArray.removeAt(index);
            }
            if (codigoParametroReemplazo == 'FECCLA') {
              this.fechaClaveFormArray.removeAt(index);
            }
            if (codigoParametroReemplazo == 'ROLTER') {
              this.rolFormArray.removeAt(index);
            }
            if (codigoParametroReemplazo == 'TIPNEG') {
              this.negocioFormArray.removeAt(index);
            }
            if (codigoParametroReemplazo == 'TIPNATJUR') {
              this.naturalezaJuridicaFormArray.removeAt(index);
            }
            if (codigoParametroReemplazo == 'TIPGAS') {
              this.tipoGastoFormArray.removeAt(index);
            }
            this.openDialog(true, null, 'reemplazar');
            this.onClickResetLoadInfo();
          },
          err => {
            this.openDialog(false, err.error, 'guardar');
          }
        );
      }
    });
    this.onClickResetLoadInfo();
  }

  openDialog(ok, context, tipoModal): void {
    let data;
    if (ok && tipoModal == 'guardar') {
      data = {
        titulo: "Datos guardados",
        contenido: "Los  parámetros han sido guardados de manera exitosa.",
        button: "ACEPTAR",
        urlimg: false
      };
    }
    if (!ok && tipoModal == 'guardar') {
      let contenido = "Los  parámetros no se pueden guardar. Intente más tarde.";
      if (context != null) {
        contenido = context;
      }
      data = {
        titulo: "Datos no guardados",
        contenido: contenido,
        button: "ACEPTAR",
        urlimg: true
      };
    }
    if (ok && tipoModal == 'eliminar') {
      data = {
        titulo: "Parámetro eliminado",
        contenido: "El parámetro ha sido eliminado con éxito de todos los negocios y/o vinculados.",
        button: "ACEPTAR",
        urlimg: false
      };
    }

    if (ok && tipoModal == 'reemplazar') {
      data = {
        titulo: "Parámetro reemplazado",
        contenido: "El parámetro ha sido reemplazado con éxito de todos los negocios y/o vinculados.",
        button: "ACEPTAR",
        urlimg: false
      };
    }

    const dialogRef = this.dialog.open(DialogParametrosSaveComponent, {
      width: "40rem",
      data
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  saveSimples() {
    let ret: ParametroSimple;
    let pag: ParametroSimple;
    let fec: ParametroSimple;
    let parametrosSimples: ParametroSimple[] = [];
    let valores: Valores[] = [];
    valores = [];
    let swRepetidoCosmedpag: boolean = false;
    let swRepetidoRet: boolean = false;
    let swRepetidoFecla: boolean = false;
    let buscarRepetidos = new Array();

    if (this.dynamicForm.value.retenciones) {      
      buscarRepetidos = [];
      this.dynamicForm.value.retenciones.forEach((element, index) => {
        if (element.unidad === 'COP') {
          element.valor = this._util.cambiarFloatante(element.valor);
        }
        valores[index] = {
          activo: true,
          descripcion: element.descripcion,
          id: element.id,
          unidad: element.unidad,
          valor: element.valor
        };
        buscarRepetidos.push(element.descripcion.toUpperCase());
      });
      swRepetidoRet = this._util.buscarRepetidos(buscarRepetidos);
      if (swRepetidoRet) {
        this.openDialog(false, 'Las Retenciones no se pueden guardar. Revise los nombres repetidos.', 'guardar');
      }
      ret = {
        codigo: "RET",
        nombre: "Retenciones",
        valores: valores
      };
    }
    parametrosSimples[0] = ret;
    valores = [];

    if (this.dynamicForm.value.costosPago) {
      buscarRepetidos = [];
      this.dynamicForm.value.costosPago.forEach((element, index) => {
        if (element.unidad === 'COP') {
          element.valor = this._util.cambiarFloatante(element.valor);
        }
        valores[index] = {
          activo: true,
          descripcion: element.descripcion,
          id: element.id,
          unidad: element.unidad,
          valor: element.valor
        };
        buscarRepetidos.push(element.descripcion.toUpperCase());
      });
      swRepetidoCosmedpag = this._util.buscarRepetidos(buscarRepetidos);
      if (swRepetidoCosmedpag) {
        this.openDialog(false, 'Los Costos de medios de pagos no se pueden guardar. Revise los nombres repetidos.', 'guardar');
      }
      pag = {
        codigo: "COSMEDPAG",
        nombre: "Costos de medios de pagos",
        valores: valores
      };
    }
    parametrosSimples[1] = pag;
    valores = [];
    if (this.dynamicForm.value.fechasClave) {
      buscarRepetidos = [];
      this.dynamicForm.value.fechasClave.forEach((element, index) => {
        valores[index] = {
          activo: true,
          descripcion: element.descripcion,
          id: element.id,
          unidad: null,
          valor: element.valor
        };
        buscarRepetidos.push(element.descripcion.toUpperCase());
      });
      swRepetidoFecla = this._util.buscarRepetidos(buscarRepetidos);
      if (swRepetidoFecla) {
        this.openDialog(false, 'Las Fechas claves no se pueden guardar. Revise los nombres repetidos.', 'guardar');
      }
      fec = {
        codigo: "FECCLA",
        nombre: "Fechas claves",
        valores: valores
      };
    }
    parametrosSimples[2] = fec;

    if (!swRepetidoRet && !swRepetidoCosmedpag && !swRepetidoFecla) {
      this._dataService
        .patchActualizarParametrosSimples(parametrosSimples)
        .subscribe(
          (data: any) => {
            this.openDialog(true, null, 'guardar');
            this.onClickResetLoadInfo();
          },
          (err: any) => {
            this.openDialog(false, err.error, 'guardar');
          }
        );
    }
  }

  saveRolTercero() {
    let rolTercero: Compuesto[] = [];
    let tercero: ValoresCompuesto[] = [];
    let tipoTercero: DetalleValorCompuesto[] = [];
    let buscarRepetidos = new Array();
    let swRepetidoTercero: boolean = false;
    let swValidaNombreRol: boolean = false;
    let rolDescripcion: string = '';

    buscarRepetidos = [];
    this.dynamicFormTercero.value.roles.forEach((item, index) => {
      let idComite = item.idComite == null ? this.idRolTercero.idComite : item.idComite;
      let idTercero = item.idTercero == null ? this.idRolTercero.idTercero : item.idTercero;
      let valor = item.comite ? "true" : "false";
      tipoTercero[0] = {
        id: idComite,
        descripcion: "comite",
        valor: valor
      };
      valor = item.tercero ? "true" : "false";
      tipoTercero[1] = {
        id: idTercero,
        descripcion: "tercero",
        valor: valor
      };

      if (tipoTercero[0].valor == "false" && tipoTercero[1].valor == "false") {
        swValidaNombreRol = true;
        rolDescripcion = item.rol;
      }
      tercero[index] = {
        descripcion: item.rol,
        activo: true,
        id: item.idRol,
        valores: tipoTercero
      };
      buscarRepetidos.push(item.rol.toUpperCase());
      tipoTercero = [];
    });

    swRepetidoTercero = this._util.buscarRepetidos(buscarRepetidos);
    if (swRepetidoTercero) {
      this.openDialog(false, 'Los Roles de terceros no se pueden guardar. Revise los nombres repetidos.', 'guardar');
    }

    if (swValidaNombreRol) {
      this.openDialog(false, 'Los Roles de terceros no se pueden guardar. Debe activar al menos un tipo del Rol: ' + rolDescripcion, 'guardar');
    }

    rolTercero[0] = {
      codigo: "ROLTER",
      nombre: "Roles de terceros",
      valores: tercero
    };

    if (!swRepetidoTercero && !swValidaNombreRol) {
      this._dataService.patchActualizarParametrosCompuestos(rolTercero).subscribe(
        (data: any) => {
          this.openDialog(true, null, 'guardar');
          this.onClickResetLoadInfo();
        },
        (err: any) => {
          this.openDialog(false, err.error, 'guardar');
        }
      );
    }
  }

  comparerSent(ctrl: any, compDetalle: any): string {
    let value = 'false'
    ctrl.forEach(element => {
      if (compDetalle.name == element.name) {
        value = 'true';
      }
      else if (compDetalle.name == element.descripcion) {
        value = element.valor;
      }
    });
    return value;
  }

  crearObjetoCompuestosValoresDetalle(item) {
    let pCompuestoValoresDetalle: DetalleValorCompuesto[] = [];
    this.retencionSent.forEach((element, index) => {
      let valor = null;
      if (item.retencionMultiCtrl != null) {
        valor = this.comparerSent(item.retencionMultiCtrl, element);
      }
      pCompuestoValoresDetalle[index] = {
        descripcion: element.name,
        id: element.id,
        valor: valor
      }
    })
    return pCompuestoValoresDetalle;
  }

  saveTipoNegocio() {
    let pCompuesto: Compuesto[] = [];
    let pCompuestoValores: ValoresCompuesto[] = [];
    let pCompuestoValoresDetalle: DetalleValorCompuesto[] = [];
    let buscarRepetidos = new Array();
    let swRepetidoTipneg: boolean = false;
    let swRepetidoTipnatjur: boolean = false;
    let swRepetidoTipgas: boolean = false;

    pCompuestoValores = [];
    pCompuestoValoresDetalle = [];
    buscarRepetidos = [];
    this.dynamicFormNegocios.value.negocios.forEach((item, index) => {
      pCompuestoValoresDetalle = this.crearObjetoCompuestosValoresDetalle(item);
      pCompuestoValores[index] = {
        activo: true,
        descripcion: item.descripcion,
        id: item.id,
        valores: pCompuestoValoresDetalle
      }
      buscarRepetidos.push(item.descripcion.toUpperCase());
    });

    pCompuesto[0] = {
      codigo: "TIPNEG",
      nombre: "Tipo de negocios",
      valores: pCompuestoValores
    }

    swRepetidoTipneg = this._util.buscarRepetidos(buscarRepetidos);
    if (swRepetidoTipneg) {
      this.openDialog(false, 'Los Tipo de negocios no se pueden guardar. Revise los nombres repetidos.', 'guardar');
    }

    pCompuestoValores = [];
    pCompuestoValoresDetalle = [];
    buscarRepetidos = [];

    this.dynamicFormNegocios.value.naturalezaJuridica.forEach((item, index) => {
      pCompuestoValoresDetalle = this.crearObjetoCompuestosValoresDetalle(item);
      pCompuestoValores[index] = {
        activo: true,
        descripcion: item.descripcion,
        id: item.id,
        valores: pCompuestoValoresDetalle
      }
      buscarRepetidos.push(item.descripcion.toUpperCase());
    });

    swRepetidoTipnatjur = this._util.buscarRepetidos(buscarRepetidos);
    if (swRepetidoTipnatjur) {
      this.openDialog(false, 'Los Tipos de naturaleza juridica no se pueden guardar. Revise los nombres repetidos.', 'guardar');
    }
    pCompuesto[1] = {
      codigo: "TIPNATJUR",
      nombre: "Tipos de naturaleza juridica",
      valores: pCompuestoValores
    }
    pCompuestoValores = [];
    pCompuestoValoresDetalle = [];
    buscarRepetidos = [];

    this.dynamicFormNegocios.value.tipoGasto.forEach((item, index) => {
      pCompuestoValoresDetalle = this.crearObjetoCompuestosValoresDetalle(item);
      pCompuestoValores[index] = {
        activo: true,
        descripcion: item.descripcion,
        id: item.id,
        valores: pCompuestoValoresDetalle
      }
      buscarRepetidos.push(item.descripcion.toUpperCase());
    });
    swRepetidoTipgas = this._util.buscarRepetidos(buscarRepetidos);
    if (swRepetidoTipgas) {
      this.openDialog(false, 'Los Tipos de gastos no se pueden guardar. Revise los nombres repetidos.', 'guardar');
    }
    pCompuesto[2] = {
      codigo: "TIPGAS",
      nombre: "Tipos de gastos",
      valores: pCompuestoValores
    }
    if (!swRepetidoTipneg && !swRepetidoTipnatjur && !swRepetidoTipgas) {
      this._dataService
        .patchActualizarParametrosCompuestos(pCompuesto)
        .subscribe(
          (data: any) => {
            this.openDialog(true, null, 'guardar');
            this.onClickResetLoadInfo();
          },
          (err: any) => {
            this.openDialog(false, err.error, 'guardar');
          }
        );
    }

  }

  setFormatNumberDecimal(event, decimal, unidad) {
    let valor = parseFloat(event.target.value).toFixed(decimal);
    if (valor == 'NaN') {
      valor = '0';
    }
    if (unidad == 'COP') {
      event.target.value = this.currencyPipe.transform(valor);
    }
    else {
      event.target.value = valor;
    }    
    return event;
  }

}
