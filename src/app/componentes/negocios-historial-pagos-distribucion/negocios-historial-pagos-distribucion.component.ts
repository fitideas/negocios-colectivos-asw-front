import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormArray, FormControl, FormBuilder } from '@angular/forms';
import { MatTableDataSource, MatDialog, MatPaginator, MatSort } from '@angular/material';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Utils } from 'src/app/core/utils';
import { AuthService } from 'src/app/services/auth.service';
import { CurrencyPipe, DatePipe } from '@angular/common';

export interface CriterioList {
  value: string;
  viewValue: string;
  active: boolean;
}

export interface Parametros {
  codigoNegocio: string;
  rangoFecha: any[];
  codigoTipoNegocio: any[];
  numeroEncargo: any;
}

export interface ListPagos {
  periodoPago?: string;
  tipoNegocio?: string;
  encargoDebitado?: string;
  valorIngreso?: string;
  valorDistribuido?: string;
  verMas?: string;
}

@Component({
  selector: 'app-negocios-historial-pagos-distribucion',
  templateUrl: './negocios-historial-pagos-distribucion.component.html',
  styleUrls: ['./negocios-historial-pagos-distribucion.component.css']
})

/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Marzo 2020
 * @Proposito: clase para mostrar el historial de pagos distribucion por negocio.
 */

export class NegociosHistorialPagosDistribucionComponent implements OnInit {

  public buscarPagosDynamicForm: FormGroup;
  codigoNegocioEspecifico: string;
  get filtrosFormArray() {
    return <FormArray>this.buscarPagosDynamicForm.get("filtros");
  }
  public submitted: boolean = false;
  public dataNotFound: boolean = null;
  public dataFound: boolean = null;
  public detalleNegocio: boolean = false;

  public isFecha: boolean = false;
  public isTipoNegocio: boolean = false;
  public isNumeroEncargo: boolean = false;
  public isTodo: boolean = false;
  public swAddFiltros = true;

  public arrayFiltro: any[] = [];
  public nuevoItem: boolean = false;
  public fechaFinal: any;
  public fechaInicial: any;
  public listFechaRango: any[] = [];
  public listTipoNegocio: any[] = [];
  public numeroEncargo: string;
  public request: Parametros;
  public codigoNegocio: string;
  public ver_negocios_historial_pagos: boolean = false;

  listSelectCriterio: CriterioList[] = [
    { value: "todo", viewValue: "Sin Filtros", active: true },
    { value: "tipoNegocio", viewValue: "Tipo de Negocio", active: true },
    { value: "fecha", viewValue: "Fecha", active: true },
    { value: "numeroEncargo", viewValue: "Encargo debitado", active: true }
  ];

  listSelectCriterioNuevo: CriterioList[] = [];

  displayedColumns: string[] = [
    'periodoPago',
    'tipoNegocio',
    'encargoDebitado',
    'valorIngreso',
    'valorDistribuido',
    'verMas'
  ];

  dataSource: MatTableDataSource<ListPagos>;
  public resultsLength: number = 0;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  public listError: any = null;
  public swListError: boolean = false;
  public infoNegocio: any;

  public page: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPages: number = 1; //Número total de páginas 
  public numElementos: number = 10; //Total de elemento por pagina   

  constructor(
    public fb: FormBuilder,
    private _dataService: DataService,
    private _storageData: StorageDataService,
    private _util: Utils,
    private datePipe: DatePipe,
    private _authService: AuthService,
    private currencyPipe: CurrencyPipe,
    public dialog: MatDialog
  ) {

  }

  ngOnInit() {
    this.codigoNegocioEspecifico = this._storageData.getcodigoSFCNegocio();
    this.loadPermisos();
    this.loadInfoNegocio();
    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarPagosDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();
    this.resultsLength = 0;
    // document.getElementById("dataFound").style.display = 'none';
    // document.getElementById("dataNotFound").style.display = 'none';
    if (this._storageData.getCodigoNegocioBenficiarioPago() != null) {
      this.enviarDatos(true);
      this._storageData.setCodigoNegocioBenficiarioPago(null);
    }
  }

  private loadPermisos() {
    this.ver_negocios_historial_pagos = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'negocios_historial_pagos');
  }

  private loadInfoNegocio() {
    this.infoNegocio = this._storageData.getInfoNegocioDetalle();
    this.codigoNegocio = this.infoNegocio.codigoSFC;
  }

  checkFiltros() {
    if (this.filtrosFormArray.length == 1) {
      return false
    }
    return true;
  }

  limpiarformulario() {
    this.arrayFiltro = [];
    this.fechaInicial=null;
    this.fechaFinal=null;  
    this.filtrosFormArray.clear();
    this.filtrosFormArray.reset([]);
    this.loadInfoNegocio();
    this.dataSource = null;
    this.dataNotFound = false;
    this.dataFound = false;
    this.listFechaRango = [];
    this.numeroEncargo = "";
    this.listTipoNegocio = [];
    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarPagosDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();
    this.resultsLength = 0;
    this.swAddFiltros = true;
    this.listSelectCriterioNuevo.forEach(element => {
      element.active = true;
    });
    document.getElementById("dataFound").style.display = 'none';
    document.getElementById("dataNotFound").style.display = 'none';
  }

  private setFormatNumberDecimal(monto, decimal) {
    let valor = parseFloat(monto).toFixed(decimal);
    if (valor == 'NaN') {
      valor = '0';
    }
    monto = this.currencyPipe.transform(valor);
    return monto;
  }

  buscarListaXCriterioSeleccionado(eventSeleccionado, ind) {
    this.isFecha = false;
    this.isTipoNegocio = false;
    this.isNumeroEncargo = false;
    this.isTodo = true;
    if (eventSeleccionado.value == 'todo') {
      this.isTodo = true;
    }
    if (eventSeleccionado.value == 'tipoNegocio') {
      this.arrayFiltro[ind] = 'tipoNegocio';
      this.isTipoNegocio = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }
    if (eventSeleccionado.value == 'fecha') {
      this.arrayFiltro[ind] = 'fecha';
      this.isFecha = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }
    if (eventSeleccionado.value == 'numeroEncargo') {
      this.arrayFiltro[ind] = 'numeroEncargo';
      this.isNumeroEncargo = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }

    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == eventSeleccionado.value) {
        element.active = false;
      }
      if (!this.isTodo) {
        if (element.value == 'todo') {
          element.active = false;
        }
      }
    });
  }

 

  getTipoNegocio(tipoNegocio) {
    this.listTipoNegocio = tipoNegocio;
  }

  getNumeroEncargo(numeroEncargo) {
    this.numeroEncargo = numeroEncargo;
  }

  createGroup() {
    return this.fb.group({
      selectCriterio: new FormControl("", []),
      argumento: new FormControl("", []),
      numeroEncargo: new FormControl("")
    });
  }

  addToFiltrosFormArray() {
    this.filtrosFormArray.push(this.createGroup());
    if (this.filtrosFormArray.length == 4) {
      this.swAddFiltros = false;
    }
    this.nuevoItem = true;
  }

  removeFromFiltrosFormArray(index, itemFiltro) {
    this.filtrosFormArray.removeAt(index);

    this.arrayFiltro = this.arrayFiltro.filter(item => {
      return (item !== itemFiltro.value.selectCriterio);
    });

    if (this.filtrosFormArray.length < 4) {
      this.swAddFiltros = true;
    }

    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == itemFiltro.value.selectCriterio) {
        element.active = true;
      }
    });
  }

  goToPage(page: number) {
    this.page = page;
    let requestPaginado: any = this.request;
    requestPaginado.pagina = page;
    requestPaginado.elementos = this.numElementos;
    this.negocioListHistorialPagos(requestPaginado);
  }

  enviarDatos(listar): void {
    this.listFechaRango=[];
    this.dataNotFound = false;
    this.dataFound = false;
    this.submitted = true;
    this.detalleNegocio = false;
    this.numeroEncargo = '';
    this.listFechaRango.push(this.fechaInicial);
    this.listFechaRango.push(this.fechaFinal);
    if(this.fechaInicial==null || this.fechaFinal==null){
      this.listFechaRango=[];
    }
    this.buscarPagosDynamicForm.value.filtros.forEach(element => {
      if (element.selectCriterio == "numeroEncargo") {
        this.numeroEncargo = element.numeroEncargo;
      }
    });
    this.request = {
      codigoNegocio: this.codigoNegocio,
      rangoFecha: this.listFechaRango,
      numeroEncargo: this.numeroEncargo,
      codigoTipoNegocio: this.listTipoNegocio
    }
    if (listar) {
      let requestPaginado: any = this.request;
      requestPaginado.pagina = 1;
      requestPaginado.elementos = this.numElementos;
      this.negocioListHistorialPagos(requestPaginado);
    }
  }

  negocioListHistorialPagos(requestBuscar) {
    this.dataSource = null;
    let listPagos: ListPagos[] = new Array();
    let pago: ListPagos;
    this._dataService
      .listadoNegocioHistorialGeneralPagos(requestBuscar)
      .subscribe((data: any) => {
        if (!data || data.totalRegistros == 0) {
          this.dataNotFound = true;
          this.dataFound = false;
          this.detalleNegocio = false;
          document.getElementById("dataFound").style.display = 'none';
          document.getElementById("dataNotFound").style.display = 'inline';
          return;
        }

        this.dataFound = true;
        this.dataNotFound = false;
        this.detalleNegocio = false;
        document.getElementById("dataFound").style.display = 'inline';
        document.getElementById("dataNotFound").style.display = 'none';

        data.historialPagos.forEach(element => {
          pago = {
            periodoPago: element.periodo,
            tipoNegocio: element.tipoNegocio,
            encargoDebitado: element.encargoDebitado,
            valorIngreso: this.setFormatNumberDecimal(element.valorIngreso, 2),
            valorDistribuido: this.setFormatNumberDecimal(element.valorDistribuido, 2),
            verMas: 'ver más'
          }
          listPagos.push(pago);
        });

        this.dataSource = new MatTableDataSource(listPagos);
        this.resultsLength = data.totalRegistros;
        this.totalPages = this.resultsLength / this.numElementos;
        let totalPagina = this.resultsLength % this.numElementos;
        if (totalPagina > 1) {
          this.totalPages = Number(parseInt(String(this.totalPages))) + 1;
        }
        if (this.totalPages < 1) {
          this.totalPages = 1;
        }
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
        (err) => {
          this.dataNotFound = true;
          this.dataFound = true;
          document.getElementById("dataFound").style.display = 'none';
          document.getElementById("dataNotFound").style.display = 'inline';
        }
      );
  }

  getNegocioHistorialIngresoEgreso(item) {
    this._storageData.setNegocioHistorialPagosIngresoEgresoPeriodo(item.periodoPago);
    this._storageData.setNegocioHistorialPagosIngresoEgresoTipoNegocio(item.tipoNegocio);
    this._storageData.setActiveNegocioPagosDistribucion(false);
    this._storageData.setActiveNegocioHistorialPagosDistribucion(false);
    this._storageData.setActivePagosDistribucion(false);
    this._storageData.setActiveNegocioHistorialPagosIngresoEgreso(true);
    this._storageData.setActiveNegocioHistorialPagosBeneficiarios(false);
  }

  getDateRangeFin(range) {
    this.fechaFinal=this.datePipe.transform(range, '01/MM/yyyy')
  }

  

  getDateRange(range) {
    this.fechaInicial=this.datePipe.transform(range, '01/MM/yyyy')
  }


}
