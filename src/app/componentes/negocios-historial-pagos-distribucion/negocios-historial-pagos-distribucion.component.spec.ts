import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NegociosHistorialPagosDistribucionComponent } from './negocios-historial-pagos-distribucion.component';

describe('NegociosHistorialPagosDistribucionComponent', () => {
  let component: NegociosHistorialPagosDistribucionComponent;
  let fixture: ComponentFixture<NegociosHistorialPagosDistribucionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NegociosHistorialPagosDistribucionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NegociosHistorialPagosDistribucionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
