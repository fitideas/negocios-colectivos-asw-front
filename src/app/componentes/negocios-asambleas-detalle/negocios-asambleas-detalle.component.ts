import { Component, OnInit, EventEmitter, Input, Output, ViewChild, NgZone } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { FileUploadModel } from 'src/app/modelo/list-cotitulares';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Utils } from 'src/app/core/utils';
import { AuthService } from 'src/app/services/auth.service';
import moment from 'moment';

@Component({
  selector: 'app-negocios-asambleas-detalle',
  templateUrl: './negocios-asambleas-detalle.component.html',
  styleUrls: ['./negocios-asambleas-detalle.component.css']
})
export class NegociosAsambleasDetalleComponent implements OnInit {

  public placeholderFechaReunion: string;
  public placeholderHoraReunion: string;
  public placeholderLugarReunion: string;
  public placeholderQuorumAprobatorio: string;
  public placeholderResponsable: string;
  public placeholderDecisionesTomada: string;
  public placeholderTareaPendiente: string;
  public placeholderNombre: string;
  public placeholderRol: string;
  public placeholderRolFuncionario: string;
  public placeholderNumeroAsistentes: string;
  public placeholderNumeroRadicado: string;
  public placeholderTipoArchivo: string;


  public dynamicForm: FormGroup;
  public seedDataDecisionTomada: any[];
  public seedDataTareaPendiente: any[];
  public seedDataMesaDirectiva: any[];
  public seedDataExpositor: any[];
  public mesaDirectiva: any[];
  public listExpositor: any[];
  public date: moment.Moment;
  public dateControl = new FormControl(moment());
  public files: Array<FileUploadModel> = [];
  public filesNull = true;
  uploadResponse = { status: '', message: '', filePath: '' };
  public fileUpload: any;
  public nombreArchivo: string;
  public guardarBotton = false;
  private codigoSFC: string = '';
  public asamblea_agregar: boolean = false;
  public fechaLimite = new Date();

  public infoAsambleas:any;

  //base64s
  archivoBase64: string;

  @Input() accept = 'application/pdf, application/vnd.ms-powerpoint';
  /** Allow you to add handler after its completion. Bubble up response text from remote. */
  @Output() complete = new EventEmitter<string>();

  @ViewChild('picker', { static: false }) picker: any;

  constructor(
    public fb: FormBuilder,
    private httpClient: HttpClient,
    private zone: NgZone,
    public dialog: MatDialog,
    public _dataService: DataService,
    private _storageData: StorageDataService,
    public _util: Utils,
    private _authService: AuthService
  ) {
  }

  ngOnInit() {
    this.getDataJson();
    this.date = null;
    this.dynamicForm = this.fb.group({
      decisionTomada: this.fb.array([]),
      tareaPendiente: this.fb.array([]),
      mesaDirectiva: this.fb.array([]),
      expositor: this.fb.array([]),
      codigoSFC: new FormControl(""),
      fechaReunion: new FormControl("", Validators.required),
      direccionReunion: new FormControl(""),
      ordenDia: new FormControl(""),
      nombreArchivo: new FormControl("", Validators.required),
      archivo: new FormControl("", Validators.required),
      numeroAsistentes: new FormControl(""),
      numeroRadicado: new FormControl("")
    });
    this.mesaDirectiva = new Array();
    this.listExpositor = new Array();

    this.codigoSFC = this._storageData.getcodigoSFCNegocio();
    this.loadPermisos();
    this.cargarServicio();
  }

  loadDetalle(){
    this.codigoSFC = this.infoAsambleas.codigoSFC;
    this.dynamicForm.get('codigoSFC').setValue(this.infoAsambleas.codigoSFC);
    this.dynamicForm.get('fechaReunion').setValue(this.infoAsambleas.fechaReunion);
    this.dynamicForm.get('direccionReunion').setValue(this.infoAsambleas.direccionReunion);
    this.dynamicForm.get('ordenDia').setValue(this.infoAsambleas.ordenDia);
    this.dynamicForm.get('nombreArchivo').setValue(this.infoAsambleas.nombreArchivo);
    this.filesNull = false;
    this.dynamicForm.get('numeroAsistentes').setValue(this.infoAsambleas.numeroAsistentes);
    this.dynamicForm.get('numeroRadicado').setValue(this.infoAsambleas.numeroRadicado);
    this.dynamicForm.get('archivo').setValue(this.infoAsambleas.archivo);
    
    this.infoAsambleas.decision.filter(element => element.tipo === true).forEach(element => {
      this.decisionTomadaFormArray.push(this.createGroupResult(element.descripcion, element.responsable, element.quorumAprobatorio));
    });
    this.infoAsambleas.decision.filter(element => element.tipo === false).forEach(element => {
      this.tareaPendienteFormArray.push(this.createGroupResult(element.descripcion, element.responsable, element.quorumAprobatorio));
    });
    this.mesaDirectiva = this.infoAsambleas.mesaDirectiva.filter(element => element.tipo === true);
    this.listExpositor = this.infoAsambleas.mesaDirectiva.filter(element => element.tipo === false);  
  }

  private cargarServicio () {
    let infoReunion = this._storageData.getInfoReunion();
    this._dataService.getObtenerInformacionReunion(infoReunion.codigoSFC,infoReunion.fecha,infoReunion.numeroRadicado)
      .subscribe((data: any) => {
        if (!data) {          
          return;
        }
        this.infoAsambleas = data;
        this.loadDetalle();
      },
        (err) => {
        }
      );
  }

  private loadPermisos() {
    this.asamblea_agregar = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'asamblea_agregar');
  }

  closePicker() {
    this.picker.cancel();
  }

  seedDecisionAprobadaFormArray(item: any) {
    this.seedDataDecisionTomada = item;
    this.seedDataDecisionTomada.forEach(seedDatum => {
      const formGroup = this.createGroup();
      formGroup.patchValue(seedDatum);
      this.decisionTomadaFormArray.push(formGroup);
    });
  }

  get decisionTomadaFormArray() {
    return <FormArray>this.dynamicForm.get("decisionTomada");
  }

  get tareaPendienteFormArray() {
    return <FormArray>this.dynamicForm.get("tareaPendiente");
  }

  seedTareaPendienteFormArray(item: any) {
    this.seedDataTareaPendiente = item;
    this.seedDataTareaPendiente.forEach(seedDatum => {
      const formGroup = this.createGroup();
      formGroup.patchValue(seedDatum);
      this.tareaPendienteFormArray.push(formGroup);
    });
  }

  get mesaDirectivaFormArray() {
    return <FormArray>this.dynamicForm.get("mesaDirectiva");
  }

  seedMesaDirectivaFormArray(item: any) {
    let seedDatum = {
      tipoDocumento: item.tipoDocumento,
      numeroDocumento: item.numeroDocumento,
      rol: item.rol
    }
    const formGroup = this.createGroupMesaDirectiva();
    formGroup.patchValue(seedDatum);
    this.mesaDirectivaFormArray.push(formGroup);
  }

  get expositorFormArray() {
    return <FormArray>this.dynamicForm.get("expositor");
  }

  seedExpositorFormArray(item: any) {
    let seedDatum = {
      tipoDocumento: item.tipoDocumento,
      numeroDocumento: item.numeroDocumento,
      rol: item.rol
    }
    const formGroup = this.createGroupMesaDirectiva();
    formGroup.patchValue(seedDatum);
    this.expositorFormArray.push(formGroup);
  }

  createGroup() {
    return this.fb.group({
      descripcion: ['', []],
      responsable: [],
      quorumAprobatorio: ['', []]
    });
  }

  createGroupResult(descripcionInfo, responsableInfo, quorumAprobatorioInfo) {
    return this.fb.group({
      descripcion: [descripcionInfo, []],
      responsable: [responsableInfo,[]],
      quorumAprobatorio: [quorumAprobatorioInfo, []]
    });
  }

  createGroupMesaDirectiva() {
    return this.fb.group({
      tipoDocumento: [''],
      numeroDocumento: [''],
      rol: ['']
    });
  }

  createGroupMesaDirectivaResult(tipoDocumentoInfo, numeroDocumentoInfo, rolInfo) {
    return this.fb.group({
      tipoDocumento: [tipoDocumentoInfo],
      numeroDocumento: [numeroDocumentoInfo],
      rol: [rolInfo]
    });
  }



  addToDecisionTomadaFormArray() {
    this.decisionTomadaFormArray.push(this.createGroup());
  }

  removeFromDecisionTomadaFormArray(index) {
    this.decisionTomadaFormArray.removeAt(index);
  }

  addToTareaPendienteFormArray() {
    this.tareaPendienteFormArray.push(this.createGroup());
  }

  removeFromTareaPendienteFormArray(index) {
    this.tareaPendienteFormArray.removeAt(index);
  }

  addToMesaDirectivaFormArray() {
    this.mesaDirectivaFormArray.push(this.createGroupMesaDirectiva());
  }

  removeFromMesaDirectivaFormArray(index) {
    this.mesaDirectivaFormArray.removeAt(index);
  }

  addToExpositorFormArray() {
    this.expositorFormArray.push(this.createGroupMesaDirectiva());
  }

  removeFromExpositorFormArray(index) {
    this.expositorFormArray.removeAt(index);
  }

  removeFromMesaDirectiva(item) {
    for (var i = 0; i < this.mesaDirectiva.length; i++) {
      if (this.mesaDirectiva[i].numeroDocumento === item.numeroDocumento && this.mesaDirectiva[i].tipoDocumento === item.tipoDocumento) {
        this.mesaDirectiva.splice(i, 1);
      }
    }
    while (this.mesaDirectivaFormArray.length !== 0) {
      this.mesaDirectivaFormArray.removeAt(0)
    }
    this.mesaDirectiva.forEach(element => {
      this.seedMesaDirectivaFormArray(element);
    });
  }


  getDataJson() {
    this.httpClient.get("assets/i18n/es.json").subscribe(data => {
      this.placeholderFechaReunion = data['negocioAsambleasNuevaEntrada'].fecha_reunión;
      this.placeholderHoraReunion = data['negocioAsambleasNuevaEntrada'].hora_reunion;
      this.placeholderLugarReunion = data['negocioAsambleasNuevaEntrada'].lugar_reunion;
      this.placeholderDecisionesTomada = data['negocioAsambleasNuevaEntrada'].decisiones_tomadas;
      this.placeholderTareaPendiente = data['negocioAsambleasNuevaEntrada'].tareas_pendientes;
      this.placeholderResponsable = data['negocioAsambleasNuevaEntrada'].responsable;
      this.placeholderQuorumAprobatorio = data['negocioAsambleasNuevaEntrada'].quorum_aprobatorio;
      this.placeholderNombre = data['negocioAsambleasNuevaEntrada'].nombres;
      this.placeholderRol = data['negocioAsambleasNuevaEntrada'].rol_tercero;
      this.placeholderRolFuncionario = data['negocioAsambleasNuevaEntrada'].rol_funcionario;
      this.placeholderNumeroAsistentes = data['negocioAsambleasNuevaEntrada'].numero_asistentes;
      this.placeholderNumeroRadicado = data['negocioAsambleasNuevaEntrada'].numero_radicado;
      this.placeholderTipoArchivo = data['negocioAsambleasNuevaEntrada'].tipo_archivo;
    })
  }


  descargarArchivo(){
    const enlace = document.createElement("a");
    enlace.href = 'data:application/octet-stream;base64,'+ this.dynamicForm.value.archivo;
    enlace.download = this.dynamicForm.value.nombreArchivo;
    enlace.click();
  }

  removeItemFromArr = (arr, item) => {
    return arr.filter(e => e.numeroDocumento !== item);
  };

}
