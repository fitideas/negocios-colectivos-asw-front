import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NegociosAsambleasDetalleComponent } from './negocios-asambleas-detalle.component';

describe('NegociosAsambleasDetalleComponent', () => {
  let component: NegociosAsambleasDetalleComponent;
  let fixture: ComponentFixture<NegociosAsambleasDetalleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NegociosAsambleasDetalleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NegociosAsambleasDetalleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
