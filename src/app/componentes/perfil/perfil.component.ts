import {
  Component, OnInit,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver,
  ComponentRef,
  ComponentFactory
} from '@angular/core';
import { DynamicPerfilComponent } from '../dynamic-perfil/dynamic-perfil.component';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { RolFuncionalidad, Perfil } from 'src/app/modelo/perfil';
import { MatDialog } from '@angular/material';
import { DialogParametrosSaveComponent } from '../dialog-parametros-save/dialog-parametros-save.component';
import { Utils } from 'src/app/core/utils';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Marzo 2020
 * @Proposito: clase para gestionar perfiles de usuarios .
 */
export class PerfilComponent implements OnInit {

  @ViewChild('container', { read: ViewContainerRef, static: false }) container: ViewContainerRef;

  private _counter = 1;
  componentRef: any;
  public seedDataPerfil;
  public funcionalidadList;
  public rolFuncionalidad: RolFuncionalidad;
  public rolesFuncionalidad: RolFuncionalidad[];
  public roles_permisos: boolean = false;
  public roles_permisos_escribe: boolean = false;
  objPerfil: Perfil[];

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private _dataService: DataService,
    private _storageService: StorageDataService,
    private _router: Router,
    public dialog: MatDialog,
    private _authService: AuthService,
    public _util: Utils
  ) {
    this.rolesFuncionalidad = new Array<RolFuncionalidad>();
  }

  ngOnInit() {
    this.loadPermisos();
    this.loadServiceRoles();
    if (this._storageService.getSwInicio()) {
      this._router.navigate(['accionfiduciaria']);
    }
  }

  private loadServiceRoles() {
    this.seedDataPerfil = [];
    this._dataService.getRolesRequest().subscribe(
      (data: any) => {
        if (!data) {
          return;
        }
        this.seedDataPerfil = data;
        this.seedDataPerfil.forEach(element => {
          this.add(element);
        });
      },
      err => {
      }
    );
  }

  private loadPermisos() {
    this.roles_permisos = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'roles_permisos');
    this.roles_permisos_escribe = this._util.permiteEscribir(this._authService.getFuncionalidades(), 'roles_permisos');
  }

  add(itemRol): void {
    // create the component factory
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(DynamicPerfilComponent);
    // add the component to the view
    this.componentRef = this.container.createComponent(componentFactory);
    // pass some data to the component
    this.componentRef.instance.index = this._counter++;
    this.componentRef.instance.itemRol = itemRol;
    this.componentRef.instance.editing = this.roles_permisos_escribe;
  }

  clear() {
    this.container.clear();
  }

  destroyComponent() {
    this.componentRef.destroy();
  }

  openDialog(ok, context): void {
    let data;
    if (ok) {
      data = {
        titulo: "Datos guardados",
        contenido: "Los permisos asociados a los roles han sido guardados de manera exitosa.",
        button: "ACEPTAR",
        urlimg: false
      };
    }
    if (!ok) {
      let contenido = "Los permisos asociados no se pueden guardar. Intente más tarde.";
      if (context != null) {
        contenido = context;
      }
      data = {
        titulo: "Datos no guardados",
        contenido: contenido,
        button: "ACEPTAR",
        urlimg: true
      };
    }
    const dialogRef = this.dialog.open(DialogParametrosSaveComponent, {
      width: "40rem",
      data
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  save() {
    this.objPerfil = this._storageService.getPerfilesRolPermisos();
    this._storageService.inicializarRolesFuncionalidad();
    this._dataService.patchActualizarRolesActualizar(this.objPerfil).subscribe(
      (data: any) => {
        this.openDialog(true, null);
        this.clear();
        this.loadServiceRoles();
      },
      (err: any) => {
        this.openDialog(false, err.error);
      }
    );
  }

}
