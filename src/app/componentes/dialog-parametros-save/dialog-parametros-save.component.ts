import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

export interface DialogData {
  titulo: string,
  contenido: string,
  button: string,
  urlimg: boolean
}

@Component({
  selector: "app-dialog-parametros-save",
  templateUrl: "./dialog-parametros-save.component.html",
  styleUrls: ["./dialog-parametros-save.component.css"]
})
/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Marzo 2020
 * @Proposito: clase de dialog modal para guardar parametros.
 */
export class DialogParametrosSaveComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogParametrosSaveComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
