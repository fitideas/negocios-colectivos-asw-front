import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogParametrosSaveComponent } from './dialog-parametros-save.component';

describe('DialogParametrosSaveComponent', () => {
  let component: DialogParametrosSaveComponent;
  let fixture: ComponentFixture<DialogParametrosSaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogParametrosSaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogParametrosSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
