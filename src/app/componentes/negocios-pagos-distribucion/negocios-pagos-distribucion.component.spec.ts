import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NegociosPagosDistribucionComponent } from './negocios-pagos-distribucion.component';

describe('NegociosPagosDistribucionComponent', () => {
  let component: NegociosPagosDistribucionComponent;
  let fixture: ComponentFixture<NegociosPagosDistribucionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NegociosPagosDistribucionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NegociosPagosDistribucionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
