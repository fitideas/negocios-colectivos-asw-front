import { Component, OnInit } from '@angular/core';
import { Utils } from 'src/app/core/utils';
import { AuthService } from 'src/app/services/auth.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-negocios-pagos-distribucion',
  templateUrl: './negocios-pagos-distribucion.component.html',
  styleUrls: ['./negocios-pagos-distribucion.component.scss']
})
/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Abril 2020
 * @Proposito: clase para iniciar el proceso de pagos distribuidos.
 */
export class NegociosPagosDistribucionComponent implements OnInit {

  public ver_historial_pagos_distribucion: boolean = false;
  public ver_anadir_pagos_distribucion: boolean = false;
  public historial_pagos_distribucion: boolean = false;
  public anadir_pagos_distribucion: boolean = false;
  public infoNegocioCompleta: boolean = false;
  private infoGeneralDesdeNegocio: any;
  public codigoNegocio: any;  

  constructor(
    public _util: Utils,
    private _dataService: DataService,
    private _authService: AuthService,
    private _storageData: StorageDataService
  ) { }

  ngOnInit() {
    this.infoNegocioCompleta = false;
    this.loadPermisos();
    this.obtenerInfoNegocio();
  }

  async asyncObtenerInformacionNegocioCompleto() {
    return this._dataService.getObtenerInformacionNegocio(this.codigoNegocio).toPromise();
  }

  private obtenerInfoNegocio() {
    this.infoNegocioCompleta = false;
    let data: any = null;
    let tiposNegocios: boolean = false;
    let negocioCompleto: boolean = false;
    this.infoGeneralDesdeNegocio = this._storageData.getInfoNegocioDetalle();
    this.codigoNegocio = this.infoGeneralDesdeNegocio.codigoSFC;
    data = this.asyncObtenerInformacionNegocioCompleto();
    
    data.then((res) => {
      this._storageData.setInfoNegocioDetalle(res);     
    });

    this.infoGeneralDesdeNegocio = this._storageData.getInfoNegocioDetalle();
    if (this.infoGeneralDesdeNegocio.tiposNegocio.length > 0) {
      tiposNegocios =  true;
    }    
    negocioCompleto = this.infoGeneralDesdeNegocio.completo;
    if (tiposNegocios && negocioCompleto) {
      this.infoNegocioCompleta = true;
    }        
  }

  private loadPermisos() {
    this.ver_historial_pagos_distribucion = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'negocios_historial_pagos');
    this.ver_anadir_pagos_distribucion = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'negocios_entrada_pagos');    
  }

  public verHistorialPagosDistribucion() {
    this._storageData.setActiveNegocioPagosDistribucion(false);
    this._storageData.setActiveNegocioHistorialPagosDistribucion(true);
    this.historial_pagos_distribucion = true; 
  }
  public anadirEntradaPago() {
    this._storageData.setActiveNegocioPagosDistribucion(false);
    this._storageData.setActivePagosDistribucion(true);
    this.anadir_pagos_distribucion = true; 
  }

}
