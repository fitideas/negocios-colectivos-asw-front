import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NegociosHistorialPagosIngresoEgresoComponent } from './negocios-historial-pagos-ingreso-egreso.component';

describe('NegociosHistorialPagosIngresoEgresoComponent', () => {
  let component: NegociosHistorialPagosIngresoEgresoComponent;
  let fixture: ComponentFixture<NegociosHistorialPagosIngresoEgresoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NegociosHistorialPagosIngresoEgresoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NegociosHistorialPagosIngresoEgresoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
