import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { MatTableDataSource } from '@angular/material';
import { Utils } from 'src/app/core/utils';
import { AuthService } from 'src/app/services/auth.service';
import { CurrencyPipe } from '@angular/common';

export interface ListConceptos {
  concepto?: string;
  fecha?: string;
  valor?: string
}

export interface Parametros {
  codigoNegocio: string;
  codigoTipoNegocio: string;
  periodo: any;
}


@Component({
  selector: 'app-negocios-historial-pagos-ingreso-egreso',
  templateUrl: './negocios-historial-pagos-ingreso-egreso.component.html',
  styleUrls: ['./negocios-historial-pagos-ingreso-egreso.component.css']
})

/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Abril 2020
 * @Proposito: clase para mostrar el historial de pagos distribucion ingresos y egresos por negocio.
 */
export class NegociosHistorialPagosIngresoEgresoComponent implements OnInit {

  public infoNegocio: any;
  public codigoNegocio: string;
  public periodo: any[];
  public tipoNegocio: string;
  public request: Parametros;


  public submitted: boolean = false;
  public dataNotFound: boolean = null;
  public dataFound: boolean = null;
  public ver_historial_pagos_distribucion: boolean = false;

  dataSourceIngreso: MatTableDataSource<ListConceptos>;
  dataSourceEgreso: MatTableDataSource<ListConceptos>;
  dataSourceTotal: MatTableDataSource<ListConceptos>;

  displayedColumns: string[] = [
    'concepto',
    'fecha',
    'valor'
  ];

  constructor(
    public _util: Utils,
    private _authService: AuthService,
    private _dataService: DataService,
    private currencyPipe: CurrencyPipe,
    private _storageData: StorageDataService
  ) { }

  ngOnInit() {
    this.loadPermisos();
    this.loadInfoNegocio();
    this.tipoNegocio = this._storageData.getNegocioHistorialPagosIngresoEgresoTipoNegocio();
    this.periodo = this._storageData.getNegocioHistorialPagosIngresoEgresoPeriodo();
    this.request = {
      codigoNegocio: this.codigoNegocio,
      codigoTipoNegocio: this.tipoNegocio,
      periodo: this.periodo
    };
    this.negocioListHistorialPagosIngresoEgreso(this.request);    
  }

  private loadPermisos() {
    this.ver_historial_pagos_distribucion = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'negocios_historial_pagos');    
  }

  private loadInfoNegocio() {
    this.infoNegocio = this._storageData.getInfoNegocioDetalle();
    this.codigoNegocio = this.infoNegocio.codigoSFC;
  }

  
  setFormatNumberDecimal(monto, decimal) {
    let valor = parseFloat(monto).toFixed(decimal);
    if (valor == 'NaN') {
      valor = '0';
    }
    monto = this.currencyPipe.transform(valor);
    return monto;
  }

  private negocioListHistorialPagosIngresoEgreso(requestBuscar) {
    this.dataSourceIngreso = null;
    this.dataSourceEgreso = null;
    this.dataSourceTotal = null;
    let listConceptos: ListConceptos[] = new Array();
    let conceptos: ListConceptos;
    this._dataService
      .negocioListHistorialPagosIngresoEgreso(requestBuscar)
      .subscribe((data: any) => {
        if (!data) {
          this.dataNotFound = true;
          this.dataFound = false;
          document.getElementById("dataFound").style.display = 'none';
          document.getElementById("dataFoundBoton").style.display = 'none';
          document.getElementById("dataNotFound").style.display = 'inline';
          return;
        }

        this.dataFound = true;
        this.dataNotFound = false;
        document.getElementById("dataFound").style.display = 'inline';
        document.getElementById("dataFoundBoton").style.display = 'inline';
        document.getElementById("dataNotFound").style.display = 'none';

        listConceptos = [];
        data.listaIngresos.forEach(element => {

          conceptos = {
            concepto: element.concepto,
            fecha: element.fecha,
            valor: this.setFormatNumberDecimal(element.valor, 2),
          }
          listConceptos.push(conceptos);
        });
        conceptos = {
          concepto: 'TOTAL INGRESOS',
          fecha: '',
          valor: this.setFormatNumberDecimal(data.ingresosTotal, 2),
        };
        listConceptos.push(conceptos);
        this.dataSourceIngreso = new MatTableDataSource(listConceptos);
        listConceptos = [];

        data.listaObligaciones.forEach(item => {
          conceptos = {
            concepto: item.concepto,
            fecha: item.fecha,
            valor: this.setFormatNumberDecimal(item.valor, 2),
          }
          listConceptos.push(conceptos);
        });
        conceptos = {
          concepto: 'TOTAL ENGRESOS',
          fecha: '',
          valor: this.setFormatNumberDecimal(data.obligacionesTotal, 2),
        };
        listConceptos.push(conceptos);
        this.dataSourceEgreso = new MatTableDataSource(listConceptos);

        listConceptos = [];
        conceptos = {
          concepto: 'TOTAL A DISTRIBUIR',
          fecha: '',
          valor: this.setFormatNumberDecimal(data.totalDistribuir, 2),
        };
        listConceptos.push(conceptos);
        this.dataSourceTotal = new MatTableDataSource(listConceptos);
      },
        (err) => {
          this.dataNotFound = true;
          this.dataFound = true;
          document.getElementById("dataFound").style.display = 'none';
          document.getElementById("dataFoundBoton").style.display = 'none';
          document.getElementById("dataNotFound").style.display = 'inline';
        }
      );
  }

  public ver_beneficiarios_pagos() {
    this._storageData.setActiveNegocioPagosDistribucion(false);
    this._storageData.setActiveNegocioHistorialPagosDistribucion(false);
    this._storageData.setActivePagosDistribucion(false);
    this._storageData.setActiveNegocioHistorialPagosIngresoEgreso(false);
    this._storageData.setActiveNegocioHistorialPagosBeneficiarios(true);
  }

}
