import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagoListarTitularesBenComponent } from './pago-listar-titulares-ben.component';

describe('PagoListarTitularesBenComponent', () => {
  let component: PagoListarTitularesBenComponent;
  let fixture: ComponentFixture<PagoListarTitularesBenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagoListarTitularesBenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagoListarTitularesBenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
