import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from "@angular/core";
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from "@angular/material";
import { ListTitularBeneficiario } from "src/app/modelo/negocio-lista-titulares-ben";
import { StorageDataService } from "src/app/services/storage-data.service";
import { DataService } from "src/app/services/data.service";
import { MensajeEsperaComponent } from 'src/app/dialogs/mensaje-espera/mensaje-espera.component';

@Component({
  selector: "app-pago-listar-titulares-ben",
  templateUrl: "./pago-listar-titulares-ben.component.html",
  styleUrls: ["./pago-listar-titulares-ben.component.css"]
})
/**
 * @Autor: Lizeth Patiño
 * @Fecha: Abril 2020
 * @Proposito: clase para listar titulares y generar archivo
 */

export class PagoListarTitularesBenComponent implements OnInit {
  dataSource: MatTableDataSource<ListTitularBeneficiario>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  resultsLength: any;
  @Output() propagar = new EventEmitter<boolean>();
  @Input() informacionQueLlegoServicio: any;

  displayedColumns: string[] = [
    "nombreTitular",
    "tipoDocumentoTitular",
    "documentoTitular",
    "porcentajeParticipacion",
    "numeroDerechos",
    "nombreBeneficiario",
    "tipoDocumento",
    "documento",
    "porcentajeGiro",
    "porcentajedistribuido",
    "valorGiro",
    "porcentajeParticipacionDistribuido",
    "subtotalPago",
    "gravamenMovimientoFinanciero",
    "tipoPago",
    "valorTipoPago",
    "subtotalGirado",
    "tipoCuenta",
    "cuentaEncargo",
    "entidadBancariaFideicomiso",
    "codigoBanco"
  ];
  listaBeneficiariosTitulares: boolean;
  pintarIngresos: boolean = false;
  listaConceptoDos: any;
  datosParaCalcularDistribucion: any;
  public page: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPages: number = 1; //Número total de páginas 
  public numElementos: number = 10; //Total de elemento por pagina
  public request: any;
  @Input('childToMaster') reloadVinculado: boolean;
  vistaDos: boolean=false;
  periodoElegido: any;
  vistaListarTitulares: boolean=false;
  listaIngresoPagosIngresos: boolean = false;

 
  constructor(
    private _storageData: StorageDataService,
    private _dataService: DataService,
    public dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.vistaListarTitulares=this._storageData.getRegresarPagoDistribucionVistaDos();
    this.periodoElegido=this._storageData.getPagoDistribucionEnviarPeriodo();
    this.listaConceptoDos = this._storageData.getListarConcepto();
    this.datosParaCalcularDistribucion = this._storageData.getInfoNegocioTB();
    let pagina = 1;
    this.mostraPaginaDataLocal(pagina);
      
  }

  public regresar() {
    this.vistaListarTitulares=false;
    this._storageData.setRegresarPagoDistribucionVistaDos(this.vistaListarTitulares);
    this.listaIngresoPagosIngresos = false;
    this._storageData.setRegresarPagoDistribucionVistaUno(this.listaIngresoPagosIngresos);
  }
  

  public generarArchivo() {
    this._dataService
      .negocioGenerarPago(
        this.datosParaCalcularDistribucion.periodo,
        this.datosParaCalcularDistribucion.codigoNegocio,
        this.datosParaCalcularDistribucion.codigoTipoNegocio
      )
      .subscribe((informacionArchivo: any) => {
        let data;
        data = {
          urlimg: false
        };
        const cargaDialog=this.dialog.open(MensajeEsperaComponent, {
          width: "46rem",
          data
        });
        setTimeout (() => {
          var nameOfFileToDownload = informacionArchivo["nombreArchivoZip"];
          const byteCharacters = atob(informacionArchivo["recursoB64"]);
          const byteNumbers = new Array(byteCharacters.length);
          for (let i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
          }
          const byteArray = new Uint8Array(byteNumbers);
          const blob = new Blob([byteArray], {
            type: informacionArchivo.tipoArchivo
          });
          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob, nameOfFileToDownload);
          } else {
            var a = document.createElement("a");
            a.href = URL.createObjectURL(blob);
            a.download = nameOfFileToDownload;
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
            cargaDialog.close();
          }
       }, 4000); 
      
      });
  }

  mostraPaginaDataLocal(pagina) {
    let registrosPagina: any[] = new Array();
    this.page = pagina;
    this.numElementos = 10;
    this.resultsLength = this.informacionQueLlegoServicio.listaBeneficiarios.length;
    this.totalPages = this.resultsLength / this.numElementos;
    let totalPagina = this.resultsLength % this.numElementos;
    if (totalPagina > 1) {
      this.totalPages = Number(parseInt(String(this.totalPages))) + 1;
    }
    if (this.totalPages < 1) {
      this.totalPages = 1;
    }   

    let inicioRegistros = ((pagina - 1) * this.numElementos);
    let ultimoRegistros = pagina * this.numElementos;
    for (let i = inicioRegistros; ((i < this.informacionQueLlegoServicio.listaBeneficiarios.length) && (i < ultimoRegistros)); i++) {
      registrosPagina.push(this.informacionQueLlegoServicio.listaBeneficiarios[i]);
    }

    this.dataSource = new MatTableDataSource(registrosPagina);         
    this.dataSource.sort = this.sort;
  }

  goToPage(page: number) {
    this.mostraPaginaDataLocal(page);
  }
}
