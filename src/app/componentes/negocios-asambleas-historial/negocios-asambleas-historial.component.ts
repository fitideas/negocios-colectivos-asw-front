import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Router } from '@angular/router';

export interface CriterioList {
  value: string;
  viewValue: string;
  active: boolean;
}

interface PeticionHistorialAsambleas{
  codigoSFC: string;
  tipoReunion: string;
  usuario: any[];
  numeroRadicado: string;
  decision:string;
  tareaPendiente:string;
  rangoFechas:any[];
}

interface ListAsambleas {
  codigoSFC?: string;
  fechaReunion?: string;
  numeroRadicado?: string;
  usuario?: string;
}

@Component({
  selector: 'app-negocios-asambleas-historial',
  templateUrl: './negocios-asambleas-historial.component.html',
  styleUrls: ['./negocios-asambleas-historial.component.css']
})
export class NegociosAsambleasHistorialComponent implements OnInit {

  

  public buscarHistorialAsambleasDynamicForm: FormGroup;
  

  
  get filtrosFormArray() {
    return <FormArray>this.buscarHistorialAsambleasDynamicForm.get("filtros");
  }
  public submitted: boolean = false;
  public dataNotFound: boolean = null;
  public dataFound: boolean = null;  
  public arrayFiltro: any[] = [];
  public nuevoItem: boolean = false;
  public listFechaRango: any[] = [];
  public listUsuario: any[] = [];
  public isFecha: boolean = false;
  public isUsuario: boolean = false;
  public isNumeroRadicado: boolean = false;
  public isDecision: boolean = false;
  public isPendiente: boolean = false;
  public isTodo: boolean = false;
  public swAddFiltros = true;
  public infoNegocio: any;
  public numElementos: number = 10;
  listSelectCriterio: CriterioList[] = [
    { value: "fecha", viewValue: "Fecha", active: true },
    { value: "numeroRadicado", viewValue: "Número de Radicado", active: true },
    { value: "decision", viewValue: "Decisiones", active: true },
    { value: "pendiente", viewValue: "Tareas Pendientes", active: true },
    { value: "usuario", viewValue: "Usuario", active: true }
  ];

  listSelectCriterioNuevo: CriterioList[] = [];

  displayedColumns: string[] = [
    'fecha',
    'usuario',
    'numeroRadicado',
    'opciones'
  ];
  
  dataSource: MatTableDataSource<ListAsambleas>;
  public resultsLength: number = 0;

  constructor(
    public fb: FormBuilder,
    private _dataService: DataService,
    private _router:Router,
    private _storageData: StorageDataService,
    public dialog: MatDialog
  ) {

  }

  ngOnInit() {
    this.loadInfoNegocio();
    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarHistorialAsambleasDynamicForm = this.fb.group({
      filtros:this.fb.array([])
    });
    this.addToFiltrosFormArray();
    document.getElementById("dataFound").style.display = 'none';
    document.getElementById("dataNotFound").style.display = 'none';
  }

  checkFiltros() {
    if (this.filtrosFormArray.length == 1) {
      return false
    }
    return true;
  }

  removeFromFiltrosFormArray(index, itemFiltro) {
    this.filtrosFormArray.removeAt(index);

    this.arrayFiltro = this.arrayFiltro.filter(item => {
      return (item !== itemFiltro.value.selectCriterio);
    });

    if (this.filtrosFormArray.length < 5) {
      this.swAddFiltros = true;
    }

    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == itemFiltro.value.selectCriterio) {
        element.active = true;
      }
    });
  }

  createGroup() {
    return this.fb.group({
      selectCriterio: new FormControl("", []),
      argumento: new FormControl("", []),

    });
  }

  addToFiltrosFormArray() {
    this.filtrosFormArray.push(this.createGroup());
    if (this.filtrosFormArray.length == 5) {
      this.swAddFiltros = false;
    }
    this.nuevoItem = true;
  }

  buscarListaXCriterioSeleccionado(eventSeleccionado, ind) {
    this.isFecha = false;
    this.isUsuario = false;
    this.isPendiente = false;
    this.isNumeroRadicado = false;
    this.isDecision = false;
    this.isTodo = true;
    
    if (eventSeleccionado.value == 'usuario') {
      this.arrayFiltro[ind] = 'usuario';
      this.isUsuario = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }
    if (eventSeleccionado.value == 'fecha') {
      this.arrayFiltro[ind] = 'fecha';
      this.isFecha = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }
    if (eventSeleccionado.value == 'numeroRadicado') {
      this.arrayFiltro[ind] = 'numeroRadicado';
      this.isNumeroRadicado = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }
    if (eventSeleccionado.value == 'decision') {
      this.arrayFiltro[ind] = 'decision';
      this.isDecision = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }
    if (eventSeleccionado.value == 'pendiente') {
      this.arrayFiltro[ind] = 'pendiente';
      this.isPendiente = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }
  
    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == eventSeleccionado.value) {
        element.active = false;
      }
      if (!this.isTodo) {
        if (element.value == 'todo') {
          element.active = false;
        }
      }
    });
  }

  limpiarformulario() {
    this.arrayFiltro = [];
    this.filtrosFormArray.clear();
    this.filtrosFormArray.reset([]);
    this.dataNotFound = false;
    this.dataFound = false;
    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarHistorialAsambleasDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();
    this.listUsuario = [];
    this.listFechaRango = [];
    this.resultsLength = 0;
    this.swAddFiltros = true;
    this.listSelectCriterioNuevo.forEach(element => {
      element.active = true;
    });
    document.getElementById("dataFound").style.display = 'none';
    document.getElementById("dataNotFound").style.display = 'none';
  }

  getDateRange(range) {
    this.listFechaRango = range;
  }

  getUsuarios(usuario) {
    this.listUsuario = usuario;
  }

  private loadInfoNegocio() {
    this.infoNegocio = this._storageData.getInfoNegocioDetalle();
  }

  private obtieneValorPorCriterio(criterio:string):string{
    var encontrado = this.filtrosFormArray.controls.filter(element => element.value.selectCriterio === criterio);
    return encontrado.length > 0 ? encontrado[0].value.argumento : "";
  }

  enviarDatos(listar): void {
    this.dataNotFound = false;
    this.dataFound = false;
    this.submitted = true;
    let request: PeticionHistorialAsambleas;
    request = {
      rangoFechas: this.listFechaRango,
      codigoSFC: this.infoNegocio.codigoSFC,
      usuario: this.listUsuario,
      tipoReunion:this._storageData.getTipoReunion(),
      decision:this.obtieneValorPorCriterio("decision"),
      numeroRadicado: this.obtieneValorPorCriterio("numeroRadicado"),
      tareaPendiente:this.obtieneValorPorCriterio("pendiente")
    }
    if (listar) {
      let requestPaginado: any = request;
      requestPaginado.pagina = 1;
      requestPaginado.elementos = this.numElementos;
      this.obtenerHistorialAsambleas(requestPaginado);
    }
  }

  obtenerHistorialAsambleas(request: PeticionHistorialAsambleas) {
    this.dataSource = null;
    let listReuniones: ListAsambleas[] = new Array();
    let reunion: ListAsambleas;
    this._dataService
      .listadoAsambleasHistorial(request)
      .subscribe((data: any) => {
        if (!data || data.totalRegistro == 0) {
          this.dataNotFound = true;
          this.dataFound = false;
          document.getElementById("dataFound").style.display = 'none';
          document.getElementById("dataNotFound").style.display = 'inline';
          return;
        }

        this.dataFound = true;
        this.dataNotFound = false;
        document.getElementById("dataFound").style.display = 'inline';
        document.getElementById("dataNotFound").style.display = 'none';

        data.historicoReuniones.forEach(element => {
          reunion = {
            codigoSFC:element.codigoSFC,
            fechaReunion:element.fechaReunion,
            numeroRadicado:element.numeroRadicado,
            usuario:element.usuario
          }
          listReuniones.push(reunion);
        });

        this.dataSource = new MatTableDataSource(listReuniones);
        this.resultsLength = data.totalRegistros;
      },
        (err) => {
          this.dataNotFound = true;
          this.dataFound = true;
          document.getElementById("dataFound").style.display = 'none';
          document.getElementById("dataNotFound").style.display = 'inline';
        }
      );
  }

  obtenerInformacionReunion(codigoSFC, fecha, numeroRadicado){
    let infoReunion = {
      codigoSFC,
      fecha,
      numeroRadicado
    }
    if(this._storageData.getTipoReunion() === 'asamblea'){
      this._storageData.setInfoReunion(infoReunion);
        this._storageData.setActiveReunionDetalles(true);
        this._storageData.setActiveAsambleaHistorial(false);
        this._storageData.setAsambleaHistorial(false);     
    }else {
        this._storageData.setInfoReunion(infoReunion);
        this._storageData.setActiveComiteDetalles(true);
        this._storageData.setActiveAsambleaHistorial(false);
        this._storageData.setAsambleaHistorial(false);
    }
  } 

}
