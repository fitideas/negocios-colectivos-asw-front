import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NegociosAsambleasHistorialComponent } from './negocios-asambleas-historial.component';

describe('NegociosAsambleasHistorialComponent', () => {
  let component: NegociosAsambleasHistorialComponent;
  let fixture: ComponentFixture<NegociosAsambleasHistorialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NegociosAsambleasHistorialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NegociosAsambleasHistorialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
