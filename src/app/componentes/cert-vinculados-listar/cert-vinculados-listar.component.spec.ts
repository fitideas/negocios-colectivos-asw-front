import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertVinculadosListarComponent } from './cert-vinculados-listar.component';

describe('CertVinculadosListarComponent', () => {
  let component: CertVinculadosListarComponent;
  let fixture: ComponentFixture<CertVinculadosListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertVinculadosListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertVinculadosListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
