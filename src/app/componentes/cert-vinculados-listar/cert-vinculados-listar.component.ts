import { Component, OnInit, ViewChild } from "@angular/core";
import { DataService } from "src/app/services/data.service";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { ListTitulares } from 'src/app/modelo/list-titulares';
import { ResponseTitulares } from 'src/app/modelo/response-titulares';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { environment } from 'src/environments/environment';

export interface Criterio {
  value: string;
  viewValue: string;
}

/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: junio 2020
 * @Proposito: clase para mostrar las los vinculados para generar certificados
 */

@Component({
  selector: 'app-cert-vinculados-listar',
  templateUrl: './cert-vinculados-listar.component.html',
  styleUrls: ['./cert-vinculados-listar.component.scss']
})
export class CertVinculadosListarComponent implements OnInit {

  displayedColumns: string[] = [
    "nombre",
    "tipoDocumento",
    "numeroDocumento",
    "id"
  ];

  selectCriterio: Criterio[] = [
    { value: "numeroIdentificacion", viewValue: "Número de Identificación" },
    { value: "nombre", viewValue: "Nombre/Apellidos" }
  ];
  tiposCertificado: any[] = [
    {
      value: "PART",
      viewValue: "Certificado de participación"
    },
    {
      value: "INGRET",
      viewValue: "Certificado de ingresos y retenciones"
    },
    {
      value: "RENTAEXC",
      viewValue: "Certificado de renta exenta"
    }
  ];


  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  public buscarNegociosForm: FormGroup;
  public iniciarPestanaVinculado: number = 0;
  public dataSource: MatTableDataSource<ListTitulares>;
  public dataNotFound: boolean = false;
  public dataFound: boolean = false;
  public listTitulares: ListTitulares[] = new Array();
  public listAnoSelect: any[] = [];
  public certificadoSeleccionado: string = "";
  public periodoSeleccionado: string = "";
  public titulares: ResponseTitulares;
  public nombreSeleccionado: string = "";
  public tipoDocumentoSeleccionado: string = "";
  public numeroDocumentoSeleccionado: string = "";
  public verMas: boolean = false;

  public page: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPages: number = 1; //Número total de páginas 
  public numElementos: number = 10; //Total de elemento por pagina
  public request: any;
  public resultsLength = 0;
  private ELEMENTOS_POR_PAGINA: number = environment.elementosPorPagina;

  constructor(
    private _dataService: DataService,
    private _storageData: StorageDataService,
    public fb: FormBuilder
  ) {
    this.buscarNegociosForm = this.fb.group(
      {
        selectCriterio: new FormControl("", [Validators.required]),
        argumento: new FormControl("", [Validators.required]),
        tiposCertificado: new FormControl("", [Validators.required]),
        anoGrabable: new FormControl("", [Validators.required])
      }
    )
  }

  ngOnInit() {
    this.iniciarPestanaVinculado = this._storageData.getInicializarCertificadosVinculados();
    this.comboAno();
    this.listTitulares = new Array();
    this.dataNotFound = false;
    this.dataFound = false;
  }

  get f() {
    return this.buscarNegociosForm.controls;
  }

  private comboAno() {
    let d = new Date();
    let n = d.getFullYear();
    let year: any;
    for (let i = n; i >= 1992; i--) {
      year = {
        value: i,
        viewValue: i
      }
      this.listAnoSelect.push(year);
    }
  }

  public limpiarformulario() {
    this.dataSource = null;
    this.dataNotFound = false;
    this.dataFound = false;
    this.dataSource = null;
    this.buscarNegociosForm.controls['selectCriterio'].setValue('');
    this.buscarNegociosForm.controls['argumento'].setValue('');
    this.buscarNegociosForm.controls['tiposCertificado'].setValue('');
    this.buscarNegociosForm.controls['anoGrabable'].setValue('');
  }

  goToPage(page: number) {
    this.mostraPaginaDataLocal(page);
  }

  loadTitulares(titulares): void {
    this.listTitulares = [];
    titulares.forEach(t => {
      this.listTitulares.push(t);
    });
  }

  enviarDatos(): void {
    this.dataNotFound = false;
    this.dataFound = false;
    if (this.buscarNegociosForm.invalid) {
      return;
    }

    if (this.buscarNegociosForm.value.selectCriterio) {
      if (this.buscarNegociosForm.value.selectCriterio == 'numeroIdentificacion') {
        this.request = {
          codigoNegocio: "",
          nombreNegocio: "",
          numeroDocumento: this.buscarNegociosForm.value.argumento,
          tipoDocumento: "",
          nombreTitular: "",
          anoGrabable: this.periodoSeleccionado,
          tipoCertificado: this.certificadoSeleccionado

        }
      }
      if (this.buscarNegociosForm.value.selectCriterio == 'nombre') {
        this.request = {
          codigoNegocio: "",
          nombreNegocio: "",
          numeroDocumento: "",
          tipoDocumento: "",
          nombreTitular: this.buscarNegociosForm.value.argumento,
          anoGrabable: this.periodoSeleccionado,
          tipoCertificado: this.certificadoSeleccionado
        }
      }
    }

    let requestPaginado: any = this.request;
    requestPaginado.pagina = 1;
    requestPaginado.elementos = this.numElementos;
    this.buscarTitularesList(requestPaginado);
  }

  buscarTitularesList(requestBuscar) {
    this.titulares = null;
    this._dataService.postCertificadosTitularesListRequest(requestBuscar)
      .subscribe((data: any) => {
        if (!data || data.totalTitulares == 0) {
          this.dataNotFound = true;
          this.dataFound = false;
          return;
        } else {
          this.dataFound = true;
          this.dataNotFound = false;
          this.titulares = data;
        }
        setTimeout(() => {
          this.loadTitulares(this.titulares['titulares']);
          let pagina = 1;
          this.mostraPaginaDataLocal(pagina);
        });
      },
        (err) => {
          this.dataNotFound = true;
        }
      );
  }

  mostraPaginaDataLocal(pagina) {
    let registrosPagina: any[] = new Array();
    this.page = pagina;
    this.numElementos = this.ELEMENTOS_POR_PAGINA;
    this.resultsLength = this.listTitulares.length;
    this.totalPages = this.resultsLength / this.numElementos;
    let totalPagina = this.resultsLength % this.numElementos;
    if (totalPagina > 1) {
      this.totalPages = Number(parseInt(String(this.totalPages))) + 1;
    }
    if (this.totalPages < 1) {
      this.totalPages = 1;
    }

    let inicioRegistros = ((pagina - 1) * this.numElementos);
    let ultimoRegistros = pagina * this.numElementos;
    for (let i = inicioRegistros; ((i < this.listTitulares.length) && (i < ultimoRegistros)); i++) {
      registrosPagina.push(this.listTitulares[i]);
    }

    this.dataSource = new MatTableDataSource(registrosPagina);         
    this.dataSource.sort = this.sort;
  }

  mostrarSubbusqueda(nombre: string, tipoDoc: string, numeroDoc: string) {
    this.verMas = true;
    this.nombreSeleccionado = nombre;
    this.tipoDocumentoSeleccionado = tipoDoc;
    this.numeroDocumentoSeleccionado = numeroDoc;
    this._storageData.setInicializarCertificadosVinculados(1);
    this.iniciarPestanaVinculado = this._storageData.getInicializarCertificadosVinculados();
  }

  setTipoCertificado(valor: string) {
    this.certificadoSeleccionado = valor;
  }

  setAnoSeleccionado(valor: string) {
    this.periodoSeleccionado = valor;
  }

}
