import { Component, OnInit, Input } from '@angular/core';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { MatDialog } from '@angular/material';
import { DataService } from 'src/app/services/data.service';
import { FormBuilder, FormGroup, FormArray, Validators} from '@angular/forms';
import { Utils } from 'src/app/core/utils';
import { AuthService } from 'src/app/services/auth.service';
import { NegocioInfoGeneral } from 'src/app/modelo/negocio-general-info';
import { NegocioTercero } from 'src/app/modelo/negocio-terceros';
import { DialogTwoButtonsComponent } from '../dialog-two-buttons/dialog-two-buttons.component';
import { formatDate, DatePipe, CurrencyPipe, PercentPipe, DecimalPipe } from '@angular/common';
import { NegocioEquipoComponent } from '../negocio-equipo/negocio-equipo.component';
import { DialogNegocioListTitularesComponent } from 'src/app/dialogs/dialog-negocio-list-titulares/dialog-negocio-list-titulares.component';
import { DialogBuscarArchivoComponent } from 'src/app/dialogs/dialog-buscar-archivo/dialog-buscar-archivo.component';
import { DialogParametrosSaveComponent } from '../dialog-parametros-save/dialog-parametros-save.component';
import { isDate } from 'util';
import { Router } from '@angular/router';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import { MY_FORMATS } from './../../datepickers/datepicker-mes-anio/datepicker-mes-anio.component';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-negocio-detalle',
  templateUrl: './negocio-detalle.component.html',
  styleUrls: ['./negocio-detalle.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})

/**
* Autor:Lizeth Patiño
* Actualización: José Reinaldo García Becerra
* Proposito: Formulario de creación de negocio
* Fecha: 08/04/2020
*/

export class NegocioDetalleComponent implements OnInit {

  private PAIS_DEFECTO: string = environment.codigoPaisDefecto;
  private DEPARTAMENTO_DEFECTO: string = environment.codigoDepartamentoDefecto;
  private CIUDAD_DEFECTO: string = environment.codigoCiudadDefecto;

  public informacionGeneralNegocioForm: FormGroup;
  public objetoNegocioGeneralDesdeBack: NegocioInfoGeneral;
  public estadosProyecto =
    [
      { codigo: "LIQUIDADO", descripcion: "LIQUIDADO" },
      { codigo: "VIGENTE", descripcion: "VIGENTE" },
    ];

  public unidadComision =
    [
      { id: '', descripcion: 'SELECCIONAR' },
      { id: '%', descripcion: '%' },
      { id: 'COP', descripcion: 'COP' }
    ];

  public  listaCalidadfiduciaria  =[
    "PROPIETARIO","PARTICIPE"
  ];

  public  listaCalidadfideicomitente  =[
    "GESTOR","OPERADOR"
  ];

  @Input() datosDeNegocioGeneral: any;
  tiposDeNegocio: any[] = [];
  public arrayFiltro: any[] = [];
  datosQuevienenDeTerceros: any[];
  datosQuevienenDeNegocio: any;
  datosAEnviarATerceros: NegocioTercero[];
  objetoPaBack: NegocioInfoGeneral;
  codigoNegocio: any;
  nombre_negocio: any;
  datosAenviarNegocio: any[];
  fechasImportantes: any;
  responseData: any;
  infoGeneralDesdeNegocio: any;
  tipoTerceros: any;
  botonGuardarInformacion: boolean = false;
  botonCargarArchivo: boolean = false;
  botonVerTitulares: boolean = false;
  banderallamarTipoNegocio: boolean;
  public codigoSFC: string;
  public asamblea_comite: boolean = false;
  public asamblea_agregar_escribe: boolean = false;
  public negocio_detalle_permiso: boolean = false;
  public negocio_detalle_escribe: boolean = false;
  banderaFormularioNuevo: boolean = true;
  private swBotonArchivo: boolean = false;
  public fechaMaxLimiteApertura = new Date();
  public fechaMinLimiteApertura = new Date();
  public fechaMaxLimiteConstitucion = new Date();
  public fechaMinLimiteConstitucion = new Date();
  public fechaLimiteLiquidacion = new Date();
  public agregarTipoNegocio: boolean = false;
  public agregarMasTipoNegocio: boolean = false;

  public periodicoLista = [
    { codigo: 1, descripcion: "Enero" },
    { codigo: 2, descripcion: "Febrero" },
    { codigo: 3, descripcion: "Marzo" },
    { codigo: 4, descripcion: "Abril" },
    { codigo: 5, descripcion: "Mayo" },
    { codigo: 6, descripcion: "Junio" },
    { codigo: 7, descripcion: "Julio" },
    { codigo: 8, descripcion: "Agosto" },
    { codigo: 9, descripcion: "Septiembre" },
    { codigo: 10, descripcion: "Octubre" },
    { codigo: 11, descripcion: "Noviembre" },
    { codigo: 12, descripcion: "Diciembre" },
  ];
  listRetenciones: any;
  listaTipoDocumento: any;
  negociosActivos: boolean;
  listTipoGasto: any;
  listUnidad: any;
  noPuedeEditar: boolean = false;
  fechasImportantesSeleccionables: (valorA: any) => void;
  banderaEsconderFecha: boolean;
  estadoProyecto: any;
  numberTab: string = "0";
  request: { argumento: any; criterioBuscar: string; pagina: string; elementos: string; };
  tiposFideicomiso: any;
  subTipoFide: any;
  listaPaisesConstantes: any;
  listaMunicipiosConstantes: any;
  listaDepartamentosConstantes: any;
  listaDepartamentosAduana: any;
  listaMunicipiosAduana: any;
  listaDepartamentosUbicacion: any;
  listaMunicipiosUbicacion: any;
  listaPaisesUbicacion: any;  

  setFormatNumberDecimal(event, tipoDato, unidad2 = null) {
    let val: any = null;
    if (tipoDato == undefined) tipoDato = unidad2;
    switch (tipoDato) {
      case 'COP':
        if (event.target.value == undefined) {
          event.target.value = 0;
        } else {
          if (tipoDato == 'NaN') {
            event.target.value = 0;
          }
          else {
            event.target.value = this._util.cambiarFloatante(event.target.value);
            val = parseFloat(event.target.value).toFixed(2);
            event.target.value = this.currencyPipe.transform(val, 'USD');
          }
        }
        break;
      case '%':
        if (event.target.value > 100 || event.target.value < 0) {
          event.target.value = 0;
        } else {
          if (tipoDato == 'NaN') {
            event.target.value = 0;
          } else {
            val = parseFloat(event.target.value).toFixed(2);
            if (val == 'NaN') {
              val = 0;
            }
            event.target.value = this.decimalPipe.transform(val);
          }
        }
        break;
      case undefined:
        event.target.value = 0;
    }

    return event;
  }
  public setFormatNumeroDocumento(event, tipoDato, terceroTipoDocumento2) {
    let soloNumeros = null;
    let guion = 45;
    if (tipoDato == undefined) tipoDato = terceroTipoDocumento2;
    let objetoTipoDocumento = this._util.buscarObjetoId(this.listaTipoDocumento, tipoDato);
    switch (objetoTipoDocumento.valor) {
      case 'CC':
      case 'CE':
      case 'TI':
      case 'FID':
      case 'RUC':
        if (!this._util.numberCaracterEspecial(event, soloNumeros)) {
          event.preventDefault();
        }
        break;

      case 'NIT':
      case 'UNP':
      case 'PAS':
        if (!this._util.numberCaracterEspecial(event, guion)) {
          event.preventDefault();
        }
        break;
      case 'RCI':
      case 'CAE':
      case 'OTR':
      case 'CD':
      case 'DE':
      case 'EE':
        break;
      default:
        break
    }
    return event;
  }
  public cantidadFICRepetidos: number = 0;
  public cantidadFechaClaveRepetida: number = 0;
  public cantidadRetencionRepetida: number = 0;
  public cantidadGastosRepetido: number = 0;
  public cantidadTercerosRepetido: number = 0;

  selectAll(ix,iz,listaValores, campo, segmento ) {
    let valor = new Array();
    listaValores.forEach(element => {
      valor.push(element.codigo);
    });
    if (segmento == 'gastos') {
      this.actualizarGastoCampo(ix,iz,valor, campo);
    }
    if (segmento == 'terceros') {
      this.actualizarTerceroCampo(ix,iz,valor, campo);      
    }
  }

  deselectAll(ix,iz,listaValores, campo, segmento) {   
    let valor = new Array();    
    if (segmento == 'gastos') {
      this.actualizarGastoCampo(ix,iz,valor, campo);
    }
    if (segmento == 'terceros') {
      this.actualizarTerceroCampo(ix,iz,valor, campo);      
    }
  }

  constructor(
    private _storageData: StorageDataService,
    private _dataService: DataService,
    public _util: Utils,
    private _authService: AuthService,
    private _router: Router,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private decimalPipe: DecimalPipe,
    private currencyPipe: CurrencyPipe,
    private percentPipe: PercentPipe,
    private datePipe: DatePipe
  ) {

  }

  ngOnInit() {
    this.fechaLimiteLiquidacion.setDate(this.fechaLimiteLiquidacion.getDate() - 1);       
    this.inicializarFormulario();
    this.loadInfoDesdeStorage();
    
    if (this._storageData.getCodigoNegocioBenficiarioPago() == null) {      
      this.numberTab = "0";
    }
    else {
      this.numberTab = "1";            
       this._storageData.setActiveNegocioPagosDistribucion(false);
       this._storageData.setActiveNegocioHistorialPagosDistribucion(true);      
    }   
    this.loadPermisos();
  }

  public loadInfoDesdeStorage() {
    this.arrayFiltro = [];
    this.infoGeneralDesdeNegocio = this._storageData.getInfoNegocioDetalle();
    if (!this.infoGeneralDesdeNegocio || this._storageData.getSwInicio() || this.infoGeneralDesdeNegocio == undefined) {
      this._router.navigate(['accionfiduciaria']);
    }
    else {
      this.estadoProyecto = this._storageData.getEstadoProyecto();
      this.codigoSFC = this.infoGeneralDesdeNegocio.codigoSFC;
      this._storageData.setcodigoSFCNegocio(this.codigoSFC);
      this.obtenerServiciosParaFormGeneral();
    }
  }

  inicializarFormulario() {
    this.estadoProyecto = this._storageData.getEstadoProyecto();
    this.codigoSFC = null;
    this.infoGeneralDesdeNegocio = null;
    this.informacionGeneralNegocioForm = this.fb.group({
      fechaApertura: [''],
      numeroDerechosTotales: ['', Validators.pattern('[0-9]+')],
      fechaLiquidacionNegocioPromotor: [''],
      fechaConstitucionNegocio: [''],
      estado: ['', Validators.compose([Validators.required])],
      avaluoInmueble: ['', [Validators.minLength(1), Validators.maxLength(20)]],
      valorUltimoPredial: ['', [Validators.minLength(1), Validators.maxLength(20)]],
      fechaCobroPredial: [''],
      valorUltimaValorizacion: ['', [Validators.minLength(1), Validators.maxLength(20)]],
      fechaCobroValorizacion: [''],
      seccionalAduanaDepartamento: [this.DEPARTAMENTO_DEFECTO,Validators.compose([Validators.required])],
	    seccionalAduanaCiudad: [this.CIUDAD_DEFECTO,Validators.compose([Validators.required])],
    	matriculaInmobiliaria: ['',Validators.compose([Validators.required])],
	    licenciaConstruccion: ['',Validators.compose([Validators.required])],
	    ciudadUbicacionPais: [this.PAIS_DEFECTO],
	    ciudadUbicacionDepartamento: [this.DEPARTAMENTO_DEFECTO],
	    ciudadUbicacion: [this.CIUDAD_DEFECTO],
	    fideicomitenteNombreCompleto: [''],
	    fideicomitenteTipoDocumento: [''],
	    fideicomitenteNumeroDocumento: [''],
    	calidadFidecomitente: [''],
      calidadFiduciariaFideicomiso: ['',Validators.compose([Validators.required])],
      encargos: this.fb.array([]),
      terceros: this.fb.array([]),
      fechasClaves: this.fb.array([]),
      equipoResponsable: this.fb.array([]),
      tipoFideicomiso: ['', [
        Validators.required,
        Validators.pattern('[0-9]+'),
        Validators.minLength(1),
        Validators.maxLength(5)
      ]],
      subtipoFideicomiso: ['', [
        Validators.required,
        Validators.pattern('[0-9]+'),
        Validators.minLength(1),
        Validators.maxLength(5)
      ]],
      movimientoPago: ['', [
        Validators.pattern('[0-9]+'),
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(6)
      ]],
      tipoProceso: ["A", [
        Validators.required,
        Validators.pattern('[A-Z]+'),
        Validators.minLength(1),
        Validators.maxLength(6)
      ]],
      tipoMovimiento: ['', [
        Validators.pattern('[0-9]+'),
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(6)
      ]],
      codigoPais: [this.PAIS_DEFECTO, [
        Validators.pattern('[A-Z]+'),
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(6)
      ]],
      codigoDepartamento: [this.DEPARTAMENTO_DEFECTO, [
        Validators.pattern('[0-9]+'),
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(6)
      ]],
      codigoCiudad: [this.CIUDAD_DEFECTO, [
        Validators.pattern('[0-9]+'),
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(6)
      ]],
      tipoNegocios: this.fb.array([])
    });
  }
  public actualizarFechaConstitucion() {    
    let fechaLimiteLiquidacion = new Date(this.informacionGeneralNegocioForm.controls.fechaLiquidacionNegocioPromotor.value);    
    this.fechaMinLimiteConstitucion = new Date(fechaLimiteLiquidacion);
    this.fechaMinLimiteConstitucion.setDate(this.fechaMinLimiteConstitucion.getDate()); 
  }

  public actualizarFechaApertura() {
    let fechaLimiteConstitucion = new Date(this.informacionGeneralNegocioForm.controls.fechaConstitucionNegocio.value);    
    this.fechaMinLimiteApertura = new Date(fechaLimiteConstitucion);
    this.fechaMinLimiteApertura.setDate(this.fechaMinLimiteApertura.getDate()); 
  }
  
  public get arrayDeCamposFormularioTipoNegocioGeneral() {
    return <FormArray>this.informacionGeneralNegocioForm.get("tipoNegocios");
  }

  public get arrayDeCamposEncargos() {
    return (<FormArray>this.informacionGeneralNegocioForm.get('encargos'));
  }

  public get arrayDeCamposFechasClaves() {
    return (<FormArray>this.informacionGeneralNegocioForm.get('fechasClaves'));
  }

  public get arrayDeCamposEquipoResponsable() {
    return (<FormArray>this.informacionGeneralNegocioForm.get('equipoResponsable'));
  }

  private loadPermisos() {
    this.asamblea_comite = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'asamblea_comite');
    this.asamblea_agregar_escribe = this._util.permiteEscribir(this._authService.getFuncionalidades(), 'asamblea_agregar');
    this.negocio_detalle_permiso = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(),'negocio_detalle');
    this.negocio_detalle_escribe = this._util.permiteEscribir(this._authService.getFuncionalidades(), 'negocio_detalle');
  }

  public obtenerServiciosParaFormGeneral() {
     
    this._dataService
      .getTipoFideicomiso()
      .subscribe(
        (tipoFidei: any) => {
         this.tiposFideicomiso=tipoFidei;
        },
        (err: any) => {
        }
      );
      
    this._dataService
      .getServiceParametrosValoresTipoCompuesto("TIPNEG")
      .subscribe(
        (tiposNegocio: any) => {
          tiposNegocio.forEach(element => {
            element.active = true;
            this.tiposDeNegocio.push(element)
          });
          this.actualizarListasTiposNegocios();
        },
        (err: any) => {
        }
      );

    this._dataService
      .getParametrosValoresDominio("TIPGAS")
      .subscribe((tiposNegocio: any) => {
        this.listTipoGasto = tiposNegocio;
        this.listUnidad = this.unidadComision;
      });
    this._dataService.getParametrosValoresDominio('TIP_DOC').subscribe((listaTipoDocumento: any) => {
      this.listaTipoDocumento = listaTipoDocumento;
    });
    this._dataService.getParametrosValoresDominio('RET').subscribe((retencion: any) => {
      this.listRetenciones = retencion;
    });

    this._dataService.getServiceParametrosObtenerTipoTercero('tercero').subscribe((tipoTerceros: any) => {
      this.tipoTerceros = tipoTerceros;
    });
    this._dataService.getParametrosValoresDominio('FECCLA').subscribe((fechasImportantes: any) => {
      this.fechasImportantes = fechasImportantes;
    });
    this.codigoNegocio = this.infoGeneralDesdeNegocio.codigoInterno;
    this.nombre_negocio = this.infoGeneralDesdeNegocio.nombre;
    this.actualizarDepartamentoAduana(this.PAIS_DEFECTO);
    if(this.infoGeneralDesdeNegocio.seccionalAduanaCiudad!=null){
      this.actualizarMunicipioAduana(this.infoGeneralDesdeNegocio.seccionalAduanaDepartamento);
    }else{
      this.actualizarMunicipioAduana(this.DEPARTAMENTO_DEFECTO);
    }
     
    let respuestaPaisUbicacion = this.getDatosPais();
    respuestaPaisUbicacion.then((res) => {
      this.listaPaisesUbicacion = res;
    });

    if(this.infoGeneralDesdeNegocio.ciudadUbicacionPais!=null && this.infoGeneralDesdeNegocio.ciudadUbicacionDepartamento!=null &&this.infoGeneralDesdeNegocio.ciudadUbicacion!=null ){
      this.actualizarDepartamentoUbicacion(this.infoGeneralDesdeNegocio.ciudadUbicacionPais);
      this.actualizarMunicipioUbicacion(this.infoGeneralDesdeNegocio.ciudadUbicacionDepartamento);
    }else{
      this.actualizarDepartamentoUbicacion(this.PAIS_DEFECTO);
      this.actualizarMunicipioUbicacion(this.DEPARTAMENTO_DEFECTO);
    }
    this.informacionGeneralNegocioForm.get('numeroDerechosTotales').setValue(this.infoGeneralDesdeNegocio.numeroDerechosTotales);
    this.informacionGeneralNegocioForm.get('fechaApertura').setValue(this.transformarFecha(this.infoGeneralDesdeNegocio.fechaApertura));
    this.informacionGeneralNegocioForm.get('fechaConstitucionNegocio').setValue(this.transformarFecha(this.infoGeneralDesdeNegocio.fechaConstitucionNegocio));
    this.informacionGeneralNegocioForm.get('estado').setValue(this.infoGeneralDesdeNegocio.estadoProyecto);
    this.informacionGeneralNegocioForm.get('avaluoInmueble').setValue(this.currencyPipe.transform(this.infoGeneralDesdeNegocio.avaluoInmueble));
    this.informacionGeneralNegocioForm.get('valorUltimoPredial').setValue(this.currencyPipe.transform(this.infoGeneralDesdeNegocio.valorUltimoPredial));
    this.informacionGeneralNegocioForm.get('fechaCobroPredial').setValue(this.transformarFecha(this.infoGeneralDesdeNegocio.fechaCobroPredial));
    this.informacionGeneralNegocioForm.get('valorUltimaValorizacion').setValue(this.currencyPipe.transform(this.infoGeneralDesdeNegocio.valorUltimaValorizacion));
    this.informacionGeneralNegocioForm.get('fechaCobroValorizacion').setValue(this.transformarFecha(this.infoGeneralDesdeNegocio.fechaCobroValorizacion));
    //Sección parametrización contrato
    this.informacionGeneralNegocioForm.get('seccionalAduanaDepartamento').setValue(this.infoGeneralDesdeNegocio.seccionalAduanaDepartamento);
    this.informacionGeneralNegocioForm.get('seccionalAduanaCiudad').setValue(this.infoGeneralDesdeNegocio.seccionalAduanaCiudad);
    this.informacionGeneralNegocioForm.get('matriculaInmobiliaria').setValue(this.infoGeneralDesdeNegocio.matriculaInmobiliaria);
    this.informacionGeneralNegocioForm.get('licenciaConstruccion').setValue(this.infoGeneralDesdeNegocio.licenciaConstruccion);
    this.informacionGeneralNegocioForm.get('ciudadUbicacionPais').setValue(this.infoGeneralDesdeNegocio.ciudadUbicacionPais);
    this.informacionGeneralNegocioForm.get('ciudadUbicacionDepartamento').setValue(this.infoGeneralDesdeNegocio.ciudadUbicacionDepartamento);
    this.informacionGeneralNegocioForm.get('ciudadUbicacion').setValue(this.infoGeneralDesdeNegocio.ciudadUbicacion);
    this.informacionGeneralNegocioForm.get('fideicomitenteNombreCompleto').setValue(this.infoGeneralDesdeNegocio.fideicomitenteNombreCompleto);
    this.informacionGeneralNegocioForm.get('fideicomitenteTipoDocumento').setValue(this.infoGeneralDesdeNegocio.fideicomitenteTipoDocumento);
    this.informacionGeneralNegocioForm.get('fideicomitenteNumeroDocumento').setValue(this.infoGeneralDesdeNegocio.fideicomitenteNumeroDocumento);
    this.informacionGeneralNegocioForm.get('calidadFidecomitente').setValue(this.infoGeneralDesdeNegocio.calidadFidecomitente);
    this.informacionGeneralNegocioForm.get('calidadFiduciariaFideicomiso').setValue(this.infoGeneralDesdeNegocio.calidadFiduciariaFideicomiso);

    this.informacionGeneralNegocioForm.get('fechaLiquidacionNegocioPromotor').setValue(this.transformarFecha(this.infoGeneralDesdeNegocio.fechaLiquidacionNegocioPromotor));
    this.actualizarFechaConstitucion();
    this.actualizarFechaApertura();
    if (this.infoGeneralDesdeNegocio.encargos) {
      this.construirElArrayEncargos(this.infoGeneralDesdeNegocio.encargos);
    }
    
    if (this.infoGeneralDesdeNegocio.constanteNegocio) {
      this._dataService
      .getSubTipoFideicomiso(this.infoGeneralDesdeNegocio.constanteNegocio['tipoFideicomiso'])
      .subscribe(
        (subTipo: any) => {
         this.subTipoFide=subTipo;
        },
        (err: any) => {
        }
      );
      
      let respuestaPaisConstante = this.getDatosPais();
      respuestaPaisConstante.then((res) => {
        this.listaPaisesConstantes = res;
      });
      this.actualizarDepartamentoConstante(this.infoGeneralDesdeNegocio.constanteNegocio['codigoPais']);
      this.actualizarMunicipioConstante(this.infoGeneralDesdeNegocio.constanteNegocio['codigoDepartamento']);
      this.construirConstanteNegocio(this.infoGeneralDesdeNegocio.constanteNegocio);
    }else{
      let respuestaPaisConstante = this.getDatosPais();
      respuestaPaisConstante.then((res) => {
        this.listaPaisesConstantes = res;
      });
      this.actualizarDepartamentoConstante(this.PAIS_DEFECTO);
      this.actualizarMunicipioConstante(this.DEPARTAMENTO_DEFECTO);
    }
        if (this.infoGeneralDesdeNegocio.tiposNegocio) {
      this.construirArrayTipoNegocio(this.infoGeneralDesdeNegocio.tiposNegocio);
      this.construirArrayRetenciones(this.infoGeneralDesdeNegocio.tiposNegocio);
      this.construirArrayGastos(this.infoGeneralDesdeNegocio.tiposNegocio);
      this.construirElArrayTerceros(this.infoGeneralDesdeNegocio.tiposNegocio);
      this.actualizarListasTiposNegocios();
    }
    if (this.infoGeneralDesdeNegocio.fechasClaves) {
      this.infoGeneralDesdeNegocio.fechasClaves.forEach(element => {
        element['fechaCalendario'] = this.transformarFecha(element['fechaCalendario']);
      });
      this.construirElArrayFechasClaves(this.infoGeneralDesdeNegocio.fechasClaves);
    }
    if (this.infoGeneralDesdeNegocio.equipoResponsable) {
      this.construirElArrayEquipoResponsable(this.infoGeneralDesdeNegocio.equipoResponsable);
    }
    this.datosAenviarNegocio = [];
    this.datosAEnviarATerceros = [];
    this.datosAenviarNegocio.push(this.infoGeneralDesdeNegocio.tiposNegocio);

    if (this.infoGeneralDesdeNegocio.archivoCargado === null) {
      this._storageData.setNegocioCargarArchivoCliente(false);
      this.botonCargarArchivo = this._storageData.getNegocioCargarArchivoCliente();
      this._storageData.setNegocioVerTitulares(false);
      this.botonVerTitulares = this._storageData.getNegocioVerTitulares();
      this.noPuedeEditar = false;
      this.swBotonArchivo = false;
    }
    if (this.infoGeneralDesdeNegocio.archivoCargado === false) {
      this._storageData.setNegocioCargarArchivoCliente(true);
      this.botonCargarArchivo = this._storageData.getNegocioCargarArchivoCliente();
      this._storageData.setNegocioVerTitulares(false);
      this.botonVerTitulares = this._storageData.getNegocioVerTitulares();
      this.noPuedeEditar = true;
      this.swBotonArchivo = false;
    }
    if (this.infoGeneralDesdeNegocio.archivoCargado === true) {
      this._storageData.setNegocioCargarArchivoCliente(false);
      this.botonCargarArchivo = this._storageData.getNegocioCargarArchivoCliente();
      this._storageData.setNegocioVerTitulares(true);
      this.botonVerTitulares = this._storageData.getNegocioVerTitulares();
      this.noPuedeEditar = true;
      this.swBotonArchivo = true;
    }
  }

  public transformarFecha(dateString) {
    let dateParts: any;
    let dateObject: any = null;
    if (dateString != null) {
      dateParts = dateString.split("/");
      dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    }
    return dateObject;
  }

  public transformaFechaParaBack(fechaAtransformar) {
    if (typeof(fechaAtransformar) == 'object') {
      const format = 'dd/MM/yyyy';
      const myDate = fechaAtransformar;
      const locale = 'en-US';
      const formattedDate = formatDate(myDate, format, locale);
      return formattedDate;
    }
    return fechaAtransformar;
  }

  public cargarConstanteNegocio(): any {
    let constanteNegocio = {
      tipoFideicomiso: this.informacionGeneralNegocioForm.value.tipoFideicomiso,
      subtipoFideicomiso: this.informacionGeneralNegocioForm.value.subtipoFideicomiso,
      movimientoPago: this.informacionGeneralNegocioForm.value.movimientoPago,
      codigoPais: this.informacionGeneralNegocioForm.value.codigoPais,
      codigoDepartamento: this.informacionGeneralNegocioForm.value.codigoDepartamento,
      codigoCiudad: this.informacionGeneralNegocioForm.value.codigoCiudad,
      tipoProceso: this.informacionGeneralNegocioForm.value.tipoProceso,
      tipoMovimiento: this.informacionGeneralNegocioForm.value.tipoMovimiento
    }
    return constanteNegocio;
  }



  private tranformarFechaVencimientoTerceros() {
    //Este fue el arreglo propuesto para las fechas moment y date      
    this.informacionGeneralNegocioForm.value.tipoNegocios.forEach(itemTipoNegocio => {
      itemTipoNegocio.terceros.forEach(itemTercero => {
        if (itemTercero['fechaVencimiento']['_d'] || isDate(itemTercero['fechaVencimiento'])) {
          itemTercero['fechaVencimiento'] = this.datePipe.transform(itemTercero['fechaVencimiento'], 'dd/MM/yyyy');
        }
      });
    });
  }

  private tranformarFechaVencimientoGastos() {
    //Este fue el arreglo propuesto para las fechas moment y date      
    this.informacionGeneralNegocioForm.value.tipoNegocios.forEach(itemTipoNegocio => {
      itemTipoNegocio.gastos.forEach(itemGasto => {
        if (itemGasto['fechaVencimiento']['_d'] || isDate(itemGasto['fechaVencimiento'])) {
          itemGasto['fechaVencimiento'] = this.datePipe.transform(itemGasto['fechaVencimiento'], 'dd/MM/yyyy');
        }
      });
    });
  }

  private cambiarFloatanteTipoNegocioValor() {
    this.informacionGeneralNegocioForm.value.tipoNegocios.forEach(itemTipoNegocio => {
      itemTipoNegocio.terceros.forEach(itemTercero => {
        itemTercero['valor'] = this._util.cambiarFloatante(itemTercero['valor']);
      });
      itemTipoNegocio.gastos.forEach(itemGasto => {
        itemGasto['valor'] = this._util.cambiarFloatante(itemGasto['valor']);
      });
    });
  }

  public validarTipoNegocio() {
    let formularioValido = true;
    this.informacionGeneralNegocioForm.value.tipoNegocios.forEach(itemTipoNegocio => {
      itemTipoNegocio.retenciones.forEach(itemRetencion => {
        if (itemRetencion.invalido > 1) {
          formularioValido = false;
        }
      });
      itemTipoNegocio.gastos.forEach(itemGasto => {
        if (itemGasto.invalido > 1) {
          formularioValido = false;
        }
      });
      itemTipoNegocio.terceros.forEach(itemTercero => {
        if (itemTercero.invalido > 1) {
          formularioValido = false;
        }
      });
    });
    if (!formularioValido) {
      this.informacionGeneralNegocioForm.setErrors({ 'invalid': true });
      let dataMensajeGuardado = {
        titulo: "Cambios NO guardados",
        contenido: "Verificar al información registrada en el formulario.",
        button: "ACEPTAR",
        urlimg: true
      };
      this.dialog.open(DialogParametrosSaveComponent, {
        width: "40rem",
        data: dataMensajeGuardado
      });
    } 
     return formularioValido;
  }

  public guardarInformacionGeneralNegocio() {    
    this.tranformarFechaVencimientoTerceros();
    this.tranformarFechaVencimientoGastos();
    this.cambiarFloatanteTipoNegocioValor();
    this.arrayDeCamposFechasClaves.value.forEach(element => {
      element['fechaCalendario'] = this.transformaFechaParaBack(element['fechaCalendario']);
      const formGroup = this.crearGrupoFechasClaves();
      formGroup.patchValue(element['fechaCalendario']);
      this.arrayDeCamposFechasClaves.push(formGroup);
      this.arrayDeCamposFechasClaves.removeAt(this.arrayDeCamposFechasClaves.length - 1);

    });
    this.informacionGeneralNegocioForm.value.fechaLiquidacionNegocioPromotor = this.transformaFechaParaBack(this.informacionGeneralNegocioForm.value.fechaLiquidacionNegocioPromotor);
    this.informacionGeneralNegocioForm.value.fechaCobroValorizacion = this.transformaFechaParaBack(this.informacionGeneralNegocioForm.value.fechaCobroValorizacion);
    this.informacionGeneralNegocioForm.value.fechaCobroPredial = this.transformaFechaParaBack(this.informacionGeneralNegocioForm.value.fechaCobroPredial);
    this.informacionGeneralNegocioForm.value.fechaConstitucionNegocio = this.transformaFechaParaBack(this.informacionGeneralNegocioForm.value.fechaConstitucionNegocio);
    this.informacionGeneralNegocioForm.value.fechaApertura = this.transformaFechaParaBack(this.informacionGeneralNegocioForm.value.fechaApertura);
    this.botonGuardarInformacion = true;    

    this.objetoPaBack = {
      seccionalAduanaDepartamento: this.informacionGeneralNegocioForm.value.seccionalAduanaDepartamento,
	    seccionalAduanaCiudad: this.informacionGeneralNegocioForm.value.seccionalAduanaCiudad,
    	matriculaInmobiliaria:this.informacionGeneralNegocioForm.value.matriculaInmobiliaria,
	    licenciaConstruccion: this.informacionGeneralNegocioForm.value.licenciaConstruccion,
	    ciudadUbicacionPais: this.informacionGeneralNegocioForm.value.ciudadUbicacionPais,
	    ciudadUbicacionDepartamento: this.informacionGeneralNegocioForm.value.ciudadUbicacionDepartamento,
	    ciudadUbicacion: this.informacionGeneralNegocioForm.value.ciudadUbicacion,
	    fideicomitenteNombreCompleto: this.informacionGeneralNegocioForm.value.fideicomitenteNombreCompleto,
	    fideicomitenteTipoDocumento:this.informacionGeneralNegocioForm.value.fideicomitenteTipoDocumento,
	    fideicomitenteNumeroDocumento: this.informacionGeneralNegocioForm.value.fideicomitenteNumeroDocumento,
    	calidadFidecomitente: this.informacionGeneralNegocioForm.value.calidadFidecomitente,
      calidadFiduciariaFideicomiso: this.informacionGeneralNegocioForm.value.calidadFiduciariaFideicomiso,
      nombre: this.infoGeneralDesdeNegocio.nombre,
      codigoSFC: this.infoGeneralDesdeNegocio.codigoSFC,
      codigoInterno: this.infoGeneralDesdeNegocio.codigoInterno,
      completo: this.infoGeneralDesdeNegocio.completo,
      estado: this.informacionGeneralNegocioForm.value.estado,
      fechaApertura: this.informacionGeneralNegocioForm.value.fechaApertura,
      numeroDerechosTotales: this.informacionGeneralNegocioForm.value.numeroDerechosTotales,
      fechaLiquidacionNegocioPromotor: this.informacionGeneralNegocioForm.value.fechaLiquidacionNegocioPromotor,
      fechaConstitucionNegocio: this.informacionGeneralNegocioForm.value.fechaConstitucionNegocio,
      estadoProyecto: this.informacionGeneralNegocioForm.value.estado,
      avaluoInmueble: this._util.cambiarFloatante(this.informacionGeneralNegocioForm.value.avaluoInmueble),
      valorUltimoPredial: this._util.cambiarFloatante(this.informacionGeneralNegocioForm.value.valorUltimoPredial),
      valorUltimaValorizacion: this._util.cambiarFloatante(this.informacionGeneralNegocioForm.value.valorUltimaValorizacion),
      fechaCobroPredial: this.informacionGeneralNegocioForm.value.fechaCobroPredial,
      fechaCobroValorizacion: this.informacionGeneralNegocioForm.value.fechaCobroValorizacion,
      archivoCargado: this.infoGeneralDesdeNegocio.archivoCargado,
      encargos: this.informacionGeneralNegocioForm.value.encargos,
      tiposNegocio: this.informacionGeneralNegocioForm.value.tipoNegocios,
      fechasClaves: this.informacionGeneralNegocioForm.value.fechasClaves,
      constanteNegocio: this.cargarConstanteNegocio()
    }
    if (this.informacionGeneralNegocioForm.valid && this.validarTipoNegocio()) {
      this._dataService.putGuardarInformacionNegocio(this.objetoPaBack)
        .subscribe(
          (data: any) => {
            if (this._storageData.getNegocioCargarArchivoCliente() && !this._storageData.getNegocioVerTitulares() && this.swBotonArchivo) {
              this._storageData.setNegocioVerTitulares(true);
              this.botonVerTitulares = this._storageData.getNegocioVerTitulares();

              this._storageData.setNegocioCargarArchivoCliente(false);
              this.botonCargarArchivo = this._storageData.getNegocioCargarArchivoCliente();
            }

            if (!this._storageData.getNegocioCargarArchivoCliente() && !this._storageData.getNegocioVerTitulares() && !this.swBotonArchivo) {
              this._storageData.setNegocioCargarArchivoCliente(true);
              this.botonCargarArchivo = this._storageData.getNegocioCargarArchivoCliente();
            }

            let dataMensajeGuardado = {
              titulo: "Cambios guardados",
              contenido:
                "Los cambios de la información de negocio han sido guardados exitosamente.",
              button: "ACEPTAR",
              urlimg: false
            };
            this.dialog.open(DialogParametrosSaveComponent, {
              width: "40rem",
              data: dataMensajeGuardado
            });
            // this.getNegocioDetalle();
          },
          (err: any) => {
            let dataMensajeGuardado = {
              titulo: "Cambios NO guardados",
              contenido: "Los cambios no han podido ser guardados intente más tarde.",
              button: "ACEPTAR",
              urlimg: true
            };

            this.dialog.open(DialogParametrosSaveComponent, {
              width: "40rem",
              data: dataMensajeGuardado
            });
          }

        );
    }
  }

  public getNegocioDetalle(codigoNegocio) {
    this.inicializarFormulario();
    this._dataService.getObtenerInformacionNegocio(codigoNegocio)
      .subscribe((response: any) => {        
        this._storageData.setInfoNegocioDetalle(response);
      },
        (err) => {
        }
      );
    this.infoGeneralDesdeNegocio = this._storageData.getInfoNegocioDetalle();    
    this.estadoProyecto = this.infoGeneralDesdeNegocio.estadoProyecto;
    if (!this.infoGeneralDesdeNegocio || this._storageData.getSwInicio() || this.infoGeneralDesdeNegocio == undefined) {
      this._router.navigate(['accionfiduciaria']);
    }
    else {
      this.estadoProyecto = this._storageData.getEstadoProyecto();
      this.obtenerServiciosParaFormGeneral();
    }    
  }

  public crearGrupoFechasClaves() {
    return this.fb.group({
      nombreFechaClave: ['', Validators.compose([Validators.required])],
      idTipoFechaClave: ['', Validators.compose([Validators.required])],
      fechaCalendario: ['', Validators.compose([Validators.required])]
    });
  }

  public crearGrupoEquipoResponsable() {
    return this.fb.group({
      nombreCompleto: ['', Validators.compose([Validators.required])],
      rol: ['', Validators.compose([Validators.required])]
    });
  }

  public crearGrupoEncargos() {
    return this.fb.group({
      nombreEncargo: [''],
      estado: [''],
      numeroEncargo: [''],
      movimientoFondo: ['', [Validators.required]],
      numeroPagoOrdinario: ['', [Validators.required]]
    });
  }

  public construirElArrayEncargos(infoServicio) {
    let informacionServicio = infoServicio;
    informacionServicio.forEach(dato => {
      const formGroup = this.crearGrupoEncargos();
      formGroup.patchValue(dato);
      this.arrayDeCamposEncargos.push(formGroup);
    });
  }

  public construirConstanteNegocio(constanteNegocio) {
    this.informacionGeneralNegocioForm.get('tipoFideicomiso').setValue(constanteNegocio.tipoFideicomiso);
    this.informacionGeneralNegocioForm.get('subtipoFideicomiso').setValue(constanteNegocio.subtipoFideicomiso);
    this.informacionGeneralNegocioForm.get('movimientoPago').setValue(constanteNegocio.movimientoPago);
    this.informacionGeneralNegocioForm.get('tipoProceso').setValue(constanteNegocio.tipoProceso);
    this.informacionGeneralNegocioForm.get('tipoMovimiento').setValue(constanteNegocio.tipoMovimiento);
    this.informacionGeneralNegocioForm.get('codigoPais').setValue(constanteNegocio.codigoPais);
    this.informacionGeneralNegocioForm.get('codigoDepartamento').setValue(constanteNegocio.codigoDepartamento);
    this.informacionGeneralNegocioForm.get('codigoCiudad').setValue(constanteNegocio.codigoCiudad);
  }

  public construirElArrayFechasClaves(infoServicio) {
    let informacionServicio = infoServicio;
    informacionServicio.forEach(dato => {
      const formGroup = this.crearGrupoFechasClaves();
      formGroup.patchValue(dato);
      this.arrayDeCamposFechasClaves.push(formGroup);
    });
  }

  public construirElArrayEquipoResponsable(infoServicio) {
    infoServicio.forEach(dato => {
      const formGroup = this.crearGrupoEquipoResponsable();
      formGroup.patchValue(dato);
      this.arrayDeCamposEquipoResponsable.push(formGroup);
    });
  }

  public agregarCadaCampoDelFormularioEncargos() {
    this.arrayDeCamposEncargos.push(this.crearGrupoEncargos());
  }

  public agregarCadaCampoDelFormularioFechasClave() {
    this.arrayDeCamposFechasClaves.push(this.crearGrupoFechasClaves());
  }

  public validarNombreFechaClave(nombreFechaClave) {
    this.cantidadFechaClaveRepetida = 0;
    this.arrayDeCamposFechasClaves.value.forEach(element => {
      if (element.nombreFechaClave.toUpperCase() == nombreFechaClave.toUpperCase()) {
        this.cantidadFechaClaveRepetida++;
      }
    });
    if (this.cantidadFechaClaveRepetida > 1) {
      this.informacionGeneralNegocioForm.setErrors({ 'invalid': true });
    } else {
      this.informacionGeneralNegocioForm.setErrors(null);
    }
  }

  private validarRepetidosTerceros(tipoDoc, numeroDoc, idTipoTercero, array) {
    let cantidadRepetido = 0;
    array.forEach(element => {
      if (element.tipoDocumento == tipoDoc && element.numeroDocumento == numeroDoc && element.idTipo == idTipoTercero) {
        cantidadRepetido++;
      }
    });
    if (cantidadRepetido > 1) {
      this.informacionGeneralNegocioForm.setErrors({ 'invalid': true });
    } else {
      this.informacionGeneralNegocioForm.setErrors(null);
    }
    return cantidadRepetido;
  }

  private validarRepetidosNombres(itemNombre, array) {
    let cantidadRepetido = 0;
    array.forEach(element => {
      if (element.nombre == itemNombre) {
        cantidadRepetido++;
      }
    });
    if (cantidadRepetido > 1) {
      this.informacionGeneralNegocioForm.setErrors({ 'invalid': true });
    } else {
      this.informacionGeneralNegocioForm.setErrors(null);
    }
    return cantidadRepetido;
  }

  public validarRepetidosID(itemId, array) {
    let cantidadRepetido = 0;
    array.forEach(element => {
      if (element.id == itemId) {
        cantidadRepetido++;
      }
    });
    if (cantidadRepetido > 1) {
      this.informacionGeneralNegocioForm.setErrors({ 'invalid': true });
    } else {
      this.informacionGeneralNegocioForm.setErrors(null);
    }
    return cantidadRepetido;
  }

  public llamarFormularioEquipo() {
    let data;
    data = {
      buttonCancelar: "CANCELAR",
      buttonAceptar: "GUARDAR",
      urlimg: false,
      param: {
        codigonegocio: this.codigoNegocio,
        criterioBuscar: null,
        argumento: null
      }
    };
    const dialogRef = this.dialog.open(NegocioEquipoComponent, {
      width: "94rem",
      data
    });

    dialogRef.afterClosed().subscribe(async result => {
    });
  }

  public openDialogSubirArchivo() {

    let data;
    data = {
      titulo: "Cargar titulares y cotitulares",
      contenido: "dialogo para subir archivo ",
      buttonBuscar: "Buscar",
      buttonCargar: "Cargar",
      buttonCancelar: "Cancelar",
      urlimg: false
    };
    const dialogRef = this.dialog.open(DialogBuscarArchivoComponent, {
      width: "52rem",
      data
    });

    dialogRef.afterClosed().subscribe(async resultado => {
      if (resultado == true) {
        this._storageData.setNegocioCargarArchivoCliente(false);
        this.botonCargarArchivo = this._storageData.getNegocioCargarArchivoCliente();
        this._storageData.setNegocioVerTitulares(true);
        this.botonVerTitulares = this._storageData.getNegocioVerTitulares();
        this.botonGuardarInformacion = true;
        this.swBotonArchivo = true;
      }
      if (resultado == false) {
        this._storageData.setNegocioCargarArchivoCliente(true);
        this.botonCargarArchivo = this._storageData.getNegocioCargarArchivoCliente();
        this._storageData.setNegocioVerTitulares(false);
        this.botonVerTitulares = this._storageData.getNegocioVerTitulares();
        this.noPuedeEditar = true;
        this.swBotonArchivo = false;
      }
    });

  }

  openDialogVerListadoTitulares(): void {
    let data;
    data = {
      titulo: "Titulares y cotitulares",
      button: "ACEPTAR",
      urlimg: false,
      codigoSFC: this.codigoSFC
    };

    const dialogRef = this.dialog.open(DialogNegocioListTitularesComponent, {
      width: "400rem",
      data
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  initTipoNegocios() {
    this.negociosActivos = true;
    return this.fb.group({
      //  ---------------------forms fields on level Tipos de Negocios ------------------------
      id: ['', Validators.compose([Validators.required])],
      // ------------------------ Retenciones ---------------------------------------------
      retenciones: this.fb.array([]),
      // ----------------------- Gastos ----------------------------------------------
      gastos: this.fb.array([]),
      // ----------------------- Terceros ----------------------------------------------
      terceros: this.fb.array([])
    });
  }

  initRetenciones() {
    return this.fb.group({
      //  ---------------------forms fields on y level retenciones------------------------
      id: ['', Validators.compose([Validators.required])],
      descripcion: ['', []],
      valor: ['', []],
      unidad: ['', []],
      invalido: [0, []]
    });
  }

  initGastos() {
    return this.fb.group({
      nombre: ['', Validators.compose([Validators.required])],
      idTipo: ['', Validators.compose([Validators.required])],
      unidad: ['', Validators.compose([Validators.required])],
      valor: ["0", []],
      diaPago: ['', Validators.compose([Validators.required])],
      periodicidad: ['',  Validators.compose([Validators.required])],
      fechaVencimiento: [''],
      numeroRadicado: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])],
      invalido: [0, []]
    });
  }

  initTerceros() {
    return this.fb.group({
      nombre: ['', [Validators.required]],
      tipoDocumento: ['', [Validators.required]],
      numeroDocumento: ['', [Validators.required]],
      idTipo: [''],
      unidad: [''],
      valor: [''],
      fechaVencimiento: [''],
      diaPago: [''],
      periodicidad: ['', Validators.compose([Validators.required])],
      numeroRadicado: [''],
      invalido: [0, []]
    });
  }

  public construirArrayTipoNegocio(infoServicio) {
    let informacionServicio = infoServicio;
    informacionServicio.forEach(dato => {
      const formGroup = this.initTipoNegocios();
      formGroup.patchValue(dato);
      this.arrayDeCamposFormularioTipoNegocioGeneral.push(formGroup);
    });
  }

  public construirArrayRetenciones(infoServicio) {
    let informacionServicio = infoServicio;
    let primera: any = this.informacionGeneralNegocioForm["controls"].tipoNegocios["controls"];
    var vari: any;
    informacionServicio.forEach((elemento, ind) => {
      elemento.retenciones.forEach((dato) => {
        if (dato.unidad == 'COP') {
          dato.valor = this._util.cambiarFloatante(dato.valor, 2);
          dato.valor = this.currencyPipe.transform(dato.valor);
        }
        if (dato.unidad == '%') {
          dato.valor = parseFloat(dato.valor).toFixed(2);
        }
        const formGroup = this.initRetenciones();
        formGroup.patchValue(dato);
        vari = primera[ind].controls.retenciones.controls;
        vari.push(formGroup);
      });
    });
  }

  public construirArrayGastos(infoServicio) {
    let informacionServicio = infoServicio;
    let primera: any = this.informacionGeneralNegocioForm["controls"].tipoNegocios["controls"];
    var vari: any;
    informacionServicio.forEach((elementos, ind) => {
      elementos.gastos.forEach((dato) => {
        if (dato.unidad == 'COP') {
          dato.valor = this._util.cambiarFloatante(dato.valor, 2);
          dato.valor = this.currencyPipe.transform(dato.valor);
        }
        if (dato.unidad == '%') {
          dato.valor = parseFloat(dato.valor).toFixed(2);
        }
        dato.fechaVencimiento = this.transformarFecha(dato.fechaVencimiento);
        const formGroup = this.initGastos();
        formGroup.patchValue(dato);
        vari = primera[ind].controls.gastos.controls;
        vari.push(formGroup);
      });
    });
  }

  public construirElArrayTerceros(infoServicio) {
    let informacionServicio = infoServicio;
    let primera: any = this.informacionGeneralNegocioForm["controls"].tipoNegocios["controls"];
    var tercero: any;
    informacionServicio.forEach((elementos, ind) => {
      elementos.terceros.forEach((dato) => {
        if (dato.unidad == 'COP') {
          dato.valor = this._util.cambiarFloatante(dato.valor, 2);
          dato.valor = this.currencyPipe.transform(dato.valor);
        }
        if (dato.unidad == '%') {
          dato.valor = parseFloat(dato.valor).toFixed(2);
        }
        dato.fechaVencimiento = this.transformarFecha(dato.fechaVencimiento);
        const formGroup = this.initTerceros();
        formGroup.patchValue(dato);
        tercero = primera[ind].controls.terceros.controls;
        tercero.push(formGroup);
      });
    });

  }

  puedeAgregarMasTipoNegocio() {
    let inactive = 0;
    this.tiposDeNegocio.forEach(element => {
      if (!element.active) {
        inactive++;
      }
    }); 
    return (inactive === this.tiposDeNegocio.length);
  }

  private actualizarListasTiposNegocios(eliminar=false) {
    // this.agregarTipoNegocio = false;   
    if(eliminar){
      this.arrayFiltro=[];
      this.tiposDeNegocio.forEach(tipoNegocio => {
        tipoNegocio.active=true;
      });
    }
    this.informacionGeneralNegocioForm.controls.tipoNegocios.value.forEach((element, ind) => {
      for (let i = 0; i < this.tiposDeNegocio.length; i++) {
        if (this.tiposDeNegocio[i].id === element.id) {
          this.arrayFiltro[ind] = this.tiposDeNegocio[i].descripcion;
          this.tiposDeNegocio[i].active = false;
        }
        
      }
    });
  }

  actualizarTipoNegocioRetencion(ix, tipoNegocioId) {
    let tipoNegocioRetenciones = this._util.buscarObjetoId(this.tiposDeNegocio, tipoNegocioId);
    let tipoNegocioControl: any = this.informacionGeneralNegocioForm["controls"].tipoNegocios["controls"];
    let tipoNegocioRetencion: any;
    let arrayRetenciones = new Array();
    tipoNegocioRetenciones.valores.forEach((item) => {
      if (item.valor == "true") {
        let retencion = this._util.buscarObjetoId(this.listRetenciones, item.id);
        const formGroup = this.initRetenciones();
        if (retencion.unidad == 'COP') {
          retencion.valor = this._util.cambiarFloatante(retencion.valor, 2);
          retencion.valor = this.currencyPipe.transform(retencion.valor);
        }
        if (retencion.unidad == '%') {
          retencion.valor = parseFloat(retencion.valor).toFixed(2);
        }
        formGroup.patchValue(retencion);
        tipoNegocioRetencion = tipoNegocioControl[ix].controls.retenciones.controls;
        tipoNegocioRetencion.push(formGroup);
        retencion.invalido = 0;
        arrayRetenciones.push(retencion);
      }
    });
    this.banderaFormularioNuevo = true;
    tipoNegocioControl[ix].value.retenciones = arrayRetenciones;
    let ind = this.arrayFiltro.length;
    for (var i = 0; i < this.tiposDeNegocio.length; i++) {
      if (this.tiposDeNegocio[i].id === tipoNegocioId) {
        this.arrayFiltro[ind] = tipoNegocioRetenciones.descripcion;
        this.tiposDeNegocio[i].active = false;
      }
    }
    this.agregarTipoNegocio = false;    
    arrayRetenciones = null;  
    this.actualizarRetenciones(ix);  
  }

  actualizarRetenciones(ix) {
    let controlRetencion = (<FormArray>this.informacionGeneralNegocioForm.controls["tipoNegocios"])
      .at(ix)
      .get("retenciones") as FormArray;
      controlRetencion.controls.forEach((item,ind) => {
          controlRetencion.controls[ind].patchValue({
            id: item.value.id,
            descripcion: item.value.descripcion,
            valor: item.value.valor, 
            unidad: item.value.unidad,
            invalido: 0     
        });
    });
    let retencionesA = (<FormArray>this.informacionGeneralNegocioForm.controls["tipoNegocios"]).at(
      ix
    ) as FormArray;
    retencionesA.controls["retenciones"].removeAt(controlRetencion.controls.length);   
  }

  addTipoNegocio() {
    this.banderaFormularioNuevo = false;
    // this.informacionGeneralNegocioForm.controls.tipoNegocios.value.forEach((element, ind) => {
    const tipoNegocioControl = <FormArray>this.informacionGeneralNegocioForm.controls["tipoNegocios"];
    tipoNegocioControl.push(this.initTipoNegocios());
    this.informacionGeneralNegocioForm.controls.tipoNegocios.value.forEach((element, ind) => {
      for (let i = 0; i < this.tiposDeNegocio.length; i++) {
        if (this.tiposDeNegocio[i].id === element.id) {
          this.arrayFiltro[ind] = this.tiposDeNegocio[i].descripcion;
          this.tiposDeNegocio[i].active = false;
        }
      }
    });
    this.agregarTipoNegocio = true;
  }

  addRetenciones(ix) {
    const control = (<FormArray>this.informacionGeneralNegocioForm.controls["tipoNegocios"])
      .at(ix)
      .get("retenciones") as FormArray;
    control.push(this.initRetenciones());
  }

  addGastos(ix) {
    const control = (<FormArray>this.informacionGeneralNegocioForm.controls["tipoNegocios"])
      .at(ix)
      .get("gastos") as FormArray;
    control.push(this.initGastos());
  }

  addTercero(ix) {
    const control = (<FormArray>this.informacionGeneralNegocioForm.controls["tipoNegocios"])
      .at(ix)
      .get("terceros") as FormArray;
    control.push(this.initTerceros());
  }

  actualizarRetencionValor(ix, iz, id) {
    this.cantidadRetencionRepetida = 0;
    let retencion = this._util.buscarObjetoId(this.listRetenciones, id);
    let controlRetencion = (<FormArray>this.informacionGeneralNegocioForm.controls["tipoNegocios"])
      .at(ix)
      .get("retenciones") as FormArray;
    if (retencion.unidad == 'COP') {
      if (typeof (retencion.valor) == 'string') {
        retencion.valor = retencion.valor.replace("$", "");
        retencion.valor = retencion.valor.replace(/,/gi, "");
      }
      retencion.valor = this.currencyPipe.transform(retencion.valor);
    }
    controlRetencion.controls[iz].patchValue({
      id: retencion.id,
      descripcion: retencion.descripcion,
      valor: retencion.valor,
      unidad: retencion.unidad
    });
    this.cantidadRetencionRepetida = this.validarRepetidosID(id, controlRetencion.value);
    controlRetencion.controls[iz].patchValue({
      invalido: this.cantidadRetencionRepetida
    });
    controlRetencion.push(this.initRetenciones());
    let retencionesA = (<FormArray>this.informacionGeneralNegocioForm.controls["tipoNegocios"]).at(
      ix
    ) as FormArray;
    retencionesA.controls["retenciones"].removeAt(controlRetencion.controls.length - 1);
  }


  actualizarGastoCampo(ix, iz, valor, campo) {
    var control = (<FormArray>this.informacionGeneralNegocioForm.controls["tipoNegocios"])
      .at(ix)
      .get("gastos") as FormArray;
    switch (campo) {
      case 'nombre':
        this.cantidadGastosRepetido = 0;
        this.cantidadGastosRepetido = this.validarRepetidosNombres(valor, control.value);
        control.controls[iz].patchValue({
          nombre: valor
        });
        control.controls[iz].patchValue({
          invalido: this.cantidadGastosRepetido
        });
        break;
      case 'idTipo':
        control.controls[iz].patchValue({
          idTipo: valor
        });
        break;
      case 'unidad':
        control.controls[iz].patchValue({
          unidad: valor
        });
        break;
      case 'valor':
        control.controls[iz].patchValue({
          valor: valor
        });
        break;
      case 'diaPago':
        control.controls[iz].patchValue({
          diaPago: valor
        });
        break;
      case 'periodicidad':
        control.controls[iz].patchValue({
          periodicidad: valor
        });
        break;
      case 'fechaVencimiento':
        control.controls[iz].patchValue({
          fechaVencimiento: valor
        });
        break;
      case 'numeroRadicado':
        control.controls[iz].patchValue({
          numeroRadicado: valor
        });
        break;
      default:
        break;
    }
    control.push(this.initGastos());
    let gastoA = (<FormArray>this.informacionGeneralNegocioForm.controls["tipoNegocios"]).at(
      ix
    ) as FormArray;
    gastoA.controls["gastos"].removeAt(control.controls.length - 1);
  }

  actualizarTerceroCampo(ix, iz, valor, campo, tipoDoc = null, numeroDoc = null, idTipoTercero = null) {
    let control = (<FormArray>this.informacionGeneralNegocioForm.controls["tipoNegocios"])
      .at(ix)
      .get("terceros") as FormArray;
    switch (campo) {
      case 'nombre':
        control.controls[iz].patchValue({
          nombre: valor
        });
        break;
      case 'tipoDocumento':
        control.controls[iz].patchValue({
          tipoDocumento: valor
        });
        this.cantidadTercerosRepetido = 0;
        this.cantidadTercerosRepetido = this.validarRepetidosTerceros(tipoDoc, numeroDoc, idTipoTercero, control.value);
        control.controls[iz].patchValue({
          invalido: this.cantidadTercerosRepetido
        });
        break;
      case 'numeroDocumento':
        control.controls[iz].patchValue({
          numeroDocumento: valor
        });
        this.cantidadTercerosRepetido = 0;
        this.cantidadTercerosRepetido = this.validarRepetidosTerceros(tipoDoc, numeroDoc, idTipoTercero, control.value);
        control.controls[iz].patchValue({
          invalido: this.cantidadTercerosRepetido
        });
        break;
      case 'idTipo':
        control.controls[iz].patchValue({
          idTipo: valor
        });
        this.cantidadTercerosRepetido = 0;
        this.cantidadTercerosRepetido = this.validarRepetidosTerceros(tipoDoc, numeroDoc, valor, control.value);
        control.controls[iz].patchValue({
          invalido: this.cantidadTercerosRepetido
        });
        break;
      case 'unidad':
        control.controls[iz].patchValue({
          unidad: valor
        });
        break;
      case 'valor':
        control.controls[iz].patchValue({
          valor: valor
        });
        break;
      case 'fechaVencimiento':
        control.controls[iz].patchValue({
          fechaVencimiento: valor
        });
        break;
      case 'diaPago':
        control.controls[iz].patchValue({
          diaPago: valor
        });
        break;
      case 'periodicidad':
        control.controls[iz].patchValue({
          periodicidad: valor
        });
        break;
      case 'numeroRadicado':
        control.controls[iz].patchValue({
          numeroRadicado: valor
        });
        break;
      default:
        break;
    }
    control.push(this.initTerceros());
    let terceroA = (<FormArray>this.informacionGeneralNegocioForm.controls["tipoNegocios"]).at(
      ix
    ) as FormArray;
    terceroA.controls["terceros"].removeAt(control.controls.length - 1);
  }

  public deleteTipoNegocio(index, X) {
    let data;
    data = {
      titulo: "Eliminar Tipo de negocio",
      contenido: "Se esta intentando eliminar un tipo de negocio¿Está seguro?",
      buttonContinuar: "Continuar",
      buttonCancelar: "Cancelar",
      urlimg: true
    };
    const dialogRef = this.dialog.open(DialogTwoButtonsComponent, {
      width: "42rem",
      data
    });

    dialogRef.afterClosed().subscribe(async resultado => {

      if (resultado == "Continuar") {
        (<FormArray>this.informacionGeneralNegocioForm.controls["tipoNegocios"]).removeAt(index);
        this.actualizarListasTiposNegocios(true);
      }
    });
  }

  public deleteRetenciones(idxtipNeg, idxRet) {
    let data;
    let idRet;
    let retencionesA = (<FormArray>this.informacionGeneralNegocioForm.controls["tipoNegocios"]).at(
      idxtipNeg
    ) as FormArray;
    idRet = retencionesA.controls["retenciones"].at(idxRet).value.id;
    data = {
      titulo: "Eliminar Retención",
      contenido: "Se esta intentando eliminar una retención ¿Está seguro?",
      buttonContinuar: "Continuar",
      buttonCancelar: "Cancelar",
      urlimg: true
    };
    const dialogRef = this.dialog.open(DialogTwoButtonsComponent, {
      width: "42rem",
      data
    });

    dialogRef.afterClosed().subscribe(async resultado => {
      if (resultado == "Continuar") {
        retencionesA.controls["retenciones"].removeAt(idxRet);
        this.cantidadRetencionRepetida = this.validarRepetidosID(idRet, retencionesA.controls["retenciones"].value);
      }
    });
  }

  public deleteGastos(idxtipNeg, idxGasto) {
    let data;
    let nombreGasto;
    data = {
      titulo: "Eliminar Gasto",
      contenido: "Se esta intentando eliminar un gasto ¿Está seguro?",
      buttonContinuar: "Continuar",
      buttonCancelar: "Cancelar",
      urlimg: true
    };
    const dialogRef = this.dialog.open(DialogTwoButtonsComponent, {
      width: "42rem",
      data
    });


    dialogRef.afterClosed().subscribe(async resultado => {
      if (resultado == "Continuar") {
        let gastosA = (<FormArray>this.informacionGeneralNegocioForm.controls["tipoNegocios"]).at(
          idxtipNeg
        ) as FormArray;
        nombreGasto = gastosA.controls["gastos"].at(idxGasto).value.nombre;
        gastosA.controls["gastos"].removeAt(idxGasto);
        this.cantidadGastosRepetido = this.validarRepetidosNombres(nombreGasto, gastosA.controls["gastos"].value);
      }
    });
  }

  public deleteTerceros(idxtipNeg, idxTercero) {
    let data;
    data = {
      titulo: "Eliminar Tercero",
      contenido:
        "Se esta intentando eliminar un Tercero ¿Está seguro?",
      buttonContinuar: "Continuar",
      buttonCancelar: "Cancelar",
      urlimg: true,
    };
    const dialogRef = this.dialog.open(DialogTwoButtonsComponent, {
      width: "42rem",
      data
    });
    dialogRef.afterClosed().subscribe(async resultado => {
      if (resultado == "Continuar") {
        let tercerosA = (<FormArray>this.informacionGeneralNegocioForm.controls["tipoNegocios"]).at(
          idxtipNeg
        ) as FormArray;
        tercerosA.controls["terceros"].removeAt(idxTercero);
      }
    });
  }

  public removerFormularioFechasClaves(index) {
    let data;
    data = {
      titulo: "Eliminar fecha clave",
      contenido:
        "Se esta intentando eliminar una fecha clave ¿Está seguro?",
      buttonContinuar: "Continuar",
      buttonCancelar: "Cancelar",
      urlimg: true,
    };
    const dialogRef = this.dialog.open(DialogTwoButtonsComponent, {
      width: "42rem",
      data
    });

    dialogRef.afterClosed().subscribe(async resultado => {
      if (resultado == "Continuar") {
        this.arrayDeCamposFechasClaves.removeAt(index);
      }
    });
  }

  async asyncObtenerInformacionNegocioCompleto() {
    return this._dataService.getObtenerInformacionNegocio(this.codigoNegocio).toPromise();
  }  

  private obtenerInfoNegocio() {
    let data: any = null;
    let tiposNegocios: boolean = false;
    let negocioCompleto: boolean = false;
    let infoNegocioCompleta: boolean = false;
    this.infoGeneralDesdeNegocio = this._storageData.getInfoNegocioDetalle();
    this.codigoNegocio = this.infoGeneralDesdeNegocio.codigoSFC;     
    data = this.asyncObtenerInformacionNegocioCompleto();
    
    data.then((res) => {
      this._storageData.setInfoNegocioDetalle(res);     
    });

    this.infoGeneralDesdeNegocio = this._storageData.getInfoNegocioDetalle();    
    if (this.infoGeneralDesdeNegocio.tiposNegocio != undefined && this.infoGeneralDesdeNegocio.tiposNegocio.length > 0) {
      tiposNegocios =  true;
    }    
    negocioCompleto = this.infoGeneralDesdeNegocio.completo;
    if (tiposNegocios && negocioCompleto) {
      infoNegocioCompleta = true;
    }
    this._storageData.setInfoNegocioDetalleCompletoPagos(infoNegocioCompleta);
    if (!infoNegocioCompleta) {
      this.dialogNegocioIncompleto();
    }      
  }

  public dialogNegocioIncompleto() {    
    let dataMensaje = {
        titulo: "Aviso",
        contenido: "La información del negocio (tipo de negocio) no está completa, verificar los datos para registrar un pago.",
        button: "ACEPTAR",
        urlimg: true
    };
    this.dialog.open(DialogParametrosSaveComponent, {
      width: "40rem",
      data: dataMensaje
    });
  }

  public iniciarPestanaPagosDistribucion() {
    this._storageData.setActiveNegocioPagosDistribucion(true);
    this._storageData.setActiveNegocioHistorialPagosDistribucion(false);
    this._storageData.setActivePagosDistribucion(false);
    this._storageData.setActiveNegocioHistorialPagosIngresoEgreso(false);
    this._storageData.setActiveNegocioHistorialPagosBeneficiarios(false);
    this._storageData.setInfoNegocioDetalleCompletoPagos(false); 
    this.obtenerInfoNegocio();
  }

  public iniciarPestanaHistorial() {
    this._storageData.setActiveNegocioHistorial(true);
    this._storageData.setNegocioHistoriallModificaciones(false);
  }

  public iniciarPestanaAsambleaHistorial() {
    this._storageData.setActiveAsambleaHistorial(true);
    this._storageData.setAsambleaHistorial(false);
    this._storageData.setTipoReunion("");
    this._storageData.setActiveReunionDetalles(false);
    this._storageData.setActiveComiteDetalles(false);
    this._storageData.setAsambleaNuevaEntrada(false);
    this._storageData.setComiteNuevaEntrada(false);
  }

  public showNegociosPagosDistribucion() {
    return this._storageData.getActiveNegocioPagosDistribucion();
  }

  public showPagosDistribucion() {
    return this._storageData.getActivePagosDistribucion();
  }

  public showHistorialPagosDistribucion() {
    return this._storageData.getActiveNegocioHistorialPagosDistribucion();
  }

  public showHistorialPagosIngresoEgreso() {
    return this._storageData.getActiveNegocioHistorialPagosIngresoEgreso();
  }

  public showHistorialPagosBeneficiarios() {
    return this._storageData.getActiveNegocioHistorialPagosBeneficiarios();
  }

  public validarNumeroPagoOrdinario(item) {
    this.cantidadFICRepetidos = 0;
    this.arrayDeCamposEncargos.value.forEach(element => {
      if (item.numeroPagoOrdinario == element.numeroPagoOrdinario) {
        this.cantidadFICRepetidos++;
      }
    });
    if (this.cantidadFICRepetidos > 1) {
      this.informacionGeneralNegocioForm.setErrors({ 'invalid': true });
    } else {
      this.informacionGeneralNegocioForm.setErrors(null);
    }
  }

  public showNegocioHistorial() {
    return this._storageData.getActiveNegocioHistorial();
  }

  public showNegocioHistorialModificaciones() {
    return this._storageData.getNegocioHistoriallModificaciones();
  }

  public showOpcionesAsambleas() {
    return (this._storageData.getActiveAsambleaHistorial() 
      && !this._storageData.getAsambleaNuevaEntrada()
      && !this._storageData.getComiteNuevaEntrada() );    
  }

  public showNuevaAsamblea() {
    return this._storageData.getAsambleaNuevaEntrada();
  }

  public showNuevoComite() {
    return this._storageData.getComiteNuevaEntrada();
  }

  public showAsambleaHistorial() {
    return this._storageData.getAsambleaHistorial();
  }

  public showAsambleaDetalle(){
    return this._storageData.getActiveReunionDetalles();
  }

  public showComiteDetalle(){
    return this._storageData.getActiveComiteDetalles();
  }

  public actualizarDepartamentoConstante(codigoPais){
    let codPai;
    if(codigoPais.value==undefined){
      codPai= codigoPais;
    }
    else{
      codPai=codigoPais.value;
    }
    let respuestaDeptoConstante = this.getDatosDepto(codPai);
      respuestaDeptoConstante.then((res) => {
        this.listaDepartamentosConstantes = res;
      });
  }

  public actualizarDepartamentoUbicacion(codigoPais){
    let codPai;
    if(codigoPais.value==undefined){
      codPai= codigoPais;
    }
    else{
      codPai=codigoPais.value;
    }
    let respuestaDeptoUbicacion = this.getDatosDepto(codPai);
    respuestaDeptoUbicacion.then((res) => {
        this.listaDepartamentosUbicacion = res;
      });
  }

  public actualizarDepartamentoAduana(codigoPais){
    let codPai;
    if(codigoPais.value==undefined){
      codPai= codigoPais;
    }
    else{
      codPai=codigoPais.value;
    }
    let respuestaDeptoAduana = this.getDatosDepto(codPai);
    respuestaDeptoAduana.then((res) => {
        this.listaDepartamentosAduana = res;
      });
  }

  public actualizarMunicipioUbicacion(codigoDepto){
    let codDepartamento;
    if(codigoDepto.value==undefined){
      codDepartamento= codigoDepto;
    }
    else{
      codDepartamento=codigoDepto.value;
    }
      let respuestaMunicipioUbicacion = this.getDatosMunicipios(codDepartamento);
      respuestaMunicipioUbicacion.then((res) => {
        this.listaMunicipiosUbicacion = res;
      });
  }


  public actualizarMunicipioConstante(codigoDepto){
    let codDepartamento;
    if(codigoDepto.value==undefined){
      codDepartamento= codigoDepto;
    }
    else{
      codDepartamento=codigoDepto.value;
    }
      let respuestaMunicipioConstante = this.getDatosMunicipios(codDepartamento);
      respuestaMunicipioConstante.then((res) => {
        this.listaMunicipiosConstantes = res;
      });
  }

  public actualizarMunicipioAduana(codigoDepto){
    let codDepartamento;
    if(codigoDepto.value==undefined){
      codDepartamento= codigoDepto;
    }
    else{
      codDepartamento=codigoDepto.value;
    }
      let respuestaMunicipioAduana = this.getDatosMunicipios(codDepartamento);
      respuestaMunicipioAduana.then((res) => {
        this.listaMunicipiosAduana = res;
      });
  }

  public obtenerFideicomitente(){
    this.request = {
      argumento: this.informacionGeneralNegocioForm.value.fideicomitenteNumeroDocumento,
      criterioBuscar: "documento",
      pagina:"1",
      elementos:"1"
    }
    this._dataService
    .getBeneficiarioListRequest(this.request)
    .subscribe((data: any) => {
      const fideiNombre = this.informacionGeneralNegocioForm.get('fideicomitenteNombreCompleto');
      const fideiTipo = this.informacionGeneralNegocioForm.get('fideicomitenteTipoDocumento');
      let valorNombre= data['beneficiarios'][0]['nombreCompleto'];
      let valorTipo= data['beneficiarios'][0]['tipoDocumento'];
      fideiNombre.setValue(valorNombre);
      fideiTipo.setValue(valorTipo);
    },
    (err: any) => {
      let dataMensajeGuardado = {
        titulo: "Operador no encontrado",
        contenido:
          "El Operador no ha sido encontrado. Intente con otro número de documento",
        button: "ACEPTAR",
        urlimg: true
      };
      const dialogRef = this.dialog.open(DialogParametrosSaveComponent, {
        width: "40rem",
        data: dataMensajeGuardado
      });
      dialogRef.afterClosed().subscribe(result => {
      });
    });
  }

  public actualizarSubTipoFideicomiso(tipofideicomiso){
    this._dataService
    .getSubTipoFideicomiso(tipofideicomiso.value)
    .subscribe(
      (subTipo: any) => {
       this.subTipoFide=subTipo;
      },
      (err: any) => {
      }
    );
  }

  async getDatosPais() {
    return this._dataService.getPaises().toPromise();
  }

  async getDatosDepto(idPais) {
    return this._dataService.getDepartamentos(idPais).toPromise();
  }

  async getDatosMunicipios(idDepto) {
    return this._dataService.getMunicipios(idDepto).toPromise();
  }
}
