import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificadosBuscarNegocioComponent } from './certificados-buscar-negocio.component';

describe('CertificadosBuscarNegocioComponent', () => {
  let component: CertificadosBuscarNegocioComponent;
  let fixture: ComponentFixture<CertificadosBuscarNegocioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertificadosBuscarNegocioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificadosBuscarNegocioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
