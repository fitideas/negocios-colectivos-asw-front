import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificadosVerificarVinculadosComponent } from './certificados-verificar-vinculados.component';

describe('CertificadosVerificarVinculadosComponent', () => {
  let component: CertificadosVerificarVinculadosComponent;
  let fixture: ComponentFixture<CertificadosVerificarVinculadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertificadosVerificarVinculadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificadosVerificarVinculadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
