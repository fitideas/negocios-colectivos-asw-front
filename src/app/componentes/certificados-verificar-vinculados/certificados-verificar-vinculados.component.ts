import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatDialog } from '@angular/material';
import { DataService } from 'src/app/services/data.service';
import { MensajeEsperaGenericoComponent } from 'src/app/dialogs/mensaje-espera-generico/mensaje-espera-generico.component';
import { ModalSincronizacionErrorComponent } from 'src/app/dialogs/modal-sincronizacion-error/modal-sincronizacion-error.component';
import {SelectionModel} from '@angular/cdk/collections';

@Component({
  selector: 'app-certificados-verificar-vinculados',
  templateUrl: './certificados-verificar-vinculados.component.html',
  styleUrls: ['./certificados-verificar-vinculados.component.css']
})

/**
* Autor:Fabian Herrera  
* Proposito: Modal usado para mostrar el detalle de los certificados a generar del vinculado
* Fecha: 12/04/2020
*/

export class CertificadosVerificarVinculadosComponent implements OnInit {


  dataSource: MatTableDataSource<any>;
  
  public dataFound :boolean=true;
  public dataNotFound:boolean=false;
  public displayedColumns :any[]=[];
  public seleccionadosReporte :any[]=[];
  public periodoSeleccionado:string;
  public certificadoSeleccionado : string;
  public datosEntrada:any[]=[];
  selection = new SelectionModel<any>(true, []);
  public request:any[]=[];
  public mensajeDeError :string ="";
  todosLosDatosReporte: any;
  botonMostrar: boolean;
  constructor(
    public dialogRef: MatDialogRef<CertificadosVerificarVinculadosComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dataService: DataService, 
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.data['seleccionadosReporte']);

    this.datosEntrada = this.data["seleccionadosReporte"];
    this.periodoSeleccionado = this.data["periodoSeleccionado"];
    this.certificadoSeleccionado = this.data["certificadoSeleccionado"]; 
    this.crearDisplayedColums();
   this.datosEntrada.forEach(element => {
    this.request.push({
      codigoNegocio:element.codSFC,
      tipoDocumento:element.tipoDocumento,
      numeroDocumento:element.numeroDocumento,
      anoGrabable:this.periodoSeleccionado
    })

   });

   this.buscarDatosCertificado(this.request, this.certificadoSeleccionado);

  }
  crearDisplayedColums(){
    if (this.certificadoSeleccionado=='PART'){
    this.displayedColumns = [
      "nombreCompleto",
      "tipoDocumento",
      "numeroDocumento",
      "participacion",
      "numeroderechos",
      "id"
    ];
  }else if (this.certificadoSeleccionado=='INGRET'){

  this.displayedColumns = [
    "nombreCompleto",
    "tipoDocumento",
    "numeroDocumento",
      "baseretencion",
      "totalretenido",
      "id"
    ];
  }else if (this.certificadoSeleccionado=='RENTAEXC'){
    this.displayedColumns = [
      "nombreCompleto",
      "tipoDocumento",
      "numeroDocumento",
      "rentaexcenta",
      "id"
    ];
  }
  }
  administrarEscogidos(row){
    if(this.seleccionadosReporte.includes(row)){
      this.seleccionadosReporte 
        = this.seleccionadosReporte.filter(item => item!=row);
    }else{
      this.seleccionadosReporte.push(row);
    }
    
  }

  seleccionarTodos(){
    this.botonMostrar=true;
  }

  todoGenerarArchivo(){
    this.request=[];
    this.todosLosDatosReporte.forEach(element => {
      this.request.push({
        codigoNegocio:element.codSFC,
        tipoDocumento:element.tipoDocumento,
        numeroDocumento:element.numeroDocumento,
        anoGrabable:this.periodoSeleccionado
      })
    });
    this.generarArchivo(this.certificadoSeleccionado, this.request);
  }



  selected(){
    if (this.seleccionadosReporte.length>0){
      this.botonMostrar=false;
      return true;
    }else{
      return false;
    }
  }

  buscarDatosCertificado(requestBuscar, certificadoSeleccionado) {
    this.dataSource = null;
    
    this._dataService.postCertificadosObtenerDatos(requestBuscar, certificadoSeleccionado)
    .subscribe((response: any) => {
      if (!response || response.length==0) {
         this.dataNotFound = true;
          this.dataFound = false;
          return;
        }else{
        this.dataFound = true;
        this.dataNotFound = false;
        
        }
      setTimeout(() => {
       this.todosLosDatosReporte=response;
        this.dataSource = new MatTableDataSource(response);
      
        });
      },
        (err) => {
         this.dataNotFound = true;
         this.mensajeDeError = err.error;
         this.mensajeDeError= this.mensajeDeError.split(":", 3)[2];         
        }
      );
  }


  tipoReporte(claveTipo){
      if (claveTipo==this.certificadoSeleccionado){
        return true;
      }else{
        return false;
      }
  }


  verificarSeleccion(){
     this.request=[];
    this.seleccionadosReporte.forEach(element => {
      this.request.push({
        codigoNegocio:element.codSFC,
        tipoDocumento:element.tipoDocumento,
        numeroDocumento:element.numeroDocumento,
        anoGrabable:this.periodoSeleccionado
      })
    });
   this.generarArchivo(this.certificadoSeleccionado, this.request);
  
    }
  
    public generarArchivo(paramTipo:string, request) {
      this._dataService
        .generarCertificado(paramTipo, request)
        .subscribe((informacionArchivo: any) => {
          let data;
          data = {
            titulo: "Generando certificados", 
            contenido: "Se esta generando el certificado del vinculado."  
          };
          const cargaDialog=this.dialog.open(MensajeEsperaGenericoComponent, {
            width: "46rem",
            data
          });
          setTimeout (() => {
            var nameOfFileToDownload = informacionArchivo["nombreArchivoZip"];
            const byteCharacters = atob(informacionArchivo["recursoB64"]);
            const byteNumbers = new Array(byteCharacters.length);
            for (let i = 0; i < byteCharacters.length; i++) {
              byteNumbers[i] = byteCharacters.charCodeAt(i);
            }
            const byteArray = new Uint8Array(byteNumbers);
            const blob = new Blob([byteArray], {
              type: informacionArchivo.tipoArchivo
            });
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(blob, nameOfFileToDownload);
            } else {
              var a = document.createElement("a");
              a.href = URL.createObjectURL(blob);
              a.download = nameOfFileToDownload;
              document.body.appendChild(a);
              a.click();
              document.body.removeChild(a);
              cargaDialog.close();
            }
         }, 4000); 
        
        }, 
        (error)=>{
          let data = {
            titulo: "Error en la generación del archivo",
            contenido: "Ha ocurrido un error en la generación del archivo",
            buttonAceptar: "ACEPTAR",
            urlimg: true
          };
          //se usa este modal ya que  permite mostrar mensajes de error
          //genericos
          this.dialog.open(ModalSincronizacionErrorComponent, {
            width: "46rem",
            data
          });
        });
    }
  
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    return numSelected;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row){
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

}
