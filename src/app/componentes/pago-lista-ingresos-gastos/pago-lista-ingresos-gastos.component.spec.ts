import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagoListaIngresosGastosComponent } from './pago-lista-ingresos-gastos.component';

describe('PagoListaIngresosGastosComponent', () => {
  let component: PagoListaIngresosGastosComponent;
  let fixture: ComponentFixture<PagoListaIngresosGastosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagoListaIngresosGastosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagoListaIngresosGastosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
