import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog } from '@angular/material';
import { ListIngresosGastos } from 'src/app/modelo/list-conceptos-ing-gas';
import { DialogAddIngresoComponent } from 'src/app/dialogs/dialog-add-ingreso/dialog-add-ingreso.component';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';

@Component({
  selector: 'app-pago-lista-ingresos-gastos',
  templateUrl: './pago-lista-ingresos-gastos.component.html',
  styleUrls: ['./pago-lista-ingresos-gastos.component.css']
})
/**
 * @Autor: Lizeth Patino
 * @Fecha: Marzo 2020
 * @Proposito: clase para listar ingreso y egresos Gastos.
 */
export class PagoListaIngresosGastosComponent implements OnInit {

  displayedColumns: string[] = [
    "concepto",
    "fecha",
    "valor",
  ];

  displayedColumns2: string[] = [
    "concepto",
    "fecha",
    "valor",
  ];
  dataSource: MatTableDataSource<ListIngresosGastos>;
  dataSource2: MatTableDataSource<ListIngresosGastos>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @Input() listaConceptos: any;
  //@Input() listaIngresoPagosIngresos: boolean;
  listaIngresoPagosIngresos:boolean;
  //@Output() propagar = new EventEmitter<boolean>();
  
  resultsLength: any;
  resultsLength2: any;
  informacionQueLlegoServicio: any;
  datosParaCalcularDistribucion: any;
  vistaListarTitulares: boolean = false;
  listaConceptoDos: any;
  terceraVista:boolean=false;
  noMostrar: boolean=false;
  periodoElegido: any;

  /**
* Autor:Lizeth Patiño
* Proposito: Generación de listado de ingresos y egresos
* Fecha: 08/04/2020
*/
  constructor(public dialog: MatDialog,
    private _dataService: DataService,
    private _storageData: StorageDataService) { }

  ngOnInit() {
    this.listaIngresoPagosIngresos=this._storageData.getRegresarPagoDistribucionVistaUno();
    this.periodoElegido=this._storageData.getPagoDistribucionEnviarPeriodo();
    this.listaConceptoDos = this._storageData.getListarConcepto();
    this.datosParaCalcularDistribucion = this._storageData.getInfoNegocioTB();
    this.dataSource = new MatTableDataSource(this.listaConceptoDos.listaIngresos);
    this.dataSource2 = new MatTableDataSource(this.listaConceptoDos.listaObligaciones);
    setTimeout(() => this.dataSource.paginator = this.paginator);
    this.resultsLength = this.listaConceptoDos.listaIngresos.length;
    this.resultsLength2 = this.listaConceptoDos.listaObligaciones.length;
    this.dataSource.sort = this.sort;
  }

  public regresar() {
    this.listaIngresoPagosIngresos = false;
    this._storageData.setRegresarPagoDistribucionVistaUno(this.listaIngresoPagosIngresos);
  }


 public recicibirInfoComponenteTres(mensaje){
      this.terceraVista=mensaje;
      this.noMostrar=true;
 }

  public AceptarContinuar() {
    this.vistaListarTitulares = false;
    if (this.listaConceptoDos.sePuedeDistribuir === false) {
      let dataMensajeGuardado = {
        titulo: "No es posible hacer distribución",
        contenido:
          "No se cuenta con suficientes fondos para hacer una distribución entre los beneficiarios. ",
        button: "AÑADIR OTRO INGRESO",
        urlimg: true
      };
      const dialogRef = this.dialog.open(DialogAddIngresoComponent, {
        width: "42rem",
        data: dataMensajeGuardado
      });
      dialogRef.afterClosed().subscribe(async res => {
        this.listaIngresoPagosIngresos = false;
        this._storageData.setRegresarPagoDistribucionVistaUno(this.listaIngresoPagosIngresos);
        //this.propagar.emit(this.listaIngresoPagosIngresos);
      });
    } else {
      this._dataService.negocioCalcularDistribucion(this.datosParaCalcularDistribucion.periodo, this.datosParaCalcularDistribucion.codigoNegocio, this.datosParaCalcularDistribucion.codigoTipoNegocio).subscribe((informacionTipoNegocio: any) => {
        this.informacionQueLlegoServicio = informacionTipoNegocio;
        this.vistaListarTitulares = true;
        this.listaIngresoPagosIngresos=false;
        this._storageData.setRegresarPagoDistribucionVistaUno(this.listaIngresoPagosIngresos);
        this._storageData.setRegresarPagoDistribucionVistaDos(this.vistaListarTitulares);
      });
    }
  }

  public getTotalCost() {
    return this.listaConceptoDos.listaIngresos.map(t => t.valor).reduce((acc, value) => acc + value, 0);
  }

  public getTotalGasto() {
    return this.listaConceptoDos.listaObligaciones.map(t => t.valor).reduce((acc, value) => acc + value, 0);
  }

}
