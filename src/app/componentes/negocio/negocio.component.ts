import { Component, OnInit, ViewChild } from "@angular/core";
import { DataService } from "src/app/services/data.service";
import { IconService } from "src/app/services/icon.service";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { StorageDataService } from 'src/app/services/storage-data.service';
import { AuthService } from 'src/app/services/auth.service';
import { Utils } from 'src/app/core/utils';
import { ResponseNegocios } from 'src/app/modelo/response-negocios';
import { ListNegocios } from 'src/app/modelo/list-negocios';
import { Router } from '@angular/router';

export interface Criterio {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-negocio',
  templateUrl: './negocio.component.html',
  styleUrls: ['./negocio.component.css']
})


/**
* Autor:Lizeth Patiño
* Proposito: listado de negocios asociados y busqueda por filtro de nombre y número de documento
* Fecha: 08/04/2020
*/

export class NegocioComponent implements OnInit {
  public buscarNegociosForm: FormGroup;
  public submitted: boolean = false;
  public dataNotFound: boolean = false;
  public dataFound: boolean = false;
  public listNegocio: ListNegocios[] = new Array();
  public resultsLength = 0;
  public negocios: ResponseNegocios;
  public negocioDetalle:boolean=false;
  public consultar_vinculado: boolean = false; 
  public datos_generales: boolean = false;
  public negocios_asociados: boolean = false; 
  public historial_modificaciones: boolean = false; 
  public negocio_detalle_permiso: boolean = false;
  public negocio_detalle_escribe: boolean = false;
  public errorServer: null;

  displayedColumns: string[] = [
    "nombre",
    "codigoSFC",
    "codigoInterno",
    "id"
  ];

  public codigoNegocio;
  public nombreNegocio;

  dataSource: MatTableDataSource<ListNegocios>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  selectCriterio: Criterio[] = [
    { value: "codigo", viewValue: "Código del negocio" },
    { value: "nombre", viewValue: "Nombre del negocio" }
  ];
  
  public page: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPages: number = 1; //Número total de páginas 
  public numElementos: number = 10; //Total de elemento por pagina
  public request: any; 

  constructor(
    private _dataService: DataService,
    private _iconService: IconService,
    private _storageData: StorageDataService,
    private _authService: AuthService,
    private _router: Router,
    public fb: FormBuilder,
    public _util: Utils
  ) {
    this.buscarNegociosForm = this.fb.group({
      selectCriterio: new FormControl("", [Validators.required]),
      argumento: new FormControl("", Validators.required)
    });
  }

  ngOnInit() {

    this._iconService.registerIcons();
    this.listNegocio = new Array();
    this.dataNotFound = false;
    this.dataFound = false;
    this.negocioDetalle = false;
    this.loadPermisos();
    if (this._storageData.getSwInicio()) {
      this._router.navigate(['accionfiduciaria']);
    }

  }

  public resetTables() {
    this.dataNotFound = false;
    this.dataFound = false;
    this.listNegocio = null;
    this.buscarNegociosForm.controls['argumento'].setValue('');
  }
  
  private loadPermisos() {    
    this.consultar_vinculado = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(),'consultar_vinculado');    
    this.datos_generales = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(),'datos_generales');
    this.negocios_asociados = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(),'negocios_asociados');
    this.historial_modificaciones = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(),'historial_modificaciones');        
    this.negocio_detalle_permiso = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(),'negocio_detalle');
    this.negocio_detalle_escribe = this._util.permiteEscribir(this._authService.getFuncionalidades(), 'negocio_detalle');
    this._storageData.setPDatosGenerales(this.datos_generales);
  }

  public verPestanas() {     
    if (this.datos_generales || this.negocios_asociados || this.historial_modificaciones) {
      return true;
    }
    return false;
  }

  get f() {
    return this.buscarNegociosForm.controls;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  goToPage(page: number) {
    this.page = page;
    let requestPaginado: any = this.request;
    requestPaginado.pagina = page;
    requestPaginado.elementos = this.numElementos;
    this.buscarNegociosList(requestPaginado);
  }


  enviarDatos(): void {
    this.dataNotFound = false;
    this.dataFound = false;
    this.submitted = true;
    this.negocioDetalle = false;
    if (this.buscarNegociosForm.invalid) {
      return;
    }

    if (this.buscarNegociosForm.value.selectCriterio) {
      this.request = {
      argumento: this.buscarNegociosForm.value.argumento,
      criterioBuscar: this.buscarNegociosForm.value.selectCriterio
      }
    }
    let requestPaginado: any = this.request;
    requestPaginado.pagina = 1;
    requestPaginado.elementos = this.numElementos;
    this.buscarNegociosList(requestPaginado);
  }

 public getNegocioDetalle(codigoNegocio, estadoProyecto) {
   if (this.negocio_detalle_permiso) {  
   this._dataService.getObtenerInformacionNegocio(codigoNegocio)
      .subscribe((dati: any) => {
        if (!dati) {
          this.dataNotFound = true;
          this.dataFound = false;
          this.negocioDetalle = false;
          return;
        }
        this.dataFound = false;
        this.dataNotFound = false;
        this.negocioDetalle = true;
        this._storageData.setEstadoProyecto(estadoProyecto);
        this._storageData.setInfoNegocioDetalle(dati);
        this._router.navigate(['accionfiduciaria/negocios/detalles']);
      },
        (err) => {
          this.dataFound=false;
          //this.dataNotFound = true;
          this.errorServer = err.error;   
        }
      );
    }     
  }

  

  buscarNegociosList(requestBuscar) {
    this.negocios = null;
    this.dataFound = false;
    this.negocioDetalle = false;
    this.errorServer = null;
    this.dataNotFound = false;

    this._dataService.getNegociosListRequest(requestBuscar).subscribe((data: any) => {
      if (!data) {
          this.dataNotFound = true;          
          return;
        }else{
        this.dataFound = true;        
        this.negocios = data;
       
        }
      setTimeout(() => {
         this.loadnegocios(this.negocios['negocios']);
         this.dataSource = new MatTableDataSource(this.listNegocio);
         this.resultsLength = data.totalNegocios;
         this.totalPages = this.resultsLength / this.numElementos;          
          let totalPagina = this.resultsLength % this.numElementos;
          if (totalPagina > 1) {
            this.totalPages = Number(parseInt(String(this.totalPages))) + 1;
          }
          if (this.totalPages < 1) {
            this.totalPages = 1;
          } 
         this.dataSource.paginator = this.paginator;
         this.dataSource.sort = this.sort;
        });
      },
        (err) => {
          this.errorServer = err.error;                  
        }
      );
  }

  loadnegocios(negocios): void {
    this.listNegocio = [];

    negocios.forEach(negocio => {
      this.listNegocio.push(negocio);
    });
  }
}
