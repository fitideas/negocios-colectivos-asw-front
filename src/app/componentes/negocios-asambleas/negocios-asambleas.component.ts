import { Component, OnInit,ChangeDetectorRef  } from '@angular/core';
import { Utils } from 'src/app/core/utils';
import { AuthService } from 'src/app/services/auth.service';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { ComiteAdministrarComiteComponent } from '../comite-administrar-comite/comite-administrar-comite.component';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-negocios-asambleas',
  templateUrl: './negocios-asambleas.component.html',
  styleUrls: ['./negocios-asambleas.component.css']
})
/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Marzo 2020
 * @Proposito: clase para gestionar asambleas de negocios .
 */
export class NegociosAsambleasComponent implements OnInit {

  public aplicanComite: boolean = false;
  public nuevaEntradaAsamblea: boolean = false;
  public asamblea_agregar: boolean = false;
  public asamblea_ver_historial: boolean = false;
  public codigoSFC: any;

  public historial_asambleas: boolean = false;

  displayedColumns: string[] = [
    "name",
    "typedocument",
    "documentNumber",
     "rol"
  ];

  dataSource: MatTableDataSource<any>;
  modalFueAbierto: boolean=false;
  public nuevaEntradaComite: boolean=false;
  estadoProyecto: any;
  form: FormGroup;
  deshabilitarBotonNuevaEntrada: boolean=true;



  constructor(
    private changeDetectorRefs: ChangeDetectorRef,
    public dialog: MatDialog,
    private _storageData: StorageDataService,
    private _dataService: DataService,
    public _util: Utils,
    private _authService: AuthService
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      'checkAplicanComite': new FormControl(false)
    });

    this.estadoProyecto=this._storageData.getEstadoProyecto();
    this.loadPermisos();
    this.llamarServicios();
  }

  private loadPermisos() {
    this.asamblea_agregar = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'asamblea_agregar');
    this.asamblea_ver_historial = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'asamblea_ver_historial');    
  }

  public llamarServicios(){
    this.codigoSFC=this._storageData.getcodigoSFCNegocio();
    let arregloFiltrado=[];
    this._dataService.getObtenerInformacionComite(this.codigoSFC).subscribe(
      (dataResultado: any) => { 
      dataResultado.forEach(miembros => {
        if(miembros.estado=='ACTIVO'){
          arregloFiltrado.push(miembros);
          this.aplicanComite = true;
          this.modalFueAbierto=true;
          this.form.patchValue({checkAplicanComite: true});
          if(arregloFiltrado==[]|| arregloFiltrado==null|| arregloFiltrado==undefined){
            this.deshabilitarBotonNuevaEntrada=true;
          }else{
            this.deshabilitarBotonNuevaEntrada=false;
          }
        }
        this.llenarDatasource(arregloFiltrado);
       
      });

    }, (err) => {
      this.aplicanComite = false;
      this.form.patchValue({checkAplicanComite: false});
    });
  }

  public llenarDatasource(dataSource){
   this.dataSource = new MatTableDataSource(dataSource);
  }

  aplicarComite() {
    this.aplicanComite = !this.form.value.checkAplicanComite;
  }

  public administrarMiembros(){
    
    let data;
    data = {
      buttonAceptar: "ACEPTAR",
      buttonCancelar: "CANCELAR",
      urlimg: false

    };
    const dialogRef = this.dialog.open(ComiteAdministrarComiteComponent, {
      width: "77rem",
      data
    });

    dialogRef.afterClosed().subscribe(async resultado => {
      if  (resultado == "ACEPTAR") {         
         this.llamarServicios(); 
      }
    });
  }
  
  public anadirNuevaEntradacomite(){
    this._storageData.setComiteNuevaEntrada(true);
    this.nuevaEntradaComite=this._storageData.getComiteNuevaEntrada();
  }

  public anadirNuevaEntradaAsamblea() {  
    this._storageData.setAsambleaNuevaEntrada(true);
    this.nuevaEntradaAsamblea = this._storageData.getAsambleaNuevaEntrada();
  }

  public verHistorialAsambleas() {
    this._storageData.setActiveAsambleaHistorial(false);    
    this._storageData.setAsambleaHistorial(true);
    this.historial_asambleas = true;
    this._storageData.setTipoReunion("asamblea"); 
  }

  public verHistorialComites(){
    this._storageData.setActiveAsambleaHistorial(false);    
    this._storageData.setAsambleaHistorial(true);
    this._storageData.setTipoReunion("comite");
  }

}
