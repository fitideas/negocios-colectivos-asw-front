import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NegociosAsambleasComponent } from './negocios-asambleas.component';

describe('NegociosAsambleasComponent', () => {
  let component: NegociosAsambleasComponent;
  let fixture: ComponentFixture<NegociosAsambleasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NegociosAsambleasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NegociosAsambleasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
