import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportesTipoNegocioComponent } from './reportes-tipo-negocio.component';

describe('ReportesTipoNegocioComponent', () => {
  let component: ReportesTipoNegocioComponent;
  let fixture: ComponentFixture<ReportesTipoNegocioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportesTipoNegocioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportesTipoNegocioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
