import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, FormBuilder } from '@angular/forms';
import {  MatDialog } from '@angular/material';
import { DataService } from 'src/app/services/data.service';
import { ModalSincronizacionErrorComponent } from 'src/app/dialogs/modal-sincronizacion-error/modal-sincronizacion-error.component';

export interface CriterioList {
  value: string;
  viewValue: string;
  active: boolean;
}

export interface tipo {
  tipo: string;
  numero: number;
}

@Component({
  selector: 'app-reportes-tipo-negocio',
  templateUrl: './reportes-tipo-negocio.component.html',
  styleUrls: ['./reportes-tipo-negocio.component.scss']
})
export class ReportesTipoNegocioComponent implements OnInit {
  public buscarPagosDynamicForm: FormGroup;
  isResponsableNegocio: boolean;
  tiposArr: tipo[];
  get filtrosFormArray() {
    return <FormArray>this.buscarPagosDynamicForm.get("filtros");
  }
  public isFecha: boolean = false;
  public isTipoNegocio: boolean = false;
  public isNegocioAsociado: boolean = false;
  public isUsuario: boolean = false;
  public isTodo: boolean = false;
  public swAddFiltros = true;
  public request: any;


  listSelectCriterio: CriterioList[] = [
    { value: "fecha", viewValue: "Fecha", active: true },
    { value: "responsablesNegocio", viewValue: "Responsables de negocio", active: true },    
  ];
  listSelectCriterioNuevo: CriterioList[] = [];

  public arrayFiltro: any[] = [];
  public nuevoItem: boolean = false;
  public listFechaRango: any[] = [];
  public listUsuario: any[] = [];
  public listNegocio: any[] = [];
  public listTipoNegocio: any[] = [];
  public listResponsableNegocio: any[] = [];

  public filtroEscogido:boolean =false;
  constructor(public fb: FormBuilder,
    private _dataService: DataService,
    public dialog: MatDialog) {
  }

  createGroup() {
    return this.fb.group({
      selectCriterio: new FormControl("", []),
      argumento: new FormControl("", []),
    });
  }


  addToFiltrosFormArray() {
    this.filtrosFormArray.push(this.createGroup());
    if (this.filtrosFormArray.length == 2) {
      this.swAddFiltros = false;
    }
    this.nuevoItem = true;
  }

  ngOnInit(): void {
    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarPagosDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();
  }

  checkFiltros() {
    if (this.filtrosFormArray.length == 1) {
      return false
    }
    return true;
  }

  removeFromFiltrosFormArray(index, itemFiltro) {
    this.filtrosFormArray.removeAt(index);


    if (itemFiltro.value.selectCriterio == "fecha") {
      this.listFechaRango = [];
      }else if (itemFiltro.value.selectCriterio == "responsablesNegocio") {
        this.listResponsableNegocio = [];
      }  
      this.isFiltroEscogido();

    //controla el boton mas
    if (this.filtrosFormArray.length < 3) {
      this.swAddFiltros = true;
    }

    //remueve un elemento del array de filtros
    this.arrayFiltro = this.arrayFiltro.filter(item => {
      return (item !== itemFiltro.value.selectCriterio);
    });
    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == itemFiltro.value.selectCriterio) {
        element.active = true;
      }
    });
  }

  buscarListaXCriterioSeleccionado(eventSeleccionado, ind) {
    this.isFecha = false;
    this.isNegocioAsociado = false;
    this.isTodo = true;
    this.isResponsableNegocio=false;
    this.isTipoNegocio = false;
    this.isUsuario = false;
    if (eventSeleccionado.value == 'todo') {
      this.isTodo = true;
    }
    if (eventSeleccionado.value == 'fecha') {
      this.arrayFiltro[ind] = 'fecha';
      this.isFecha = true;
      this.isResponsableNegocio=false;
      this.isTodo = false;
    }
    if (eventSeleccionado.value == 'responsablesNegocio') {
      this.arrayFiltro[ind] = 'responsablesNegocio';
      this.isFecha = false;
      this.isResponsableNegocio=true;
      this.isTodo = false;
    }
    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == eventSeleccionado.value) {
        element.active = false;
      }
      if (!this.isTodo) {
        if (element.value == 'todo') {
          element.active = false;
        }
      }
    });
  }

  isFiltroEscogido() {
    if (this.listFechaRango.length == 0
      && this.listResponsableNegocio.length == 0)
      {
      this.filtroEscogido = false;
    } else {
      this.filtroEscogido = true;
    }
  }

  getDateRange(range) {
   
    this.listFechaRango = range;
    this.isFiltroEscogido();
  }
  getResponsableNegocio(responsables) {   
    this.listResponsableNegocio = responsables;
    this.isFiltroEscogido();
  }

  limpiarformulario() {
    this.arrayFiltro = [];
    this.filtrosFormArray.clear();
    this.filtrosFormArray.reset([]);
    this.tiposArr = [];
    this.listFechaRango= [];
    this.listResponsableNegocio=[];
    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarPagosDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();
    this.swAddFiltros = true;
    this.listSelectCriterioNuevo.forEach(element => {
      element.active = true;
    });
  }

  enviarDatos(): void {
    let listFinalResponsableNegocio=[];
    this.listResponsableNegocio.forEach(numeroDoc => {
    listFinalResponsableNegocio.push(numeroDoc.numeroDocumento);
    });
    this.request = {
      responsablesNegocio: listFinalResponsableNegocio,
      rangoFecha: this.listFechaRango,
    }

    this._dataService.postServiceReporteTiposNegocio(this.request)
      .subscribe((data: tipo[]) => {
        
        if (!data) {
          return;
        }
        this.tiposArr=data;
        },   (error)=>{
          let data = {
            titulo: "Error realizando la consulta ",
            contenido: "Ha ocurrido un error en la consulta",
            buttonAceptar: "ACEPTAR",
            urlimg: true
          };
      
          //se usa este modal ya que  permite mostrar mensajes de error
          //genericos
          this.dialog.open(ModalSincronizacionErrorComponent, {
            width: "46rem",
            data
          });
        });
      
  }
}
