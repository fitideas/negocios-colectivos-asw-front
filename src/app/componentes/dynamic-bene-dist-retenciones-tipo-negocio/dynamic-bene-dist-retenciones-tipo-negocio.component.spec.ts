import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicBeneDistRetencionesTipoNegocioComponent } from './dynamic-bene-dist-retenciones-tipo-negocio.component';

describe('DynamicBeneDistRetencionesTipoNegocioComponent', () => {
  let component: DynamicBeneDistRetencionesTipoNegocioComponent;
  let fixture: ComponentFixture<DynamicBeneDistRetencionesTipoNegocioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicBeneDistRetencionesTipoNegocioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicBeneDistRetencionesTipoNegocioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
