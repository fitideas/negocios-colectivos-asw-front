import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormArray, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Utils } from 'src/app/core/utils';
import { DialogTwoButtonsComponent } from '../dialog-two-buttons/dialog-two-buttons.component';
import { MatDialog } from '@angular/material';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'app-dynamic-bene-dist-retenciones-tipo-negocio',
  templateUrl: './dynamic-bene-dist-retenciones-tipo-negocio.component.html',
  styleUrls: ['./dynamic-bene-dist-retenciones-tipo-negocio.component.scss']
})
export class DynamicBeneDistRetencionesTipoNegocioComponent implements OnInit {

  @Input() index: number;
  @Input() listRetenciones: any;
  @Input() editing: boolean;
  @Output() messageEvent = new EventEmitter<string>();

  public arrayRenciones: any[] = [];
  public showListRentencion: boolean = false;

  dynamicRetTipNegForm = this.fb.group({
    beneDistRetTipNegFormArray: this.fb.array([this.fb.control('', Validators.required)])
  });

  get beneDistRetTipNegFormArray(): FormArray {
    return this.dynamicRetTipNegForm.get('beneDistRetTipNegFormArray') as FormArray;
  }
  public get arrayDeCamposFormularioTipoNegocioGeneral() {
    return <FormArray>this.informacionGeneralNegocioForm.get("tipoNegocios");
  }
  public informacionGeneralNegocioForm: FormGroup;
  tiposDeNegocio: any[] = [];
  infoGeneralDesdeNegocio: any;


  constructor(
    private fb: FormBuilder,
    private _utils: Utils,
    private _storageService: StorageDataService,
    private currencyPipe: CurrencyPipe,
    public dialog: MatDialog
  ) {

  }

  ngOnInit() {
    this.loadRetencionesFormArray();
  }

  public loadRetencionesFormArray() {
    this.inicializarFormulario();
    this.infoGeneralDesdeNegocio = {
      tiposNegocio: this.listRetenciones
    }
    if (this.infoGeneralDesdeNegocio.tiposNegocio) {
      this.construirArrayTipoNegocio(this.infoGeneralDesdeNegocio.tiposNegocio);
      this.construirArrayRetenciones(this.infoGeneralDesdeNegocio.tiposNegocio);
    }
  }

  public inicializarFormulario() {
    this.informacionGeneralNegocioForm = this.fb.group({
      tipoNegocios: this.fb.array([])
    });
  }

  initTipoNegocios() {
    // this.negociosActivos = true;
    return this.fb.group({
      //  ---------------------forms fields on level Tipos de Negocios ------------------------
      id: ["", []],
      descripcion: ["", []],
      // ------------------------ Retenciones ---------------------------------------------
      retenciones: this.fb.array([])
    });
  }

  initRetenciones() {
    return this.fb.group({
      //  ---------------------forms fields on y level retenciones------------------------
      id: ["", Validators.compose([Validators.required])],
      descripcion: ["", []],
      valor: ["", []],
      valorunidad: ["", []],
      unidad: ["", []]
    });
  }

  public construirArrayTipoNegocio(infoServicio) {
    let informacionServicio = infoServicio;
    informacionServicio.forEach(dato => {
      const formGroup = this.initTipoNegocios();
      formGroup.patchValue(dato);
      this.arrayDeCamposFormularioTipoNegocioGeneral.push(formGroup);
    });
  }

  public construirArrayRetenciones(infoServicio) {
    let informacionServicio = infoServicio;
    let primera: any = this.informacionGeneralNegocioForm["controls"].tipoNegocios["controls"];
    var vari: any;
    informacionServicio.forEach((elemento, ind) => {
      elemento.retenciones.forEach((dato) => {
        if (dato.unidad == 'COP') {
          dato.valor = this._utils.cambiarFloatante(dato.valor, 2);
          dato.valor = this.currencyPipe.transform(dato.valor);
        }
        dato.valorunidad = dato.valor + ' ' + dato.unidad;
        const formGroup = this.initRetenciones();
        formGroup.patchValue(dato);
        vari = primera[ind].controls.retenciones.controls;
        vari.push(formGroup);
      });
    });
  }


  public deleteRetenciones(idxtipNeg, idxRet) {
    let data;
    let idRet;
    let idTipoNegocio: any = null;
    let retencionesA = (<FormArray>this.informacionGeneralNegocioForm.controls["tipoNegocios"]).at(
      idxtipNeg
    ) as FormArray;
    idTipoNegocio = retencionesA.value.id;
    idRet = retencionesA.controls["retenciones"].at(idxRet).value.id;
    data = {
      titulo: "Eliminar Retención",
      contenido: "Se esta intentando eliminar una retención ¿Está seguro?",
      buttonContinuar: "Continuar",
      buttonCancelar: "Cancelar",
      urlimg: true
    };
    const dialogRef = this.dialog.open(DialogTwoButtonsComponent, {
      width: "42rem",
      data
    });

    dialogRef.afterClosed().subscribe(async resultado => {
      if (resultado == "Continuar") {
        this.almacenarMemoria(idTipoNegocio, idRet);
        retencionesA.controls["retenciones"].removeAt(idxRet);
      }
    });
  }

  public almacenarMemoria(idTipoNegocio, idRet) {   
    this.messageEvent.emit('true');
    let retencionesTipoNegocioEliminadas = {
      idTipoNegocio: idTipoNegocio,
      idRet: idRet,
    }
    this._storageService.setBeneficiariosDistribucionRetencionesTipoNegocioEliminar(retencionesTipoNegocioEliminadas);
  }
}
