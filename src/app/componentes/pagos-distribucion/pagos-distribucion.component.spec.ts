import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagosDistribucionComponent } from './pagos-distribucion.component';

describe('PagosDistribucionComponent', () => {
  let component: PagosDistribucionComponent;
  let fixture: ComponentFixture<PagosDistribucionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagosDistribucionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagosDistribucionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
