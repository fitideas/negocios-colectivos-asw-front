import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Utils } from 'src/app/core/utils';
import { DialogTwoButtonsComponent } from '../dialog-two-buttons/dialog-two-buttons.component';
import { MatDialog, MatDatepicker, DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import { DialogParametrosSaveComponent } from '../dialog-parametros-save/dialog-parametros-save.component';
import { MY_FORMATS } from './../../datepickers/datepicker-mes-anio/datepicker-mes-anio.component';
import moment from 'moment';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { formatDate, DatePipe, CurrencyPipe } from '@angular/common';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { NegocioPago } from 'src/app/modelo/negocio-pago-distribucion';
import { isDate } from 'util';
import { ModalSincronizacionErrorComponent } from 'src/app/dialogs/modal-sincronizacion-error/modal-sincronizacion-error.component';
import { ModalSincronizacionTitularnuevoComponent } from 'src/app/dialogs/modal-sincronizacion-titularnuevo/modal-sincronizacion-titularnuevo.component';
import { ModalSincronizacionInformacionFinancieraIncompletaComponent } from 'src/app/dialogs/modal-sincronizacion-informacion-financiera-incompleta/modal-sincronizacion-informacion-financiera-incompleta.component';
import { ModalSincronizacionConstantesComponent } from 'src/app/dialogs/modal-sincronizacion-constantes/modal-sincronizacion-constantes.component';
import { ModalSincronizacionCambiodatotitularComponent } from 'src/app/dialogs/modal-sincronizacion-cambiodatotitular/modal-sincronizacion-cambiodatotitular.component';
import { ModalSincronizacionComponent } from 'src/app/dialogs/modal-sincronizacion/modal-sincronizacion.component';


@Component({
  selector: 'app-pagos-distribucion',
  templateUrl: './pagos-distribucion.component.html',
  styleUrls: ['./pagos-distribucion.component.css'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
/**
 * @Autor: Lizeth Patiño
 * @Fecha: Marzo 2020
 * @Proposito: clase para gestionar Pagos Distribucion Component .
 */
export class PagosDistribucionComponent implements OnInit {
  public todayDate = moment();
  public gastosTipoNegocioForm: FormGroup;
  infoGeneralDesdeNegocio: any;
  codigoSFC: any;
  listaIngresoPagosIngresos: boolean = false;
  listaEncargosPorNegocio: any;
  listaFiltradaEncargos: any;
  GuardarnuevaEntradaPaBack: NegocioPago;
  informacionQueLlegoServicio: any;
  activarBotonCalcularObligacion: boolean = false;
  listaConceptos: any
  today = moment().format('DD/MM/YYYY');
  periodoActual = Date();
  @ViewChild(MatDatepicker, { static: false }) picker;
  pastDate: string;
  datePipeES: DatePipe = new DatePipe('es-CO');
  fechaMinimaIngreso = new Date();
  fechaLimite = new Date();
  botonInhabilitado: boolean = true;
  constructor(
    private datePipe: DatePipe,
    private fb: FormBuilder,
    public _util: Utils,
    private _dataService: DataService,
    public dialog: MatDialog,
    private currencyPipe: CurrencyPipe,
    private _storageData: StorageDataService,
  ) {

  }

  ngOnInit() {
    this.listaIngresoPagosIngresos=this._storageData.getRegresarPagoDistribucionVistaUno();
    var finnewDate = new Date(this.fechaMinimaIngreso.setDate(1))
    this.fechaMinimaIngreso = finnewDate;
    this.periodoActual = this.datePipe.transform(this.periodoActual, '01/MM/yyyy')
    this.pastDate = moment(this.todayDate).subtract(1, 'months').format('YYYY-MM-01');
    this.infoGeneralDesdeNegocio = this._storageData.getInfoNegocioDetalle();
    this.codigoSFC = this.infoGeneralDesdeNegocio.codigoSFC;
    this.obtenerInformacionDeServicios();
    this.listaEncargosPorNegocio = this.infoGeneralDesdeNegocio.encargos;
    this.listaFiltradaEncargos = this.obtenerListadoFiltradoEncargo(this.listaEncargosPorNegocio);
    this.gastosTipoNegocioForm = this.fb.group({
      periodoActual: ["", Validators.compose([])],
      camposTipoNegocio: this.fb.array([])
    });
  }

  public obtenerInformacionDeServicios() {
    this._dataService.getObtenerIngresosOperacionPorPeriodo(this.codigoSFC, this.periodoActual).subscribe((informacionTipoNegocio: any) => {
      this.informacionQueLlegoServicio = informacionTipoNegocio;
      this.gastosTipoNegocioForm.get('periodoActual').setValue(this.transformarFecha(this.periodoActual));
      this.construirArrayTipoNegocio(this.informacionQueLlegoServicio.listaTipoNegocio);
      this.inhabilitarBotonGuardar(this.informacionQueLlegoServicio);
      this.contruirArrayIngresos(this.informacionQueLlegoServicio.listaTipoNegocio);
    });
  }


  public inhabilitarBotonGuardar(informacion) {
    informacion['listaTipoNegocio'].forEach(listaIngresosOperacion => {
      if (listaIngresosOperacion['listaIngresosOperacion'].length == 0) {
        this.botonInhabilitado = true;
      } else {
        this.botonInhabilitado = false;
      }
    });
  }

  public obtenerListadoFiltradoEncargo(listadoEncargos) {
    return listadoEncargos.filter(encargo => encargo.estado === "ACT");
  }

  public transformarFecha(dateString) {
    let dateParts: any;
    let dateObject: any = null;
    if (dateString != null) {
      dateParts = dateString.split("/");
      dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
    }
    return dateObject;
  }

  public transformaFechaParaBack(fechaAtransformar) {
    const format = 'dd/MM/yyyy';
    const myDate = fechaAtransformar;
    const locale = 'en-CO';
    const formattedDate = formatDate(myDate, format, locale);
    return formattedDate;
  }

  public llamarServicioConEleccionPeriodo() {
    this.arrayDeCamposTipoNegocio.clear();
    this._dataService.getObtenerIngresosOperacionPorPeriodo(this.codigoSFC, this.transformaFechaParaBack(this.gastosTipoNegocioForm.value.periodoActual)).subscribe((informacionTipoNegocio: any) => {
      this.informacionQueLlegoServicio = informacionTipoNegocio;
      this.gastosTipoNegocioForm.get('periodoActual').setValue(this.gastosTipoNegocioForm.value.periodoActual);
      this.construirArrayTipoNegocio(this.informacionQueLlegoServicio.listaTipoNegocio);
      this.inhabilitarBotonGuardar(this.informacionQueLlegoServicio);
      this.contruirArrayIngresos(this.informacionQueLlegoServicio.listaTipoNegocio);
    });
  }

  public construirArrayTipoNegocio(infoServicio) {
    let informacionServicio = infoServicio;
    informacionServicio.forEach(dato => {
      const formGroup = this.crearGrupoTipoNegocio();
      formGroup.patchValue(dato);
      this.arrayDeCamposTipoNegocio.push(formGroup);
    });
  }

  public contruirArrayIngresos(infoServicio) {
    let informacionServicio = infoServicio;
    let primera: any = this.gastosTipoNegocioForm["controls"].camposTipoNegocio["controls"];
    var vari: any;
    informacionServicio.forEach((elemento, ind) => {
      elemento.listaIngresosOperacion.forEach((dato) => {
        const formGroup = this.crearGrupoIngreso();
        dato.fechaIngreso = this.transformarFecha(dato.fechaIngreso);
        dato.valorIngreso = this.currencyPipe.transform(dato.valorIngreso);
        formGroup.patchValue(dato);
        vari = primera[ind].controls.listaIngresosOperacion;
        vari.push(formGroup);
      });
    });
  }

  public crearGrupoTipoNegocio() {
    return this.fb.group({
      nombreTipoNegocio: ["", Validators.compose([])],
      codigoTipoNegocio: [],
      cuenta: ["", Validators.compose([])],
      listaIngresosOperacion: this.fb.array([]),
    });
  }

  public crearGrupoIngreso() {
    return this.fb.group({
      //  ---------------------forms fields on y level ingresos ------------------------
      valorIngreso: ["", Validators.compose([Validators.required])],
      radicadoIngreso: ["", Validators.compose([Validators.required])],
      fechaIngreso: ["", Validators.compose([Validators.required])],
      esEditable: [true],
      fechaSistema: [this.today],
      periodo: [this.transformaFechaParaBack(this.gastosTipoNegocioForm.get('periodoActual').value)],
    });
  }

  public get arrayDeCamposTipoNegocio() {
    return <FormArray>this.gastosTipoNegocioForm.get("camposTipoNegocio");
  }

  public getIngresos(ix) {
    return (<FormArray>(
      (<FormArray>this.gastosTipoNegocioForm.get("camposTipoNegocio")).controls[ix].get("listaIngresosOperacion")
    )).controls;
  }

  public addIngresos(ix) {
    this.botonInhabilitado = false;
    this.activarBotonCalcularObligacion = false;
    const control = (<FormArray>this.gastosTipoNegocioForm.controls["camposTipoNegocio"]).at(ix).get("listaIngresosOperacion") as FormArray;
    control.push(this.crearGrupoIngreso());

  }

  public deleteIngresos(idxtipNeg, idxRet) {
    let retencionesA = (<FormArray>this.gastosTipoNegocioForm.controls["camposTipoNegocio"]).at(
      idxtipNeg
    ) as FormArray;
    if (retencionesA.controls["listaIngresosOperacion"].length !== 1) {
      let data;
      data = {
        titulo: "Eliminar Ingreso",
        contenido: "Se esta intentando eliminar un Ingreso ¿Está seguro?",
        buttonContinuar: "Continuar",
        buttonCancelar: "Cancelar",
        urlimg: true
      };
      const dialogRef = this.dialog.open(DialogTwoButtonsComponent, {
        width: "42rem",
        data
      });

      dialogRef.afterClosed().subscribe(async resultado => {
        switch (resultado) {
          case "Continuar":
            let retencionesAA = (<FormArray>this.gastosTipoNegocioForm.controls["camposTipoNegocio"]).at(
              idxtipNeg
            ) as FormArray;
            retencionesAA.controls["listaIngresosOperacion"].removeAt(idxRet);
            this.activarBotonCalcularObligacion = false;
            break;
          case "Cancelar":
        }
      });
    } else {
      let dataMensajeGuardado;

      dataMensajeGuardado = {
        titulo: "Gasto NO Eliminado",
        contenido: "El Gasto no puede ser eliminado.",
        button: "ACEPTAR",
        urlimg: true
      };
      this.dialog.open(DialogParametrosSaveComponent, {
        width: "40rem",
        data: dataMensajeGuardado
      });
    }

  }

  setFormatNumberDecimal(event, decimal) {
    let valor = parseFloat(event.target.value).toFixed(decimal);
    if (valor == 'NaN') {
      valor = '0';
    }
    event.target.value = this.currencyPipe.transform(valor);
    return event;
  }

  public guardarNuevaEntradaPago() {
    this.GuardarnuevaEntradaPaBack = {
      codigoNegocio: this.informacionQueLlegoServicio.codigoNegocio,
      periodo: this.datePipe.transform(this.gastosTipoNegocioForm.get('periodoActual').value, 'dd/MM/yyyy'),
      listaTipoNegocio: this.gastosTipoNegocioForm.value.camposTipoNegocio,
    }
    // validación fechas moment y date
    this.GuardarnuevaEntradaPaBack.listaTipoNegocio.forEach(listasIngresosOperacion => {
      listasIngresosOperacion.listaIngresosOperacion.forEach(ingresos => {
        ingresos.valorIngreso = this._util.cambiarFloatante(ingresos.valorIngreso);
        if (ingresos['fechaIngreso']['_d'] || isDate(ingresos['fechaIngreso'])) {
          ingresos['fechaIngreso'] = this.datePipe.transform(ingresos['fechaIngreso'], 'dd/MM/yyyy');
        }
      });
    })
    this._dataService.putGuardarListaIngresosOperacionales(this.GuardarnuevaEntradaPaBack)
      .subscribe(
        (data: any) => {
          this.activarBotonCalcularObligacion = true;
          let dataMensajeGuardado = {
            titulo: "Datos de pago guardados",
            contenido:
              "Los datos de los pagos han sido guardados exitosamente. Si se desea hacer la distribución es necesario dar click en Calcular Obligaciones. ",
            button: "ACEPTAR",
            urlimg: false
          };
          this.dialog.open(DialogParametrosSaveComponent, {
            width: "42rem",
            data: dataMensajeGuardado
          });
        },
        (err: any) => {
          let dataMensajeGuardado = {
            titulo: "Cambios NO guardados",
            contenido: "Los cambios no han podido ser guardados intente más tarde.",
            button: "ACEPTAR",
            urlimg: true
          };
          this.dialog.open(DialogParametrosSaveComponent, {
            width: "40rem",
            data: dataMensajeGuardado
          });
        }
      );
  }

  public calcularObligaciones(codigotiponegocio) {
    let objetoParaLaVistaTres = {
      periodo: this.transformaFechaParaBack(this.gastosTipoNegocioForm.get('periodoActual').value),
      codigoNegocio: this.informacionQueLlegoServicio.codigoNegocio,
      codigoTipoNegocio: codigotiponegocio.value.codigoTipoNegocio
    }
    this._storageData.setInfoNegocioListaTitularBen(objetoParaLaVistaTres);
    this._dataService
      .negocioCalcularObligacion(
        this.transformaFechaParaBack(
          this.gastosTipoNegocioForm.get("periodoActual").value
        ),
        this.informacionQueLlegoServicio.codigoNegocio,
        codigotiponegocio.value.codigoTipoNegocio
      )
      .subscribe((data: any) => {
        this._storageData.setListarConceptos(data);
        this.listaIngresoPagosIngresos = true;
        this._storageData.setRegresarPagoDistribucionVistaUno(this.listaIngresoPagosIngresos);
        this._storageData.setRegresarPagoDistribucionVistaDos(false);
        this.listaConceptos = data;
      });
  }

  public sincronizarConElBus(codigotiponegocio) {
    this._dataService
      .negocioSincronizarDatos(this.informacionQueLlegoServicio.codigoNegocio)
      .subscribe((laData: any) => {
        switch (laData["error"]) {
          case true:
            if (laData["constantesIncompletas"].length > 0) {
              let dataMensajeGuardado = {
                titulo: laData.titulo,
                contenido: laData.mensaje,
                buttonAceptar: "ACEPTAR",
                urlimg: true,
                constante: laData.constantesIncompletas,
              }
              this.dialog.open(ModalSincronizacionConstantesComponent, {
                width: "66rem",
                data: dataMensajeGuardado
              });
            } else if (laData["titularesNuevos"].length > 0) {
              let dataMensaje = {
                titulo: laData.titulo,
                contenido: laData.mensaje,
                buttonAceptar: "ACEPTAR",
                urlimg: true,
                titularNuevo: laData.titularesNuevos,
              }
              this.dialog.open(ModalSincronizacionTitularnuevoComponent, {
                width: "70rem",
                data: dataMensaje
              });
            } else if (laData["informacionTitularesIncompleta"].length > 0) {
              let dataMensaje = {
                titulo: laData.titulo,
                contenido: laData.mensaje,
                buttonAceptar: "ACEPTAR",
                urlimg: true,
                infoIncorrecta: laData.informacionTitularesIncompleta,
              }
              this.dialog.open(ModalSincronizacionInformacionFinancieraIncompletaComponent, {
                width: "92rem",
                data: dataMensaje
              });
            }
            else {
              let data;
              data = {
                titulo: laData.titulo,
                contenido: laData.mensaje,
                urlimg: false,
                buttonAceptar: "ACEPTAR"
              };
              this.dialog.open(ModalSincronizacionErrorComponent, {
                width: "46rem",
                data
              });
            }
            break;
          case false:
            if (laData["cambioDatosTitularesNegocio"].length > 0) {
              let mensaje = {
                titulo: laData.titulo,
                contenido: laData.mensaje,
                buttonAceptar: "ACEPTAR",
                urlimg: false,
                infoTitular: laData.cambioDatosTitularesNegocio,
              }
              this.dialog.open(ModalSincronizacionCambiodatotitularComponent, {
                width: "92rem",
                data: mensaje
              });
            } else {
              let data;
              data = {
                urlimg: false,
                buttonAceptar: "ACEPTAR"
              };
              const dialogoExitoso = this.dialog.open(ModalSincronizacionComponent, {
                width: "46rem",
                data
              });
              dialogoExitoso.afterClosed().subscribe(async resultado => {
                this.calcularObligaciones(codigotiponegocio);
              });
            }
            break;
        }
      },
        (err: any) => {
          let context = {
            titulo: "Solicitud no enviada",
            contenido: "La solicitud no se puede enviar",
            button: "ACEPTAR",
            urlimg: true
          };
          this.openDialog(context);
        })

  }

  openDialog(context): void {
    let data;
    data = {
      titulo: context.titulo,
      contenido: context.contenido,
      button: context.button,
      urlimg: context.urlimg
    };
    const dialogRef = this.dialog.open(DialogParametrosSaveComponent, {
      width: "40rem",
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  procesaPropagar(mensaje) {
    this.listaIngresoPagosIngresos = mensaje;
  }

  public actualizarFechaIngreso(ix, iz, value) {
    var control = (<FormArray>this.gastosTipoNegocioForm.controls["camposTipoNegocio"])
      .at(ix)
      .get("listaIngresosOperacion") as FormArray;
    let valor = value;
    control.controls[iz].patchValue({
      fechaIngreso: valor
    });
    control.push(this.crearGrupoIngreso());
    let ingresoA = (<FormArray>this.gastosTipoNegocioForm.controls["listaIngresosOperacion"]).at(
      ix
    ) as FormArray;
    ingresoA.controls["listaIngresosOperacion"].removeAt(control.controls.length - 1);
  }

  public monthSelected(params) {

    this.gastosTipoNegocioForm.get('periodoActual').setValue(params);
    this.picker.close();
    this.llamarServicioConEleccionPeriodo();
  }
  // Este método permite formatear la fecha en el formato de mar 2020
  get fullDate() {
    const dateValue = this.gastosTipoNegocioForm.controls['periodoActual'].value;
    if (!dateValue) { return ''; }
    moment.locale('es');
    this._storageData.setPagoDistribucionEnviarPeriodo(moment(dateValue).format('MMMM YYYY'));
    return moment(dateValue).format('MMMM YYYY');
  }
}
