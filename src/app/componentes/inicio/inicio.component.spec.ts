import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { InicioComponent } from "./inicio.component";
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from "@angular/core";
import {
  HttpClientTestingModule
} from "@angular/common/http/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { RouterModule } from "@angular/router";
import { IconService } from "src/app/services/icon.service";
import { DataService } from "src/app/services/data.service";
import { ReactiveFormsModule } from "@angular/forms";

describe('InicioComponent', () => {
  let component: InicioComponent;
  let fixture: ComponentFixture<InicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      declarations: [InicioComponent],
      imports: [
        ReactiveFormsModule,
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([]),
        RouterModule.forRoot([])
      ],
      providers: [IconService, DataService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
