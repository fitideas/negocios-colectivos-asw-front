import { Component, OnInit } from '@angular/core';
import { IconService } from 'src/app/services/icon.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styles: ['src/styles.css']
})
/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Marzo 2020
 * @Proposito: clase para el inicio de la aplicación despues de login.
 */
export class InicioComponent implements OnInit {

  events: string[] = [];
  opened: boolean = true;
  swInicio: boolean = true;
  swBeneficiario: boolean = false;
  swNegocio: boolean = false;
  swCertificado: boolean = false;
  swInforme: boolean = false;
  swPerfil: boolean = false;
  swParametro: boolean = false;
  swCerrarSesion: boolean = false;
  fechaUltimoAcceso: string;

  vinculados: boolean = false;
  configuracion_perfiles: boolean = false;
  parametros: boolean = false;
  informes_reportes: boolean = false;
  certificaciones: boolean = false;
  negocios: boolean = false;
  reloadVinculado: boolean = false;
  reloadNegocio: boolean = false;

  constructor(
    private iconService: IconService,
    private _authService: AuthService,
    private dataService: DataService,
    private _router: Router,
    private _storageData: StorageDataService
  ) {
    this.swInicio = true;
    this.fechaUltimoAcceso = _authService.getFechaUltimoAcceso();
  }

  ngOnInit() {
    this.iconService.registerIcons();
    this.vinculados = this._authService.getOpcionesMenu().includes('vinculados');
    this.negocios = this._authService.getOpcionesMenu().includes('negocios');
    this.certificaciones = this._authService.getOpcionesMenu().includes('certificaciones');
    this.informes_reportes = this._authService.getOpcionesMenu().includes('informes_reportes');
    this.configuracion_perfiles = this._authService.getOpcionesMenu().includes('configuracion_perfiles');
    this.parametros = this._authService.getOpcionesMenu().includes('parametros');
    this.swInicio = true;
    this._storageData.setSwInicio(this.swInicio);
  }

  logout() {
    this.dataService
      .getLogout().subscribe(
        (data: any) => {
          this._authService.logout();
          this._router.navigate(['/login']);
        },
        (err) => {
        }
      );
  }

  public irOpcionMenu(opcion) {
    this.swInicio = false;
    switch (opcion) {
      case 'inicio': {
        this.swInicio = true;
        this.swBeneficiario = false;        
        this.swNegocio = false;
        this.swCertificado = false;
        this.swInforme = false;
        this.swPerfil = false;
        this.swParametro = false;
        this._storageData.setSwInicio(this.swInicio);
        this._router.navigate(['accionfiduciaria']);
        break;
      }
      case 'beneficiario': {
        this.swBeneficiario = true;        
        this.swNegocio = false;
        this.swCertificado = false;
        this.swInforme = false;
        this.swPerfil = false;
        this.swParametro = false;
        this.swInicio = false;
        this._storageData.setSwInicio(this.swInicio);
        this._router.navigate(['accionfiduciaria/vinculados']);
        break;
      }
      case 'negocio': {
        this.swBeneficiario = false;        
        this.swNegocio = true;
        this.swCertificado = false;
        this.swInforme = false;
        this.swPerfil = false;
        this.swParametro = false;
        this.swInicio = false;
        this._storageData.setSwInicio(this.swInicio);
        this._router.navigate(['accionfiduciaria/negocios']);
        break;
      }
      case 'informe': {
        this.swBeneficiario = false;        
        this.swNegocio = false;
        this.swCertificado = false;
        this.swInforme = true;
        this.swPerfil = false;
        this.swParametro = false;
        this.swInicio = false;
        this._storageData.setSwInicio(this.swInicio);
        this._router.navigate(['accionfiduciaria/informes']);
        break;
      }
      case 'certificacion': {
        this.swBeneficiario = false;        
        this.swNegocio = false;
        this.swCertificado = true;
        this.swInforme = false;
        this.swPerfil = false;
        this.swParametro = false;
        this.swInicio = false;
        this._storageData.setSwInicio(this.swInicio);
        this._router.navigate(['accionfiduciaria/certificacion']);
        break;
      }
      case 'parametro': {
        this.swBeneficiario = false;        
        this.swNegocio = false;
        this.swCertificado = false;
        this.swInforme = false;
        this.swPerfil = false;
        this.swParametro = true;
        this.swInicio = false;
        this._storageData.setSwInicio(this.swInicio);
        this._router.navigate(['accionfiduciaria/parametros']);
        break;
      }
      case 'perfil': {
        this.swBeneficiario = false;        
        this.swNegocio = false;
        this.swCertificado = false;
        this.swInforme = false;
        this.swPerfil = true;
        this.swParametro = false;
        this.swInicio = false;
        this._storageData.setSwInicio(this.swInicio);
        this._router.navigate(['accionfiduciaria/perfil']);
        break;
      }
    }
  }
  

}
