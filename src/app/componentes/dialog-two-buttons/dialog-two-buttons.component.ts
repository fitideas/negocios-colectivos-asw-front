import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

export interface DialogData {
  titulo: string,
  contenido: string,
  button: string,
  urlimg: boolean,
  buttonCancelar: string,
  buttonContinuar:string,
}

@Component({
  selector: 'app-dialog-two-buttons',
  templateUrl: './dialog-two-buttons.component.html',
  styleUrls: ['./dialog-two-buttons.component.css']
})
/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Marzo 2020
 * @Proposito: clase para mostrar dialogo de 2 botones  .
 */
export class DialogTwoButtonsComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogTwoButtonsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {} 
}
