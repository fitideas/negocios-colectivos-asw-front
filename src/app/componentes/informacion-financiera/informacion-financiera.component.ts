import { Component, OnInit, ViewChild } from "@angular/core";
import { IconService } from "src/app/services/icon.service";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
  FormArray
} from "@angular/forms";
import { Utils } from 'src/app/core/utils';
import { AuthService } from 'src/app/services/auth.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { DataService } from 'src/app/services/data.service';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { DialogTwoButtonsComponent } from '../dialog-two-buttons/dialog-two-buttons.component';
import { DialogPorcentajeGiroComponent } from '../dialog-porcentaje-giro/dialog-porcentaje-giro.component';
import { BeneficiarioInformacionFinanciera, InformacionFinancieraListaDocumentos, BeneficiarioDetalleInfoFinanciera } from '../../modelo/beneficiario-info-financiera';
import { DialogParametrosSaveComponent } from "../dialog-parametros-save/dialog-parametros-save.component";
import { ListNegocios } from 'src/app/modelo/list-negocios';
import { ResponseNegocios } from 'src/app/modelo/response-negocios';
import { Criterio } from '../beneficiario/beneficiario.component';
import { DialogDetallenovedadJuridicaComponent } from 'src/app/dialogs/dialog-detallenovedad-juridica/dialog-detallenovedad-juridica.component';

@Component({
  selector: "app-informacion-financiera",
  templateUrl: "./informacion-financiera.component.html",
  styleUrls: ["./informacion-financiera.component.css"]
})

/**
* Autor:Lizeth Patiño
* Proposito: Generación de formulario para enrolar Beneficiarios a un titular
* Fecha: 08/04/2020
*/

export class InformacionFinancieraComponent implements OnInit {
  public formularioDinamico: FormGroup;
  public nombreNegocio: string;
  public codigoNegocio: string;
  public beneficiario: BeneficiarioInformacionFinanciera;

  public listamediosPago =
    [
      { codigo: "CHE", descripcion: "CHEQUE" },
      { codigo: "CHEGER", descripcion: "CHEQUE DE GERENCIA" },
      { codigo: "TRANS", descripcion: "TRANSFERENCIA" },
      { codigo: "TRASL", descripcion: "TRASLADO" }
    ]

  public tipoCuenta =
    [
      { codigo: "COR", descripcion: "CORRIENTE" },
      { codigo: "AHO", descripcion: "AHORROS" }

    ]
  public buscarNegociosForm: FormGroup;
  public submitted: boolean = false;
  public dataNotFound: boolean = false;
  public dataFound: boolean = false;
  public listNegocio: ListNegocios[] = new Array();
  public resultsLength = 0;
  public negocios: ResponseNegocios;
  public negocioDetalle: boolean = false;
  public consultar_vinculado: boolean = false;
  public datos_generales: boolean = false;
  public negocios_asociados: boolean = false;
  public historial_modificaciones: boolean = false;
  public elEncargo: string;
  public listaTipoDocumento: InformacionFinancieraListaDocumentos;
  public seedData: any;
  public tipoDocumento: string;
  public documento: string;
  public datosParaGiros: any;
  public totalDerechosNegocio: number;
  public numeroDerechosTitular: number;
  documentoTitular: any;
  public idBenefi: any;
  arregloBeneficiarios: any;
  arrayBeneficiarios: any[];
  medioSeleccionado: any;
  envioAlBack: BeneficiarioInformacionFinanciera;
  fide: any;
  listaDeBancos: any;
  totalDerechos: any;
  numeroDerechos: any;
  informacion_financiera: boolean;
  beneficiario_informacion_financiera_permisos_escribe: any;
  lectura: boolean;
  numeroNegativo: boolean;
  datosTitular: any;

  displayedColumns: string[] = [
    "tipoEvento",
    "estado",
    "fechaRegistro",
    "id"
  ];

  dataSource: MatTableDataSource<ListNegocios>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  selectCriterio: Criterio[] = [
    { value: "codigo", viewValue: "Código del negocio" },
    { value: "nombre", viewValue: "Nombre del negocio" }
  ];
  infoTitular: any;
  enviandoAjuridicaComponent: any;

  public listData: any[] = new Array();
  public page: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPages: number = 1; //Número total de páginas 
  public numElementos: number = 10; //Total de elemento por pagina

  constructor(
    public dialog: MatDialog,
    private storageDataService: StorageDataService,
    private _dataService: DataService,
    private iconService: IconService,
    private fb: FormBuilder,
    public _util: Utils,
    private _authService: AuthService,
  ) {
    this.formularioDinamico = this.fb.group({
      camposBeneficiario: this.fb.array([])
    });
    this.buscarNegociosForm = this.fb.group({
      selectCriterio: new FormControl("", [Validators.required]),
      argumento: new FormControl("", Validators.required)
    });
  }

  ngOnInit() {
    this.obtenerInformacionNegocio();
    this.getNovedadesJuridicas(this.datosTitular);
    this.iconService.registerIcons();
    this.loadPermisos();
    this.negocioDetalle = false;
    this.listNegocio = new Array();
    this.dataNotFound = false;
    this.dataFound = false;
  }

  private loadPermisos() {
    this.informacion_financiera = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'informacion_financiera');
    this.beneficiario_informacion_financiera_permisos_escribe = this._util.permiteEscribir(this._authService.getFuncionalidades(), 'informacion_financiera');
    if (this.beneficiario_informacion_financiera_permisos_escribe == false) {
      this.lectura = true;
    }
  }

  get f() {
    return this.buscarNegociosForm.controls;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  enviarDatos(): void {
    this.dataNotFound = false;
    this.dataFound = false;
    this.submitted = true;
    this.negocioDetalle = false;
    if (this.buscarNegociosForm.invalid) {
      return;
    }

  }

  public obtenerInformacionNegocio() {
    this.infoTitular = this.storageDataService.getInfoBeneficiario();
    this.datosTitular = {
      documento: null,
      tipodocumento: null,
      codigoEvento: null,
      codigoNegocio: null
    }
    let negocioDatos = this.storageDataService.getPDatosGenerales();
    this.nombreNegocio = negocioDatos["nombreNegocio"];
    this.codigoNegocio = negocioDatos["codigoNegocio"];
    this.tipoDocumento = negocioDatos["tipoDocumento"];
    this.documento = negocioDatos["documento"];
    this.datosTitular.documento = this.infoTitular.numeroDocumento;
    let alcanzarDatoTipoDocumento = this.infoTitular.tipoDocumento;
    this.datosTitular.tipodocumento = alcanzarDatoTipoDocumento['id'];
    this.datosTitular.codigoNegocio = this.codigoNegocio;
    this._dataService.getTipoDocumentos().subscribe((listaTipoDocumento: any) => {
      this.listaTipoDocumento = listaTipoDocumento;
    });
    this._dataService.getObtenerBancos().subscribe((listaDeBancos: any) => {
      this.listaDeBancos = listaDeBancos;
    });

    this._dataService
      .getObtenerInformacionFinanciera(
        this.codigoNegocio,
        this.tipoDocumento,
        this.documento
      )
      .subscribe((dataDeTodoElServicio: any) => {
        this.totalDerechos = dataDeTodoElServicio['totalDerechosNegocio'];
        this.numeroDerechos = dataDeTodoElServicio['numeroDerechosTitular'];
        sessionStorage.setItem(this.totalDerechos, 'derechos')
        sessionStorage.setItem(this.numeroDerechos, 'numeroDerechos')
        this.arrayBeneficiarios = [];
        dataDeTodoElServicio.beneficiarios.forEach(bene => {
          let obj: BeneficiarioDetalleInfoFinanciera = {
            beneficiario: null,
            tipoPago: null,
            tipoDocumento: null,
            numeroDocumento: null,
            porcentajeGiro: null,
            porcentajeDistribucion: null,
            idEntidadBancaria: null,
            codigoBanco: null,
            tipoCuenta: null,
            numeroCuentaBancaria: null,
            numeroEncargo: null,
            fideicomisoDestino: null,
            codigoPago: null,
            numeroReferencia: null,
            descripcionOtro: null
          };
          obj.beneficiario = bene.beneficiario;
          obj.tipoPago = bene.tipoPago['codigo'];
          obj.tipoDocumento = bene.tipoDocumento;
          obj.numeroDocumento = bene.numeroDocumento;
          obj.porcentajeGiro = bene.porcentajeGiro;          
          obj.codigoPago = bene.tipoPago['codigoPago'];
          obj.numeroEncargo = bene.tipoPago['numeroEncargo'];
          obj.numeroReferencia = bene.tipoPago['numeroReferencia'];
          obj.descripcionOtro = bene.tipoPago['descripcionOtro'];
          obj.fideicomisoDestino = bene.tipoPago['fideicomisoDestino'];
          obj.numeroCuentaBancaria = bene.tipoPago['numeroCuentaBancaria'];
          obj.idEntidadBancaria = bene.tipoPago['idEntidadBancaria'];          
          obj.codigoBanco = null;
          if (obj.idEntidadBancaria) {
            let banco = this._util.buscarObjetoId(this.listaDeBancos, bene.tipoPago['idEntidadBancaria']);
            obj.codigoBanco = banco.codigo;
          }          
          obj.tipoCuenta = bene.tipoPago['tipoCuenta'];

          this.arrayBeneficiarios.push(obj);
        });
        //este array es el que lleva la data del servicio y al final lo pone en un un array que el pinta toda la info
        this.construirElArrayDelFormulario(this.arrayBeneficiarios);
      });
  }



  public construirElArrayDelFormulario(infoServicio) {
    let informacionServicio = infoServicio;

    informacionServicio.forEach(dato => {
      const formGroup = this.crearGrupoFormulario();

      formGroup.patchValue(dato);
      this.arrayDeCamposFormulario.push(formGroup);
    });
  }

  //aqui se agrega toda una plantilla nueva para un nuevo beneficiario de 0
  public agregarCadaCampoDelFormulario() {
    this.arrayDeCamposFormulario.push(this.crearGrupoFormulario());
  }

  dialogErrorValidarEncargo() {
    let dataMensajeGuardado = {
      titulo: "Encargo no válido",
      contenido:
        "El encargo que se está intentando asociar no está activo o no pertenece al vinculado. Es necesario verificar la información.",
      button: "ACEPTAR",
      urlimg: true
    };
    const dialogRef = this.dialog.open(DialogParametrosSaveComponent, {
      width: "40rem",
      data: dataMensajeGuardado
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  removerFormularioBeneficiario(index) {
    if (this.arrayDeCamposFormulario.length !== 1) {
      this.arrayDeCamposFormulario.removeAt(index);
      let data;
      data = {
        titulo: "Cambio de porcentaje de giro",
        contenido:
          "Se esta intentando eliminar un beneficiario. Para llevar a cabo esta acción es necesario ajustar los porcentajes del(los) otro(s) beneficiario(s) ¿Desea continuar?",
        buttonContinuar: "Continuar",
        buttonCancelar: "Cancelar",
        urlimg: false,
        //esto es la informacioón que se le añadiria a el arreglo de beneficiarios para enviar
        infoParaModalGiros: this.formularioDinamico.value.camposBeneficiario
      };

      const dialogRef = this.dialog.open(DialogTwoButtonsComponent, {
        width: "52rem",
        data
      });

      dialogRef.afterClosed().subscribe(async resultado => {

        switch (resultado) {
          case "Continuar":
            const dialog = this.dialog.open(DialogPorcentajeGiroComponent, {
              width: "84rem",
              data
            });
            dialog.afterClosed().subscribe(result => {

              data.infoParaModalGiros.forEach(element => {
                result.forEach(item => {
                  if (
                    element.numeroDocumento == item.numeroDocumento &&
                    element.tipoDocumento == item.tipoDocumento
                  ) {
                    element.porcentajeGiro = parseFloat(item.porcentajeGiro);                    
                  }
                });
              });

              this.arrayDeCamposFormulario.clear();

              this.construirElArrayDelFormulario(data.infoParaModalGiros);
            });
            break;
          case "Cancelar":

        }
      });
    }
    else {
      let dataMensajeGuardado;

      dataMensajeGuardado = {
        titulo: "Beneficiario NO Eliminado",
        contenido: "El beneficiario no puede ser eliminado.",
        button: "ACEPTAR",
        urlimg: true
      };
      this.dialog.open(DialogParametrosSaveComponent, {
        width: "40rem",
        data: dataMensajeGuardado
      });

    }
  }

  public getFormularioControl() {
    return this.fb.control(null);
  }

  public GuardarInformacionFinancieraBeneficiarios() {
    this.envioAlBack = {
      codigoNegocio: null,
      tipoDocumentoTitular: null,
      documento: null,
      totalDerechosNegocio: this.totalDerechos,
      numeroDerechosTitular: this.numeroDerechos,
      beneficiarios: []
    }
    this.envioAlBack['codigoNegocio'] = this.codigoNegocio;
    this.envioAlBack['tipoDocumentoTitular'] = this.tipoDocumento;
    this.envioAlBack['documento'] = this.documento;
    this.envioAlBack['beneficiarios'] = this.formularioDinamico.value.camposBeneficiario;

    this._dataService
      .putGuardarInformacionFinanciera(this.envioAlBack)
      .subscribe(
        (data: any) => {

          let dataMensajeGuardado = {
            titulo: "Cambios guardados",
            contenido:
              "Los cambios de la información financiera han sido guardados exitosamente.",
            button: "ACEPTAR",
            urlimg: false
          };
          this.dialog.open(DialogParametrosSaveComponent, {
            width: "40rem",
            data: dataMensajeGuardado
          });
        },
        (err: any) => {
          let dataMensajeGuardado = {
            titulo: "Cambios NO guardados",
            contenido: "Los cambios no han podido ser guardados intente más tarde.",
            button: "ACEPTAR",
            urlimg: true
          };
          this.dialog.open(DialogParametrosSaveComponent, {
            width: "40rem",
            data: dataMensajeGuardado
          });
        }

      );
  }

  public get arrayDeCamposFormulario() {
    return (<FormArray>this.formularioDinamico.get('camposBeneficiario'));
  }



  public crearGrupoFormulario() {
    return this.fb.group({
    beneficiario: ['',[Validators.required]],
    tipoDocumento: ['',Validators.required],
    numeroDocumento:['',[
      Validators.maxLength(30),
      Validators.required]],
    porcentajeGiro:['',Validators.compose([Validators.required, Validators.min(0),Validators.max(100)])],
    tipoPago: ['', Validators.required],
    numeroEncargo: [''],
    numeroReferencia: [''],
    descripcionOtro: [''],
    fideicomisoDestino:[''],
    numeroCuentaBancaria:[''],
    idEntidadBancaria:[''],
    codigoBanco:[''],
    tipoCuenta:[''],
  });

  }


  public tomarMedioSeleccionado(tomarValorSeleccionado) {
    this.medioSeleccionado = tomarValorSeleccionado.value;
  }

  public validarMostrar(variable: string, index: any, estado: string) {
    if (variable === estado) {
      switch (estado) {
        case "OTR":
          let busquedaControl1: any = this.formularioDinamico.controls.camposBeneficiario;
          busquedaControl1.controls[index].controls.numeroReferencia.enable();
          busquedaControl1.controls[index].controls.numeroReferencia.setValidators([Validators.required, Validators.min(0)]);
          busquedaControl1.controls[index].controls.descripcionOtro.enable();
          busquedaControl1.controls[index].controls.descripcionOtro.setValidators([Validators.required, Validators.pattern('[a-zA-Z ]*')]);
          busquedaControl1.controls[index].controls.numeroEncargo.setValidators([]);
          busquedaControl1.controls[index].controls.numeroEncargo.disable();
          busquedaControl1.controls[index].controls.fideicomisoDestino.setValidators([]);
          busquedaControl1.controls[index].controls.fideicomisoDestino.disable();
          busquedaControl1.controls[index].controls.numeroCuentaBancaria.setValidators([]);
          busquedaControl1.controls[index].controls.numeroCuentaBancaria.disable();
          busquedaControl1.controls[index].controls.idEntidadBancaria.setValidators([]);
          busquedaControl1.controls[index].controls.idEntidadBancaria.disable();
          busquedaControl1.controls[index].controls.tipoCuenta.setValidators([]);
          busquedaControl1.controls[index].controls.tipoCuenta.disable();

          break;
        case "TRASL":
          let busquedaControl2: any = this.formularioDinamico.controls.camposBeneficiario
          busquedaControl2.controls[index].controls.numeroEncargo.enable();
          busquedaControl2.controls[index].controls.numeroEncargo.setValidators([Validators.required, Validators.min(0)]);
          busquedaControl2.controls[index].controls.fideicomisoDestino.enable();
          busquedaControl2.controls[index].controls.fideicomisoDestino.setValidators([Validators.required]);
          busquedaControl2.controls[index].controls.numeroReferencia.setValidators([]);
          busquedaControl2.controls[index].controls.numeroReferencia.disable();
          busquedaControl2.controls[index].controls.descripcionOtro.setValidators([]);
          busquedaControl2.controls[index].controls.descripcionOtro.disable();
          busquedaControl2.controls[index].controls.numeroCuentaBancaria.setValidators([]);
          busquedaControl2.controls[index].controls.numeroCuentaBancaria.disable();
          busquedaControl2.controls[index].controls.idEntidadBancaria.setValidators([]);
          busquedaControl2.controls[index].controls.idEntidadBancaria.disable();
          busquedaControl2.controls[index].controls.tipoCuenta.setValidators([]);
          busquedaControl2.controls[index].controls.tipoCuenta.disable();
          break;
        case "TRANS":
          let busquedaControl3: any = this.formularioDinamico.controls.camposBeneficiario
          busquedaControl3.controls[index].controls.numeroCuentaBancaria.enable();
          busquedaControl3.controls[index].controls.numeroCuentaBancaria.setValidators([Validators.required, Validators.min(0)]);
          busquedaControl3.controls[index].controls.idEntidadBancaria.enable();
          busquedaControl3.controls[index].controls.idEntidadBancaria.setValidators([Validators.required]);
          busquedaControl3.controls[index].controls.tipoCuenta.enable();
          busquedaControl3.controls[index].controls.tipoCuenta.setValidators([Validators.required]);
          busquedaControl3.controls[index].controls.numeroReferencia.setValidators([]);
          busquedaControl3.controls[index].controls.numeroReferencia.disable();
          busquedaControl3.controls[index].controls.descripcionOtro.setValidators([]);
          busquedaControl3.controls[index].controls.descripcionOtro.disable();
          busquedaControl3.controls[index].controls.fideicomisoDestino.setValidators([]);
          busquedaControl3.controls[index].controls.fideicomisoDestino.disable();
          busquedaControl3.controls[index].controls.numeroEncargo.setValidators([]);
          busquedaControl3.controls[index].controls.numeroEncargo.disable();
          break;
      }
    }
    return variable === estado;
  }

  public getFormGroup(index) {
    return (<FormGroup>this.arrayDeCamposFormulario.at(index));
  }

  public obtenerFideicomiso(item, i) {
    if (parseInt(item.value.numeroEncargo) >= 0) {
      let request = {
        numeroencargo: item.value.numeroEncargo,
        documento: item.value.numeroDocumento
      }
      this._dataService.getValidarEncargo(request).subscribe(

        (data: any) => {
          if (data['activo'] === true) {
            this.fide = data['fideicomiso']
            this.actualizarValorFideicomiso(i);
          } else {
            const myForm = (<FormArray>this.formularioDinamico.get('camposBeneficiario')).at(i);
            let currentVal = null;
            myForm.patchValue({
              fideicomisoDestino: currentVal
            });
            this.arrayDeCamposFormulario[i] = currentVal;

            if (myForm['fideicomisoDestino'] != this.fide) {
              currentVal = null;
              myForm.patchValue({
                fideicomisoDestino: currentVal
              });
              this.arrayDeCamposFormulario[i] = currentVal;
            }
           this.dialogErrorValidarEncargo();
          }
        },
        (err: any) => {
          this.dialogErrorValidarEncargo();
        }

      );
    }
  }

  public validarCHE(index, valor) {

    if (valor == 'CHE' || valor == 'CHEGER') {
      let busquedaControl4: any = this.formularioDinamico.controls.camposBeneficiario
      busquedaControl4.controls[index].controls.numeroEncargo.setValidators([]);
      busquedaControl4.controls[index].controls.numeroEncargo.disable();
      busquedaControl4.controls[index].controls.fideicomisoDestino.setValidators([]);
      busquedaControl4.controls[index].controls.fideicomisoDestino.disable([]);
      busquedaControl4.controls[index].controls.numeroReferencia.setValidators([]);
      busquedaControl4.controls[index].controls.numeroReferencia.disable();
      busquedaControl4.controls[index].controls.descripcionOtro.setValidators([]);
      busquedaControl4.controls[index].controls.descripcionOtro.disable();
      busquedaControl4.controls[index].controls.numeroCuentaBancaria.setValidators([]);
      busquedaControl4.controls[index].controls.numeroCuentaBancaria.disable();
      busquedaControl4.controls[index].controls.idEntidadBancaria.setValidators([]);
      busquedaControl4.controls[index].controls.idEntidadBancaria.disable();
      busquedaControl4.controls[index].controls.tipoCuenta.setValidators([]);
      busquedaControl4.controls[index].controls.tipoCuenta.disable();
    }

  }


  public actualizarValorFideicomiso(index: number) {
    const myForm = (<FormArray>this.formularioDinamico.get('camposBeneficiario')).at(index);

    if (myForm['fideicomisoDestino'] != this.fide) {
      let currentVal = this.fide;
      myForm.patchValue({
        fideicomisoDestino: currentVal
      });
      this.arrayDeCamposFormulario[index] = currentVal;
    }
  }

  public actualizarCodigoBanco(index: number, id) {
    let banco = this._util.buscarObjetoId(this.listaDeBancos, id);
    const myForm = (<FormArray>this.formularioDinamico.get('camposBeneficiario')).at(index);
    if (myForm['codigoBanco'] != banco.codigo) {
      myForm.patchValue({
        codigoBanco: banco.codigo
      });
      this.arrayDeCamposFormulario[index] = banco.codigo;
    }
  }

  public actualizarCodigoTipoCuenta(index: number, valor) {
    const myForm = (<FormArray>this.formularioDinamico.get('camposBeneficiario')).at(index);

    if (myForm['tipoCuenta'] != valor) {
      let currentVal = valor;

      myForm.patchValue({
        tipoCuenta: currentVal
      });
      this.arrayDeCamposFormulario[index] = currentVal;

    }
  }

  public abrirModalGiros(index, valor) {
    sessionStorage.setItem('totalDerechos', this.totalDerechos);
    sessionStorage.setItem('numeroDerechos', this.numeroDerechos);
    let data;
    data = {
      titulo: "Cambio de porcentaje de giro",
      contenido:
        "Se esta intentando dar un porcentaje de giro a un nuevo beneficiario. Para llevar a cabo esta acción es necesario ajustar los porcentajes de los otros beneficiarios ¿Desea continuar?",
      buttonContinuar: "Continuar",
      buttonCancelar: "Cancelar",
      urlimg: false,
      //esto es la informacioón que se le añadiria a el arreglo de beneficiarios para enviar
      infoParaModalGiros: this.formularioDinamico.value.camposBeneficiario
    };

    const dialogRef = this.dialog.open(DialogTwoButtonsComponent, {
      width: "52rem",
      data
    });

    dialogRef.afterClosed().subscribe(async resultado => {

      switch (resultado) {
        case "Continuar":
          const dialog = this.dialog.open(DialogPorcentajeGiroComponent, {
            width: "84rem",
            data
          });
          dialog.afterClosed().subscribe(result => {

            data.infoParaModalGiros.forEach(element => {
              result.forEach(item => {
                if (
                  element.numeroDocumento == item.numeroDocumento &&
                  element.tipoDocumento == item.tipoDocumento
                ) {
                  element.porcentajeGiro = parseFloat(item.porcentajeGiro);                  
                }
              });
            });

            this.arrayDeCamposFormulario.clear();

            this.construirElArrayDelFormulario(data.infoParaModalGiros);
          });
          break;
        case "Cancelar":
      }
    });
  }

  public getNovedadesJuridicas(request: any) {
    this._dataService.getNovedadesJuridicas(request)
      .subscribe((response: any) => {
        if (!response) {
          this.dataNotFound = true;
          this.dataFound = false;
          this.negocioDetalle = false;
          return;
        } else {
          this.dataFound = true;
          this.dataNotFound = false;
          this.negocioDetalle = false;
        }
        setTimeout(() => {
          this.listData = response;
          let pagina = 1;
          this.mostraPaginaDataLocal(pagina);          
        });
      },
        (err) => {
          this.dataNotFound = true;
        }
      );
  }


  public getDetalleNovedadJuridica(codigoEvento) {
    this.datosTitular.codigoEvento = codigoEvento;
    this._dataService.getEventoNovedadJuridica(this.datosTitular)
      .subscribe((dati: any) => {
        let dataMensajeGuardado;
        dataMensajeGuardado = {
          urlimg: false,
          buttonAceptar: "ACEPTAR",
          dataDeAlla: dati
        };
        this.dialog.open(DialogDetallenovedadJuridicaComponent, {
          width: "94rem",
          data: dataMensajeGuardado
        });
      },
        (err) => {
        }
      );
  }

  mostraPaginaDataLocal(pagina) {
    let registrosPagina: any[] = new Array();
    this.page = pagina;
    this.numElementos = 10;
    this.resultsLength = this.listData.length;
    this.totalPages = this.resultsLength / this.numElementos;
    let totalPagina = this.resultsLength % this.numElementos;
    if (totalPagina > 1) {
      this.totalPages = Number(parseInt(String(this.totalPages))) + 1;
    }
    if (this.totalPages < 1) {
      this.totalPages = 1;
    }   

    let inicioRegistros = ((pagina - 1) * this.numElementos);
    let ultimoRegistros = pagina * this.numElementos;
    for (let i = inicioRegistros; ((i < this.listData.length) && (i < ultimoRegistros)); i++) {
      registrosPagina.push(this.listData[i]);
    }

    this.dataSource = new MatTableDataSource(registrosPagina);         
    this.dataSource.sort = this.sort;
  }

  goToPage(page: number) {
    this.mostraPaginaDataLocal(page);
  }
}
