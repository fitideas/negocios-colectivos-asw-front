import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiarioHistorialModificacionesComponent } from './beneficiario-historial-modificaciones.component';

describe('BeneficiarioHistorialModificacionesComponent', () => {
  let component: BeneficiarioHistorialModificacionesComponent;
  let fixture: ComponentFixture<BeneficiarioHistorialModificacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiarioHistorialModificacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiarioHistorialModificacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
