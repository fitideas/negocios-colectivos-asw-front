import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { MatDialog, MatTableDataSource, MatSort } from '@angular/material';
import { BeneficiarioDetalle, Documento } from 'src/app/modelo/beneficiario-detalle';
import { PeticionHistorialCambios } from 'src/app/modelo/request-historicos';

export interface CriterioList {
  value: string;
  viewValue: string;
  active: boolean;
}

export interface ListCambios {
  fechaCambio?: string;
  usuario?: string;
  direccionIp?: string;
  negocioVinculado?: string;
  codigoNegocioVinculado?:string;
  operacion?:string;
  campo?: string;
  datoNuevo?: string;
  datoAnterior?: string;
}

export interface ListCesionCambios {
  codigoNegocio?: string;
  nombreNegocio?: string;
  fechaCambio?: string;
  porcentajeActualCedente?: string;
  porcentajeNuevoCedente?: string;
  diferenciaPorcentaje?: string;
  nombrePropietarioNuevo?: string;
  nombrePropietarioAnterior?:string;
}

@Component({
  selector: 'app-beneficiario-historial-modificaciones',
  templateUrl: './beneficiario-historial-modificaciones.component.html',
  styleUrls: ['./beneficiario-historial-modificaciones.component.scss']
})

/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Marzo 2020
 * @Proposito: clase para mostrar el historial de modificaciones solicitadas por el usuario 
 */

export class BeneficiarioHistorialModificacionesComponent implements OnInit {

  public buscarModificacionesTitularDynamicForm: FormGroup;
  
  get filtrosFormArray() {
    return <FormArray>this.buscarModificacionesTitularDynamicForm.get("filtros");
  }
  public submitted: boolean = false;
  public dataNotFound: boolean = null;
  public dataFound: boolean = null;
  public dataNotFoundChanges: boolean = null;
  public dataNotFoundCesiones: boolean = null;
  public dataFoundChanges: boolean = null;
  public dataFoundCesiones: boolean = null;
  
  public arrayFiltro: any[] = [];
  public nuevoItem: boolean = false;
  public negocioAsociado: any;

  public listFechaRango: any[] = [];
  public listUsuario: any[] = [];
  public listNegocioAsociado: any[] = [];
  public listNegocioPreliminar: any[] = [];

  public isFecha: boolean = false;
  public isUsuario: boolean = false;
  public isNegocioAsociado: boolean = false;
  public isCampo: boolean = false;
  public isTodo: boolean = false;
  public swAddFiltros = true;

  public infoBeneficiario: BeneficiarioDetalle;
  public documento:Documento;

  listSelectCriterio: CriterioList[] = [
    { value: "fecha", viewValue: "Fecha del cambio", active: true },
    { value: "usuario", viewValue: "Usuario que registra", active: true },
    { value: "negocio", viewValue: "Negocio", active: true }
  ];

  listSelectCriterioNuevo: CriterioList[] = [];

  displayedColumns: string[] = [
    'codigoNegocioVinculado',
    'negocioVinculado',
    'fechaCambio',
    'usuario',
    'direccionIp',
    'operacion',
    'campo',
    'datoNuevo',
    'datoAnterior'
  ];

  displayedColumnsChanges: string[] = [
    'fechaCambio',
    'usuario',
    'campo',
    'datoNuevo',
    'datoAnterior'
  ];

  displayedColumnsCesion: string[] = [
    'codigoNegocioVinculado',
    'negocioVinculado',
    'fechaCambio',
    'porcentajeActualCedente',
    'porcentajeNuevoCedente',
    'diferenciaPorcentaje',
    'propietarioNuevo',
    'propietarioAnterior'
  ];
  
  @ViewChild(MatSort, { static: false }) sortCambios: MatSort;
  @ViewChild(MatSort, { static: false }) sortSolicitudCambios: MatSort;
  @ViewChild(MatSort, { static: false }) sortCesion: MatSort;
  
  dataSource: MatTableDataSource<ListCambios>;
  dataSourceChanges: MatTableDataSource<ListCambios>;
  dataSourceCesiones: MatTableDataSource<ListCesionCambios>;
  
  public resultsLengthCambios: number = 0;
  public pageCambios: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPagesCambios: number = 1; //Número total de páginas 
  public numElementosCambios: number = 10; //Total de elemento por pagina  
  public listCambios: ListCambios[] = new Array();
  
  public resultsLengthSolicitudCambios: number = 0;
  public pageSolicitudCambios: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPagesSolicitudCambios: number = 1; //Número total de páginas 
  public numElementosSolicitudCambios: number = 10; //Total de elemento por pagina  
  public listSolicitudCambios: ListCambios[] = new Array();

  public resultsLengthCesionCambios: number = 0;
  public pageCesionCambios: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPagesCesionCambios: number = 1; //Número total de páginas 
  public numElementosCesionCambios: number = 10; //Total de elemento por pagina  
  public listCesionCambios: ListCesionCambios[] = new Array();

  public panelOpenStateCambios = false;
  public panelOpenStateSolicitudCambios = false;
  public panelOpenStateCesiones = false;
  public guardarBotton: boolean = false;

  constructor(
    public fb: FormBuilder,
    private _dataService: DataService,
    private _storageData: StorageDataService,
    public dialog: MatDialog
  ) {

  }

  ngOnInit() {
    this.loadInfoNegocio();
    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarModificacionesTitularDynamicForm = this.fb.group({
      filtros:this.fb.array([])
    });
    this.addToFiltrosFormArray();
    document.getElementById("dataFound").style.display = 'none';
    document.getElementById("dataNotFound").style.display = 'none';

    document.getElementById("dataFoundChanges").style.display = 'none';
    document.getElementById("dataNotFoundChanges").style.display = 'none';
    
    document.getElementById("dataFoundCesiones").style.display = 'none';
    document.getElementById("dataNotFoundCesiones").style.display = 'none';
  } 

  checkFiltros() {
    if (this.filtrosFormArray.length == 1) {
      return false
    }
    return true;
  }

  removeFromFiltrosFormArray(index, itemFiltro) {
    this.filtrosFormArray.removeAt(index);

    this.arrayFiltro = this.arrayFiltro.filter(item => {
      return (item !== itemFiltro.value.selectCriterio);
    });

    if (this.filtrosFormArray.length < 3) {
      this.swAddFiltros = true;
    }

    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == itemFiltro.value.selectCriterio) {
        element.active = true;
      }
    });
  }

  createGroup() {
    return this.fb.group({
      selectCriterio: new FormControl("", []),
      argumento: new FormControl("", [])
    });
  }

  addToFiltrosFormArray() {
    this.filtrosFormArray.push(this.createGroup());
    if (this.filtrosFormArray.length == 3) {
      this.swAddFiltros = false;
    }
    this.nuevoItem = true;
  }

  buscarListaXCriterioSeleccionado(eventSeleccionado, ind) {
    this.isFecha = false;
    this.isUsuario = false;
    this.isNegocioAsociado = false;
    this.isCampo = false;
    this.isTodo = true;
    if (eventSeleccionado.value == 'usuario') {
      this.arrayFiltro[ind] = 'usuario';
      this.isUsuario = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }
    if (eventSeleccionado.value == 'fecha') {
      this.arrayFiltro[ind] = 'fecha';
      this.isFecha = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }
    if (eventSeleccionado.value == 'negocio') {
      this.arrayFiltro[ind] = 'negocio';
      this.isNegocioAsociado = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }
    
    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == eventSeleccionado.value) {
        element.active = false;
      }
      if (!this.isTodo) {
        if (element.value == 'todo') {
          element.active = false;
        }
      }
    });
  }

  limpiarformulario() {
    this.guardarBotton = false;
    this.arrayFiltro = [];
    this.filtrosFormArray.clear();
    this.filtrosFormArray.reset([]);
    this.dataFound = false;
    this.dataNotFound = false;
    this.dataFoundCesiones = false;
    this.dataNotFoundCesiones = false;
    this.dataFoundChanges = false;
    this.dataNotFoundChanges = false;
    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarModificacionesTitularDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();
    this.resultsLengthCambios = 0;
    this.resultsLengthSolicitudCambios = 0;
    this.resultsLengthCesionCambios = 0;
    this.swAddFiltros = true;
    this.listSelectCriterioNuevo.forEach(element => {
      element.active = true;
    });
    this.listUsuario = [];
    this.listNegocioAsociado = [];
    this.listFechaRango = [];
    document.getElementById("dataFound").style.display = 'none';
    document.getElementById("dataNotFound").style.display = 'none';

    document.getElementById("dataFoundChanges").style.display = 'none';
    document.getElementById("dataNotFoundChanges").style.display = 'none';
    
    document.getElementById("dataFoundCesiones").style.display = 'none';
    document.getElementById("dataNotFoundCesiones").style.display = 'none';
  }

  getDateRange(range) {
    this.listFechaRango = range;
    if (this.listFechaRango.length > 0) {
      this.guardarBotton = true;
    }
  }

  getUsuarios(usuario) {
    this.listUsuario = usuario;
    if (this.listUsuario.length > 0) {
      this.guardarBotton = true;
    }    
  }

  getNegocioAsociados(negocioAsociado) {
    this.listNegocioAsociado = negocioAsociado;
    if (this.listNegocioAsociado.length > 0) {
      this.guardarBotton = true;
    }   
  }

  private loadInfoNegocio() {
    this.infoBeneficiario = this._storageData.getInfoBeneficiario();
    this.documento = {
      documento: this.infoBeneficiario.numeroDocumento,
      tipodocumento: this.infoBeneficiario.tipoDocumento.id
    }

    this._dataService
      .getBeneficiarioObtenerNegociosListRequest(this.documento)
      .subscribe((data: any) => {
        if (!data) {
          return;
        }
        setTimeout(() => {
          data.negocios.forEach(element => {
            this.negocioAsociado = {
              codigo: element.codigo,
              descripcion: element.nombre
            }
            this.listNegocioPreliminar.push(this.negocioAsociado.codigo);
          });
        });
      },
        (err) => {
        }
      );
  }  

  enviarDatos(listar): void {
    this.dataNotFound = false;
    this.dataFound = false;
    this.submitted = true;
    let request: PeticionHistorialCambios;
    if(this.listNegocioAsociado.length == 0){
      this.listNegocioAsociado = this.listNegocioPreliminar;
    }
    request = {
      rangoFecha: this.listFechaRango,
      negocios: this.listNegocioAsociado,
      solicitadoPor: this.listUsuario,
      tipoNegocio:[],
      numeroDocumentoVinculado:this.documento.documento,
      tipoDocumentoVinculado:this.documento.tipodocumento  
    }
    if (!listar) {
      return;
    }
    let requestPaginado: any = request;
    requestPaginado.pagina = 1;
    requestPaginado.elementos = 10;
    switch (listar) {
      case 1: {
        this.obtenerBeneficiarioHistorialCambios(requestPaginado);
        break; 
      } 
      case 2:  { 
        this.obtenerBeneficiarioHistorialSolicitudCambios(requestPaginado);
        break; 
      }    
      case 3: {     
        this.obtenerBeneficiarioHistorialCesiones(requestPaginado);
        break; 
      }   
    }
  }

  obtenerBeneficiarioHistorialCambios(request: any) {
    this.dataSource = null;
    let cambio: ListCambios;
    this.listCambios = new Array();
    if (!this.panelOpenStateCambios) {
      this.listCambios = [];
      request = 0;
      this.limpiarformulario();
      this.guardarBotton = false;
    }
    if (this.panelOpenStateCambios && request != 0) {
    this._dataService
      .listadoBeneficiarioHistorialCambios(request)
      .subscribe((data: any) => {
        if (!data || data.totalRegistro == 0) {
          this.dataNotFound = true;
          this.dataFound = false;
          document.getElementById("dataFound").style.display = 'none';
          document.getElementById("dataNotFound").style.display = 'inline';
          return;
        }

        this.dataFound = true;
        this.dataNotFound = false;
        document.getElementById("dataFound").style.display = 'inline';
        document.getElementById("dataNotFound").style.display = 'none';

        data.listaCambios.forEach(element => {
          cambio = {
            fechaCambio: element.fechaCambio,
            negocioVinculado: element.nombreNegocio,
            campo: element.campoCambio,
            datoNuevo: element.datoNuevo,
            datoAnterior: element.datoAnterior,
            usuario: element.registradoPor,
            direccionIp: element.direccionIp,
            codigoNegocioVinculado: element.codigoNegocio,
            operacion: element.operacion
          }
          this.listCambios.push(cambio);
        });
        let pagina = 1;
        this.mostraPaginaDataLocalCambios(pagina);        
      },
        (err) => {
          this.dataNotFound = true;
          this.dataFound = true;
          document.getElementById("dataFound").style.display = 'none';
          document.getElementById("dataNotFound").style.display = 'inline';
        }
      );
    }
  }

  obtenerBeneficiarioHistorialSolicitudCambios(request: any) {
    this.dataSourceChanges = null;    
    let cambio: ListCambios;
    this.listSolicitudCambios = new Array();
    if (!this.panelOpenStateSolicitudCambios) {
      this.listSolicitudCambios = [];
      request = 0;
      this.limpiarformulario();
      this.guardarBotton = false;
    }
    if (this.panelOpenStateSolicitudCambios && request != 0) {
    this._dataService
      .listadoBeneficiarioHistorialSolicitudCambios(request)
      .subscribe((data: any) => {
        if (!data || data.totalRegistro == 0) {
          this.dataNotFoundChanges = true;
          this.dataFoundChanges = false;
          document.getElementById("dataFoundChanges").style.display = 'none';
          document.getElementById("dataNotFoundChanges").style.display = 'inline';
          return;
        }
        this.dataFoundChanges = true;
        this.dataNotFoundChanges = false;
        document.getElementById("dataFoundChanges").style.display = 'inline';
        document.getElementById("dataNotFoundChanges").style.display = 'none';

        data.listaCambios.forEach(element => {
          cambio = {
            fechaCambio: element.fechaCambio,
            campo: element.campoCambio,
            datoNuevo: element.datoNuevo,
            datoAnterior: element.datoAnterior,
            usuario: element.registradoPor
          }
          this.listSolicitudCambios.push(cambio);
        });
        let pagina = 1;
        this.mostraPaginaDataLocalSolicitudCambios(pagina);

       },
        (err) => {
          this.dataNotFoundChanges = true;
          this.dataFoundChanges = true;
          document.getElementById("dataFoundChanges").style.display = 'none';
          document.getElementById("dataNotFoundChanges").style.display = 'inline';
        }
      );
    }
  }

  obtenerBeneficiarioHistorialCesiones(request: any) {
    this.dataSourceCesiones = null;
    this.listCesionCambios = new Array();
    let cambio: ListCesionCambios;
    if (!this.panelOpenStateCesiones) {
      this.listCesionCambios = new Array();
      request = 0;
      this.limpiarformulario();
      this.guardarBotton = false;
    }
    if (this.panelOpenStateCesiones && request != 0) {
    this._dataService
      .listadoBeneficiarioHistorialCesiones(request)
      .subscribe((data: any) => {
        if (!data || data.totalRegistros == 0) {
          this.dataNotFoundCesiones = true;
          this.dataFoundCesiones = false;
          document.getElementById("dataFoundCesiones").style.display = 'none';
          document.getElementById("dataNotFoundCesiones").style.display = 'inline';
          return;
        }
        this.dataFoundCesiones = true;
        this.dataNotFoundCesiones = false;
        document.getElementById("dataFoundCesiones").style.display = 'inline';
        document.getElementById("dataNotFoundCesiones").style.display = 'none';

        data.listaCambios.forEach(element => {
          cambio = {
            codigoNegocio: element.codigoNegocio,
            diferenciaPorcentaje: element.diferenciaPorcentaje,
            fechaCambio: element.fechaCambio,
            nombreNegocio: element.nombreNegocio,
            nombrePropietarioAnterior: element.nombrePropietarioAnterior,
            nombrePropietarioNuevo: element.nombrePropietarioNuevo,
            porcentajeActualCedente: element.porcentajeActualCedente,
            porcentajeNuevoCedente: element.porcentajeNuevoCedente
          }
          this.listCesionCambios.push(cambio);
        });
        let pagina = 1;
        this.mostraPaginaDataLocalCesionCambios(pagina);
      },
        (err) => {
          this.dataNotFound = true;
          this.dataFound = true;
          document.getElementById("dataFoundCesiones").style.display = 'none';
          document.getElementById("dataNotFoundCesiones").style.display = 'inline';
        }
      );
    }
  }

  mostraPaginaDataLocalCambios(pagina) {
    let registrosPagina: any[] = new Array();
    this.pageCambios = pagina;
    this.numElementosCambios = 10;
    this.resultsLengthCambios = this.listCambios.length;
    this.totalPagesCambios = this.resultsLengthCambios / this.numElementosCambios;
    let totalPagina = this.resultsLengthCambios % this.numElementosCambios;
    if (totalPagina > 1) {
      this.totalPagesCambios = Number(parseInt(String(this.totalPagesCambios))) + 1;
    }
    if (this.totalPagesCambios < 1) {
      this.totalPagesCambios = 1;
    }   

    let inicioRegistros = ((pagina - 1) * this.numElementosCambios);
    let ultimoRegistros = pagina * this.numElementosCambios;
    for (let i = inicioRegistros; ((i < this.listCambios.length) && (i < ultimoRegistros)); i++) {
      registrosPagina.push(this.listCambios[i]);
    }
    // this.dataSource.sort = this.sortCambios;
    this.dataSource = new MatTableDataSource(registrosPagina);
  }

  goToPageCambios(page: number) {
    this.mostraPaginaDataLocalCambios(page);
  } 

  mostraPaginaDataLocalSolicitudCambios(pagina) {
    let registrosPagina: any[] = new Array();
    this.pageSolicitudCambios = pagina;
    this.numElementosSolicitudCambios = 10;
    this.resultsLengthSolicitudCambios = this.listSolicitudCambios.length;
    this.totalPagesSolicitudCambios = this.resultsLengthSolicitudCambios / this.numElementosSolicitudCambios;
    let totalPagina = this.resultsLengthSolicitudCambios % this.numElementosSolicitudCambios;
    if (totalPagina > 1) {
      this.totalPagesSolicitudCambios = Number(parseInt(String(this.totalPagesSolicitudCambios))) + 1;
    }
    if (this.totalPagesSolicitudCambios < 1) {
      this.totalPagesSolicitudCambios = 1;
    }   

    let inicioRegistros = ((pagina - 1) * this.numElementosSolicitudCambios);
    let ultimoRegistros = pagina * this.numElementosSolicitudCambios;
    for (let i = inicioRegistros; ((i < this.listSolicitudCambios.length) && (i < ultimoRegistros)); i++) {
      registrosPagina.push(this.listSolicitudCambios[i]);
    }
    // this.dataSource.sort = this.sortCambios;
    this.dataSourceChanges = new MatTableDataSource(registrosPagina);
  }

  goToPageSolicitudCambios(page: number) {
    this.mostraPaginaDataLocalSolicitudCambios(page);
  } 

  mostraPaginaDataLocalCesionCambios(pagina) {
    let registrosPagina: any[] = new Array();
    this.pageCesionCambios = pagina;
    this.numElementosCesionCambios = 10;
    this.resultsLengthCesionCambios = this.listCesionCambios.length;
    this.totalPagesCesionCambios = this.resultsLengthCesionCambios / this.numElementosCesionCambios;
    let totalPagina = this.resultsLengthCesionCambios % this.numElementosCesionCambios;
    if (totalPagina > 1) {
      this.totalPagesCesionCambios = Number(parseInt(String(this.totalPagesCesionCambios))) + 1;
    }
    if (this.totalPagesCesionCambios < 1) {
      this.totalPagesCesionCambios = 1;
    }   

    let inicioRegistros = ((pagina - 1) * this.numElementosCesionCambios);
    let ultimoRegistros = pagina * this.numElementosCesionCambios;
    for (let i = inicioRegistros; ((i < this.listCesionCambios.length) && (i < ultimoRegistros)); i++) {
      registrosPagina.push(this.listCesionCambios[i]);
    }
    // this.dataSourceCesiones.sort = this.sortCesion;
    this.dataSourceCesiones = new MatTableDataSource(registrosPagina);
  }

  goToPageCesionCambios(page: number) {
    this.mostraPaginaDataLocalCesionCambios(page);
  } 

}
