import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Utils } from 'src/app/core/utils';
import { AuthService } from 'src/app/services/auth.service';
import { ModalSincronizacionErrorComponent } from 'src/app/dialogs/modal-sincronizacion-error/modal-sincronizacion-error.component';


export interface CriterioList {
  value: string;
  viewValue: string;
  active: boolean;
}

@Component({
  selector: 'app-reportes-beneficiarios',
  templateUrl: './reportes-beneficiarios.component.html',
  styleUrls: ['./reportes-beneficiarios.component.scss']
})
export class ReportesBeneficiariosComponent implements OnInit {

  public buscarPagosDynamicForm: FormGroup;
  get filtrosFormArray() {
    return <FormArray>this.buscarPagosDynamicForm.get("filtros");
  }
  public isFecha: boolean = false;
  public isTipoNegocio: boolean = false;
  public isNegocioAsociado: boolean = false;
  public isUsuario: boolean = false;
  public isTodo: boolean = false;
  public swAddFiltros = true;
  public request: any;


  listSelectCriterio: CriterioList[] = [
    //{ value: "todo", viewValue: "Sin Filtros", active: true },
    { value: "tipoNegocio", viewValue: "Tipo de Negocio", active: true },
    { value: "fecha", viewValue: "Fecha", active: true },
    { value: "usuarios", viewValue: "Usuario que realiza cambio", active: true },
    { value: "negocios", viewValue: "Negocios", active: true }
  ];
  listSelectCriterioNuevo: CriterioList[] = [];

  public arrayFiltro: any[] = [];
  public nuevoItem: boolean = false;
  public listFechaRango: any[] = [];
  public listUsuario: any[] = [];
  public listNegocio: any[] = [];
  public listTipoNegocio: any[] = [];
  public cambioTotalPorcentajes: any;
  public cambioTotalTitulares: any;
  public cambioMensualPorcentajes: any;
  cambioMensualTitulares: any;
  cambioAnualPorcentajes: any;
  cambioAnualTitulares: any;

  public numeroBeneficiarios: any;
  public cambioBeneficiarios: any;
  public cambioBeneficiariosMes: any;
  public cambioBeneficiariosAnual: any;
  public porcentajeGiro: any;
  public porcentajeGiroMes: any;
  public porcentajeGiroAnual: any;
  public tipoPago: any;
  public tipoPagoMes: any;
  public tipoPagoAnual: any;
  public porcentajeDist: any;
  public porcentajeDistMes: any;
  public porcentajeDistAnual: any;

  public filtroEscogido: boolean = false;
  constructor(public fb: FormBuilder,
    private _dataService: DataService,
    private _storageData: StorageDataService,
    private _util: Utils,
    private _authService: AuthService,
    public dialog: MatDialog) {
  }

  createGroup() {
    return this.fb.group({
      selectCriterio: new FormControl("", []),
      argumento: new FormControl("", []),
    });
  }


  addToFiltrosFormArray() {
    this.filtrosFormArray.push(this.createGroup());
    if (this.filtrosFormArray.length == 4) {
      this.swAddFiltros = false;
    }
    this.nuevoItem = true;
  }

  ngOnInit(): void {

    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarPagosDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();
    //se quita por que es muy pesado mandarlo desde el principio
    //this.enviarDatos("");

  }

  checkFiltros() {
    if (this.filtrosFormArray.length == 1) {
      return false
    }
    return true;
  }

  removeFromFiltrosFormArray(index, itemFiltro) {
    this.filtrosFormArray.removeAt(index);


    if (itemFiltro.value.selectCriterio == "usuarios") {
      this.listUsuario = [];
    } else if (itemFiltro.value.selectCriterio == "fecha") {
      this.listFechaRango = [];
    } else
      if (itemFiltro.value.selectCriterio == "tipoNegocio") {
        this.listTipoNegocio = [];
      } else if (itemFiltro.value.selectCriterio == "negocios") {
        this.listNegocio = [];
      }
    this.isFiltroEscogido();

    //controla el boton mas
    if (this.filtrosFormArray.length < 4) {
      this.swAddFiltros = true;
    }

    //remueve un elemento del array de filtros
    this.arrayFiltro = this.arrayFiltro.filter(item => {
      return (item !== itemFiltro.value.selectCriterio);
    });
    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == itemFiltro.value.selectCriterio) {
        element.active = true;
      }
    });


  }
  buscarListaXCriterioSeleccionado(eventSeleccionado, ind) {
    this.isFecha = false;
    this.isNegocioAsociado = false;
    this.isTodo = true;
    this.isTipoNegocio = false;
    this.isUsuario = false;
    if (eventSeleccionado.value == 'todo') {
      this.isTodo = true;
    }
    if (eventSeleccionado.value == 'tipoNegocio') {
      this.arrayFiltro[ind] = 'tipoNegocio';
      this.isFecha = false;
      this.isNegocioAsociado = false;
      this.isTodo = false;
      this.isTipoNegocio = true;
      this.isUsuario = false;
    }
    if (eventSeleccionado.value == 'fecha') {
      this.arrayFiltro[ind] = 'fecha';
      this.isFecha = true;
      this.isNegocioAsociado = false;
      this.isTodo = false;
      this.isTipoNegocio = false;
      this.isUsuario = false;
    }
    if (eventSeleccionado.value == 'negocios') {
      this.arrayFiltro[ind] = 'negocios';
      this.isFecha = false;
      this.isNegocioAsociado = true;
      this.isTodo = false;
      this.isTipoNegocio = false;
      this.isUsuario = false;
    }
    if (eventSeleccionado.value == 'usuarios') {
      this.arrayFiltro[ind] = 'usuarios';
      this.isFecha = false;
      this.isNegocioAsociado = false;
      this.isTodo = false;
      this.isTipoNegocio = false;
      this.isUsuario = true;
    }
    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == eventSeleccionado.value) {
        element.active = false;
      }
      if (!this.isTodo) {
        if (element.value == 'todo') {
          element.active = false;
        }
      }
    });
  }

  isFiltroEscogido() {
    if (this.listFechaRango.length == 0
      && this.listUsuario.length == 0
      && this.listTipoNegocio.length == 0
      && this.listNegocio.length == 0) {
      this.filtroEscogido = false;
    } else {
      this.filtroEscogido = true;

    }

  }
  getDateRange(range) {
    this.listFechaRango = range;
    this.isFiltroEscogido();

  }

  getUsuario(usuario) {
    this.listUsuario = usuario;
    this.isFiltroEscogido();
  }

  getNegocios(negocio) {
    //solo se necesita el cod_sfc
    this.listNegocio = negocio;
    this.isFiltroEscogido();

  }
  getTipoNegocio(tipoN) {
    this.listTipoNegocio = tipoN;
    this.isFiltroEscogido();

  }

  limpiarformulario() {
    this.arrayFiltro = [];
    this.filtrosFormArray.clear();
    this.filtrosFormArray.reset([]);

    //Limpiar arreglos
    this.listFechaRango=[];
    this.listUsuario=[];
    this.listTipoNegocio=[];
    this.listNegocio=[];

    this.numeroBeneficiarios = null;
    this.cambioBeneficiarios = null;
    this.cambioBeneficiariosMes = null;
    this.cambioBeneficiariosAnual = null;
    this.porcentajeGiro = null;
    this.porcentajeGiroMes = null;
    this.porcentajeGiroAnual = null;
    this.tipoPago = null;
    this.tipoPagoMes = null;
    this.tipoPagoAnual = null;
    this.porcentajeDist = null;
    this.porcentajeDistMes = null;
    this.porcentajeDistAnual = null;

    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarPagosDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();
    this.swAddFiltros = true;
    this.listSelectCriterioNuevo.forEach(element => {
      element.active = true;
    });
  }

  enviarDatos(listar): void {
    this.request = {

      rangoFecha: this.listFechaRango,
      solicitadoPor: this.listUsuario,

      tipoNegocio: this.listTipoNegocio,

      //de los negocios solo se necesita el cod_sfc
      negocios: this.listNegocio

    }
    this._dataService.postServiceReporteBeneficiaros(this.request)
      .subscribe(data => {

        this.numeroBeneficiarios = data.numeroBeneficiarios;
        this.cambioBeneficiarios = data.cambioBeneficiarios;
        this.cambioBeneficiariosMes = data.cambioBeneficiariosMes;
        this.cambioBeneficiariosAnual = data.cambioBeneficiariosAnual;
        this.porcentajeGiro = data.porcentajeGiro;
        this.porcentajeGiroMes = data.porcentajeGiroMes;
        this.porcentajeGiroAnual = data.porcentajeGiroAnual;
        this.tipoPago = data.tipoPago;
        this.tipoPagoMes = data.tipoPagoMes;
        this.tipoPagoAnual = data.tipoPagoAnual;
        this.porcentajeDist = data.porcentajeDist;
        this.porcentajeDistMes = data.porcentajeDistMes;
        this.porcentajeDistAnual = data.porcentajeDistAnual;
      }, (error) => {
        let data = {
          titulo: "Error realizando la consulta ",
          contenido: "Ha ocurrido un error en la consulta",
          buttonAceptar: "ACEPTAR",
          urlimg: true
        };

        //se usa este modal ya que  permite mostrar mensajes de error
        //genericos
        this.dialog.open(ModalSincronizacionErrorComponent, {
          width: "46rem",
          data
        });
      });
  }
}
