import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportesBeneficiariosComponent } from './reportes-beneficiarios.component';

describe('ReportesBeneficiariosComponent', () => {
  let component: ReportesBeneficiariosComponent;
  let fixture: ComponentFixture<ReportesBeneficiariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportesBeneficiariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportesBeneficiariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
