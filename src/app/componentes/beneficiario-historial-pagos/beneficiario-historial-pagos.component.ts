import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { DialogCargarArchivoComponent } from 'src/app/dialogs/dialog-cargar-archivo/dialog-cargar-archivo.component';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Documento, BeneficiarioDetalle } from 'src/app/modelo/beneficiario-detalle';
import { CurrencyPipe } from '@angular/common';
import { Router } from '@angular/router';

export interface CriterioList {
  value: string;
  viewValue: string;
  active: boolean;
}

export interface Parametros {
  numeroDocumento: string;
  tipoDocumento: string;
  fecha: any[];
  tipoPagos: any[];
  negociosAsociados: any[];
  estadoPago: any[];
}

export interface ListPagos {
  fechaPago?: string;
  codigoNegocio: string;
  negocioVinculado?: string;
  tipoNegocio?: string;
  porcentajeParticipacion?: string;
  nombreBeneficiario?: string;
  tipoDocumentoBeneficiario?: string;
  numeroDocumentoBeneficiario?: string;
  porcentajeGiro?: string;
  porcentajeDistribuido?: string;
  valorAGirar?: string;
  gravamentMovimientoFinanciero?: string;
  tipoPago?: string;
  valorTipoPago?: string;
  subTotalGirados?: string;
  tipoCuenta?: string;
  entidadBancariaFidecomiso?: string;
  codigoEntidadBancaria?: string;
  numeroCuentaEncargo?: string;
  estadoPago?: string;
}

@Component({
  selector: 'app-beneficiario-historial-pagos',
  templateUrl: './beneficiario-historial-pagos.component.html',
  styleUrls: ['./beneficiario-historial-pagos.component.scss']
})
/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Marzo 2020
 * @Proposito: clase para mostrar el historial de pagos a beneficiarios.
 */
export class BeneficiarioHistorialPagosComponent implements OnInit {

  public buscarPagosDynamicForm: FormGroup;
  get filtrosFormArray() {
    return <FormArray>this.buscarPagosDynamicForm.get("filtros");
  }
  public submitted: boolean = false;
  public dataNotFound: boolean = null;
  public dataFound: boolean = null;
  public detalleBeneficiario: boolean = false;

  public isFecha: boolean = false;
  public isTipoPago: boolean = false;
  public isNegocioAsociado: boolean = false;
  public isEstadoPago: boolean = false;
  public isTodo: boolean = false;
  public swAddFiltros = true;

  public arrayFiltro: any[] = [];
  public nuevoItem: boolean = false;

  public listFechaRango: any[] = [];
  public listTipoPago: any[] = [];
  public listNegocioAsociado: any[] = [];
  public listEstadoPago: any[] = [];
  public request: Parametros;


  listSelectCriterio: CriterioList[] = [
    { value: "todo", viewValue: "Sin Filtros", active: true },
    { value: "tipoPago", viewValue: "Tipo de pago", active: true },
    { value: "fecha", viewValue: "Fecha", active: true },
    { value: "negocioAsociado", viewValue: "Negocio asociado", active: true },
    { value: "estadoPago", viewValue: "Estado de pago", active: true }
  ];

  listSelectCriterioNuevo: CriterioList[] = [];

  displayedColumns: string[] = [
    'fechaPago',
    'negocioVinculado',
    'tipoNegocio',
    'porcentajeParticipacion',
    'nombreBeneficiario',
    'tipoDocumentoBeneficiario',
    'numeroDocumentoBeneficiario',
    'porcentajeGiro',
    'porcentajeDistribuido',
    'valorAGirar',
    'gravamentMovimientoFinanciero',
    'tipoPago',
    'valorTipoPago',
    'subTotalGirados',
    'tipoCuenta',
    'entidadBancariaFidecomiso',
    'codigoEntidadBancaria',
    'numeroCuentaEncargo',
    'estadoPago'
  ];

  dataSource: MatTableDataSource<ListPagos>;
  public resultsLength: number = 0;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  public listError: any = null;
  public swListError: boolean = false;
  public documento: Documento;
  public infoBeneficiario: BeneficiarioDetalle;

  public page: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPages: number = 1; //Número total de páginas 
  public numElementos: number = 10; //Total de elemento por pagina   

  constructor(
    public fb: FormBuilder,
    private _dataService: DataService,
    private _storageData: StorageDataService,
    private currencyPipe: CurrencyPipe,
    public dialog: MatDialog,        
    private _router: Router
  ) {

  }

  ngOnInit() {
    this.loadInfoNegocio();
    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarPagosDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();
    this.resultsLength = 0;
  }

  private loadInfoNegocio() {
    this.infoBeneficiario = this._storageData.getInfoBeneficiario();
    this.documento = {
      documento: this.infoBeneficiario.numeroDocumento,
      tipodocumento: this.infoBeneficiario.tipoDocumento.id
    }
  }

  checkFiltros() {
    if (this.filtrosFormArray.length == 1) {
      return false
    }
    return true;
  }

  limpiarformulario() {
    this.arrayFiltro = [];
    this.filtrosFormArray.clear();
    this.filtrosFormArray.reset([]);
    this.loadInfoNegocio();
    this.dataSource = null;
    this.dataNotFound = false;
    this.dataFound = false;
    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarPagosDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();
    this.resultsLength = 0;
    this.swAddFiltros = true;
    this.listSelectCriterioNuevo.forEach(element => {
      element.active = true;
    });
    document.getElementById("dataFound").style.display = 'none';
    document.getElementById("dataFoundBoton").style.display = 'none';
    document.getElementById("dataNotFound").style.display = 'none';    
    this.listFechaRango = [];
    this.listTipoPago = [];
    this.listNegocioAsociado = [];
    this.listEstadoPago = [];
  }

  buscarListaXCriterioSeleccionado(eventSeleccionado, ind) {
    this.isFecha = false;
    this.isTipoPago = false;
    this.isNegocioAsociado = false;
    this.isEstadoPago = false;
    this.isTodo = true;
    if (eventSeleccionado.value == 'todo') {
      this.isTodo = true;
    }
    if (eventSeleccionado.value == 'tipoPago') {
      this.arrayFiltro[ind] = 'tipoPago';
      this.isTipoPago = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }
    if (eventSeleccionado.value == 'fecha') {
      this.arrayFiltro[ind] = 'fecha';
      this.isFecha = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }
    if (eventSeleccionado.value == 'negocioAsociado') {
      this.arrayFiltro[ind] = 'negocioAsociado';
      this.isNegocioAsociado = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }
    if (eventSeleccionado.value == 'estadoPago') {
      this.arrayFiltro[ind] = 'estadoPago';
      this.isEstadoPago = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }
    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == eventSeleccionado.value) {
        element.active = false;
      }
      if (!this.isTodo) {
        if (element.value == 'todo') {
          element.active = false;
        }
      }
    });
  }

  getDateRange(range) {
    this.listFechaRango = range;
  }

  getTipoPagos(tipoPago) {
    this.listTipoPago = tipoPago;
  }

  getNegocioAsociados(negocioAsociado) {
    this.listNegocioAsociado = negocioAsociado;
  }

  getEstadoPago(estadoPago) {
    this.listEstadoPago = estadoPago;
  }

  createGroup() {
    return this.fb.group({
      selectCriterio: new FormControl("", []),
      argumento: new FormControl("", [])
    });
  }

  addToFiltrosFormArray() {
    this.filtrosFormArray.push(this.createGroup());
    if (this.filtrosFormArray.length == 4) {
      this.swAddFiltros = false;
    }
    this.nuevoItem = true;
  }

  removeFromFiltrosFormArray(index, itemFiltro) {
    this.filtrosFormArray.removeAt(index);
    this.limpiarFiltro(itemFiltro.value);
    this.arrayFiltro = this.arrayFiltro.filter(item => {
      return (item !== itemFiltro.value.selectCriterio);
    });

    if (this.filtrosFormArray.length < 4) {
      this.swAddFiltros = true;
    }

    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == itemFiltro.value.selectCriterio) {
        element.active = true;
      }
    });
  }

  limpiarFiltro(filtroLimpiar){
    switch (filtroLimpiar.selectCriterio) {
      case 'estadoPago':
        this.listEstadoPago = [];
        break;
      case 'fecha':
        this.listFechaRango = [];
        break;  
      case 'negocioAsociado':
        this.listNegocioAsociado = [];
        break;
      case 'tipoPago':
        this.listTipoPago = [];
        break;  
      default:
        this.listFechaRango = [];
        this.listTipoPago = [];
        this.listNegocioAsociado = [];
        this.listEstadoPago = [];
      break
    }    
  }

  goToPage(page: number) {
    this.page = page;
    let requestPaginado: any = this.request;
    requestPaginado.pagina = page;
    requestPaginado.elementos = this.numElementos;
    this.beneficiarioListHistorialPagos(requestPaginado);
  }

  buscarDatos(listar): void {
    this.dataNotFound = false;
    this.dataFound = false;
    this.submitted = true;
    this.detalleBeneficiario = false;
    this.request = {
      tipoDocumento: this.documento.tipodocumento,
      numeroDocumento: this.documento.documento,
      estadoPago: this.listEstadoPago,
      fecha: this.listFechaRango,
      negociosAsociados: this.listNegocioAsociado,
      tipoPagos: this.listTipoPago
    }
    if (listar) {
      let requestPaginado: any = this.request;
      requestPaginado.pagina = 1;
      requestPaginado.elementos = this.numElementos;
      this.beneficiarioListHistorialPagos(requestPaginado);
    } else {
      let requestPaginado: any = this.request;
      requestPaginado.pagina = 0;
      requestPaginado.elementos = 0;
      this.descargarArchivo(this.request);
    }
  }

  setFormatNumberDecimal(monto, decimal) {
    let valor = parseFloat(monto).toFixed(decimal);
    if (valor == 'NaN') {
      valor = '0';
    }
    monto = this.currencyPipe.transform(valor);
    return monto;
  }

  beneficiarioListHistorialPagos(requestBuscar) {
    this.dataSource = null;
    let listPagos: ListPagos[] = new Array();
    let pago: ListPagos;
    this._dataService
      .listadoHistorialPagos(requestBuscar)
      .subscribe((data: any) => {
        if (!data || data.totalRegistros == 0) {
          this.dataNotFound = true;
          this.dataFound = false;
          this.detalleBeneficiario = false;
          document.getElementById("dataFound").style.display = 'none';
          document.getElementById("dataFoundBoton").style.display = 'none';
          document.getElementById("dataNotFound").style.display = 'inline';
          return;
        }

        this.dataFound = true;
        this.dataNotFound = false;
        this.detalleBeneficiario = false;
        document.getElementById("dataFound").style.display = 'inline';
        document.getElementById("dataFoundBoton").style.display = 'inline';
        document.getElementById("dataNotFound").style.display = 'none';

        data.historialPagos.forEach(element => {
          pago = {
            fechaPago: element.fechaPago,
            codigoNegocio: element.codigoNegocio,
            negocioVinculado: element.negocioVinculado,
            tipoNegocio: element.tipoNegocio,
            porcentajeParticipacion: element.porcentajeParticipacion,
            nombreBeneficiario: element.nombreBeneficiario,
            tipoDocumentoBeneficiario: element.tipoDocumentoBeneficiario,
            numeroDocumentoBeneficiario: element.numeroDocumentoBeneficiario,
            porcentajeGiro: element.porcentajeGiro,
            porcentajeDistribuido: element.porcentajeDistribuido,
            valorAGirar: this.setFormatNumberDecimal(element.valorAGirar, 2),
            gravamentMovimientoFinanciero: this.setFormatNumberDecimal(element.gravamentMovimientoFinanciero,2),
            tipoPago: element.tipoPago,
            valorTipoPago: this.setFormatNumberDecimal(element.valorTipoPago, 2),
            subTotalGirados: this.setFormatNumberDecimal(element.subTotalGirados, 2),
            tipoCuenta: element.tipoCuenta,
            entidadBancariaFidecomiso: element.entidadBancariaFidecomiso,
            codigoEntidadBancaria: element.codigoEntidadBancaria,
            numeroCuentaEncargo: element.numeroCuentaEncargo,
            estadoPago: element.estadoPago
          }
          listPagos.push(pago);
        });

        this.dataSource = new MatTableDataSource(listPagos);
        this.resultsLength = data.totalRegistros;
        this.totalPages = this.resultsLength / this.numElementos;          
        let totalPagina = this.resultsLength % this.numElementos;
        if (totalPagina > 1) {
          this.totalPages = Number(parseInt(String(this.totalPages))) + 1;
        }
        if (this.totalPages < 1) {
          this.totalPages = 1;
        } 

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

      },
        (err) => {
          this.dataNotFound = true;
          this.dataFound = true;
          document.getElementById("dataFound").style.display = 'none';
          document.getElementById("dataFoundBoton").style.display = 'none';
          document.getElementById("dataNotFound").style.display = 'inline';
        }
      );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public descargarArchivo(requestBuscar) {
    this._dataService.descargarListadoPagos(requestBuscar)
      .subscribe(
        (data: any) => {
          this.convertirArchivo(data);
          this.openDialog(true, null);

        },
        (err) => {
          this.openDialog(false, err.error);
        }
      );
  }

  convertirArchivo(data): void {
    let blob = new Blob([data], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    });
    let fileUrl = window.URL.createObjectURL(blob);

    if (window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob, fileUrl.split(':')[1]);
    } else {

      window.open(fileUrl);
    }
  }

  openDialog(ok, context): void {
    let data;
    if (ok) {
      data = {
        titulo: "Archivo generado correctamente",
        contenido: "La descarga de informacion sobre el historia de pagos en archivo han sido generada de manera exitosa.",
        button: "ACEPTAR",
        urlimg: false,
        listError: null
      };
    }
    if (!ok) {
      this.listError = null;
      let contenido = "El archivo no se puede descargar. Intentar más tarde.";
      if (context.mensaje != null) {
        contenido = context.mensaje;

        if (context.lista != null) {
          this.swListError = true;
          this.listError = context.lista;
        }
      }
      data = {
        titulo: "Error al descargar archivo",
        contenido: contenido,
        button: "ACEPTAR",
        urlimg: true,
        listError: this.listError
      };
    }
    const dialogRef = this.dialog.open(DialogCargarArchivoComponent, {
      width: "40rem",
      data
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
  public vinculoDetalleNegocioPago(codigoNegocio) {    
    this._storageData.setCodigoNegocioBenficiarioPago(codigoNegocio);
    this._dataService.getObtenerInformacionNegocio(codigoNegocio)
      .subscribe((response: any) => {        
        this._storageData.setInfoNegocioDetalle(response);
        this._storageData.setEstadoProyecto(response.estadoProyecto);
        this._router.navigate(['accionfiduciaria/negocios/detalles']);
      },
        (err) => {
        }
      );
    
  }

}

