import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiarioHistorialPagosComponent } from './beneficiario-historial-pagos.component';

describe('BeneficiarioHistorialPagosComponent', () => {
  let component: BeneficiarioHistorialPagosComponent;
  let fixture: ComponentFixture<BeneficiarioHistorialPagosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiarioHistorialPagosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiarioHistorialPagosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
