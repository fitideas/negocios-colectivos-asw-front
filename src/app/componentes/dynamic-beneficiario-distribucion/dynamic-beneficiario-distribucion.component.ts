import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { Utils } from 'src/app/core/utils';
import { DialogTwoButtonsComponent } from '../dialog-two-buttons/dialog-two-buttons.component';
import { MatDialog } from '@angular/material';
import { DialogParametrosSaveComponent } from '../dialog-parametros-save/dialog-parametros-save.component';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'app-dynamic-beneficiario-distribucion',
  templateUrl: './dynamic-beneficiario-distribucion.component.html',
  styleUrls: ['./dynamic-beneficiario-distribucion.component.css']
})
export class DynamicBeneficiarioDistribucionComponent implements OnInit {

  @Input() index: number;
  @Input() listRetenciones: any;
  @Input() idTipoNaturaleza: any;
  @Input() editing: boolean;
  @Output() messageEvent = new EventEmitter<string>();

  public arrayRenciones: any[] = [];
  public showListRentencion: boolean = false;
  public listNaturaleza: any[] = [];
  public listNaturalezaRetenciones: any[] = [];

  dynamicForm = this.fb.group({
    distribucionFormArray: this.fb.array([this.fb.control('', Validators.required)])
  });


  constructor(
    private fb: FormBuilder,
    private _dataService: DataService,
    private _utils: Utils,
    private _storageService: StorageDataService,
    private currencyPipe: CurrencyPipe,
    public dialog: MatDialog
  ) {

  }

  ngOnInit() {
    this.loadRetencionesFormArray(this.listRetenciones);
    this.loadListRetenciones();
  }

  get distribucionFormArray(): FormArray {
    return this.dynamicForm.get('distribucionFormArray') as FormArray;
  }

  private loadListRetenciones() {
    this.listNaturalezaRetenciones = [];
    this._dataService
      .getParametrosValoresDominio('RET')
      .subscribe((data: any) => {
        if (!data) {
          return;
        }
        data.forEach(element => {
          this.arrayRenciones.push(element);
        });
      },
        (err) => {

        }
      );
  }

  private loadListTipoNaturlezaRetenciones() {
    this._dataService
    .getServiceParametrosValoresTipoCompuesto("TIPNATJUR")
    .subscribe(
      (tiposNaturaleza: any) => {        
        this.listNaturaleza = tiposNaturaleza;        
      },
      (err: any) => {
      }
    );
  }

  public loadRetencionesFormArray(listRetenciones) {
    if (listRetenciones.length > 0) {
      this.showListRentencion = true;
    }
    this.distribucionFormArray.clear();
    listRetenciones.forEach(item => {
      this.distribucionFormArray.push(
        this.patchValues(
          item.id,
          item.descripcion,
          item.valor,
          item.unidad
        )
      );
      const formGroup = this.createRetencionGroup();
      formGroup.patchValue(item);
    });
  }

  patchValues(id, descripcion, valor, unidad) {
    valor = this._utils.cambiarFloatante(valor,2);
    valor = this.currencyPipe.transform(valor);
    let valores = valor + ' ' + unidad;
    return this.fb.group({
      id: [id],
      descripcion: [descripcion],
      valor: [valores]
    });
  }

  public existeRetencion(listRetenciones: any): boolean {
    let retencionBuscar;
    let contador = 0;
    listRetenciones.forEach(element => {
      if (element.id == "0") {
        retencionBuscar = this._utils.buscarRetencion(listRetenciones, element.descripcion);
      }
    });
    listRetenciones.forEach(element => {
      if (element.descripcion == retencionBuscar.descripcion) {
        contador++;
      }
    });
    return (contador > 1) ? true : false;
  }

  public createRetencionGroup() {
    return this.fb.group({
      id: ['0'],
      descripcion: [''],
      valor: ['']
    });
  }

  addRetencion(): void {
    this.distribucionFormArray.push(this.createRetencionGroup());
  }

  openDialogEliminar(index, itemRentecion): void {
    let respuesta = '';
    let data;
    let contenido = "Este tipo de negocio aplica la retención " + itemRentecion.descripcion + " ¿Seguro desea eliminarlo? Nota: Los cambios solo se aplicarán cuando se haga clic en el botón guardar.";
    data = {
      titulo: "Eliminar retención",
      contenido: contenido,
      buttonContinuar: 'Continuar',
      buttonCancelar: 'Cancelar',
      urlimg: true
    };

    const dialogRef = this.dialog.open(DialogTwoButtonsComponent, {
      width: "40rem",
      data
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == data.buttonContinuar) {
        this._storageService.setBeneficiariosDistribucionRetencionesEliminar(itemRentecion.id);
        this.messageEvent.emit('true');
        this.distribucionFormArray.removeAt(index);
        if (this.distribucionFormArray.value.length == 0) {
          this.showListRentencion = false;
        }
      }
    });
  }

  public removeFromDistribucionFormArray(index, itemRentecion): void {
    this.openDialogEliminar(index, itemRentecion);
  }

  openDialogRetencionRepetida(): void {
    let data = {
        titulo: "Datos no guardados",
        contenido: "La retención seleccionada ya se encuentra aplicada, verificar",
        button: "ACEPTAR",
        urlimg: true
      };
    
    const dialogRef = this.dialog.open(DialogParametrosSaveComponent, {
      width: "40rem",
      data
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  public almacenarMemoria() {
    let retencion: any;
    let retenciones = [];
    let swAgregar: boolean = false;
    let agregarRetencion: boolean = this.existeRetencion(this.dynamicForm.value.distribucionFormArray);

    if (!agregarRetencion) {
      this.dynamicForm.value.distribucionFormArray.forEach(element => {
        if (element) {
          if (element.id == "0") {
            swAgregar = true;
          }
          retencion = this._utils.buscarRetencion(this.arrayRenciones, element.descripcion);
          element.valor = retencion.valor;
          element.unidad = retencion.unidad;
          element.id = retencion.id;
          if (element.unidad == 'COP') {
            element.valor = this.currencyPipe.transform(element.valor);
          }
          if (swAgregar) {
            this._storageService.setBeneficiarioDistribucionRentencionesNuevas(retencion.id);
            swAgregar = false;
            this.messageEvent.emit('true');
          }
          retenciones.push(element);
        }
      });
      this.loadRetencionesFormArray(retenciones);

    } else {
      this.openDialogRetencionRepetida();
      this.distribucionFormArray.removeAt(this.dynamicForm.value.distribucionFormArray.length-1);
    }
  }
}
