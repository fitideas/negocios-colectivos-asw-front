import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicBeneficiarioDistribucionComponent } from './dynamic-beneficiario-distribucion.component';

describe('DynamicBeneficiarioDistribucionComponent', () => {
  let component: DynamicBeneficiarioDistribucionComponent;
  let fixture: ComponentFixture<DynamicBeneficiarioDistribucionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DynamicBeneficiarioDistribucionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicBeneficiarioDistribucionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
