import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { DataService } from 'src/app/services/data.service';
import { StorageDataService } from 'src/app/services/storage-data.service';
import { PeticionHistorialCambios } from 'src/app/modelo/request-historicos';

export interface CriterioList {
  value: string;
  viewValue: string;
  active: boolean;
}



interface ListCambios {
  fechaCambio?: string;
  usuario?: string;
  direccionIp?: string;
  codigoNegocioVinculado?: string;
  negocioVinculado?: string;
  campo?: string;
  datoNuevo?: string;
  datoAnterior?: string;
}

@Component({
  selector: 'app-negocio-historial-modificaciones',
  templateUrl: './negocio-historial-modificaciones.component.html',
  styleUrls: ['./negocio-historial-modificaciones.component.css']
})
export class NegocioHistorialModificacionesComponent implements OnInit {
  public buscarModificacionesNegocioDynamicForm: FormGroup;
  get filtrosFormArray() {
    return <FormArray>this.buscarModificacionesNegocioDynamicForm.get("filtros");
  }
  public submitted: boolean = false;
  public dataNotFound: boolean = null;
  public dataFound: boolean = null;

  public arrayFiltro: any[] = [];
  public nuevoItem: boolean = false;

  public listFechaRango: any[] = [];
  public listUsuario: any[] = [];


  public isFecha: boolean = false;
  public isUsuario: boolean = false;
  public isNegocioAsociado: boolean = false;
  public isCampo: boolean = false;
  public isTodo: boolean = false;
  public swAddFiltros = true;

  public infoNegocio: any;

  listSelectCriterio: CriterioList[] = [
    { value: "fecha", viewValue: "Fecha del cambio", active: true },
    { value: "usuario", viewValue: "Usuario que registra", active: true }
  ];

  listSelectCriterioNuevo: CriterioList[] = [];

  displayedColumns: string[] = [
    'fechaCambio',
    'usuario',
    'direccionIp',
    'campo',
    'datoNuevo',
    'datoAnterior'
  ];

  dataSource: MatTableDataSource<ListCambios>;
  public resultsLength: number = 0;
  public page: number = 1; //Número de página en la que estamos. Será 1 la primera vez que se carga el componente 
  public totalPages: number = 1; //Número total de páginas 
  public numElementos: number = 10; //Total de elemento por pagina  
  public listCambios: ListCambios[] = new Array();

  constructor(
    public fb: FormBuilder,
    private _dataService: DataService,
    private _storageData: StorageDataService,
    public dialog: MatDialog
  ) {

  }

  ngOnInit() {
    this.loadInfoNegocio();
    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.buscarModificacionesNegocioDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();
  }



  checkFiltros() {
    if (this.filtrosFormArray.length == 1) {
      return false
    }
    return true;
  }

  removeFromFiltrosFormArray(index, itemFiltro) {
    this.filtrosFormArray.removeAt(index);

    this.arrayFiltro = this.arrayFiltro.filter(item => {
      return (item !== itemFiltro.value.selectCriterio);
    });

    if (this.filtrosFormArray.length < 2) {
      this.swAddFiltros = true;
    }

    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == itemFiltro.value.selectCriterio) {
        element.active = true;
      }
    });
  }

  createGroup() {
    return this.fb.group({
      selectCriterio: new FormControl("", []),
      argumento: new FormControl("", [])
    });
  }

  addToFiltrosFormArray() {
    this.filtrosFormArray.push(this.createGroup());
    if (this.filtrosFormArray.length == 2) {
      this.swAddFiltros = false;
    }
    this.nuevoItem = true;
  }

  buscarListaXCriterioSeleccionado(eventSeleccionado, ind) {
    this.isFecha = false;
    this.isUsuario = false;
    this.isNegocioAsociado = false;
    this.isCampo = false;
    this.isTodo = true;

    if (eventSeleccionado.value == 'usuario') {
      this.arrayFiltro[ind] = 'usuario';
      this.isUsuario = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }
    if (eventSeleccionado.value == 'fecha') {
      this.arrayFiltro[ind] = 'fecha';
      this.isFecha = true;
      this.isTodo = false;
      this.nuevoItem = false;
    }

    this.listSelectCriterioNuevo.forEach(element => {
      if (element.value == eventSeleccionado.value) {
        element.active = false;
      }
      if (!this.isTodo) {
        if (element.value == 'todo') {
          element.active = false;
        }
      }
    });
  }

  limpiarformulario() {
    this.arrayFiltro = [];
    this.listCambios = [];
    this.filtrosFormArray.clear();
    this.filtrosFormArray.reset([]);
    this.dataNotFound = false;
    this.dataFound = false;
    this.listSelectCriterioNuevo = this.listSelectCriterio;
    this.listFechaRango = [];    
    this.listUsuario = [];
    this.buscarModificacionesNegocioDynamicForm = this.fb.group({
      filtros: this.fb.array([])
    });
    this.addToFiltrosFormArray();
    this.resultsLength = 0;
    this.swAddFiltros = true;
    this.listSelectCriterioNuevo.forEach(element => {
      element.active = true;
    });
    document.getElementById("dataFound").style.display = 'none';
    document.getElementById("dataNotFound").style.display = 'none';
  }


  getDateRange(range) {
    this.listFechaRango = range;
  }

  getUsuarios(usuario) {
    this.listUsuario = usuario;
  }


  private loadInfoNegocio() {
    this.infoNegocio = this._storageData.getInfoNegocioDetalle();
  }



  enviarDatos(listar): void {
    this.dataNotFound = false;
    this.dataFound = false;
    this.submitted = true;
    this.listCambios = [];
    let request: PeticionHistorialCambios;
    request = {
      rangoFecha: this.listFechaRango,
      negocios: [this.infoNegocio.codigoSFC],
      solicitadoPor: this.listUsuario,
      tipoNegocio: [],
      numeroDocumentoVinculado: "",
      tipoDocumentoVinculado: ""
    }
    if (listar) {
      let requestPaginado: any = request;
      requestPaginado.pagina = 1;
      requestPaginado.elementos = this.numElementos;
      this.obtenerNegocioHistorialCambios(requestPaginado);
    }
  }

  obtenerNegocioHistorialCambios(request: PeticionHistorialCambios) {
    this.dataSource = null;
    let cambio: ListCambios;
    this._dataService
      .listadoNegocioHistorialCambios(request)
      .subscribe((data: any) => {
        if (!data || data.totalRegistro == 0) {
          this.dataNotFound = true;
          this.dataFound = false;
          document.getElementById("dataFound").style.display = 'none';
          document.getElementById("dataNotFound").style.display = 'inline';
          return;
        }

        this.dataFound = true;
        this.dataNotFound = false;
        document.getElementById("dataFound").style.display = 'inline';
        document.getElementById("dataNotFound").style.display = 'none';

        data.listaCambios.forEach(element => {
          cambio = {
            fechaCambio: element.fechaCambio,
            usuario: element.registradoPor,
            direccionIp: element.direccionIp,
            codigoNegocioVinculado: element.negocio,
            campo: element.campoCambio,
            datoNuevo: element.datoNuevo,
            datoAnterior: element.datoAnterior
          }
          this.listCambios.push(cambio);
        });
        let pagina = 1;
        this.mostraPaginaDataLocal(pagina);
      },
        (err) => {
          this.dataNotFound = true;
          this.dataFound = true;
          document.getElementById("dataFound").style.display = 'none';
          document.getElementById("dataNotFound").style.display = 'inline';
        }
      );
  }

  mostraPaginaDataLocal(pagina) {
    let registrosPagina: any[] = new Array();
    this.page = pagina;
    this.numElementos = 10;
    this.resultsLength = this.listCambios.length;
    this.totalPages = this.resultsLength / this.numElementos;
    let totalPagina = this.resultsLength % this.numElementos;
    if (totalPagina > 1) {
      this.totalPages = Number(parseInt(String(this.totalPages))) + 1;
    }
    if (this.totalPages < 1) {
      this.totalPages = 1;
    }

    let inicioRegistros = ((pagina - 1) * this.numElementos);
    let ultimoRegistros = pagina * this.numElementos;
    for (let i = inicioRegistros; ((i < this.listCambios.length) && (i < ultimoRegistros)); i++) {
      registrosPagina.push(this.listCambios[i]);
    }
    this.dataSource = new MatTableDataSource(registrosPagina);
  }

  goToPage(page: number) {
    this.mostraPaginaDataLocal(page);
  }

}
