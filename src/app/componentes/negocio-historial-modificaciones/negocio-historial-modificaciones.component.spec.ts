import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NegocioHistorialModificacionesComponent } from './negocio-historial-modificaciones.component';

describe('NegocioHistorialModificacionesComponent', () => {
  let component: NegocioHistorialModificacionesComponent;
  let fixture: ComponentFixture<NegocioHistorialModificacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NegocioHistorialModificacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NegocioHistorialModificacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
