import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiarioHistorialComponent } from './beneficiario-historial.component';

describe('BeneficiarioHistorialComponent', () => {
  let component: BeneficiarioHistorialComponent;
  let fixture: ComponentFixture<BeneficiarioHistorialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiarioHistorialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiarioHistorialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
