import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Utils } from 'src/app/core/utils';
import { StorageDataService } from 'src/app/services/storage-data.service';

@Component({
  selector: 'app-beneficiario-historial',
  templateUrl: './beneficiario-historial.component.html',
  styleUrls: ['./beneficiario-historial.component.scss']
})

/**
 * @Autor: José Reinaldo García Becerra
 * @Fecha: Enero 2020
 * @Proposito: clase para mostrar el historial detallado de beneficiarios.
 */

export class BeneficiarioHistorialComponent implements OnInit {

  public ver_historial_modificaciones: boolean = false;
  public ver_historial_pagos: boolean = false;
  public historial_pagos: boolean = false;
  public historial_modificaciones: boolean = false;

  constructor(
    public _util: Utils,
    private _authService: AuthService,
    private _storageData: StorageDataService
  ) { }

  ngOnInit() {
    this.loadPermisos();   
  }

  private loadPermisos() {
    this.ver_historial_modificaciones = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'historial_modificaciones');
    this.ver_historial_pagos = this._util.buscarFuncionalidad(this._authService.getFuncionalidades(), 'historial_pagos');    
  }

  public verHistorialPagos() {
    this._storageData.setActiveBeneficiarioHistorial(false);
    this._storageData.setBeneficiarioHistorialPagos(true);
    this.historial_pagos = true; 
  }
  public verHistorialModificaciones() {
    this._storageData.setActiveBeneficiarioHistorial(false);    
    this._storageData.setBeneficiarioHistoriallModificaciones(true);
    this.historial_modificaciones = true; 
  }

}
