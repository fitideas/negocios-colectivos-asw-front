import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertVinculadosNegociosAsociadosComponent } from './cert-vinculados-negocios-asociados.component';

describe('CertVinculadosNegociosAsociadosComponent', () => {
  let component: CertVinculadosNegociosAsociadosComponent;
  let fixture: ComponentFixture<CertVinculadosNegociosAsociadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertVinculadosNegociosAsociadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertVinculadosNegociosAsociadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
