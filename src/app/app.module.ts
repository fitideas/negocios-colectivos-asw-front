import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { LoginComponent } from "./auth/login/login.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "./material/material.module";
import { AppRoutingModule } from "./app-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule } from 'ngx-mat-datetime-picker';
import { NgxMatMomentModule } from 'ngx-mat-moment-adapter';

import { MAT_DATE_LOCALE, MatDatepickerModule, MAT_DATE_FORMATS } from '@angular/material';

import { InicioComponent } from "./componentes/inicio/inicio.component";
import { BeneficiarioComponent } from './componentes/beneficiario/beneficiario.component';
import { MatPaginatorIntl } from '@angular/material';
import { getSpanishPaginatorIntl } from './material/spanish-paginator-intl';
import { BeneficiarioDetalleComponent } from './componentes/beneficiario-detalle/beneficiario-detalle.component';
import { ParametrosComponent } from './componentes/parametros/parametros.component';
import { DialogParametrosSaveComponent } from './componentes/dialog-parametros-save/dialog-parametros-save.component';
import { DynamicPerfilComponent } from './componentes/dynamic-perfil/dynamic-perfil.component';
import { PerfilComponent } from './componentes/perfil/perfil.component'
import { CoreModule } from './core';
import { Utils } from './core/utils';
import { DialogThreeButtonsComponent } from './componentes/dialog-three-buttons/dialog-three-buttons.component';
import { DialogReemplazarParametrosComponent } from './componentes/dialog-reemplazar-parametros/dialog-reemplazar-parametros.component';
import { TwoDigitDecimaNumberDirective } from './core/two-digit-decima-number.directive';
import { DigitNumberDirective } from './core/digit-number.directive';
import { InformacionFinancieraComponent } from './componentes/informacion-financiera/informacion-financiera.component';
import { BeneficiarioNegociosComponent } from './componentes/beneficiario-negocios/beneficiario-negocios.component';
import { DialogTwoButtonsComponent } from './componentes/dialog-two-buttons/dialog-two-buttons.component';
import { BeneficiarioDistribucionComponent } from './componentes/beneficiario-distribucion/beneficiario-distribucion.component';
import { DialogPorcentajeGiroComponent } from './componentes/dialog-porcentaje-giro/dialog-porcentaje-giro.component';
import { DynamicBeneficiarioDistribucionComponent } from './componentes/dynamic-beneficiario-distribucion/dynamic-beneficiario-distribucion.component';
import { DialogAddCotitularComponent } from './dialogs/dialog-add-cotitular/dialog-add-cotitular.component';
import { NegocioComponent } from './componentes/negocio/negocio.component';
import { NegociosCargaArchivoComponent } from './componentes/negocios-carga-archivo/negocios-carga-archivo.component';
import { DialogCargarArchivoComponent } from './dialogs/dialog-cargar-archivo/dialog-cargar-archivo.component';
import { ExcelService } from './services/excel.service';
import { NegocioDetalleComponent } from './componentes/negocio-detalle/negocio-detalle.component';
import { NegociosAsambleasComponent } from './componentes/negocios-asambleas/negocios-asambleas.component';
import { NegociosAsambleasNuevaEntradaComponent } from './componentes/negocios-asambleas-nueva-entrada/negocios-asambleas-nueva-entrada.component';
import { DialogBuscarDirectivoComponent } from './dialogs/dialog-buscar-directivo/dialog-buscar-directivo.component';
import { NegocioEquipoComponent } from './componentes/negocio-equipo/negocio-equipo.component';
import { DialogNegocioListTitularesComponent } from './dialogs/dialog-negocio-list-titulares/dialog-negocio-list-titulares.component';
import { DialogBuscarArchivoComponent } from './dialogs/dialog-buscar-archivo/dialog-buscar-archivo.component';
import { FormArrayMultipleComponent } from './componentes/form-array-multiple/form-array-multiple.component';
import { DateAdapter, MatNativeDateModule } from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { CustomDateAdapter } from './modelo/customDateAdapter';
import { DialogDetallenovedadJuridicaComponent } from './dialogs/dialog-detallenovedad-juridica/dialog-detallenovedad-juridica.component';
import { AuthGuard } from './_helpers/auth.guard';
import { BeneficiarioHistorialComponent } from './componentes/beneficiario-historial/beneficiario-historial.component';
import { BeneficiarioHistorialPagosComponent } from './componentes/beneficiario-historial-pagos/beneficiario-historial-pagos.component';
import { BeneficiarioHistorialModificacionesComponent } from './componentes/beneficiario-historial-modificaciones/beneficiario-historial-modificaciones.component';
import { SelectMultipleTipoPagoComponent } from './selects/select-multiple-tipo-pago/select-multiple-tipo-pago.component';
import { SelectMultipleEstadoPagosComponent } from './selects/select-multiple-estado-pagos/select-multiple-estado-pagos.component';
import { SelectMultipleNegocioAsociadoComponent } from './selects/select-multiple-negocio-asociado/select-multiple-negocio-asociado.component';
import { DateRangePickerComponent } from './selects/date-range-picker/date-range-picker.component';
import { SatDatepickerModule,
  SatNativeDateModule, 
  DateAdapter as SatDateAdapter,
  MAT_DATE_FORMATS as SAT_DATE_FORMATS,
  MAT_DATE_LOCALE as SAT_DATE_LOCALE,
  } from 'saturn-datepicker';
import { PaginationComponent } from './material/pagination/pagination.component';
import { NgHttpLoaderModule } from 'ng-http-loader';

import { DatePipe, CurrencyPipe,PercentPipe, DecimalPipe } from '@angular/common';

import { PagosDistribucionComponent } from './componentes/pagos-distribucion/pagos-distribucion.component';
import { DatepickerMesAnioComponent } from './datepickers/datepicker-mes-anio/datepicker-mes-anio.component';
import { PagoListaIngresosGastosComponent } from './componentes/pago-lista-ingresos-gastos/pago-lista-ingresos-gastos.component';
import { DialogAddIngresoComponent } from './dialogs/dialog-add-ingreso/dialog-add-ingreso.component';
import { PagoListarTitularesBenComponent } from './componentes/pago-listar-titulares-ben/pago-listar-titulares-ben.component';
import { JwtInterceptorService } from './_helpers/jwt-interceptor.service';
import { DialogCerrarSesionComponent } from './dialogs/dialog-cerrar-sesion/dialog-cerrar-sesion.component';
import { MensajeEsperaComponent } from './dialogs/mensaje-espera/mensaje-espera.component';
import { NegociosPagosDistribucionComponent } from './componentes/negocios-pagos-distribucion/negocios-pagos-distribucion.component';
import { NegociosHistorialPagosDistribucionComponent } from './componentes/negocios-historial-pagos-distribucion/negocios-historial-pagos-distribucion.component';
import { SelectMultipleTipoNegocioComponent } from './selects/select-multiple-tipo-negocio/select-multiple-tipo-negocio.component';
import { NegociosHistorialPagosIngresoEgresoComponent } from './componentes/negocios-historial-pagos-ingreso-egreso/negocios-historial-pagos-ingreso-egreso.component';
import { NegociosHistorialPagosDetalleBeneficiariosComponent } from './componentes/negocios-historial-pagos-detalle-beneficiarios/negocios-historial-pagos-detalle-beneficiarios.component';
import { ComiteNuevaEntradaComponent } from './componentes/comite-nueva-entrada/comite-nueva-entrada.component';
import { ComiteAdministrarComiteComponent } from './componentes/comite-administrar-comite/comite-administrar-comite.component';
import { ComiteAdicionarMiembroComponent } from './componentes/comite-adicionar-miembro/comite-adicionar-miembro.component';
import { OnlyNumber } from './core/onlyNumber.directive';
import { OnlyLetrasDirective } from './core/only-letras.directive';
import { ModalSincronizacionErrorComponent } from './dialogs/modal-sincronizacion-error/modal-sincronizacion-error.component';
import { ModalSincronizacionComponent } from './dialogs/modal-sincronizacion/modal-sincronizacion.component';
import { ModalSincronizacionConstantesComponent } from './dialogs/modal-sincronizacion-constantes/modal-sincronizacion-constantes.component';
import { ModalSincronizacionInformacionFinancieraIncompletaComponent } from './dialogs/modal-sincronizacion-informacion-financiera-incompleta/modal-sincronizacion-informacion-financiera-incompleta.component';
import { ModalSincronizacionCambiodatotitularComponent } from './dialogs/modal-sincronizacion-cambiodatotitular/modal-sincronizacion-cambiodatotitular.component';
import { ModalSincronizacionDetalleDatocambiadoComponent } from './dialogs/modal-sincronizacion-detalle-datocambiado/modal-sincronizacion-detalle-datocambiado.component';
import { ModalSincronizacionTitularnuevoComponent } from './dialogs/modal-sincronizacion-titularnuevo/modal-sincronizacion-titularnuevo.component';
import { ReportesComponent } from './componentes/reportes/reportes.component';
import { ReporteVinculadosComponent } from './componentes/reporte-vinculados/reporte-vinculados.component';
import { SelectMultipleUsuariosComponent } from './selects/select-multiple-usuarios/select-multiple-usuarios.component';
import { DialogInfopagoRechazadoComponent } from './dialogs/dialog-infopago-rechazado/dialog-infopago-rechazado.component';
import { DetalleInfopagoRechazadoComponent } from './dialogs/detalle-infopago-rechazado/detalle-infopago-rechazado.component';
import { NegocioHistorialModificacionesComponent } from './componentes/negocio-historial-modificaciones/negocio-historial-modificaciones.component';
import { NegocioHistorialComponent } from './componentes/negocio-historial/negocio-historial.component';
import { ReportesDistribucionComponent } from './componentes/reportes-distribucion/reportes-distribucion.component';
import { ReportesCesionesComponent } from './componentes/reportes-cesiones/reportes-cesiones.component';
import { ReportesBeneficiariosComponent } from './componentes/reportes-beneficiarios/reportes-beneficiarios.component';
import { InformeInfoPagosComponent } from './componentes/informe-info-pagos/informe-info-pagos.component';
import { SelectMultipleNegocioComponent } from './selects/select-multiple-negocio/select-multiple-negocio.component';
import { SelectMultipleNegocioSearchComponent } from './selects/select-multiple-negocio-search/select-multiple-negocio-search.component';
import { ReportesProyectosComponent } from './componentes/reportes-proyectos/reportes-proyectos.component';
import { ReportesTipoNegocioComponent } from './componentes/reportes-tipo-negocio/reportes-tipo-negocio.component';
import { NegociosAsambleasHistorialComponent } from './componentes/negocios-asambleas-historial/negocios-asambleas-historial.component';
import { NegociosAsambleasDetalleComponent } from './componentes/negocios-asambleas-detalle/negocios-asambleas-detalle.component';
import { NegociosComiteDetalleComponent } from './componentes/negocios-comite-detalle/negocios-comite-detalle.component';
import { DynamicBeneDistRetencionesTipoNegocioComponent } from './componentes/dynamic-bene-dist-retenciones-tipo-negocio/dynamic-bene-dist-retenciones-tipo-negocio.component';
import { CertificadosComponent } from './componentes/certificados/certificados.component';
import { CertificadosVinculadosComponent } from './componentes/certificados-vinculados/certificados-vinculados.component';
import { CertificadosNegociosComponent } from './componentes/certificados-negocios/certificados-negocios.component';
import { CertificadosVerificarVinculadosComponent } from './componentes/certificados-verificar-vinculados/certificados-verificar-vinculados.component';
import { CertififcadosVerficarNegociosComponent } from './componentes/certififcados-verficar-negocios/certififcados-verficar-negocios.component';
import { MensajeEsperaGenericoComponent } from './dialogs/mensaje-espera-generico/mensaje-espera-generico.component';
import { DateRangePeriodoComponent } from './selects/date-range-periodo/date-range-periodo.component';
import { SelectMultipleResponsablesComponent } from './selects/select-multiple-responsables/select-multiple-responsables.component';
import { CertificadosBuscarNegocioComponent } from './componentes/certificados-buscar-negocio/certificados-buscar-negocio.component';
import { CertVinculadosListarComponent } from './componentes/cert-vinculados-listar/cert-vinculados-listar.component';
import { CertVinculadosNegociosAsociadosComponent } from './componentes/cert-vinculados-negocios-asociados/cert-vinculados-negocios-asociados.component';
import { CertNegociosListarComponent } from './componentes/cert-negocios-listar/cert-negocios-listar.component';
import { CertNegociosBeneficiariosVinculadosComponent } from './componentes/cert-negocios-beneficiarios-vinculados/cert-negocios-beneficiarios-vinculados.component';
import { SelectMultipleTipnegocioNegComponent } from './selects/select-multiple-tipnegocio-neg/select-multiple-tipnegocio-neg.component';
export function httpTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    InicioComponent,
    BeneficiarioComponent,
    BeneficiarioDetalleComponent,
    ParametrosComponent,
    DialogParametrosSaveComponent,
    DynamicPerfilComponent,
    PerfilComponent,
    DialogThreeButtonsComponent,
    DialogReemplazarParametrosComponent,
    TwoDigitDecimaNumberDirective,
    DigitNumberDirective,
    OnlyNumber,
    OnlyLetrasDirective,
    BeneficiarioNegociosComponent,
    BeneficiarioDistribucionComponent,
    InformacionFinancieraComponent,
    DialogTwoButtonsComponent,
    DynamicBeneficiarioDistribucionComponent,
    DialogAddCotitularComponent,
    DialogPorcentajeGiroComponent,
    NegocioComponent,
    NegociosCargaArchivoComponent,
    DialogCargarArchivoComponent,
    NegocioDetalleComponent,
    NegociosAsambleasComponent,
    NegociosAsambleasNuevaEntradaComponent,
    DialogBuscarDirectivoComponent,
    NegocioEquipoComponent,
    DialogNegocioListTitularesComponent,
    DialogBuscarArchivoComponent,
    FormArrayMultipleComponent,
    DialogDetallenovedadJuridicaComponent,
    BeneficiarioHistorialComponent,
    BeneficiarioHistorialPagosComponent,
    BeneficiarioHistorialModificacionesComponent,
    SelectMultipleTipoPagoComponent,
    SelectMultipleEstadoPagosComponent,
    SelectMultipleNegocioAsociadoComponent,
    DateRangePickerComponent,
    PaginationComponent,
    PagosDistribucionComponent,
    DatepickerMesAnioComponent,
    PagoListaIngresosGastosComponent,
    DialogAddIngresoComponent,
    PagoListarTitularesBenComponent,
    DialogCerrarSesionComponent,
    MensajeEsperaComponent,
    NegociosPagosDistribucionComponent,
    NegociosHistorialPagosDistribucionComponent,
    SelectMultipleTipoNegocioComponent,
    NegociosHistorialPagosIngresoEgresoComponent,
    NegociosHistorialPagosDetalleBeneficiariosComponent,
    ComiteNuevaEntradaComponent,
    ComiteAdministrarComiteComponent,
    ComiteAdicionarMiembroComponent,
    ModalSincronizacionComponent,
    ModalSincronizacionConstantesComponent,
    ModalSincronizacionInformacionFinancieraIncompletaComponent,
    ModalSincronizacionCambiodatotitularComponent,
    ModalSincronizacionDetalleDatocambiadoComponent,
    ModalSincronizacionTitularnuevoComponent,
    ModalSincronizacionErrorComponent,
    ReportesComponent,
    ReporteVinculadosComponent,
    SelectMultipleUsuariosComponent,
    DialogInfopagoRechazadoComponent,
    DetalleInfopagoRechazadoComponent,
    NegocioHistorialModificacionesComponent,
    NegocioHistorialComponent,
    ReportesDistribucionComponent,
    ReportesCesionesComponent,
    ReportesBeneficiariosComponent,
    InformeInfoPagosComponent,
    SelectMultipleNegocioComponent,
    SelectMultipleNegocioSearchComponent,
    ReportesProyectosComponent,
    ReportesTipoNegocioComponent,
    NegociosAsambleasHistorialComponent,
    NegociosAsambleasDetalleComponent,
    NegociosComiteDetalleComponent,
    DynamicBeneDistRetencionesTipoNegocioComponent,
    CertificadosComponent,
    CertificadosVinculadosComponent,
    CertificadosNegociosComponent,
    CertificadosVerificarVinculadosComponent,
    CertififcadosVerficarNegociosComponent,
    MensajeEsperaGenericoComponent,
    DateRangePeriodoComponent,
    SelectMultipleResponsablesComponent,
    CertificadosBuscarNegocioComponent,
    CertVinculadosListarComponent,
    CertVinculadosNegociosAsociadosComponent,
    CertNegociosListarComponent,
    CertNegociosBeneficiariosVinculadosComponent,
    SelectMultipleTipnegocioNegComponent,
    ],
  imports: [
    SatDatepickerModule,
    SatNativeDateModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CoreModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatMomentModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoader,
        deps: [HttpClient]
      }
    }),
    NgHttpLoaderModule.forRoot(),
  ],
  providers: [
    DatePipe,
    ExcelService,
    CurrencyPipe,
    DecimalPipe,
    PercentPipe,
    { provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl() },
    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
    CustomDateAdapter, // so we could inject services to 'CustomDateAdapter'
    { provide: DateAdapter, useClass: CustomDateAdapter },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true },
    {
      provide: SatDateAdapter,
      useExisting: DateAdapter,
    },
    { provide: SAT_DATE_FORMATS, useExisting: MAT_DATE_FORMATS },
    { provide: SAT_DATE_LOCALE, useExisting: MAT_DATE_LOCALE },
    Utils,
    AuthGuard
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    DialogDetallenovedadJuridicaComponent,
    DialogParametrosSaveComponent,
    DialogThreeButtonsComponent,
    DialogReemplazarParametrosComponent,
    DynamicPerfilComponent,
    DialogTwoButtonsComponent,
    DialogAddCotitularComponent,
    DialogPorcentajeGiroComponent,
    DynamicBeneficiarioDistribucionComponent,
    DynamicBeneDistRetencionesTipoNegocioComponent,
    DialogCargarArchivoComponent,
    DialogBuscarDirectivoComponent,
    NegocioEquipoComponent,
    DialogNegocioListTitularesComponent,
    DialogBuscarArchivoComponent,
    DialogAddIngresoComponent,
    DialogCerrarSesionComponent,
    MensajeEsperaComponent,
    ComiteAdministrarComiteComponent,
    ComiteAdicionarMiembroComponent,
    ModalSincronizacionComponent,
    ModalSincronizacionConstantesComponent,
    ModalSincronizacionInformacionFinancieraIncompletaComponent,
    ModalSincronizacionCambiodatotitularComponent,
    ModalSincronizacionDetalleDatocambiadoComponent,
    ModalSincronizacionTitularnuevoComponent,
    ModalSincronizacionErrorComponent,
    DialogInfopagoRechazadoComponent,
    DetalleInfopagoRechazadoComponent,
    CertificadosVerificarVinculadosComponent,
    CertififcadosVerficarNegociosComponent,
    MensajeEsperaGenericoComponent

  ]
})
export class AppModule { }
