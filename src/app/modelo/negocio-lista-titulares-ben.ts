export interface ListTitularBeneficiario {
    nombreTitular?: string,
    tipoDocumentoTitular?:string,
	documentoTitular?: string,
    porcentajeParticipacion?: string,
    numeroDerechos?: string,
    nombreBeneficiario: string,
	tipoDocumento: string,
	documento: string,
	porcentajeGiro: number,
	porcentajedistribuido: number,
    valorGiro: number,
	porcentajeParticipacionDistribuido: number,
	subtotalPago: number,
	gravamenMovimientoFinanciero: number,
	tipoPago: string,
	valorTipoPago: number,
	subtotalGirado: number,
	tipoCuenta: string,
	cuentaEncargo: string,
	entidadBancariaFideicomiso: string,
	codigoBanco: number
    
  }

 