export interface NegocioTipo {
    id:string,
    descripcionTipoNegocio:string,
    retenciones:Retencion[],
    tipoTercero:string,
    unidadComision:string,
    valorComision:number,
    fechaVencimiento:Date,
    numeroRadicado:number,
    gastos:Gastos[]
   // esTercero:boolean
}

export interface Retencion {
    id:string,
    descripcionRetencion:string,
    valor:string;
}

export interface Gastos {
    nombreGasto:string,
    tipoGasto:string,
    unidadGasto:number[],
    valorGasto:number,
    //periodicidad:string,
    fechaPagoGasto:Date,
    ficOrigen:string,
}
