export interface NegocioTercero {
    nombreApellidoTercero:string,
    tipoDocumentoTercero:string,
    numeroDocumentoTercero:number,
    tipoTercero:string,
    unidadComision:string,
    valorComision:number,
    fechaVencimiento:Date,
    numeroRadicado:number,
   // esTercero:boolean
}