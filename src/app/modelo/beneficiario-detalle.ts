export interface BeneficiarioDetalle {
    fechaVinculacion: string,
    ciudadVinculacion: string,
    oficinaVinculacion: string,
    tipoVinculacion: string,
    naturalezaJuridicaVinculado: string,
    nombreCompleto: string,
    numeroDocumento: string,
    tipoDocumento: TipoDocumento,
    envioCorrespondenciaTipo: string,
    razonSocial: string,
    telefono: string,
    email: string,
    paisResidencia: string,
    ciudadResidencia: string,
    tieneNegocio: boolean
}

export interface TipoDocumento {
    id: string,
    descripcion: string
}

export interface Documento {
    documento: string,
    tipodocumento: string
}

export interface ParamDatosNegocio {
    documento: string,
    tipodocumento: string,
    codigo: string
}

export interface ParamBusquedaCotitular {
    documentoTitular: string,
    tipoDocumentoTitular: string,
    codigoNegocio: string,
    criterioBuscar: string,
    argumento: string
}
