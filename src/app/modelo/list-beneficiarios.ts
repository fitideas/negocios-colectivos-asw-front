export interface ListBeneficiarios {
    nombreCompleto?: string;
	tipoDocumento?: string;
	numeroDocumento?: string;
    negocios?: Negocios[];
  }
  
  export interface Negocios {
  }