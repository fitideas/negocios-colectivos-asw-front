import { Subscription } from 'rxjs';


export interface ListEquipo {
    nombreCompleto?: string;
	numeroDocumento?: string;
	rol?: string;
    esDelEquipo?: boolean;
}


export interface ParamAdministrarEquipo {
    codigonegocio?: string,
    equipo?: ArrayEquipo[] 
}

export interface ArrayEquipo {   
    nombreCompleto?: boolean,
    numeroDocumento?: string,
    rol?: string  
}
  

export interface ParamBusquedaEq {
    codigonegocio: string,
    criterioBuscar: string,
    argumento: string
}
