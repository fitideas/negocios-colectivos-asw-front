import { Subscription } from 'rxjs';

export interface ListCotitulares {
    nombreApellidoCotitular?: string;
    tipoDocumentoCotitular?: string;
    documentoCotitular?: string;
    asociado?: boolean;
}

export interface ParamAdministrarCotitular {
    documentoTitular?: string,
    tipoDocumentoTitular?: string,
    codigoNegocio?: string,
    cotitulares?: ArrayCotitulares[]
}


export interface ArrayCotitulares {
    asociado?: boolean,
    documentoCotitular?: string,
    tipoDocumentoCotitular?: string,
}

export interface FileUploadModel {
    data: File;
    state: string;
    inProgress: boolean;
    progress: number;
    canLoad: boolean;
    canRetry: boolean;
    canCancel: boolean;
    sub?: Subscription;
}

export interface TitularList {
    tipoTitularidad?: string,
    nombreTitular?: string,
    tipoDocumentoTitular?: string,
    documentoTitular?: string,
    numeroEncargoInd?: string,
    porcentajeParticipacion?: string,
    numeroDerechosFiduciarios?: string,
    beneficiario?: string,
    tipoDocumentoBeneficiario?: string,
    numeroDocumentoBeneficiario?: string,
    tipoCuenta?: string,
    numeroCuenta?: string
}
