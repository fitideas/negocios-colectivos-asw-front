export class ResponseLogin {
  nombreUsuario?: string;
  nombreCompleto?: string;
  fechaUltimoAcceso?: string;
  token?: string;
  tiempoVidaToken?: string;
  roles?: Roles[];
}

export interface Roles {
  idRol?: string;
  nombreRol?: string;
  permisos?: Permisos[];
}

export interface Permisos {
  idPermiso?: string;
  tipoPermiso?: string;
  estado?: string;
  idPermisoPadre?: string;
  nombre?: string;
}
