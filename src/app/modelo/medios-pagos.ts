export interface MedioPago {
    codigo?: string,
    descripcion?: string
  }

export class MediosPagos {

    mediosPago?: MedioPago[] = Array();

    constructor(       
    ){
        this.mediosPago = 
        [
          { codigo: "CHE", descripcion: "CHEQUE" },
          { codigo: "CHEGER", descripcion: "CHEQUE DE GERENCIA" },
          { codigo: "TRANS", descripcion: "TRANSFERENCIA" },
          { codigo: "OTR", descripcion: "OTRO" },
          { codigo: "TRASL", descripcion: "TRASLADO" }
        ];
    }

    public getMediosPagos() {
        return this.mediosPago;
    }

}
