export interface BeneficiarioDistribucionGuardar {
    codigoNegocio?: string,
    tipoDocumento?: string,
    documento?: string,
    nombre?:string,
    idTipoNaturaleza?: string,
    asesor?: string,
    radicadoVinculacion?: string,
    derechos?: number,
    porcentajeParticipacion?:number,
    tipoTitularidad?: string,
    retencionesTipoNaturaleza?: any[],
    retencionesTipoNegocioEliminadas: RetencionesTipoNegocioEliminadas
}

export interface RetencionesTipoNegocioEliminadas {
    idTipoNegocio?:number,
	retenciones?: any[]
}
