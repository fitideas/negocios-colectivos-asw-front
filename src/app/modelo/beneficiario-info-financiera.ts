export interface BeneficiarioInformacionFinanciera {
    codigoNegocio: string,
	tipoDocumentoTitular: string,
	documento: string,
    totalDerechosNegocio: number,
	numeroDerechosTitular: number,
    beneficiarios:BeneficiarioDetalleInfoFinanciera[];
}
    

export interface Documento {
    documento: string,
    tipodocumento: string,
    codigoEvento:string
}

export interface BeneficiarioDetalleInfoFinanciera {
    //idBeneficiario:string,
    beneficiario: string,
    tipoPago:string,
    tipoDocumento: string,
    numeroDocumento: string,
    porcentajeGiro: number,
    porcentajeDistribucion: number,
    //descripcionMedioPago:string, 
    //codigo: string,
    //descripcionPago: string,
    //codigoTipoCuenta: string,
    idEntidadBancaria: string,
    codigoBanco: string,
    tipoCuenta: string,
    numeroCuentaBancaria: string,
    numeroEncargo: string,
    fideicomisoDestino: string,
    codigoPago: string,
    numeroReferencia: string,
    descripcionOtro: string
    }

    export interface InformacionFinancieraListaDocumentos {
        id: string,
		descripcion:string,
    }



