import { NativeDateAdapter, DateAdapter, MatDatepicker } from '@angular/material';
import moment from 'moment';

export interface NegocioPago {
    codigoNegocio: string ,
    periodo:string
    listaTipoNegocio: any,
}
class CustomDateAdapter extends NativeDateAdapter {
    format(date: Date, displayFormat: Object): string {
      var formatString = 'MMMM YYYY';
      return moment(date).format(formatString);
    }
  }