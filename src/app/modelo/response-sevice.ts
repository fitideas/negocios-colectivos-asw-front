export interface ResponseSevice {
    datosControl?: DatosControl;
    respuestaPeticion?: DatosRespuesta;
}

export class DatosControl {
  estado?: string;
  mensaje?: string;
}

export class DatosRespuesta {
  Objeto?: any;
}

