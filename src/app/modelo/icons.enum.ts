export enum Icons {
  Af_email = "af_email",
  Beneficiary = "beneficiary",
  Bussines = "bussines",
  Certification = "certification",
  Cloud_download = "cloud_download",
  Entrar = "entrar",  
  Entrargris = "entrargris",  
  Diary = "diary",
  Downarrow = "downarrow", 
  Home = "home",
  Link = "link",
  LogoWhite = "logowhite",
  Logoblack = "logoblack",
  Money = "money",
  Padlock = "padlock",
  Padlockred = "padlockred",
  People = "people",  
  Perfil = "perfil",
  Perfil2 = "perfil2",
  Perfilred = "perfilred",
  Reports = "reports",
  Search = "search",
  SearchRed = "searchred",
  Setting = "setting",
  Signoff = "signoff",
  Upmoney = "upmoney",
  Warning = "warning",
  infoFinanciera ="infoFinanciera"
}
