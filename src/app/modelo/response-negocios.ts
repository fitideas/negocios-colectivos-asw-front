export class ResponseNegocios {
    negocios?: Negocios[];
  }
  
  export interface Negocios {
    nombre?: string,
    codigoSFC?: string,
    codigoInterno?: string,
    completo?:boolean,
    estado?:string
  }
 