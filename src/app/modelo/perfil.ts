export interface Perfil {
    idRol?: number,
    rol?: string,
    listaFuncionalidades?:ListaFuncionalidades
}

export interface ListaFuncionalidades {
    funcionalidades?:Funcionalidades[]
}

export interface Funcionalidades {
    funcionalidad?:string,
    idFuncionalidad?:number,
    permiteEscribir?:boolean
}



export interface RolFuncionalidad {
    idRol?: number,
	idFuncionalidad?: number,
	permiteEscribir?: boolean
}