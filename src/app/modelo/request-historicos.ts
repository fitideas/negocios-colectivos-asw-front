export interface PeticionHistorialCambios{
    rangoFecha: any[];
    tipoNegocio: any[];
    negocios: any[];
    solicitadoPor: any[];
    tipoDocumentoVinculado:string;
    numeroDocumentoVinculado:string;
}

export interface PeticionNegocioHistorialCambios{
    fecha: any[];
    usuario: any[];
    negocio: string;
    campo: string;
}