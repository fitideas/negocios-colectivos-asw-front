export interface NegocioInfoGeneral {
    seccionalAduanaDepartamento:string,
    seccionalAduanaCiudad:string,
    matriculaInmobiliaria: string,
    licenciaConstruccion: string,
    ciudadUbicacionPais: string,
    ciudadUbicacionDepartamento: string,
    ciudadUbicacion:string,
    fideicomitenteNombreCompleto: string,
    fideicomitenteTipoDocumento:string,
    fideicomitenteNumeroDocumento:string,
    calidadFidecomitente:string,
    calidadFiduciariaFideicomiso:string,
    nombre: string ,
    codigoSFC: string,
    codigoInterno:string ,
    completo: boolean,
    estado: string ,
    fechaApertura: string,
    numeroDerechosTotales: string,
    fechaLiquidacionNegocioPromotor: string,
    fechaConstitucionNegocio:string ,
    estadoProyecto: string,
    avaluoInmueble: number,
    valorUltimoPredial:number ,
    valorUltimaValorizacion: number,
    fechaCobroPredial:string ,
    fechaCobroValorizacion:string,
    archivoCargado:boolean ,
    encargos: Encargos[],
    tiposNegocio: [],
    constanteNegocio: any,
    fechasClaves: FechasClaves[] 
}

export interface Encargos {
    nombreEncargo: string, 
    estado:string;
}

export interface FechasClaves {
    nombreFechaClave:string, 
    tipoFechaClave: number, 
    fechaCalendario:string
}

export interface TiposNegocio {
    id:number, 
    retenciones: Retenciones[], 
    gastos:Gastos[]
}

export interface Gastos {
    nombreGasto: string,
    tipoGasto: number
    unidadGasto: string,
    valorGasto: string
    periodicidad: string[],
    fechaPagoGasto: number,
    ficOrigen: string
}

export interface Retenciones {
    id: number,
    descripcion: string
}