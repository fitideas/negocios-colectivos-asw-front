export interface ParametroCompuesto {
  id?: string;
  descripcion?: string;
  activo?: boolean;
  valores?: ValorCompuesto[];
}

export interface ValorCompuesto {
  id?: string;
  name?: string;
  value?: string;
}

export interface RolTerceroComite {
  rol: string,
  tercero: string,
  comite: string,
  idTercero: string,
  idComite: string,
  idRol: string
}

export interface IdRolTercerosComite {
  idTercero: string,
  idComite: string,
}

export interface Compuesto {
  codigo?: string;
  nombre?: string;
  valores?: ValoresCompuesto[];
}

export interface ValoresCompuesto {
  id?: string;
  activo?: boolean;
  descripcion?: string;
  valores?: DetalleValorCompuesto[];
}

export interface DetalleValorCompuesto {
  id?: string;
  descripcion?: string;
  valor?: string;
}
