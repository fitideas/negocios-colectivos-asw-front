export interface ParametroSimple {
    codigo?: string;
	nombre?: string;
    valores?: Valores[];
}

export interface Valores {
    id?: string;
    descripcion?: string;
    activo?: boolean;
    unidad?: string;
    valor?: string;
}