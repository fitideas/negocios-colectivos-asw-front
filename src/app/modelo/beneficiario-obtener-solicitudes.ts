export interface BeneficiarioObtenerSolicitudes {
	idSolicitud: number,
    fechaSolicitud: string,
	campoAModificar: string,
	datoNuevo: string,
	profesional: string,
	numeroRadicado: string
}

export interface BeneficiarioEnviarSolicitudes {
	fechaSolicitud: string,
	campoAModificar: string,
	datoNuevo: string,
	datoAnterior: string,
	profesional: string,
	numeroRadicado: string,
	tipoDocumentoVinculado: string,
	numeroDocumentoVinculado: string,
	nombreCompletoVinculado: string
}

export interface camposModificar {
	campo: string,
	valor: any,
	descripcion: string;
}
