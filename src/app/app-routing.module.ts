import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from "./auth/login/login.component";
import { InicioComponent } from './componentes/inicio/inicio.component';
import { BeneficiarioComponent } from './componentes/beneficiario/beneficiario.component';
import { BeneficiarioDetalleComponent } from './componentes/beneficiario-detalle/beneficiario-detalle.component';
import { PerfilComponent } from './componentes/perfil/perfil.component';
import { NegocioComponent } from './componentes/negocio/negocio.component';
import { ReportesComponent } from './componentes/reportes/reportes.component';
import { AuthGuard } from './_helpers/auth.guard';
import { ParametrosComponent } from './componentes/parametros/parametros.component';
import { NegocioDetalleComponent } from './componentes/negocio-detalle/negocio-detalle.component';
import { NegociosAsambleasDetalleComponent } from './componentes/negocios-asambleas-detalle/negocios-asambleas-detalle.component';
import { CertificadosComponent } from './componentes/certificados/certificados.component';
// import { CertificadosVinculadosComponent } from './componentes/certificados-vinculados/certificados-vinculados.component';
// import { CertificadosNegociosComponent } from './componentes/certificados-negocios/certificados-negocios.component';

const routes: Routes = [
  { path: "login", component: LoginComponent },  
  { path: '', component: LoginComponent },
  
  {
    path: 'accionfiduciaria', canActivate: [AuthGuard], component: InicioComponent,
    children: [
      { path: 'vinculados', canActivate: [AuthGuard], component: BeneficiarioComponent },
      { path: 'vinculados/detalles', canActivate: [AuthGuard], component: BeneficiarioDetalleComponent },      
      { path: 'negocios', canActivate: [AuthGuard], component: NegocioComponent },
      { path: 'negocios/detalles', canActivate: [AuthGuard], component: NegocioDetalleComponent },
      { path: 'informes', canActivate: [AuthGuard], component: ReportesComponent },
      { path: 'certificacion', canActivate: [AuthGuard], component: CertificadosComponent },      
      { path: 'parametros', canActivate: [AuthGuard], component: ParametrosComponent },
      { path: 'perfil', canActivate: [AuthGuard], component: PerfilComponent },
      {path: 'asambleas/detalles', canActivate:[AuthGuard], component: NegociosAsambleasDetalleComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
