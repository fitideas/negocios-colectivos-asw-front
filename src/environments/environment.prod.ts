export const environment = {
  production: true,
  restApiServer: location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : ''),
  codigoPaisDefecto: 'CO', // COLOMBIA
  codigoDepartamentoDefecto: '25', //CUNDINAMARCA
  codigoCiudadDefecto: '101',  //BOGOTA
  elementosPorPagina: 10
};
